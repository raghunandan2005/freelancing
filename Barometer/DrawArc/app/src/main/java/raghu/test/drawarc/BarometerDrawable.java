package raghu.test.drawarc;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;


public class BarometerDrawable extends Drawable {

    private final Paint paintBackground, paintForeground, paintStroke;
    private final Paint paintRed, paintYellow, paintGreen, paintRedStroke, paintYellowStroke, paintGreenStroke, paintGreenFillingStroke, paintYellowFillingStroke, paintRedFillingStroke;
    private final RectF rectFBackground, rectFForeground, rectFStroke1, rectFStroke2, rectFStrokeColor, rectFBottomBackground;
    private final RectF rectFSector, rectFAvatar, rectFSeparator;
    private Bitmap starBitmap;

    private double value = 0.0;

    public BarometerDrawable(Context context, int backgroundColor, int foregroundColor, int strokeColor) {

        paintBackground = new Paint();
        paintBackground.setColor(backgroundColor);
        paintBackground.setStyle(Paint.Style.FILL);
        paintBackground.setAntiAlias(true);
        paintBackground.setDither(true);

        paintForeground = new Paint();
        paintForeground.setColor(foregroundColor);
        paintForeground.setStyle(Paint.Style.FILL);
        paintForeground.setAntiAlias(true);
        paintForeground.setDither(true);

        paintStroke = new Paint();
        paintStroke.setColor(strokeColor);
        paintStroke.setStyle(Paint.Style.STROKE);
        paintStroke.setAntiAlias(true);
        paintStroke.setStrokeWidth(1);
        paintStroke.setDither(true);

        rectFBackground = new RectF();
        rectFForeground = new RectF();
        rectFStroke1 = new RectF();
        rectFStroke2 = new RectF();
        rectFBottomBackground = new RectF();
        rectFSector = new RectF();
        rectFAvatar = new RectF();
        rectFSeparator = new RectF();
        rectFStrokeColor = new RectF();

        paintRed = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintRed.setColor(Color.parseColor("#CE6466"));
        paintRed.setStyle(Paint.Style.FILL);
        paintRed.setAntiAlias(true);
        paintRed.setDither(true);

        paintYellow = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintYellow.setColor(Color.parseColor("#E5E756"));
        paintYellow.setStyle(Paint.Style.FILL);
        paintYellow.setAntiAlias(true);
        paintYellow.setDither(true);

        paintGreen = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintGreen.setColor(Color.parseColor("#64CD64"));
        paintGreen.setStyle(Paint.Style.FILL);
        paintGreen.setAntiAlias(true);
        paintGreen.setDither(true);

        paintRedStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintRedStroke.setColor(Color.parseColor("#CE6466"));
        paintRedStroke.setStyle(Paint.Style.STROKE);
        paintRedStroke.setAntiAlias(true);
        paintRedStroke.setStrokeWidth(2);
        paintRedStroke.setDither(true);

        paintYellowStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintYellowStroke.setColor(Color.parseColor("#E5E756"));
        paintYellowStroke.setStyle(Paint.Style.STROKE);
        paintYellowStroke.setAntiAlias(true);
        paintYellowStroke.setStrokeWidth(2);
        paintYellowStroke.setDither(true);

        paintGreenStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintGreenStroke.setColor(Color.parseColor("#64CD64"));
        paintGreenStroke.setStyle(Paint.Style.STROKE);
        paintGreenStroke.setAntiAlias(true);
        paintGreenStroke.setStrokeWidth(2);
        paintGreenStroke.setDither(true);

        starBitmap = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.ic_star);

        paintGreenFillingStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintGreenFillingStroke.setColor(Color.parseColor("#64CD64"));
        paintGreenFillingStroke.setStyle(Paint.Style.STROKE);
        paintGreenFillingStroke.setAntiAlias(true);
        paintGreenFillingStroke.setStrokeWidth(6);
        paintGreenFillingStroke.setDither(true);

        paintYellowFillingStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintYellowFillingStroke.setColor(Color.parseColor("#E5E756"));
        paintYellowFillingStroke.setStyle(Paint.Style.STROKE);
        paintYellowFillingStroke.setAntiAlias(true);
        paintYellowFillingStroke.setStrokeWidth(6);
        paintYellowFillingStroke.setDither(true);

        paintRedFillingStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintRedFillingStroke.setColor(Color.parseColor("#CE6466"));
        paintRedFillingStroke.setStyle(Paint.Style.STROKE);
        paintRedFillingStroke.setAntiAlias(true);
        paintRedFillingStroke.setStrokeWidth(6);
        paintRedFillingStroke.setDither(true);

    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        canvas.scale(1, 2);

        int foregroundPadding = getBounds().width() / 60;

        rectFBackground.set(getBounds());
        //set(float left, float top, float right, float bottom)
        rectFForeground.

                set(getBounds().left + foregroundPadding*2,
                        getBounds().top + foregroundPadding,
                        getBounds().right - foregroundPadding*2,
                        getBounds().bottom - foregroundPadding);
        /*rectFStroke1.set(getBounds().left + foregroundPadding*6, getBounds().top + foregroundPadding*3, getBounds().right - foregroundPadding*6, getBounds().bottom - foregroundPadding*3);
        rectFStroke2.set(getBounds().left + foregroundPadding*8, getBounds().top + foregroundPadding*4, getBounds().right - foregroundPadding*8, getBounds().bottom - foregroundPadding*4);
        rectFStrokeColor.set(getBounds().left + foregroundPadding*7 - 1, getBounds().top + foregroundPadding*3.5f - 1, getBounds().right - foregroundPadding*7 - 1, getBounds().bottom - foregroundPadding*3.5f - 1);
        rectFBottomBackground.set(getBounds().left + foregroundPadding*2, getBounds().height() / 2 - foregroundPadding, getBounds().right - foregroundPadding*2, getBounds().height() / 2);
        rectFSector.set(getBounds().width() / 4, getBounds().height() / 4 - foregroundPadding, 3 * getBounds().width() / 4, 3 * getBounds().height() / 4 - foregroundPadding);
        rectFAvatar.set(getBounds().width() / 2 - foregroundPadding * 4, getBounds().height() / 2 - 4 * foregroundPadding, getBounds().width() / 2 + foregroundPadding * 4, getBounds().height() / 2);

        canvas.drawArc(rectFBackground, -180, 180, true, paintBackground);
        canvas.drawArc(rectFForeground, -180, 180, true, paintForeground);
        canvas.drawArc(rectFStroke1, -180, 180, false, paintStroke);
        canvas.drawArc(rectFStroke2, -180, 180, false, paintStroke);

        canvas.drawArc(rectFSector, -55, 55, true, paintGreen);
        canvas.drawArc(rectFSector, -125, 70, true, paintYellow);
        canvas.drawArc(rectFSector, -180, 55, true, paintRed);

        canvas.drawArc(rectFStrokeColor, -178, 26, false, paintRedStroke);
        canvas.drawArc(rectFStrokeColor, -148, 26, false, paintRedStroke);
        canvas.drawArc(rectFStrokeColor, -118, 26, false, paintYellowStroke);
        canvas.drawArc(rectFStrokeColor, -88, 26, false, paintYellowStroke);
        canvas.drawArc(rectFStrokeColor, -58, 26, false, paintGreenStroke);
        canvas.drawArc(rectFStrokeColor, -28, 26, false, paintGreenStroke);

        canvas.drawArc(rectFAvatar, -180, 360, true, paintForeground);


        RectF starRatingRectF  = new RectF();
        float starHeight = getBounds().width() / 30;
        float starWidth = starHeight * 1.3f;
        starBitmap = Bitmap.createScaledBitmap(starBitmap, (int)starWidth, (int)starHeight, false);

        // first sector
        for (int i = 1; i <= 1; i ++) {
            // cos(.26179) is .9659.
            // 0.9659*0.87 is 0.84035.
            // 1- 0.84035 is 0.15965.
            //  getbounds returns RectF bounds for the drawable.
            // .width() gives the width of the drawable. width/2 gives the center of the drawable.
            float starCenterX = (float)( (1 - Math.cos(0.261799)*0.87) * (getBounds().width() / 2));
            float starCenterY = (float)( (1 - Math.sin(0.261799)*0.87) * (getBounds().height() / 2));

            starRatingRectF.set(starCenterX - starWidth / 2, starCenterY - starHeight / 2, starCenterX + starWidth / 2, starCenterY + starHeight / 2);

            Matrix matrix = new Matrix();
            matrix.postRotate(15);
            Bitmap rotatedStar = Bitmap.createBitmap(starBitmap , 0, 0, (int)starWidth, (int)starHeight, matrix, false);
            canvas.drawBitmap(rotatedStar, null, starRatingRectF, null);
        }

        // second sector
        for (int i = 1; i <= 2; i ++) {
            float starCenterX = (float)( (1 - Math.cos(0.523599 + 0.174533*i)*0.87) * (getBounds().width() / 2));
            float starCenterY = (float)( (1 - Math.sin(0.523599 + 0.174533*i)*0.87) * (getBounds().height() / 2));

            starRatingRectF.set(starCenterX - starWidth / 2, starCenterY - starHeight / 2, starCenterX + starWidth / 2, starCenterY + starHeight / 2);

            Matrix matrix = new Matrix();
            matrix.postRotate(45);
            Bitmap rotatedStar = Bitmap.createBitmap(starBitmap , 0, 0, (int)starWidth, (int)starHeight, matrix, false);
            canvas.drawBitmap(rotatedStar, null, starRatingRectF, null);
        }

        // third sector (yellow)
        for (int i = 1; i <= 3; i ++) {
            float starCenterX = (float)( (1 - Math.cos(1.0472 + 0.1309*i)*0.87) * (getBounds().width() / 2));
            float starCenterY = (float)( (1 - Math.sin(1.0472 + 0.1309*i)*0.87) * (getBounds().height() / 2));

            starRatingRectF.set(starCenterX - starWidth / 2, starCenterY - starHeight / 2, starCenterX + starWidth / 2, starCenterY + starHeight / 2);

            Matrix matrix = new Matrix();
            matrix.postRotate(75);
            Bitmap rotatedStar = Bitmap.createBitmap(starBitmap , 0, 0, (int)starWidth, (int)starHeight, matrix, false);
            canvas.drawBitmap(rotatedStar, null, starRatingRectF, null);
        }


        // fourth sector (yellow)
        for (int i = 1; i <= 4; i ++) {
            float starCenterX = (float)( (1 - Math.cos(1.5708 + 0.10472 * i)*0.87) * (getBounds().width() / 2));
            float starCenterY = (float)( (1 - Math.sin(1.5708 + 0.10472 * i)*0.87) * (getBounds().height() / 2));

            starRatingRectF.set(starCenterX - starWidth / 2, starCenterY - starHeight / 2, starCenterX + starWidth / 2, starCenterY + starHeight / 2);

            Matrix matrix = new Matrix();
            matrix.postRotate(105);
            Bitmap rotatedStar = Bitmap.createBitmap(starBitmap , 0, 0, (int)starWidth, (int)starHeight, matrix, false);
            canvas.drawBitmap(rotatedStar, null, starRatingRectF, null);
        }

        // fifth sector (green), bit one
        for (int i = 1; i <= 5; i ++) {
            float starCenterX = (float)( (1 - Math.cos(2.0944 + 0.154533 * i)*0.87) * (getBounds().width() / 2));
            float starCenterY = (float)( (1 - Math.sin(2.0944 + 0.1574533 * i)*0.87) * (getBounds().height() / 2));

            starRatingRectF.set(starCenterX - starWidth / 2, starCenterY - starHeight / 2, starCenterX + starWidth / 2, starCenterY + starHeight / 2);

            Matrix matrix = new Matrix();
            matrix.postRotate(145);
            Bitmap rotatedStar = Bitmap.createBitmap(starBitmap , 0, 0, (int)starWidth, (int)starHeight, matrix, false);
            canvas.drawBitmap(rotatedStar, null, starRatingRectF, null);
        }

        fillRating(canvas, rectFStroke1, rectFStroke2);

        canvas.drawRect(rectFBottomBackground, paintBackground);*/
    }

    private void fillRating(Canvas canvas, RectF border1, RectF border2) {

        RectF valueRectF = new RectF();

        float diff = border2.left - border1.left;

        valueRectF.set(border1.left + diff/2, border1.top + diff/4, border1.right - diff/2, border1.bottom - diff/4);

        if (value > 1) {
            canvas.drawArc(valueRectF, -180, 30, false, paintRedFillingStroke);
        } else {
            canvas.drawArc(valueRectF, -180, (int)(30 * value), false, paintRedFillingStroke);
            return;
        }

        if (value > 2) {
            canvas.drawArc(valueRectF, -150, 30, false, paintRedFillingStroke);
        } else {
            canvas.drawArc(valueRectF, -150, (int)(30 * (value - 1)), false, paintRedFillingStroke);
            return;
        }

        if (value > 3) {
            canvas.drawArc(valueRectF, -120, 30, false, paintYellowFillingStroke);
        } else {
            canvas.drawArc(valueRectF, -120, (int)(30 * (value - 2)), false, paintYellowFillingStroke);
            return;
        }

        if (value > 4) {
            canvas.drawArc(valueRectF, -90, 30, false, paintYellowFillingStroke);
        } else {
            canvas.drawArc(valueRectF, -90, (int)(30 * (value - 3)), false, paintYellowFillingStroke);
            return;
        }

        if (value > 4.5) {
            canvas.drawArc(valueRectF, -60, 30, false, paintGreenFillingStroke);
        } else {
            canvas.drawArc(valueRectF, -60, (int)(30 * (value - 3.5)), false, paintGreenFillingStroke);
            return;
        }

        canvas.drawArc(valueRectF, -30, (int)(30 * 2*(value - 4.5)), false, paintGreenFillingStroke);
    }

    @Override
    public void setAlpha(int alpha) {
        // Has no effect
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        // Has no effect
    }

    @Override
    public int getOpacity() {
        // Not Implemented
        return 0;
    }


    public void setValue(double value) {
        this.value = value;
        invalidateSelf();
    }
}
