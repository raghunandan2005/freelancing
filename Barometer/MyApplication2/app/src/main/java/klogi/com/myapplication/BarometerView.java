package klogi.com.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Created by klogi on 1/11/2016.
 */
public class BarometerView extends FrameLayout {

    BarometerDrawable barometer = new BarometerDrawable(getContext(), Color.parseColor("#BDC4CC"), Color.parseColor("#EFEFEF"), Color.parseColor("#656764"));

    public BarometerView(Context context) {
        super(context);
        initializeView(context);
    }

    public BarometerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeView(context);
    }

    private void initializeView(Context context) {
        LayoutInflater.from(context).inflate(R.layout.barometer_view, this);
        setBackground(barometer);
    }

    public void setImage(int drawableRes) {
        CircleTransform circleTransform = new CircleTransform();
        Bitmap image = BitmapFactory.decodeResource(getContext().getResources(), drawableRes);
        Bitmap transformedImage = circleTransform.transform(image);

        ((ImageView)getRootView().findViewById(R.id.userPhotoImageView)).setImageBitmap(transformedImage);
    }

    public void setValue(double value) {
        barometer.setValue(value);
    }
}
