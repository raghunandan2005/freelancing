package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.lib.Util;

import java.io.ByteArrayOutputStream;
import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.mobsandgeeks.saripaar.annotation.TextRule;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class AddGroups extends Activity implements ValidationListener,ImageChooserListener,android.view.View.OnClickListener{
	
	private Preloader loader;
	private ImageChooserManager chooserManager;	
	private String filePath,fileName = null;
	private File f;
	private Validator validator;
	private int choiceType;
	
	@Required(order = 1)
	@TextRule(order = 2,minLength = 1,message="Enter atleast 1 character.")
	private EditText mGroupName;
	
	ImageView mGroupImage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_groups);
		
		loader = new Preloader();
		
		mGroupName = (EditText) findViewById(R.id.epGroupName);
		mGroupImage = (ImageView) findViewById(R.id.epGroupImage);
		mGroupImage.setOnClickListener(this);
		
		validator = new Validator(this);
		validator.setValidationListener(this);
	}
	

	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK&& (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
			try {
				loader.stopLoading();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (chooserManager == null) {
				reinitializeImageChooser();
			}
			chooserManager.submit(requestCode, data);
		} else {
			try {
				loader.stopLoading();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// pbar.setVisibility(View.GONE);
		}
	}
	
	private void reinitializeImageChooser() {
		chooserManager = new ImageChooserManager(this, choiceType,"Fup/temp", true);
		chooserManager.setImageChooserListener(this);
		chooserManager.reinitialize(filePath);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.action_groups_next, menu);
	    return true;
	}
	
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
	
		case R.id.add_groups_next:
			
	
			if (!Util.isNetworkAvailable(AddGroups.this)) {	
				
				DialogFactory.getBeaconStreamDialog1(AddGroups.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			} else 
			
			{
			validator.validate();
			}
			
			
			return false;
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onError(String arg0) {
		
		try {
			loader.stopLoading();
		} catch (Exception e) {

			e.printStackTrace();
		}
		
	}
	


	@Override
	public void onImageChosen(final ChosenImage image) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(image != null){
					try {
						loader.startPreloader(AddGroups.this,"Capturing Image..");
						f = new File(image.getFileThumbnail());	
						Picasso.with(getApplicationContext()).load(Uri.fromFile(f)).fit().centerCrop().transform(new RoundedTransform()).into(mGroupImage,new Callback() {
							
							@Override
							public void onSuccess() {
								Long tsLong = System.currentTimeMillis()/1000;
								fileName = tsLong.toString();
								loader.stopLoading();
							}
							
							@Override
							public void onError() {
								loader.stopLoading();
							}
						});
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
	}


	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		String message = failedRule.getFailureMessage();
		if(failedView instanceof EditText){
			failedView.requestFocus();
			((EditText) failedView).setError(message);
		}else{
			showToast(message,true);
		}
	}


	@Override
	public void onValidationSucceeded() {
		try {
			loader.startPreloader(AddGroups.this,"Creating Group..");
			final ParseObject groupInfo = new ParseObject("Groups");
			groupInfo.put("groupName",mGroupName.getText().toString());
			groupInfo.put("creator", ParseUser.getCurrentUser());
			groupInfo.put("creatorstring", ParseUser.getCurrentUser().getObjectId().toString());
			groupInfo.put("Gselected", false);
			if(fileName !=null){ 
				Bitmap bmp = BitmapFactory.decodeFile(f.toString());
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
			    bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
			    byte[] images = stream.toByteArray();
			    ParseFile file = new ParseFile(fileName+".png",images);
				file.saveInBackground();
				groupInfo.put("groupProfileImage",file);	
			}
			groupInfo.saveInBackground(new SaveCallback() {
				  public void done(ParseException e) {
				    if (e == null) {
				      try {
						// Success!
						   Intent addGroupsNext = new Intent(AddGroups.this,AddGroupsNext.class);
						   addGroupsNext.putExtra("groupID", groupInfo.getObjectId());
						   startActivity(addGroupsNext);
						   loader.stopLoading();
						   finish();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				    } else {
				      // Failure!
				    }
				  }
				});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loader.stopLoading();
		}
	}
	
	public void openSelectionDialog(){
		String[] selections = new String[]{"Gallery","Camera"};
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle("Choose Method");
		dialog.setItems(selections, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(which == 0){
					//Gallery
					Toast.makeText(getApplicationContext(),"Choose Image from Gallery",Toast.LENGTH_LONG).show();
					chooseImage();
				}else if(which == 1){
					//Camera
					Toast.makeText(getApplicationContext(),"Take a picture using Camera",Toast.LENGTH_LONG).show();
					takeImage();
				}
			}
		});
		
		dialog.setNegativeButton("cancel",new android.content.DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				try {
					dialog.dismiss();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		try {
			dialog.show();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	    //To manage image
		public void chooseImage(){

			choiceType = ChooserType.REQUEST_PICK_PICTURE;
			chooserManager = new ImageChooserManager(this,choiceType,"Fup/temp",true);
			chooserManager.setImageChooserListener(this);
			try {
				loader.startPreloader(this,"Fetching Data..");
				filePath = chooserManager.choose();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public void takeImage(){
			choiceType = ChooserType.REQUEST_CAPTURE_PICTURE;
			chooserManager = new ImageChooserManager(this,choiceType,"Fup/temp",true);
			chooserManager.setImageChooserListener(this);
			try {
				loader.startPreloader(this,"Fetching Data..");
				filePath = chooserManager.choose();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public void showToast(String message,boolean top){
			Toast toast = Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG);
			if(top){
				toast.setGravity(Gravity.TOP, 0, 0);
			}
			toast.show();
		}
		
		public void onClick(View v) {
			switch(v.getId()){
			case R.id.epGroupImage:
				openSelectionDialog();
			break;
			}
		}

}
