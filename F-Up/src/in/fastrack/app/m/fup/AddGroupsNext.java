package in.fastrack.app.m.fup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import in.fastrack.app.m.fup.adapter.PeopleAdapter;
import in.fastrack.app.m.fup.adapter.PeopleAdapter.PeopleListener;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Friend;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SendCallback;

public class AddGroupsNext extends Activity implements PeopleListener {

	private Preloader preloader;

	static PeopleAdapter mAdapter = null;

	private EditText mSearch;
	private ListView lv = null;
	private JSONArray friends = new JSONArray();
	private JSONArray friendsid = new JSONArray();

	private final ArrayList<Friend> sList = new ArrayList<Friend>();
	private String friendsListString = null;
	private ArrayList<String> selectedFriendsid = new ArrayList<String>();

	private String action = null;
	private String group_id;
	private ArrayList<Friend> friendListG = new ArrayList<Friend>();
	private ParseUser frienduser;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_groups_next);

		preloader = new Preloader();



		ActionBar bar = getActionBar();
		bar.setTitle(this.getString(R.string.selectfriends));

		mSearch = (EditText) findViewById(R.id.searchsuprisefriends);
		lv = (ListView) findViewById(R.id.friendList);
		// /////////////////
		TextView tv = new TextView(this);
		tv.setText(" No Phone Contacts");

		mAdapter = new PeopleAdapter(getApplicationContext(),
				R.layout.people_item, sList, this, friends);
		lv.setEmptyView(tv);
		lv.setAdapter(mAdapter);
		// ///////////////////
		if (getIntent().getStringExtra("friends") != null) {
			try {
				friends = new JSONArray(getIntent().getStringExtra("friends"));
				for (int i = 0; i < friends.length(); i++) {
					JSONObject explrObject = friends.getJSONObject(i);
					selectedFriendsid.add(explrObject.getString("userid"));
				}
				System.out.println(selectedFriendsid.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		if (getIntent().getStringExtra("action") != null) {
			action = getIntent().getStringExtra("action");
		}

		mSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				// String text = mSearch.getText().toString();
				// mAdapter.search(text);
				mAdapter.getFilter().filter(cs);
				mAdapter.notifyDataSetChanged();
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});

		preloader.startPreloader(this, "Fetching Your Friends");
		if (!Util.isNetworkAvailable(AddGroupsNext.this)) {
			try {
				preloader.stopLoading();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DialogFactory.getBeaconStreamDialog1(AddGroupsNext.this,
					getResources().getString(R.string.nw_error_str),
					Util.onClickListner, Util.retryClickListner).show();
		} else

		{
			final Boolean[] parms = { false };
			new LoadSocialAsync().execute(parms);
		}
	}

	public ParseUser getTheUser(String userid) {
		ParseUser user = null;
		ParseQuery<ParseUser> query1 = ParseUser.getQuery();
		query1.whereEqualTo("objectId", userid);
		List<ParseUser> objects;
		try {
			objects = query1.find();
			// The query was successful.
			for (ParseUser obj : objects) {
				user = obj;
				break;
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.action_menu_select, menu);
		return super.onCreateOptionsMenu(menu);
	}

	public class LoadSocialAsync extends
			AsyncTask<Boolean, Void, ArrayList<Friend>> {
		ArrayList<Friend> friendList = new ArrayList<Friend>();

		@Override
		protected ArrayList<Friend> doInBackground(Boolean... params) {
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
			query.whereEqualTo("user", ParseUser.getCurrentUser());
			try {
				List<ParseObject> objects = query.find();
				if (objects.size() > 0) {
					for (ParseObject obj : objects) {
						JSONArray friendsList = obj.getJSONArray("connectedF");
						if (friendsList != null) {
							for (int j = 0; j < friendsList.length(); j++) {
								try {
									String userid = friendsList.getString(j);
									if (userid != null) {
										ParseUser user = getTheUser(userid);

										Friend f = new Friend();
										f.setName(user.getString("fullname"));
										f.setUserId(userid);
										String imageUrl = "image";
										String t = null;
										ParseFile p = user
												.getParseFile("profilethumbnail");
										if (p != null) {
											String url = p.getUrl();
											imageUrl = url;
										} else if (user.get("facebookid") != null) {
											t = user.get("facebookid")
													.toString();
											if ((t != null) || (t != "")) {
												imageUrl = "https://graph.facebook.com/"
														+ user.get("facebookid")
																.toString()
														+ "/picture/?type=square";
											}
										}
										f.setUserImage(imageUrl);
										if (selectedFriendsid.contains(userid)) {
											f.setUserSelected(true);
										}
										friendList.add(f);

										// friendListG = friendList;
									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			/*
			 * ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
			 * query.whereEqualTo("user", ParseUser.getCurrentUser()); try {
			 * List<ParseObject> objects = query.find(); if(objects.size() > 0)
			 * { for(ParseObject obj : objects){ friendsListString =
			 * obj.getString("friendsList"); try { JSONArray jsonArray = new
			 * JSONArray(friendsListString); for (int i = 0; i <
			 * jsonArray.length(); i++) { JSONObject explrObject =
			 * jsonArray.getJSONObject(i); Friend f = new Friend();
			 * f.setName(explrObject.getString("name"));
			 * f.setSource(explrObject.getString("source"));
			 * f.setSupriseFriend(Boolean
			 * .parseBoolean(explrObject.getString("isSupriseFriend")));
			 * f.setUserId(explrObject.getString("userId"));
			 * f.setUserImage(explrObject.getString("userImage")); if
			 * (selectedFriendsid.contains(explrObject.getString("userId"))) {
			 * f.setUserSelected(true); } friendList.add(f); } } catch
			 * (JSONException e1) { e1.printStackTrace(); } } } } catch
			 * (ParseException e) { e.printStackTrace(); }
			 */
			return friendList;
		}

		@Override
		protected void onPostExecute(ArrayList<Friend> result) {
			super.onPostExecute(result);

			sList.addAll(result);
			updateList(sList);
		}

	}

	public void updateList(ArrayList<Friend> res) {
		try {
			mAdapter = new PeopleAdapter(getApplicationContext(),
					R.layout.people_item, res, this, friends);
			lv.setAdapter(mAdapter);
			lv.setVisibility(View.VISIBLE);
			mSearch.setVisibility(View.VISIBLE);
			preloader.stopLoading();
			Log.i("list", res.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void getPeopleAdded(JSONArray list, JSONArray friendsId) {
		// Log.d("MyApp","Friends: "+list.toString());
		friends = list;
		friendsid = friendsId;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		System.out.println(friends.toString() + " ---  " + action);
		switch (item.getItemId()) {
		case R.id.action_next:

			if (!Util.isNetworkAvailable(AddGroupsNext.this)) {

				DialogFactory.getBeaconStreamDialog1(AddGroupsNext.this,
						getResources().getString(R.string.nw_error_str),
						Util.onClickListner, Util.retryClickListner).show();
			} else

			{

				if (friends != null) {
					// friends found
					if (getIntent().getStringExtra("groupID") != null) {
						ParseQuery query = new ParseQuery("Groups");
						try {
							final ParseObject obj = query.get(getIntent()
									.getStringExtra("groupID"));
							obj.put("friendsList", friends.toString());
							obj.put("connectedGF", friendsid);
							obj.saveInBackground();

							try {

						

								for (int j = 0; j < friendsid.length(); j++) {
									Log.d("friendsId.get(j).toString()",
											"--"
													+ friendsid.get(j)
															.toString()
													+ "ParseUser.getCurrentUser().getObjectId().toString())"
													+ "--"
													+ ParseUser
															.getCurrentUser()
															.getObjectId()
															.toString());
									if (!friendsid
											.get(j)
											.toString()
											.equals(ParseUser.getCurrentUser()
													.getObjectId().toString())) {

										ParseQuery pushQuery = ParseInstallation
												.getQuery();
										ParsePush push = new ParsePush();
										// pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId().toString());
										// // not me
										pushQuery.whereEqualTo("myuser",
												friendsid.get(j).toString());
										//
										push.setQuery(pushQuery);
										JSONObject tempAlarmData = new JSONObject();

										if (obj != null)
											tempAlarmData
													.put("reference5", obj);
										tempAlarmData.put("notificationtype",
												10);
										tempAlarmData
												.put("alert",
														"You are added to a group by "
																+ ParseUser
																		.getCurrentUser()
																		.getString(
																				"fullname"));
										tempAlarmData
												.put("action",
														"in.fastrack.app.m.fup.NotificationActivityNew");
										push.setData(tempAlarmData);
										push.sendInBackground(new SendCallback() {
											public void done(
													com.parse.ParseException e) {
												if (e == null) {							
													Toast.makeText(AddGroupsNext.this,"Group request sent",Toast.LENGTH_LONG).show();
													// Shifted to here
													ParseObject notification = new ParseObject(
															"Activity");
													notification
															.put("creator",
																	ParseUser
																			.getCurrentUser());
													notification.put(
															"followers",
															friendsid);
													notification.put(
															"reference5", obj);
													notification.put(
															"notificationtype",
															10);
													notification.put(
															"notificationDone",
															false);
													notification.put("readers",
															friendsid);// changed
																		// from
																		// `new
																		// JSONArray()`
																		// to
																		// friendsid
													notification
															.add("readers",
																	ParseUser
																			.getCurrentUser()
																			.getObjectId());
													notification
															.saveInBackground();// saveEventually();
													Log.d("push", "success!");
												} else {
													Log.d("push", "failure");
												}
											}

										});
									}
								}
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}

							System.out.println(friends.toString());
							group_id = getIntent().getStringExtra("groupID");

							try {

								ParseQuery<ParseObject> query1 = ParseQuery
										.getQuery("Friends");
								query1.whereEqualTo("user",
										ParseUser.getCurrentUser());
								query1.findInBackground(new FindCallback<ParseObject>() {
									public void done(List<ParseObject> objects,
											ParseException e) {
										if (objects.size() > 0) {

											// edit
											if (e == null) {
												for (ParseObject obj : objects) {
													obj.addAllUnique(
															"connectedG",
															Arrays.asList(group_id));
													obj.saveInBackground();// saveEventually();();
												}
											} else {
											}
										} else {
											// save
											ParseObject friends = new ParseObject(
													"Friends");
											friends.addAllUnique("connectedG",
													Arrays.asList(group_id));
											friends.saveInBackground();// saveEventually();();
										}
									}
								});

								if (friendsid != null) {
									for (int j = 0; j < friendsid.length(); j++) {
										try {
											Log.i("the list is", ">>>>"
													+ friendsid.getString(j));
											String userid = friendsid
													.getString(j);
											if (userid != null) {

												ParseQuery<ParseUser> query2 = ParseUser
														.getQuery();
												query2.whereEqualTo("user",
														userid);
												query2.getInBackground(
														userid,
														new GetCallback<ParseUser>() {
															@Override
															public void done(
																	ParseUser object,
																	ParseException e) {
																frienduser = object;
																ParseQuery<ParseObject> query3 = ParseQuery
																		.getQuery("Friends");
																query3.whereEqualTo(
																		"user",
																		frienduser);
																query3.findInBackground(new FindCallback<ParseObject>() {
																	public void done(
																			List<ParseObject> objects,
																			ParseException e) {

																		if (objects
																				.size() > 0) {
																			// edit
																			if (e == null) {
																				for (ParseObject obj : objects) {
																					obj.addAllUnique(
																							"connectedG",
																							Arrays.asList(group_id));
																					obj.saveInBackground();// saveEventually();();
																				}
																			} else {
																			}
																		} else {
																			// save
																			ParseObject friends = new ParseObject(
																					"Friends");
																			friends.addAllUnique(
																					"connectedG",
																					Arrays.asList(group_id));
																			friends.saveInBackground();// saveEventually();();
																		}

																	}
																});
															}
														});
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								}
								Intent groupsActivity = new Intent(
										getApplicationContext(), Groups.class);
								groupsActivity
										.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
												& Intent.FLAG_ACTIVITY_SINGLE_TOP
												| Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(groupsActivity);
								finish();

							} catch (Exception e) {
							}

						} catch (ParseException e) {
							e.printStackTrace();
						}
					} else if (action.equals("edit")) {
						System.out.println("inside edit");
						Intent exit = new Intent();
						Bundle data = new Bundle();
						data.putString("friendsid", friendsid.toString());
						data.putString("friends", friends.toString());
						exit.putExtras(data);
						setResult(RESULT_OK, exit);
						finish();
					}
				}
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
