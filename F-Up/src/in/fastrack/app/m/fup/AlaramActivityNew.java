package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.config.AppConfig;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fima.glowpadview.GlowPadView;
import com.fima.glowpadview.GlowPadView.OnTriggerListener;

import de.greenrobot.event.EventBus;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v4.app.FragmentActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class AlaramActivityNew extends FragmentActivity implements
		OnClickListener, OnTriggerListener {

	private MediaPlayer mPlayer = null;
	private PowerManager.WakeLock mWakelock;
	private Bundle extras = null;
	TextView mEventName, mEventWith, tvAlarmTime, tvAlarmAMPM;
	//ImageButton mStopAlarm, mSnoozeAlarm;
	private AppConfig _config;
	int alarmrequestid;
	Calendar newInstance;
	Calendar calendor;
	long timeing;
	int inttime;

	private GlowPadView mGlowPadView;

	public void onEvent(boolean event) {
		Toast.makeText(this, "Phone Ringing cancelled" + alarmrequestid,
				Toast.LENGTH_LONG).show();

		// if(event.equals("Phone Ringing"))
		// {
		// Toast.makeText(this, "Phone Ringing cancelled",
		// Toast.LENGTH_LONG).show();
		// finish();
		// }
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		EventBus.getDefault().unregister(this);
	}

	@Override
	protected void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		mWakelock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
				| PowerManager.ACQUIRE_CAUSES_WAKEUP, "My Wake Log");
		mWakelock.acquire();
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
						| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
						| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
				WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
						| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
						| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		setContentView(R.layout.newalarm);

		try {
			// TELEPHONY MANAGER class object to register one listner
			TelephonyManager tmgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

			// Create Listner
			MyPhoneStateListener PhoneListener = new MyPhoneStateListener();

			// Register listener for LISTEN_CALL_STATE
			tmgr.listen(PhoneListener, PhoneStateListener.LISTEN_CALL_STATE);

		} catch (Exception e) {
			Log.e("Phone Receive Error", " " + e);
		}
		EventBus.getDefault().register(this);
		// //////////////
		mGlowPadView = (GlowPadView) findViewById(R.id.glow_pad_view);

		mGlowPadView.setOnTriggerListener(this);

		// uncomment this to make sure the glowpad doesn't vibrate on touch
		mGlowPadView.setVibrateEnabled(false);

		// uncomment this to hide targets
		// mGlowPadView.setShowTargetsOnIdle(true);
		// //////////////
		_config = new AppConfig(getApplicationContext());
		extras = getIntent().getExtras();
		mEventName = (TextView) findViewById(R.id.tvEventName);
		mEventWith = (TextView) findViewById(R.id.tvTaggedFriends);
//		mStopAlarm = (ImageButton) findViewById(R.id.stopAlarm);
//		mStopAlarm.setOnClickListener(this);
//		mSnoozeAlarm = (ImageButton) findViewById(R.id.snoozeAlarm);
//		mSnoozeAlarm.setOnClickListener(this);

		tvAlarmTime = (TextView) findViewById(R.id.tvAlarmTime);
		tvAlarmAMPM = (TextView) findViewById(R.id.tvAlarmAMPM);

		SimpleDateFormat df = new SimpleDateFormat("h:mm");
		String currentTime = df.format(Calendar.getInstance().getTime());
		if (currentTime != null)
			tvAlarmTime.setText(currentTime);

		SimpleDateFormat df1 = new SimpleDateFormat("a");
		String currentTimeAMPM = df1.format(Calendar.getInstance().getTime());
		if (currentTimeAMPM != null)
			tvAlarmAMPM.setText(currentTimeAMPM);

		if (extras != null) {
			String alarm_name = extras.getString("alarm_category");
			String breakLine = System.getProperty("line.separator");
			String friends = "";
			if (extras.getString("alarmfriends") != null) {
				try {
					JSONObject friendsobject = new JSONObject();
					JSONArray friendsArray = new JSONArray(
							extras.getString("alarmfriends"));
					if (friendsArray.length() > 0) {
						for (int f = 0; f < friendsArray.length(); f++) {
							friendsobject = friendsArray.getJSONObject(f);
							friends += "+" + friendsobject.getString("name")
									+ " ";
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			String alarm_message = extras.getString("alarm_title") + breakLine
					+ friends;
			String alarm_source = extras.getString("alarmpath");
			int alarm_type = 2;
			boolean recorded = extras.getBoolean("recorded");
			alarmrequestid = extras.getInt("alarmrequestcode");
			if (recorded) {
				alarm_type = 1;
			}
			mEventName.setText(alarm_name);
			mEventWith.setText(alarm_message);
			if (!_config.getPhoneState()) {
				playAlarm(alarm_source, alarm_type);
			} else {
				Toast.makeText(getApplicationContext(), "You are on Call",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.snoozeAlarm:
			newInstance = Calendar.getInstance();
			calendor = Calendar.getInstance();

			if (SettingsFragment.snooze_interval != null) {
				if (SettingsFragment.snooze_interval
						.equalsIgnoreCase("1 Minute")) {
					newInstance.add(Calendar.MINUTE, _config.getSnooze());
				} else if (SettingsFragment.snooze_interval
						.equalsIgnoreCase("5 Minutes")) {
					// Log.i("u r ", "here in 5 min");
					// timeing = newInstance.getTimeInMillis()+ 1000 * 60 * 5;
					// inttime = (int) timeing;
					// inttime = 1000 * 60 * 5;
					inttime = 5;
					newInstance.add(Calendar.MINUTE, inttime);
				} else if (SettingsFragment.snooze_interval
						.equalsIgnoreCase("10 Minutes")) {
					inttime = 10;
					newInstance.add(Calendar.MINUTE, inttime);
				} else if (SettingsFragment.snooze_interval
						.equalsIgnoreCase("15 Minutes")) {
					inttime = 15;
					newInstance.add(Calendar.MINUTE, inttime);
				} else if (SettingsFragment.snooze_interval
						.equalsIgnoreCase("30 Minutes")) {
					inttime = 30;
					newInstance.add(Calendar.MINUTE, inttime);
				} else {
					newInstance.add(Calendar.MINUTE, _config.getSnooze());
				}

			} else {
				newInstance.add(Calendar.MINUTE, _config.getSnooze());
			}
			// Update 2 min or based on user preferences.

			// newInstance.add(Calendar.MINUTE,_config.getSnooze());
			Date newtimechange = newInstance.getTime();
			long times = newtimechange.getTime();
			Intent intent = new Intent(getApplicationContext(),
					AlaramActivityNew.class);
			intent.putExtras(extras);
			PendingIntent pi = PendingIntent.getActivity(
					getApplicationContext(), alarmrequestid, intent,
					PendingIntent.FLAG_CANCEL_CURRENT);
			AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
			am.set(AlarmManager.RTC_WAKEUP, times, pi);
			// Toast.makeText(getApplicationContext(),"Alarm will ring in next "+Integer.toString(_config.getSnooze())+" minutes",
			// Toast.LENGTH_LONG).show();
			// Download audio from server
			//
			finish();
			// onStop();
			break;
		case R.id.stopAlarm:
			// Stop time and change the status
			finish();
			break;
		}
	}

	private void playAlarm(String dataSource, int dataResouce) {
		// If dataResource is 1 - then from the app else if its 2 then its from
		// SD card or online
		mPlayer = new MediaPlayer();

		switch (dataResouce) {
		case 1:
			Log.i("AUDIO SELECTION", "SD CARD");
			mPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
			Log.i("TRACK",
					Environment.getExternalStorageDirectory()
							+ _config.getRecordAudioPath() + dataSource);
			try {
				Log.i("AUDIO", "PLAYING");
				mPlayer.setDataSource(Environment.getExternalStorageDirectory()
						+ _config.getRecordAudioPath() + dataSource);
				triggerAlarm(true);
			} catch (IOException e) {
				Log.i("AUDIO", "ERROR");
				triggerAlarm(false);
				e.printStackTrace();
			}
			break;
		case 2:
			Log.i("AUDIO SELECTION", "APP");
			mPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
			// Toast.makeText(getApplicationContext(),dataSource,Toast.LENGTH_LONG).show();
			try {
				String change = stringtolower(dataSource);

				int trackId = getResources().getIdentifier(change, "raw",
						getApplicationContext().getPackageName());
				String fileName = "android.resource://" + getPackageName()
						+ "/" + trackId;
				mPlayer.setDataSource(getApplicationContext(),
						Uri.parse(fileName));
				triggerAlarm(true);
			} catch (IOException e) {
				triggerAlarm(false);
				e.printStackTrace();
			}
			break;
		}
	}

	private void triggerAlarm(Boolean isSafe) {
		if (isSafe) {
			// File found
			try {
				float count = 100 * .01f;
				mPlayer.setVolume(count, count);
				mPlayer.prepare();
				mPlayer.setLooping(true);
				mPlayer.start();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			// Default device alarm
			// TODO don't play sound if in silent
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (mWakelock.isHeld()) {
			mWakelock.release();
		}
		if (mPlayer != null) {
			if (this.isFinishing()) {
				mPlayer.stop();
				mPlayer.reset();
				mPlayer.release();
				mPlayer = null;
			}
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mWakelock.isHeld()) {
			mWakelock.release();
		}
		if (mPlayer != null) {
			if (this.isFinishing()) {
				mPlayer.stop();
				mPlayer.reset();
				mPlayer.release();
				mPlayer = null;
			}
		}
	}

	public String stringtolower(String myString) {
		return myString.replaceAll(" ", "_").toLowerCase(Locale.getDefault());
	}

	/* Glow pad view */
	@Override
	public void onGrabbed(View v, int handle) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReleased(View v, int handle) {
		mGlowPadView.ping();

	}

	@Override
	public void onTrigger(View v, int target) {
		final int resId = mGlowPadView.getResourceIdForTarget(target);
		switch (resId) {
		case R.drawable.snooze:
			newInstance = Calendar.getInstance();
			calendor = Calendar.getInstance();

			if (SettingsFragment.snooze_interval != null) {
				if (SettingsFragment.snooze_interval
						.equalsIgnoreCase("1 Minute")) {
					newInstance.add(Calendar.MINUTE, _config.getSnooze());
				} else if (SettingsFragment.snooze_interval
						.equalsIgnoreCase("5 Minutes")) {
					// Log.i("u r ", "here in 5 min");
					// timeing = newInstance.getTimeInMillis()+ 1000 * 60 * 5;
					// inttime = (int) timeing;
					// inttime = 1000 * 60 * 5;
					inttime = 5;
					newInstance.add(Calendar.MINUTE, inttime);
				} else if (SettingsFragment.snooze_interval
						.equalsIgnoreCase("10 Minutes")) {
					inttime = 10;
					newInstance.add(Calendar.MINUTE, inttime);
				} else if (SettingsFragment.snooze_interval
						.equalsIgnoreCase("15 Minutes")) {
					inttime = 15;
					newInstance.add(Calendar.MINUTE, inttime);
				} else if (SettingsFragment.snooze_interval
						.equalsIgnoreCase("30 Minutes")) {
					inttime = 30;
					newInstance.add(Calendar.MINUTE, inttime);
				} else {
					newInstance.add(Calendar.MINUTE, _config.getSnooze());
				}

			} else {
				newInstance.add(Calendar.MINUTE, _config.getSnooze());
			}
			// Update 2 min or based on user preferences.

			// newInstance.add(Calendar.MINUTE,_config.getSnooze());
			Date newtimechange = newInstance.getTime();
			long times = newtimechange.getTime();
			Intent intent = new Intent(getApplicationContext(),
					AlaramActivityNew.class);
			intent.putExtras(extras);
			PendingIntent pi = PendingIntent.getActivity(
					getApplicationContext(), alarmrequestid, intent,
					PendingIntent.FLAG_CANCEL_CURRENT);
			AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
			am.set(AlarmManager.RTC_WAKEUP, times, pi);
			// Toast.makeText(getApplicationContext(),"Alarm will ring in next "+Integer.toString(_config.getSnooze())+" minutes",
			// Toast.LENGTH_LONG).show();
			// Download audio from server
			//
			finish();
			break;

		case R.drawable.stop:
			finish();

			break;
		default:
			// Code should never reach here.
		}

	}

	@Override
	public void onGrabbedStateChange(View v, int handle) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFinishFinalAnimation() {
		// TODO Auto-generated method stub

	}

	private class MyPhoneStateListener extends PhoneStateListener {
		public boolean phoneRinging = false;

		public void onCallStateChanged(int state, String incomingNumber) {

			switch (state) {
			case TelephonyManager.CALL_STATE_IDLE:
				Log.d("DEBUG", "IDLE");
				phoneRinging = false;
				// if(alarmrequestid!=0)
				// {
				// Calendar calendar = Calendar.getInstance();
				// calendar.setTimeInMillis(System.currentTimeMillis());
				// Intent intent1 = new Intent( AlaramActivityNew.this,
				// AlarmActivity.class);
				// PendingIntent pi =
				// PendingIntent.getActivity(AlaramActivityNew.this,alarmrequestid,intent1,PendingIntent.FLAG_CANCEL_CURRENT);
				// AlarmManager am =
				// (AlarmManager)AlaramActivityNew.this.getSystemService(Context.ALARM_SERVICE);
				// am.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),
				// pi);
				// }
				break;
			case TelephonyManager.CALL_STATE_OFFHOOK:
				Log.d("DEBUG", "OFFHOOK");
				phoneRinging = false;
				break;
			case TelephonyManager.CALL_STATE_RINGING:
				EventBus.getDefault().post(true);
				Log.d("DEBUG", "RINGING");
				phoneRinging = true;
				//
				// Toast.makeText(AlaramActivityNew.this,"Phone Ringing",Toast.LENGTH_LONG).show();
				// Intent intent = new Intent( AlaramActivityNew.this,
				// AlarmActivity.class);
				// PendingIntent pendingIntent =
				// PendingIntent.getActivity(AlaramActivityNew.this,alarmrequestid,
				// intent, PendingIntent.FLAG_CANCEL_CURRENT);
				// AlarmManager alarmManager = (AlarmManager)
				// AlaramActivityNew.this.getSystemService(Context.ALARM_SERVICE);
				// alarmManager.cancel(pendingIntent);
				if (phoneRinging)
					AlaramActivityNew.this.finish();

				break;
			}
		}
	}

}