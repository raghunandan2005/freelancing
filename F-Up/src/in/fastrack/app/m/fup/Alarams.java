package in.fastrack.app.m.fup;

public class Alarams {

	private int id;
	private String category,title,recorded,requestcode,path,friends,friendsid,repeattype,atime,milli;
	
	
	public String getAtime() {
		return atime;
	}
	public void setAtime(String atime) {
		this.atime = atime;
	}
	public String getMilli() {
		return milli;
	}
	public void setMilli(String milli) {
		this.milli = milli;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRecorded() {
		return recorded;
	}
	public void setRecorded(String recorded) {
		this.recorded = recorded;
	}
	public String getRequestcode() {
		return requestcode;
	}
	public void setRequestcode(String requestcode) {
		this.requestcode = requestcode;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getFriends() {
		return friends;
	}
	public void setFriends(String friends) {
		this.friends = friends;
	}
	public String getFriendsid() {
		return friendsid;
	}
	public void setFriendsid(String friendsid) {
		this.friendsid = friendsid;
	}
	public String getRepeattype() {
		return repeattype;
	}
	public void setRepeattype(String repeattype) {
		this.repeattype = repeattype;
	}
	 
}
