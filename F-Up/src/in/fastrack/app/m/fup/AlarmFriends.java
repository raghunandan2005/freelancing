package in.fastrack.app.m.fup;

import java.util.ArrayList;

import com.parse.ParseUser;


import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.AlarmFriend;
import in.fastrack.app.m.fup.adapter.AlarmFriendsAdapter;
import in.fastrack.app.m.fup.adapter.FeedAdapter;
import in.fastrack.app.m.fup.config.AppConfig;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

public class AlarmFriends extends Activity{
	private AppConfig _config;
	static AlarmFriendsAdapter mAdapter = null;
	ProgressBar mPbar;
	EditText mSearch;
	ListView lv = null;
	ParseUser currentUser;
	public  ArrayList<AlarmFriend> answers = new ArrayList<AlarmFriend>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.alarm_friends_layout);
		configParse();
		mSearch = (EditText) findViewById(R.id.inputSearch);
		mPbar = (ProgressBar) findViewById(R.id.pbPhoneBook);
		lv = (ListView) findViewById(R.id.socialList);
		lv.setVisibility(View.VISIBLE);
		
		//Activity
		if (!Util.isNetworkAvailable(AlarmFriends.this)) {	
			
			DialogFactory.getBeaconStreamDialog1(AlarmFriends.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
		} else 
		
		{
		try {
			AlarmFriend alarmfriend;
			String friend_name = "";
			String friend_image = "";
			String friend_id = "";
			for(int f = 0; f < FeedAdapter.answers.size(); f++){
				alarmfriend = new AlarmFriend();				
				friend_name = FeedAdapter.answers.get(f).getName();
				friend_image = FeedAdapter.answers.get(f).getUserImage();
				friend_id = FeedAdapter.answers.get(f).getUserId();
				alarmfriend.setName(friend_name);
				alarmfriend.setUserImage(friend_image);
				alarmfriend.setUserId(friend_id);
				answers.add(alarmfriend);
				
			}
			mAdapter = new AlarmFriendsAdapter(AlarmFriends.this,R.layout.people_item,answers);
			lv.setAdapter(mAdapter);
		} catch (Exception e) {
			
		}
		}
		/*lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Log.i("list", ">>>>");
			}
		});*/
	}
	public void configParse(){
		_config = new AppConfig(this);
		currentUser = ParseUser.getCurrentUser();
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if(FeedAdapter.answers!=null)
		FeedAdapter.answers.clear();
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(FeedAdapter.answers!=null)
		FeedAdapter.answers.clear();
	}
	@Override
	protected void onPause() {
		super.onPause();
		if(FeedAdapter.answers!=null)
		FeedAdapter.answers.clear();
	}
	
}
