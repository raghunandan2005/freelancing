package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.adapter.ImageAdapter;
import in.fastrack.app.m.fup.config.AppConfig;

import java.util.ArrayList;

import com.parse.ParseUser;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class AndroidGridLayoutActivity extends Activity {
	private AppConfig _config;
	ParseUser currentUser;
	private ActionBar bar;
	
	public void configParse(){
		_config = new AppConfig(this);
		currentUser = ParseUser.getCurrentUser();
	}

	@Override
	protected void onResume() {
		super.onResume();
		checkActive();
	}
	
	public void checkActive(){
		ParseUser currentUser = ParseUser.getCurrentUser();
		if(currentUser == null) {
			//User not logged in
			Intent reset = new Intent(getApplicationContext(),SplashActivity.class);
			startActivity(reset);
		}
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.grid_layout);
		bar = getActionBar();
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		bar.setDisplayShowHomeEnabled(true);
		bar.setHomeButtonEnabled(true);
		bar.setDisplayHomeAsUpEnabled(true);
		
		GridView gridView = (GridView) findViewById(R.id.grid_view);
		
		// Instance of ImageAdapter Class
		gridView.setAdapter(new ImageAdapter(this));
		
		/**
		 * On Click event for Single Gridview Item
		 * */
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				
				// Sending image id to FullScreenActivity
				Intent i = new Intent(getApplicationContext(), EditProfileActivity.class);
				// passing array index
				i.putExtra("id", position);
				String name=parent.getItemAtPosition(position).toString();
				i.putExtra("name", name);
				startActivity(i);
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			Intent i = new Intent(getApplicationContext(), EditProfileActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			finish();
			return true;
		}
		return false;
	}
	
}