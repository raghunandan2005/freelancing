package in.fastrack.app.m.fup;

import com.crashlytics.android.Crashlytics;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

public class CheckConnectivity {
	ConnectivityManager cm;
	NetworkInfo wifiInfo,mobileInfo;
	
	public Boolean check(Context ctx){
		try{
			cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
			wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			mobileInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			if(wifiInfo.isConnected() || mobileInfo.isConnected()){
				return true;
			}
		}catch(Exception e){
			Crashlytics.log(Log.ERROR,"INTERNET CONNECTION",e.toString());
		}
		return false;
	}
	
	public Boolean checkAndToast(Context ctx){
		if(!check(ctx)){
			Toast.makeText(ctx,"No Internet connectivity",Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
		
	
	public Boolean checkAndToast(Context ctx,String message){
		if(!check(ctx)){
			Toast.makeText(ctx,message,Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	
	
}
