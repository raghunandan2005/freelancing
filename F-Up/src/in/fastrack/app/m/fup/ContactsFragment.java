package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.adapter.PhoneBookAdapter;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.model.PhoneBook;

import java.util.ArrayList;
import java.util.List;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.MenuItem.OnActionExpandListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

public class ContactsFragment extends Fragment implements
		android.view.View.OnClickListener {
	boolean _hasLoaded = false;
	private AppConfig _nConfig;
	private ListView lv;
	PhoneBookAdapter mAdapter = null;
	ArrayList<String> mTempArray = new ArrayList<String>();
	private AppConfig _config;
	Boolean isInvite = false;

	TextWatcher mWatcher;
	ProgressBar mPhoneBookPreloader;
	TextView mPhoneBookMessage;
	private Preloader preloader;
	ArrayList<PhoneBook> myList = new ArrayList<PhoneBook>();
	CheckConnectivity cn = new CheckConnectivity();
	boolean isInternetActive = false;
	Cursor c, phones;

	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.fragment_phonebook, container,
				false);
		
		_nConfig = new AppConfig(getActivity().getBaseContext());
		lv = (ListView) root.findViewById(R.id.phonebooklist);
		_config = new AppConfig(getActivity().getBaseContext());
		// preloader = new Preloader();
		// preloader.startPreloader(getActivity(),"Fetching Your Friends");
		String[] parseKeys = _config.getAppKeys("parse");
		Parse.initialize(getActivity().getBaseContext(), parseKeys[0],
				parseKeys[1]);
		isInvite = _nConfig.getInviteMode();
		mAdapter = new PhoneBookAdapter(getActivity().getBaseContext(),
				R.layout.phonebook_row, myList);
		TextView tv = new TextView(getActivity());
		tv.setText(" No Phone Contacts");
		lv.setEmptyView(tv);
		lv.setAdapter(mAdapter);

		isInternetActive = cn.checkAndToast(getActivity(),
				"No Internet Connection");

		mPhoneBookPreloader = (ProgressBar) root.findViewById(R.id.pbPhoneBook);

		mPhoneBookMessage = (TextView) root
				.findViewById(R.id.tvPhoneBookMessage);

		// initData();
		return root;
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {

		if (this.isVisible()) {
			if (isVisibleToUser && !_hasLoaded) {
				_hasLoaded = true;
				new LoadContactsAsync().execute();
			}
		}
	}

	public void initData() {

		try {
			// New way to pull contacts
			Uri uri = ContactsContract.Contacts.CONTENT_URI;
			String[] projection = new String[] { ContactsContract.Contacts._ID,
					ContactsContract.Contacts.DISPLAY_NAME,
					ContactsContract.Contacts.PHOTO_ID, "photo_uri" };
			String selection = ContactsContract.Contacts.IN_VISIBLE_GROUP
					+ "='" + ("1") + "'";
			String[] selectionArgs = null;
			String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
					+ " COLLATE LOCALIZED ASC";

			c = getActivity()
					.getBaseContext()
					.getContentResolver()
					.query(uri, projection, selection, selectionArgs, sortOrder);
			if (c != null) {
				if (isInvite) {
					// Show All
					ArrayList<PhoneBook> myList = new ArrayList<PhoneBook>();
					PhoneBook items = null;
					while (c.moveToNext()) {
						String name = c
								.getString(c
										.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
						String phoneNum = "3333333333";
						Uri userUri = ContentUris
								.withAppendedId(
										ContactsContract.Contacts.CONTENT_URI,
										c.getLong(c
												.getColumnIndex(ContactsContract.Contacts._ID)));
						Log.i("CONTACT_IMAGE", userUri.toString());
						items = new PhoneBook(name, phoneNum,
								userUri.toString(), false);
						myList.add(items);
					}
					mAdapter.notifyDataSetChanged();
					// mAdapter = new
					// PhoneBookAdapter(getActivity().getBaseContext(),R.layout.phonebook_row,
					// myList);
					// lv.setAdapter(mAdapter);
				} else {
					// Add Friend
					ParseQuery<ParseUser> query = ParseUser.getQuery();
					while (c.moveToNext()) {
						String name = c
								.getString(c
										.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
						String phoneNum = "2222222222";
						query.whereEqualTo("phone", phoneNum);
					}
					query.findInBackground(new FindCallback<ParseUser>() {

						@Override
						public void done(List<ParseUser> objects,
								ParseException e) {
							if (e == null) {
								if (objects != null) {
									ArrayList<PhoneBook> myList = new ArrayList<PhoneBook>();
									PhoneBook items = null;
									for (int i = 0; i < objects.size(); i++) {
										items = new PhoneBook(
												objects.get(i).get("name")
														.toString(),
												objects.get(i).get("phone")
														.toString(),
												objects.get(i)
														.get("profilethumbnail")
														.toString(), false);
										myList.add(items);
									}
								}
							} else {
								Toast.makeText(getActivity(), "1",
										Toast.LENGTH_LONG).show();
							}
						}
					});
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if (phones != null)
				phones.close();
			if (c != null)
				c.close();
		}

	}

	@Override
	public void onClick(View arg0) {

	}

	private class LoadContactsAsync extends
			AsyncTask<Void, Void, ArrayList<PhoneBook>> {

		@Override
		protected ArrayList<PhoneBook> doInBackground(Void... params) {
			ArrayList<PhoneBook> tList = new ArrayList<PhoneBook>();
			if (isInvite) {
				try {
					Uri uri = ContactsContract.Contacts.CONTENT_URI;
					String[] projection = new String[] {
							ContactsContract.Contacts._ID,
							ContactsContract.Contacts.DISPLAY_NAME,
							ContactsContract.Contacts.PHOTO_ID, "photo_uri" };
					String selection = ContactsContract.Contacts.IN_VISIBLE_GROUP
							+ "='" + ("1") + "'";
					String[] selectionArgs = null;
					String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
							+ " COLLATE LOCALIZED ASC";

					c = getActivity()
							.getBaseContext()
							.getContentResolver()
							.query(uri, projection, selection, selectionArgs,
									sortOrder);
					if (c != null) {

						// Load all Phone book
						PhoneBook items = null;
						while (c.moveToNext()) {
							String name = c
									.getString(c
											.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
							String contactId = c
									.getString(c
											.getColumnIndex(ContactsContract.Contacts._ID));

							String phoneNum = "";
							phones = getActivity()
									.getBaseContext()
									.getContentResolver()
									.query(Phone.CONTENT_URI,
											null,
											Phone.CONTACT_ID + " = "
													+ contactId, null, null);
							while (phones.moveToNext()) {
								String number = phones.getString(phones
										.getColumnIndex(Phone.NUMBER));
								phoneNum = number;
							}
							phones.close();

							Uri userUri = ContentUris
									.withAppendedId(
											ContactsContract.Contacts.CONTENT_URI,
											c.getLong(c
													.getColumnIndex(ContactsContract.Contacts._ID)));
							// Log.i("CONTACT_IMAGE",userUri.toString());
							items = new PhoneBook(name, phoneNum,
									userUri.toString(), false);
							tList.add(items);
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					if (phones != null)
						phones.close();
					if (c != null)
						c.close();
				}
			} else {
				try {
					// Load data based on people who are using app

					Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
					String[] projection = new String[] { Phone._ID,
							Phone.DISPLAY_NAME, Phone.NUMBER, Photo.PHOTO };
					String selection = ContactsContract.Contacts.IN_VISIBLE_GROUP
							+ "='" + ("1") + "'";
					String[] selectionArgs = null;
					String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
							+ " COLLATE LOCALIZED ASC";

					Cursor c = getActivity()
							.getBaseContext()
							.getContentResolver()
							.query(uri, projection, selection, selectionArgs,
									sortOrder);
					if (c != null && c.getCount() > 0) {
						ParseQuery<ParseUser> query = ParseUser.getQuery();
						while (c.moveToNext()) {
							String name = c.getString(c
									.getColumnIndex(Phone.DISPLAY_NAME));
							String phoneNum = c.getString(c
									.getColumnIndex(Phone.NUMBER));
							Log.i("MY CONTACTS>>>", phoneNum);
							if (phoneNum.length() > 8) {
								query.whereEqualTo("phone", phoneNum);
							}
						}
						// query.whereEqualTo("phone","+919916822785");
						/* try { */
						query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
						query.whereNotEqualTo("objectId", ParseUser
								.getCurrentUser().getObjectId());
						ArrayList<PhoneBook> myList = new ArrayList<PhoneBook>();
						PhoneBook items = null;
						List<ParseUser> qResult = query.find();
						for (int i = 0; i < qResult.size(); i++) {
							ParseFile img = (ParseFile) qResult.get(i).get(
									"profilethumbnail");
							Log.i("INFOOOOO", img.getUrl());
							items = new PhoneBook(qResult.get(i).get("name")
									.toString(), qResult.get(i).get("phone")
									.toString(), img.getUrl(), false);
							tList.add(items);
						}
						/*
						 * } catch (ParseException e) { e.printStackTrace(); }
						 */
					}
				} catch (Exception e) {
					if (phones != null)
						phones.close();
					if (c != null)
						c.close();
				}

			}
			return (tList);
		}

		@Override
		protected void onPostExecute(ArrayList<PhoneBook> result) {
			super.onPostExecute(result);
			mPhoneBookMessage.setVisibility(View.GONE);
			mPhoneBookPreloader.setVisibility(View.GONE);
			if (!result.isEmpty()) {
				try {
					mAdapter = new PhoneBookAdapter(getActivity(),
							R.layout.people_item_new, result);// phonebook_row
					lv.setAdapter(mAdapter);

					lv.setVisibility(View.VISIBLE);
					// preloader.stopLoading();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					if (phones != null)
						phones.close();
					if (c != null)
						c.close();
				}
			} else {
				mPhoneBookMessage.setText("No Contacts found.");
				mPhoneBookMessage.setVisibility(View.VISIBLE);
			}
		}

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (phones != null)
			phones.close();
		if (c != null)
			c.close();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.result_menu, menu);
		// get my MenuItem with placeholder submenuT
		//Toast.makeText(getActivity(), ".............", Toast.LENGTH_SHORT).show();
		

	}

//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// TODO Auto-generated method stub
//		Toast.makeText(getActivity(), "............."+item.getItemId(), Toast.LENGTH_SHORT).show();
//		switch (item.getItemId()) {
//		
////		case R.id.search1:
////			Toast.makeText(getActivity(), "....dfffff........."+item.getItemId(), Toast.LENGTH_SHORT).show();
////			return true;
////		case R.id.search:
////			
////			
////			MenuItem searchMenuItem = item;
////			searchMenuItem.expandActionView(); 
////			SearchManager searchManager = (SearchManager) getActivity()
////					.getSystemService(Context.SEARCH_SERVICE);
////
////			SearchView searchView = (SearchView) searchMenuItem.getActionView();
////			searchView.setSearchableInfo(searchManager
////					.getSearchableInfo(getActivity().getComponentName()));
////			searchView.setQueryHint("Search Friends");
////			searchView.setIconifiedByDefault(false);
////
////			searchMenuItem
////					.setOnActionExpandListener(new OnActionExpandListener() {
////
////						@Override
////						public boolean onMenuItemActionCollapse(MenuItem item) {
////							// TODO Auto-generated method stub
////							mAdapter.setData();
////							mAdapter.notifyDataSetChanged();
////
////							return true;
////						}
////
////						@Override
////						public boolean onMenuItemActionExpand(MenuItem item) {
////							// TODO Auto-generated method stub
////							
////							return true;
////						}
////
////					});
////
////			SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
////				@Override
////				public boolean onQueryTextChange(String newText) {
////					// this is your adapter that will be filtered
////					mAdapter.getFilter().filter(newText);
////					//Toast.makeText(getActivity(), "............."+newText, Toast.LENGTH_SHORT).show();
////					return true;
////				}
////
////				@Override
////				public boolean onQueryTextSubmit(String query) {
////					// this is your adapter that will be filtered
////					mAdapter.getFilter().filter(query);
////					mAdapter.notifyDataSetChanged();
////					//Toast.makeText(getActivity(), "............."+query, Toast.LENGTH_SHORT).show();
////					return true;
////				}
////			};
////			searchView.setOnQueryTextListener(textChangeListener);
////			return true;
//		default:
//			
//            return super.onOptionsItemSelected(item);
//            
//		}
//		
//	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		super.onPrepareOptionsMenu(menu);
		MenuItem searchMenuItem= menu.findItem(R.id.search1);
		searchMenuItem.expandActionView(); 
		SearchManager searchManager = (SearchManager) getActivity()
				.getSystemService(Context.SEARCH_SERVICE);

		SearchView searchView = (SearchView) searchMenuItem.getActionView();
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getActivity().getComponentName()));
		searchView.setQueryHint("Search Friends");
	//	searchView.setIconifiedByDefault(false);

		searchMenuItem
				.setOnActionExpandListener(new OnActionExpandListener() {

					@Override
					public boolean onMenuItemActionCollapse(MenuItem item) {
						// TODO Auto-generated method stub
						mAdapter.setData();
						mAdapter.notifyDataSetChanged();

						return true;
					}

					@Override
					public boolean onMenuItemActionExpand(MenuItem item) {
						// TODO Auto-generated method stub
						
						return true;
					}

				});

		SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextChange(String newText) {
				// this is your adapter that will be filtered
				mAdapter.getFilter().filter(newText);
				//Toast.makeText(getActivity(), "............."+newText, Toast.LENGTH_SHORT).show();
				return true;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				// this is your adapter that will be filtered
				mAdapter.getFilter().filter(query);
				mAdapter.notifyDataSetChanged();
				//Toast.makeText(getActivity(), "............."+query, Toast.LENGTH_SHORT).show();
				return true;
			}
		};
		searchView.setOnQueryTextListener(textChangeListener);
		
	}
	
	
}
