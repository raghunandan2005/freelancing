package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.alarm.CreateNewAlarmActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class DemoActivity extends Activity {

	private android.app.ActionBar actionBar;
	private Button demo, bInvite, bAlarm;
	private int ALARM_CREATE = 200;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_demo);
		// actionBar= this.getActionBar();
		// actionBar.setDisplayHomeAsUpEnabled(true);
		demo = (Button) this.findViewById(R.id.bView);
		bInvite = (Button) this.findViewById(R.id.bInvite);
		bAlarm = (Button) this.findViewById(R.id.bAlarm);
		demo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i = new Intent(DemoActivity.this, MainActivity.class);
				startActivity(i);
			}

		});

		bInvite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				Intent a = new Intent(DemoActivity.this, InvitePeople.class);
				Bundle bu = new Bundle();
				bu.putBoolean("go_back", true);
				bu.putBoolean("isInvite", true);
				a.putExtras(bu);
				startActivity(a);

			}
		});

		bAlarm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				Intent i = new Intent(DemoActivity.this,
						CreateNewAlarmActivity.class);
				Bundle b = new Bundle();
				b.putBoolean("go_back", true);
				i.putExtras(b);
				startActivityForResult(i, ALARM_CREATE);

			}
		});

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
}
