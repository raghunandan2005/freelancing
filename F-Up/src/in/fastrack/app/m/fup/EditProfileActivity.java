package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.adapter.ImageAdapter;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.dialogs.DatePickerFragment;
import in.fastrack.app.m.fup.dialogs.DatePickerFragment.DatePickedListener;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.lib.Util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.shapes.RectShape;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;





import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NumberRule;
import com.mobsandgeeks.saripaar.annotation.NumberRule.NumberType;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.mobsandgeeks.saripaar.annotation.TextRule;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class EditProfileActivity extends Activity implements ValidationListener,ImageChooserListener,android.view.View.OnClickListener,DatePickedListener {
	int position;
	ImageAdapter imageAdapter;
		private EditText mCity,mDob;
		private AppConfig _config;
		protected ProgressDialog proDialog;
		private Preloader loader;
		private Button update,epUpdate_bg_pic;
		private int choiceType;
		private ImageChooserManager chooserManager;
		private String filePath, fileName = null,fileName1 = null;
		private ParseFile p,pb;
		private String t = null;
		private String previousEmailID;
		
		private String name,profilBg;
		
		File f,f1;
		Validator validator;
		
		
		@Required(order = 1)
		@TextRule(order = 2,minLength = 4,message="Enter atleast 4 characters.")
		private EditText mFullName;
		
		@Required(order = 3)
		@Email(order=4,message="Enter a valid E-Mail address.")
		private EditText mEmail;
		
		
	//	@NumberRule(order = 5,message="Enter phone number in numeric",type=NumberType.LONG)
	//	@TextRule(order=6,minLength=10,maxLength=14,message="Enter a valid phone number")
		private EditText mPhone;
		
		ImageView mUserImage,epUpdate_bg_Image;
		ParseUser currentUser;
		private ActionBar bar;
		
		public void configParse(){
			_config = new AppConfig(this);
			currentUser = ParseUser.getCurrentUser();
		}

		@Override
		protected void onResume() {
			super.onResume();
			checkActive();
		}
		
		public void checkActive(){
			ParseUser currentUser = ParseUser.getCurrentUser();
			if(currentUser == null) {
				//User not logged in
				Intent reset = new Intent(getApplicationContext(),SplashActivity.class);
				startActivity(reset);
			}
		}
		
		

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			

			bar = getActionBar();
			bar.setTitle(getResources().getString(R.string.edit_profile));
			//bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
			bar.setDisplayShowHomeEnabled(true);
			bar.setHomeButtonEnabled(true);
			bar.setDisplayHomeAsUpEnabled(true);
			
			setContentView(R.layout.edit_profile_layout);
			
			loader = new Preloader();
			
			_config = new AppConfig(this);
			String[] parseKeys = _config.getAppKeys("parse");
			Parse.initialize(this,parseKeys[0],parseKeys[1]);
			
		
			mEmail = (EditText) findViewById(R.id.eptEmail);
			mFullName = (EditText) findViewById(R.id.eptName);
			mCity = (EditText) findViewById(R.id.eptCity);
			mDob = (EditText) findViewById(R.id.eptDob);
			mPhone = (EditText) findViewById(R.id.eptMobile);
			mUserImage = (ImageView) findViewById(R.id.epUserProfileImage);
			epUpdate_bg_Image = (ImageView) findViewById(R.id.epUpdate_bg_Image);
			mUserImage.setOnClickListener(this);
			update = (Button) findViewById(R.id.epUpdate);
			update.setVisibility(View.GONE);
			epUpdate_bg_pic = (Button)findViewById(R.id.epUpdate_bg_pic);
			update.setOnClickListener(this);
			epUpdate_bg_pic.setOnClickListener(this);
			mDob.setInputType(InputType.TYPE_NULL);
			mDob.setClickable(true);
			mDob.setOnClickListener(this);
		
			mDob.setOnFocusChangeListener(new View.OnFocusChangeListener() {
				
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if(hasFocus){
						DialogFragment df = new DatePickerFragment();
						df.show(getFragmentManager(),"datePicker");
					}
				}
			});
			
			validator = new Validator(this);
			validator.setValidationListener(this);
			
			//Activity
			if (!Util.isNetworkAvailable(EditProfileActivity.this)) {	
				
				DialogFactory.getBeaconStreamDialog1(EditProfileActivity.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			} else 
			
			{
			try {
				//Setting the values to the text fields 
				if(ParseUser.getCurrentUser().get("fullname") != null) {
					name = ParseUser.getCurrentUser().get("fullname").toString();
				} else {
					name = ParseUser.getCurrentUser().get("name").toString();
				}
				mFullName.setText(name);
				
				previousEmailID = ParseUser.getCurrentUser().get("email").toString();
				
				if(ParseUser.getCurrentUser().get("email") != null)
					mEmail.setText((ParseUser.getCurrentUser().get("email").toString() != null) ? ParseUser.getCurrentUser().get("email").toString() : "");
				if(ParseUser.getCurrentUser().get("city") != null)
					mCity.setText((ParseUser.getCurrentUser().get("city").toString() != null) ? ParseUser.getCurrentUser().get("city").toString() : "");
				if(ParseUser.getCurrentUser().get("phone") != null)
					mPhone.setText((ParseUser.getCurrentUser().get("phone").toString() != null) ? ParseUser.getCurrentUser().get("phone").toString() : "");
				if(ParseUser.getCurrentUser().get("dob") != null)
					mDob.setText((ParseUser.getCurrentUser().get("dob").toString() != null) ? ParseUser.getCurrentUser().get("dob").toString() : "");
				
				String imageUrl = "image";
				p = ParseUser.getCurrentUser().getParseFile("profilethumbnail");
				if(p!=null){
					String url = p.getUrl();
					imageUrl = url;
				}else{
					if(ParseUser.getCurrentUser().get("facebookid") != null && ParseUser.getCurrentUser().get("facebookid") != "") {
						t = ParseUser.getCurrentUser().get("facebookid").toString();
						if((t!=null)||(t!="")){
							imageUrl = "https://graph.facebook.com/"+ ParseUser.getCurrentUser().get("facebookid").toString()+ "/picture/?type=square";
						}
					}
				}
				Picasso.with(getApplicationContext()).load(imageUrl).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into((ImageView)findViewById(R.id.epUserProfileImage));


				// For background image
				Intent i = getIntent();
				// Selected image id
				imageAdapter = new ImageAdapter(this);
				name = getIntent().getStringExtra("name");
				if (name == null) {
					//epUpdate_bg_Image.setImageResource(R.drawable.cover);
					
				/*	String imageUrlb = "imageb";
					pb = ParseUser.getCurrentUser().getParseFile("profilebg");
					if(pb!=null){
						String url = pb.getUrl();
						imageUrlb = url;
					}
					Picasso.with(getApplicationContext()).load(imageUrlb).fit().placeholder(R.drawable.cover).error(R.drawable.cover).into((ImageView)findViewById(R.id.epUpdate_bg_Image));
*/
					
					
					profilBg = 	ParseUser.getCurrentUser().getString("profileBg");
					
					Log.i("name == null", "profileBG");
					
					if(profilBg!=null){
						
						if(profilBg.equalsIgnoreCase("0"))
						{
							position = 0;
						}
						else if(profilBg.equalsIgnoreCase("1"))
						{
							position = 1;
						}
						else if(profilBg.equalsIgnoreCase("2"))
						{
							position = 2;
						}
						else if(profilBg.equalsIgnoreCase("3"))
						{
							position = 3;
						}
						else if(profilBg.equalsIgnoreCase("4"))
						{
							position = 4;
						}
						epUpdate_bg_Image.setImageResource(imageAdapter.mThumbIds[position]);
					}
					
				} else {
					
					position = i.getExtras().getInt("id");
					Log.i("name!! == null", "profileBG"+position);
					profilBg = String.valueOf(position);
					epUpdate_bg_Image.setImageResource(imageAdapter.mThumbIds[position]);
					
					
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// End for background image

		
			}
		
		    //For background image
			
		  //End for background image
		
		
		
		}
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			 getMenuInflater().inflate(R.menu.action_menu_createalarm, menu);
			 return super.onCreateOptionsMenu(menu);
		}
		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			if (resultCode == RESULT_OK && (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
				loader.stopLoading();
				if (chooserManager == null) {
					reinitializeImageChooser();
				}
				chooserManager.submit(requestCode, data);
			} else {
				loader.stopLoading();
				//pbar.setVisibility(View.GONE);
			}
		}
		
		private void reinitializeImageChooser() {
			chooserManager = new ImageChooserManager(this, choiceType,"Fup/temp", true);
			chooserManager.setImageChooserListener(this);
			chooserManager.reinitialize(filePath);
		}

		@Override
		public void onError(String arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onImageChosen(final ChosenImage image) {
			runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							if(image != null){
								loader.startPreloader(EditProfileActivity.this,"Capturing Image..");
								f = new File(image.getFileThumbnail());	
							   // f1 = new File(image.getFilePathOriginal()); //for profilebg
								
								Picasso.with(getApplicationContext()).load(Uri.fromFile(f)).fit().centerCrop().transform(new RoundedTransform()).into(mUserImage,new Callback() {
								//Picasso.with(getApplicationContext()).load(Uri.fromFile(f)).fit().into(epUpdate_bg_Image,new Callback() {  //for profilebg
									@Override
									public void onSuccess() {
										Long tsLong = System.currentTimeMillis()/1000;
										fileName = tsLong.toString();
										loader.stopLoading();
									}
									
									@Override
									public void onError() { 
										loader.stopLoading();
									}
								});
								
								/*
									Picasso.with(getApplicationContext()).load(Uri.fromFile(f1)).fit().into(epUpdate_bg_Image,new Callback() {  //for profilebg
										@Override
										public void onSuccess() {
											Long tsLong = System.currentTimeMillis()/1000;
											fileName1 = tsLong.toString();
											loader.stopLoading();
										}
										
										@Override
										public void onError() {
											loader.stopLoading();
										}
									});*/
							}
						}
					});
		}

		@Override
		public void onValidationFailed(View failedView, Rule<?> failedRule) {
			String message = failedRule.getFailureMessage();
			if(failedView instanceof EditText){
				failedView.requestFocus();
				((EditText) failedView).setError(message);
			}else{
				showToast(message,true);
			}
		}

		@Override
		public void onValidationSucceeded() {
			//showToast("SUCCESS");
			loader.startPreloader(EditProfileActivity.this,"Updating Your Profile");
			ParseUser user = ParseUser.getCurrentUser();
			if(mEmail.getText().length() >0){
				user.setEmail(mEmail.getText().toString());
			}
			user.put("name",mFullName.getText().toString());
			user.put("fullname",mFullName.getText().toString());
			user.put("phone",mPhone.getText().toString());
			user.put("city", mCity.getText().toString());
			user.put("dob",mDob.getText().toString());
			
			if(profilBg!=null){				    		
	    		user.put("profileBg",profilBg);
	    	}
			
			user.saveInBackground(new SaveCallback() {
				  public void done(ParseException e) {
				    if (e == null) {
				      // Saved successfully
				    	if(fileName !=null){
				    		ParseUser user = ParseUser.getCurrentUser();
							Bitmap bmp = BitmapFactory.decodeFile(f.toString());
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
					        // Compress image to lower quality scale 1 - 100
					        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
					        byte[] images = stream.toByteArray();
					        ParseFile file = new ParseFile(fileName+".png",images);
							file.saveInBackground();
							//user.put("profilebg",file); //for profilebg
							user.put("profilethumbnail",file);
							user.saveInBackground();
						}
				    	
				    	
				    
				    /*	
				    	if(imageAdapter.mThumbIds[position] !=null){
				    		ParseUser user = ParseUser.getCurrentUser();
							Bitmap bmp = BitmapFactory.decodeResource(getResources(), imageAdapter.mThumbIds[position]);
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
					        // Compress image to lower quality scale 1 - 100
					        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
					        byte[] images = stream.toByteArray();
					        ParseFile file = new ParseFile(fileName+".png",images);
							file.saveInBackground();
							user.put("profilebg",file); //for profilebg
							user.saveInBackground();
						
						}*/
				    	loader.stopLoading();
//						Intent i = new Intent(getApplicationContext(), HomeActivity.class);
//						i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK&Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//						startActivity(i);
//						finish();
				    } else {
				      // ParseException
				    	switch(e.getCode()){
							case ParseException.EMAIL_TAKEN:
								showToast("Email alredy being used.",false);
								mEmail.setText(previousEmailID);
								ParseUser.getCurrentUser().setEmail(previousEmailID);
								loader.stopLoading();
							break;
						}
				    }
				  }
				});
			
		}

		@Override
		public void onDatePicked(int year, int month, int day) {
			String dob = String.format(Locale.getDefault(),"%02d/%02d/%d",day,month+1,year);
			mDob.setText(dob);
		}
		
		public void openSelectionDialog(){
			String[] selections = new String[]{"Gallery","Camera"};
			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.setTitle("Choose Method");
			dialog.setItems(selections, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if(which == 0){
						//Gallery						
						chooseImage();
					}else if(which == 1){
						//Camera
						takeImage();
					}
				}
			});
			
			dialog.setNegativeButton("cancel",new android.content.DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			
			dialog.show();
		}
		
		//To manage image
			public void chooseImage(){
				Toast.makeText(getApplicationContext(),"K",Toast.LENGTH_LONG).show();
				choiceType = ChooserType.REQUEST_PICK_PICTURE;
				chooserManager = new ImageChooserManager(this,choiceType,"Fup/temp",true);
				chooserManager.setImageChooserListener(this);
				try {
					loader.startPreloader(this,"Fetching Data..");
					filePath = chooserManager.choose();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			public void takeImage(){
				choiceType = ChooserType.REQUEST_CAPTURE_PICTURE;
				chooserManager = new ImageChooserManager(this,choiceType,"Fup/temp",true);
				chooserManager.setImageChooserListener(this);
				try {
					loader.startPreloader(this,"Fetching Data..");
					filePath = chooserManager.choose();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}


		@Override
		public void onClick(View v) {
			switch(v.getId()){
			case R.id.epUserProfileImage:
				openSelectionDialog();
			break;
			/*case R.id.epUpdate:
				//Activity
				if (!Util.isNetworkAvailable(EditProfileActivity.this)) {	
					
					DialogFactory.getBeaconStreamDialog1(EditProfileActivity.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
				} else 
				
				{
				validator.validate();
				}
			break;*/
			case R.id.epUpdate_bg_pic:
				Intent i=new Intent(EditProfileActivity.this,AndroidGridLayoutActivity.class);
				startActivity(i);
			//	openSelectionDialog();
			break;
			case R.id.eptDob:
				DialogFragment df = new DatePickerFragment();
				df.show(getFragmentManager(),"datePicker");
			break;
			}
		}
		
		public void showToast(String message,boolean top){
			Toast toast = Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG);
			if(top){
				toast.setGravity(Gravity.TOP, 0, 0);
			}
			toast.show();
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch(item.getItemId()){
			case android.R.id.home:
				Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
				i.putExtra("userpath", "home");
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				finish();
				break;
				
			case R.id.action_next:
			
					//Activity
					if (!Util.isNetworkAvailable(EditProfileActivity.this)) {	
						
						DialogFactory.getBeaconStreamDialog1(EditProfileActivity.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
					} else 
					
					{
					validator.validate();
					}
				break;
				
				//return true;	
			}
			return false;
		}
	
}
