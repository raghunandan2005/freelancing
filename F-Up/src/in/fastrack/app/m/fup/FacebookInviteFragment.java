package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.adapter.SocialAdapter;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.FriendsDb;
import in.fastrack.app.m.fup.model.SocialList;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.ParseFacebookUtils.Permissions;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class FacebookInviteFragment extends Fragment {

	private AppConfig _config;
	ListView lv = null;
	CheckConnectivity c = new CheckConnectivity();
	boolean isInternetActive = false;
	Boolean isInvite = false, loadable = false;
	ProgressBar mPhoneBookPreloader;
	TextView mPhoneBookMessage;
	EditText mSearch;
	static SocialAdapter mAdapter = null;
	Button mLinkFb;
	int FB_REQUEST_ID = 900;
	final ArrayList<SocialList> sList = new ArrayList<SocialList>();
	static ArrayList<String> usersconnected = new ArrayList<String>();

	static int abc = 1;
	static JSONArray facebookList = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.fragment_facebook, container,
				false);

		lv = (ListView) root.findViewById(R.id.socialList);
		_config = new AppConfig(getActivity().getBaseContext());
		String[] parseKeys = _config.getAppKeys("parse");
		Parse.initialize(getActivity().getBaseContext(), parseKeys[0],parseKeys[1]);
		ParseFacebookUtils.initialize(getString(R.string.app_id));
		final ParseUser currentUser = ParseUser.getCurrentUser();
		setHasOptionsMenu(true);
		isInvite = _config.getInviteMode();
		isInternetActive = c.checkAndToast(getActivity(),"No Internet Connection");

		mSearch = (EditText) root.findViewById(R.id.inputSearch);
		
		mSearch.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
				//mAdapter.getFilter().filter(cs.toString());
				String text = mSearch.getText().toString();
				mAdapter.afilter(text);
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}
		});

		facebookList = new JSONArray();

		mLinkFb = (Button) root.findViewById(R.id.bLinkFb);

		mPhoneBookPreloader = (ProgressBar) root.findViewById(R.id.pbPhoneBook);
		mPhoneBookMessage = (TextView) root
				.findViewById(R.id.tvPhoneBookMessage);

		mLinkFb.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				//Fragment
				if (!Util.isNetworkAvailable(getActivity())) {	
			
			     DialogFactory.getBeaconStreamDialog1(getActivity(), getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
		       } else 
		
		       {
				/*Toast.makeText(getActivity(), "FB Button Click",
						Toast.LENGTH_LONG).show();*/
				if (!ParseFacebookUtils.isLinked(currentUser)) {
					// Current User not linked, so link it
					/*Toast.makeText(getActivity(), "FB Not Linked",
							Toast.LENGTH_LONG).show();*/
					ParseFacebookUtils.link(currentUser, Arrays.asList("email",
							"user_birthday", Permissions.Friends.ABOUT_ME,
							Permissions.Friends.BIRTHDAY), getActivity(),
							FB_REQUEST_ID, new SaveCallback() {

								@Override
								public void done(ParseException e) {
									if (e == null) {
										// Toast.makeText(getActivity(),
										// "FB Connected",Toast.LENGTH_LONG).show();
										checkFbEnabled();
									} else {
										e.printStackTrace();
									}
								}
							});
				} else {
					/*Toast.makeText(getActivity(), "FB Already Linked",
							Toast.LENGTH_LONG).show();*/
				}
				
			}
			}
		});

		
		
		
		if (!Util.isNetworkAvailable(getActivity())) {	
			
		     DialogFactory.getBeaconStreamDialog1(getActivity(), getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
	       } else 
	
	       {
		checkFbEnabled();
		 String fqlQuery = "SELECT uid, name, pic_square, status FROM user WHERE uid IN " +
                 "(SELECT uid2 FROM friend WHERE uid1 = me() LIMIT 25)";
           Bundle params = new Bundle();
           params.putString("q", fqlQuery);
           Session session = Session.getActiveSession();

           Request request = new Request(session,
               "/fql",                         
               params,                         
               HttpMethod.GET,                 
               new Request.Callback(){         

                   @SuppressWarnings("deprecation")
                   public void onCompleted(Response response) {

                           GraphObject graphObject = response.getGraphObject();


                           if (graphObject != null)
                           {
                               if (graphObject.getProperty("data") != null)
                               {
                                   try {
                                       String arry = graphObject.getProperty("data").toString();

                                       JSONArray jsonNArray = new JSONArray(arry);

                                       for (int i = 0; i < jsonNArray.length(); i++) {

                                           JSONObject jsonObject = jsonNArray.getJSONObject(i);

                                           String name = jsonObject.getString("name");
                                           String uid = jsonObject.getString("uid");

                                           String pic_square = jsonObject.getString("pic_square");
                                           String status = jsonObject.getString("status");

                                           Log.i("Entry", "uid: " + uid + ", name: " + name + ", pic_square: " + pic_square + ", status: " + status);
                                       }

                                   } catch (JSONException e) {
                                       // TODO Auto-generated catch block
                                       e.printStackTrace();
                                   }                                       
                               }
                           }

                   }                  
           }); 
           Request.executeBatchAsync(request);  
	       }
		return root;
	}

	public final static void pullList() {
		if (mAdapter != null) {
			facebookList = mAdapter.showAllSelected();
		}
	}
	
	public final static ArrayList<String> pullArray(){
		if(mAdapter!=null){
			usersconnected = mAdapter.getArray();
			return usersconnected;
		}
		return null;
	}
	
	public final static ArrayList<String> pullObjects(){
		if(mAdapter !=null){
			ArrayList<String> connected = mAdapter.getObjects();
			return connected;
		}
		return null;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		/*Toast.makeText(getActivity(), "Acitivy Return", Toast.LENGTH_LONG)
				.show();*/
		if (resultCode != Activity.RESULT_OK) {
			Log.d("Activity", "Error occured during linking");
			return;
		}
		if (requestCode == FB_REQUEST_ID) {
			if (ParseFacebookUtils.getSession() != null) {
				Log.d("Facebook", "Linking");
				ParseFacebookUtils.finishAuthentication(requestCode,resultCode, data);
			}
		}
	}

	public void checkFbEnabled() {

		Session fbUser = ParseFacebookUtils.getSession();
		if (fbUser != null) {
			/*Toast.makeText(getActivity(), "SESSION found", Toast.LENGTH_LONG)
					.show();*/
			loadable = true;
			mPhoneBookPreloader.setVisibility(View.VISIBLE);
			mLinkFb.setVisibility(View.GONE);
			Boolean[] parms = {isInvite,isInternetActive};
			new LoadSocialAsync().execute(parms);
		} else {
			mPhoneBookPreloader.setVisibility(View.GONE);
			mLinkFb.setVisibility(View.VISIBLE);
			/*Toast.makeText(getActivity(), "NO SESSION", Toast.LENGTH_LONG)
					.show();*/
		}
	}

	public class LoadSocialAsync extends
			AsyncTask<Boolean, Void, ArrayList<SocialList>> {

		@Override
		protected ArrayList<SocialList> doInBackground(Boolean... params) {
			if (params[0]) {
				// Intent in Invite mode
				final String obj = null;
				Request request = Request.newMyFriendsRequest(
						ParseFacebookUtils.getSession(),
						new Request.GraphUserListCallback() {

							@Override
							public void onCompleted(List<GraphUser> users,
									Response response) {
								if (response.getError() == null) {
									// All good to go
									Log.i("FB INFO", "INISDE 1");
									SocialList list = null;
									for (int i = 0; i < users.size(); i++) {
										String imageUrl = "https://graph.facebook.com/"
												+ users.get(i).getId()
												+ "/picture/";
										list = new SocialList(users.get(i)
												.getId(),obj,users.get(i)
												.getName(), imageUrl,
												"facebook", false, isInvite);
										sList.add(list);
										Log.i("FBNAME", users.get(i).getName());
									}
								} else {
									Log.i("FB INFO", "ERROR");
								}
							}
						});
				request.executeAndWait();
			} else {
				//Get timestamp of the facebook friend sync (local db)
				/*SettingsDb settings = new SettingsDb(getActivity());
				String syncTimestamp = "0";
				long myid = settings.getMyId(ParseUser.getCurrentUser().getObjectId());
				if(myid != 0){
					SettingsDb data = settings.findById(SettingsDb.class,myid);
					syncTimestamp = data.getFriendsync();
					
				}*/
				if(params[1]){
					Log.i("FB DB STATE","Fetching from server");
				// Intent in Add More
					//String fqlQuery = "select uid, name,first_name, pic_square, is_app_user from user where is_app_user = 1 and uid in (select uid2 from friend where uid1 = me())";
					String fqlQuery = "SELECT name,first_name, email, uid,pic_square FROM user WHERE uid in (SELECT uid2 FROM friend WHERE uid1 = me())";
					Bundle ops = new Bundle();
					ops.putString("q", fqlQuery);
					Request req = new Request(ParseFacebookUtils.getSession(),
							"/fql", ops, HttpMethod.GET,
							new Request.Callback() {

								@Override
								public void onCompleted(Response response) {
									Log.i("FB OP",
											"Got results: "
													+ response.toString());
									if (response.getError() == null) {
										// No Error
										GraphObject gObject = response
												.getGraphObject();
										JSONObject jObject = gObject
												.getInnerJSONObject();
										try {
											ParseQuery<ParseUser> query = ParseUser
													.getQuery();
											JSONArray jArray = jObject
													.getJSONArray("data");
											ArrayList<String> fblist = new ArrayList<String>();
											//Arrays fblist = null;
											for (int i = 0; i < jArray.length(); i++) {
												JSONObject jObj2 = jArray
														.getJSONObject(i);
												//tempList.add(jObj2.getString("uid"));
												fblist.add(jObj2.getString("uid"));
												//query.whereEqualTo("fbid",jObj2.getString("uid"));
											}
											//String[] fblist = {"572545202","100001607820285","100000129769521"};
											/*fblist.add("572545202");
											fblist.add("100001607820285");
											fblist.add("100001872230438");
											fblist.add("100000129769521");*/
											if ((jArray != null)
													&& (isInternetActive)) {
												// Data found and Internet
												// available so do some query to
												// the server
												try {
													//query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
													query.whereContainedIn("fbid",fblist);
													query.whereNotEqualTo(
															"objectId",
															ParseUser
																	.getCurrentUser()
																	.getObjectId());
													
													List<ParseUser> objects = query
															.find();
													SocialList list = null;
													/*for(ParseUser p : objects){
														ParseObject a = p.getParseUser("objectId");
														Log.i("OBJECTS",a.toString());
													}*/
													
													Log.i("OBJECT SIZE",Integer.toString(objects.size()));
													
													for (int i = 0; i < objects
															.size(); i++) {
														//ParseUser a = objects.get(i).getObjectId();
														String fbImage = "https://graph.facebook.com/"
																+ objects
																		.get(i)
																		.get("fbid")
																+ "/picture/";
														String myObj = objects.get(i).getObjectId();
														String userobj = objects.get(i).getObjectId();
														Log.i("FRIEND OBJECT",myObj);
														list = new SocialList(
																objects.get(i)
																		.getObjectId()
																		.toString(),
																		myObj,
																objects.get(i)
																		.get("name")
																		.toString(),
																fbImage,
																"facebook",
																false, isInvite);
														sList.add(list);
														//Save to local database for later use
														FriendsDb fdbs = new FriendsDb(getActivity());
														//fdbs.deleteAll(FriendsDb.class);
														if(!fdbs.infoExists(ParseUser.getCurrentUser().getObjectId().toString(), objects.get(i).get("fbid").toString(),"facebook")){
															fdbs.setFbid(objects.get(i).get("fbid").toString());
															fdbs.setSource("facebook");
															fdbs.setName(objects.get(i).get("name").toString());
															fdbs.setUserid(ParseUser.getCurrentUser().getObjectId().toString());
															fdbs.setFriendid(objects.get(i).getObjectId().toString());
															Long currentTime = System.currentTimeMillis()/1000;
															fdbs.setLastupdate(currentTime.toString());
															fdbs.save();
														}else{
															Log.i("FB LOcal Data","DATA EXISTS");
														}
													}
												} catch (ParseException e) {
													e.printStackTrace();
												}
											}
										} catch (JSONException e) {
											e.printStackTrace();
										}
									}
								}
							});
					Request.executeBatchAndWait(req);
			}else{
				//No Internet look from db
				Log.i("FB DB STATE","No Internet Found");
				FriendsDb fdb = new FriendsDb(getActivity());
				List<FriendsDb> datares = fdb.getFriends(ParseUser.getCurrentUser().getObjectId().toString(),"facebook");
				if(datares!=null){
					/*for(FriendsDb a : datares){
						String imageUrl = "http://graph.facebook.com/"+a.fbid+"/picture/";
						SocialList list = new SocialList(a.friendid, a.name, imageUrl,"facebook", false, params[0]);
						sList.add(list);
					}*/
				}
			}
			}
			return sList;
		}

		@Override
		protected void onPostExecute(ArrayList<SocialList> result) {
			super.onPostExecute(result);
			mPhoneBookMessage.setVisibility(View.GONE);
			mPhoneBookPreloader.setVisibility(View.GONE);
			if (!result.isEmpty()) {
				mAdapter = new SocialAdapter(getActivity(),
						R.layout.phonebook_row, result);
				lv.setAdapter(mAdapter);
				mSearch.setVisibility(View.VISIBLE);
				lv.setVisibility(View.VISIBLE);
			} else {
				mPhoneBookMessage.setText("No Facebook Friends found.");
				mPhoneBookMessage.setVisibility(View.VISIBLE);
			}
		}

	}
}
