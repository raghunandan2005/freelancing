package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.adapter.CommentsAdapter;
import in.fastrack.app.m.fup.alarm.CreateNewAlarmActivity;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Comment;
import in.fastrack.app.m.fup.model.FeedEvent;
import in.fastrack.app.m.fup.model.FeedObject;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
//import android.widget.Toast;


import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SendCallback;

import de.greenrobot.event.EventBus;

public class FeedComments extends Activity implements OnClickListener {
	private EditText mCommentEdit;
	private ImageView mSubmit;
	private Bundle extras = null;
	private String alarmid = null;
	private ProgressBar mBar;
	private ArrayList<Comment> row = new ArrayList<Comment>();
	private ListView lv;
	private LinearLayout nodata;
	private ParseFile p;
	private String t = null;
	private String myImageUrl;
	private Comment comment = null;
	private int pos = 0;
	private int position = 0;
	private ParseObject parseOO;
	private CommentsAdapter mAdapter = null;
	private ParseObject alarmObject = null;
	private ProgressDialog pd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_comments_new);
		final ActionBar bar = getActionBar();
		bar.setTitle("Comments");
		bar.setDisplayShowHomeEnabled(true);
		bar.setHomeButtonEnabled(true);
		bar.setDisplayHomeAsUpEnabled(true);
		pd = new ProgressDialog(this);
		pd.setTitle("Please Wait...");
		pd.setMessage("Loading...");
		mCommentEdit = (EditText) findViewById(R.id.etComment);
		mSubmit = (ImageView) findViewById(R.id.bSubmitComment);
		TextView tv = new TextView(this);
		tv.setText("List Empty");
		lv = (ListView) findViewById(R.id.commentsList);
		lv.setVisibility(View.VISIBLE);
		lv.setEmptyView(tv);
		mAdapter = new CommentsAdapter(FeedComments.this, R.layout.comment_row,
				row);
		lv.setAdapter(mAdapter);
		nodata = (LinearLayout) findViewById(R.id.nofeed);
		mBar = (ProgressBar) findViewById(R.id.pLoader);
		mBar.setVisibility(View.GONE);
		mSubmit.setOnClickListener(this);
		extras = getIntent().getExtras();
		// Save the position
		/*
		 * SharedPreferences sp = getSharedPreferences("current_alarm_details",
		 * Activity.MODE_PRIVATE); SharedPreferences.Editor editor = sp.edit();
		 * editor.putString("alarm_position",
		 * Integer.toString(extras.getInt("position")));
		 * editor.putString("alarm_id", extras.getString("alarmid"));
		 * editor.commit();
		 */
		// Save the position

		// Activity
		if (!Util.isNetworkAvailable(FeedComments.this)) {
			DialogFactory.getBeaconStreamDialog1(FeedComments.this,
					getResources().getString(R.string.nw_error_str),
					Util.onClickListner, Util.retryClickListner).show();
		} else {
			if (extras != null) {
	
				position = extras.getInt("position");
				alarmid = extras.getString("alarmid");
				pullComments(alarmid, 0, "insert");
				ParseQuery<ParseObject> q = ParseQuery.getQuery("alarms");
				q.getInBackground(alarmid, new GetCallback<ParseObject>() {
					@Override
					public void done(ParseObject object, ParseException e) {
						alarmObject = object;
						if (e == null) {
							alarmObject = object;
							myImageUrl = "image";
							p = ParseUser.getCurrentUser().getParseFile(
									"profilethumbnail");
							if (p != null) {
								String url = p.getUrl();
								Log.i("FB IMAGE", url);
								myImageUrl = url;
							} else {
								if (ParseUser.getCurrentUser()
										.get("facebookid") != null) {
									t = ParseUser.getCurrentUser()
											.get("facebookid").toString();
									if ((t != null) || (t != "")) {
										myImageUrl = "https://graph.facebook.com/"
												+ ParseUser.getCurrentUser()
														.get("facebookid")
														.toString()
												+ "/picture/?type=square";
									}
								}
							}
						}
					}
				});
			}
		}
	}

	private void pullComments(String id, int page, String type) {

		pd.show();
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Comments");
		query.whereEqualTo("alarmid", id);
		query.whereNotEqualTo("deleteC", ParseUser.getCurrentUser()
				.getObjectId().toString());
		query.include("alarm");
		query.include("user");
		query.setLimit(15);
		query.setSkip(page);
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					try {
						for (ParseObject obj : objects) {
							Log.i("COMMENT ALARM ID",
									obj.getParseObject("alarm").getObjectId());
							Log.i("COMMENT TEXT", obj.get("comment").toString());
							Log.i("COMMENT USERNAME", obj.getParseUser("user")
									.get("fullname").toString());
							Log.i("COMMENT USERID", obj.getParseUser("user")
									.getObjectId());
							Log.i("COMMENT CREATED AT", obj.getCreatedAt()
									.toString());
							String imageUrl = "image";
							p = obj.getParseObject("user").getParseFile(
									"profilethumbnail");
							if (p != null) {
								String url = p.getUrl();
								Log.i("FB IMAGE", url);
								imageUrl = url;
							} else {
								if (obj.getParseObject("user")
										.get("facebookid") != null
										&& obj.getParseObject("user").get(
												"facebookid") != "") {
									t = obj.getParseObject("user")
											.get("facebookid").toString();
									if ((t != null) || (t != "")) {
										imageUrl = "https://graph.facebook.com/"
												+ obj.getParseObject("user")
														.get("facebookid")
														.toString()
												+ "/picture/?type=square";
									}
								}
							}
							comment = new Comment(obj.getParseObject("alarm")
									.get("creatorplain").toString(), obj
									.getParseObject("alarm").getObjectId(), obj
									.getObjectId(), obj.get("comment")
									.toString(), imageUrl, obj
									.getParseUser("user").get("fullname")
									.toString(), obj.getParseUser("user")
									.getObjectId(), obj.getCreatedAt()
									.toString(), true);
							row.add(comment);
							// mAdapter.add(comment);
						}
						// mAdapter.notifyDataSetChanged();
						if (!row.isEmpty()) {
							pd.dismiss();
							mBar.setVisibility(View.GONE);
							// mAdapter = new
							// CommentsAdapter(FeedComments.this,R.layout.comment_row,row);
							mAdapter.notifyDataSetChanged();
							// mAdapter = new
							// CommentsAdapter(FeedComments.this,R.layout.comment_row,row);
							// lv.setAdapter(mAdapter);
							// nodata.setVisibility(View.GONE);
							// lv.setVisibility(View.VISIBLE);
							// mBar.setVisibility(View.GONE);
						} else {
							pd.dismiss();
							Toast.makeText(getApplicationContext(),
									"No comments to diaply for this Feed",
									Toast.LENGTH_SHORT).show();
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bSubmitComment:
			if (!Util.isNetworkAvailable(FeedComments.this)) {
				DialogFactory.getBeaconStreamDialog1(FeedComments.this,
						getResources().getString(R.string.nw_error_str),
						Util.onClickListner, Util.retryClickListner).show();
			} else {
				if (!TextUtils.isEmpty(mCommentEdit.getText().toString())
						&& alarmObject != null) {
					try {
						String comments = mCommentEdit.getText().toString();
						final ParseObject a = new ParseObject("Comments");
						a.put("comment", comments);
						a.put("alarmid", alarmObject.getObjectId());
						a.put("alarm", alarmObject);
						a.put("user", ParseUser.getCurrentUser());
						// InputMethodManager inputManager =
						// (InputMethodManager)
						// getSystemService(Context.INPUT_METHOD_SERVICE);
						// inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
						a.saveInBackground(new SaveCallback() {
							@Override
							public void done(ParseException e) {
								// Update the comments count in the server
								ParseQuery<ParseObject> commentcount = ParseQuery
										.getQuery("alarms");
								commentcount.getInBackground(
										alarmObject.getObjectId(),
										new GetCallback<ParseObject>() {
											@Override
											public void done(
													ParseObject object,
													com.parse.ParseException e) {
												try {
													object.increment("comments");
													FeedObject object2 = new FeedObject(
															2,
															Integer.parseInt(object
																	.getString("requestcode")),
															true,
															position,
															object.getInt("comments"));
													EventBus.getDefault()
															.postSticky(
																	new FeedEvent(
																			object2));
													object.saveInBackground();
													// Earlier here
													// Notification code
													/*
													 * ParseObject notification
													 * = new
													 * ParseObject("Activity");
													 * notification
													 * .put("creator"
													 * ,ParseUser.getCurrentUser
													 * ());
													 * notification.put("followers"
													 * ,
													 * alarmObject.get("connected"
													 * ));
													 * notification.put("reference"
													 * ,alarmObject);
													 * notification
													 * .put("reference2",a);
													 * notification
													 * .put("notificationtype"
													 * ,2); notification.put(
													 * "notificationDone"
													 * ,false);
													 * notification.put(
													 * "readers",new
													 * JSONArray());
													 * notification
													 * .add("readers",
													 * ParseUser.
													 * getCurrentUser()
													 * .getObjectId());
													 * notification
													 * .saveInBackground
													 * ();//saveEventually();();
													 */
													// parse push implementation
													ArrayList<String> jarray = (ArrayList<String>) alarmObject
															.get("connected");
													for (int j = 0; j < jarray
															.size(); j++) {
														Log.d("friendsId.get(j).toString()",
																"--"
																		+ jarray.get(
																				j)
																				.toString()
																		+ "ParseUser.getCurrentUser().getObjectId().toString())"
																		+ "--"
																		+ ParseUser
																				.getCurrentUser()
																				.getObjectId()
																				.toString());
														if (!jarray
																.get(j)
																.toString()
																.equals(ParseUser
																		.getCurrentUser()
																		.getObjectId()
																		.toString())) {
															ParseQuery<ParseInstallation> pushQuery = ParseInstallation
																	.getQuery();
															ParsePush push = new ParsePush();
															// pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId().toString());
															// // not me
															pushQuery
																	.whereEqualTo(
																			"myuser",
																			jarray.get(
																					j)
																					.toString());
															//
															push.setQuery(pushQuery);
															JSONObject tempAlarmData = new JSONObject();

															if (a != null)
																tempAlarmData
																		.put("reference2",
																				a);
															if (alarmObject != null)
																tempAlarmData
																		.put("reference",
																				alarmObject
																						.getObjectId());
															Toast.makeText(
																	getApplicationContext(),
																	""
																			+ alarmObject
																					.getObjectId(),
																	Toast.LENGTH_SHORT)
																	.show();
															tempAlarmData
																	.put("notificationtype",
																			2);
															tempAlarmData
																	.put("alert",
																			"The alarm is commented by "
																					+ ParseUser
																							.getCurrentUser()
																							.getString(
																									"fullname"));
															tempAlarmData
																	.put("action",
																			"in.fastrack.app.m.fup.FeedComments");
															push.setData(tempAlarmData);
															push.sendInBackground(new SendCallback() {
																public void done(
																		com.parse.ParseException e) {
																	if (e == null) {
																		Toast.makeText(
																				FeedComments.this,
																				"Commented succesfully",
																				Toast.LENGTH_LONG)
																				.show();
																		// Modified
																		// here
																		// Notification
																		// code
																		ParseObject notification = new ParseObject(
																				"Activity");
																		notification
																				.put("creator",
																						ParseUser
																								.getCurrentUser());
																		notification
																				.put("followers",
																						alarmObject
																								.get("connected"));
																		notification
																				.put("reference",
																						alarmObject);
																		notification
																				.put("reference2",
																						a);
																		notification
																				.put("notificationtype",
																						2);
																		notification
																				.put("notificationDone",
																						false);
																		notification
																				.put("readers",
																						alarmObject
																								.get("connected"));// changed
																													// from
																													// `new
																													// JSONArray()`
																													// to
																													// alarmObject.get("connected")
																		notification
																				.add("readers",
																						ParseUser
																								.getCurrentUser()
																								.getObjectId());
																		notification
																				.saveInBackground();// saveEventually();();
																		Log.d("push",
																				"success!");
																	} else {
																		Log.d("push",
																				"failure");
																	}
																}
															});
														}
													}
												} catch (Exception e1) {
													// TODO Auto-generated catch
													// block
													e1.printStackTrace();
												}
											}
										});
							}
						});
						Log.d("alarmObject.getObjectId()",
								alarmObject.getObjectId());
						Log.d("comments", comments);
						Log.d("ParseUser.FULLNAME", ParseUser.getCurrentUser()
								.get("fullname").toString());
						Log.d("ParseUser.ObjectID", ParseUser.getCurrentUser()
								.getObjectId());
						comment = new Comment(alarmObject.get("creatorplain")
								.toString(), alarmObject.getObjectId(), "1",
								comments, myImageUrl, ParseUser
										.getCurrentUser().get("fullname")
										.toString(), ParseUser.getCurrentUser()
										.getObjectId(), "Just Now", true);
						// if(lv.getAdapter() == null){
						// //List view is not set
						row.add(comment);
						// mAdapter = new
						// CommentsAdapter(FeedComments.this,R.layout.comment_row,row);
						// lv.setAdapter(mAdapter);
						// lv.setVisibility(View.VISIBLE);
						// mAdapter.notifyDataSetChanged();
						// }else{
						// row.add(comment);
						// mAdapter.notifyDataSetChanged();
						// }
						// nodata.setVisibility(View.GONE);
						mAdapter.notifyDataSetChanged();
						mCommentEdit.setText("");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// Toast.makeText(getApplicationContext(), "OnBackPressed",
		// Toast.LENGTH_SHORT).show();
		/*
		 * ParseQuery<ParseObject> query = ParseQuery.getQuery("alarms");
		 * query.whereEqualTo("objectId", alarmid);
		 * query.getFirstInBackground(new GetCallback<ParseObject>() {
		 * SharedPreferences sp = getSharedPreferences("current_alarm_details",
		 * Activity.MODE_PRIVATE); SharedPreferences.Editor editor = sp.edit();
		 * public void done(ParseObject object, ParseException e) { if (object
		 * != null) { //editor.putString("alarm_comment_count",
		 * object.getString("comments")); editor.putInt("alarm_comment_count",
		 * object.getInt("comments")); Log.d("Counter",
		 * String.valueOf(object.getInt("comments"))); } else {
		 * 
		 * } editor.commit(); } });
		 */
		// Toast.makeText(getApplicationContext(), "OnBackPressed",
		// Toast.LENGTH_SHORT).show();
		Bundle b = new Bundle();
		Intent exit = new Intent();
		b.putInt("position", pos);
		exit.putExtras(b);
		setResult(RESULT_OK, exit);
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:

			NavUtils.navigateUpFromSameTask(this);

			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// switch(item.getItemId()){
	// case android.R.id.home:
	// finish();
	// return true;
	// }
	// return false;
	// }

}
