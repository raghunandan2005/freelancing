package in.fastrack.app.m.fup;


import in.fastrack.app.m.fup.adapter.FeedAdapter;
import in.fastrack.app.m.fup.alarm.CreateNewAlarmActivity;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.FeedEvent;
import in.fastrack.app.m.fup.model.NewsFeed;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.LayerDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.internal.Utility;
import com.fourmob.datetimepicker.Utils;
import com.nhaarman.listviewanimations.swinginadapters.prepared.ScaleInAnimationAdapter;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import de.greenrobot.event.EventBus;
import eu.erikw.PullToRefreshListView;
import eu.erikw.PullToRefreshListView.OnRefreshListener;
import eu.erikw.PullToRefreshListView.OnUpdateListListener;

public class FeedFragment extends Fragment implements OnClickListener,OnScrollListener {

	private static AppConfig _config;
	private ProgressBar pb;
	protected static ProgressDialog proDialog;
	// ListView lv = null;
	private PullToRefreshListView lv = null;
	private LayerDrawable icon ;

	static CheckConnectivity ic = new CheckConnectivity();
	static boolean isInternetActive = false;
	ArrayList<NewsFeed> nFeed;
	FeedAdapter mAdapter = null;
	ParseFile p;
	String t = null;
	int pageNumber = 0;
	Boolean firstTime = true;
	Boolean isLoading = false;
	static MediaPlayer mMediaPlayer = new MediaPlayer();
	static float count = 100 * .01f;
	String notificationid = "", chk;
	private View menu_item_1;

	private int mInterval = 30000; // 30 Seconds
	private Handler mHandler;

	int commentCounter = 0;

	String alarm_position = null;
	String alarm_id = null;

	boolean emptyfeed = true;

	int ALARM_CREATE = 200;
	LinearLayout ll, nofeed, action;

	Button accept, bInvite, bAlarm, bView;
	private Context mContext;
	//private ProgressDialog pd;

	Bundle a = null;
	boolean refresh = false;
	
	private TextView tv;
	private int counter;

	public void checkActive() {
		ParseUser currentUser = ParseUser.getCurrentUser();
		if (currentUser == null) {
			// User not logged in
			Intent i = new Intent(getActivity(), SplashActivity.class);
			startActivity(i);
		}
	}

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.news_feed, container, false);
	    
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Activity");
		query.whereNotEqualTo("creator", ParseUser.getCurrentUser());
	        query.countInBackground(new CountCallback() {
	            public void done(int count, ParseException e) {
	                if (e == null) {
	                   counter =count;
	           		
	        		updateNotificationsBadge(counter);
	                } else {
	                    e.printStackTrace();
	                }
	            }
	        });

	       
	        

		ll = (LinearLayout) view.findViewById(R.id.llLoad);
		lv = (PullToRefreshListView) view.findViewById(R.id.feedList);
		tv = new TextView(getActivity());
		tv.setText("No Alarm Feeds");
	    lv.setEmptyView(tv);
	    
		checkActive();
		mContext = getActivity();
		a = getArguments();
		fetchFeed(pageNumber, "alarm", false, "");
		mHandler = new Handler();


		pb = (ProgressBar) view.findViewById(R.id.pLoader);
		pb.setVisibility(View.VISIBLE);


		_config = new AppConfig(getActivity());
		TextView tv = new TextView(getActivity());
		tv.setText("Empty List");
		lv.setEmptyView(tv);
		nFeed = new ArrayList<NewsFeed>();
		mAdapter = new FeedAdapter(getActivity(), R.layout.feed_row, nFeed);
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),"fonts/RobotoCondensed-Light.ttf");
		Spanned text = Html.fromHtml("View <b>Demo</b>");
		Spanned text1 = Html.fromHtml("Invite <b>Friends</b>");
		Spanned text2 = Html.fromHtml("Create <b>Alarm</b>");

		nofeed = (LinearLayout) view.findViewById(R.id.nofeed);
		action = (LinearLayout) view.findViewById(R.id.llaction);
		accept = (Button) view.findViewById(R.id.bNotificationAccept2);
		bInvite = (Button) view.findViewById(R.id.bInvite);
		bInvite.setText(text1);
		bView = (Button) view.findViewById(R.id.bView);
		bView.setText(text);

		bAlarm = (Button) view.findViewById(R.id.bAlarm);
		bAlarm.setText(text2);

		lv.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// Fragment
				if (!Util.isNetworkAvailable(getActivity())) {

					DialogFactory.getBeaconStreamDialog1(getActivity(),
							getResources().getString(R.string.nw_error_str),
							Util.onClickListner, Util.retryClickListner).show();
				} else {

					refresh = true;
					Log.e("Match", "Pulltorefresh");
					fetchFeed(pageNumber, "alarm", false, "");
					nFeed.removeAll(nFeed);
				}
			}
		});

		lv.setOnUpdateListListener(new OnUpdateListListener() {

			@Override
			public void onUpdate() {
				if (!Util.isNetworkAvailable(getActivity())) {

					DialogFactory.getBeaconStreamDialog1(getActivity(),
							getResources().getString(R.string.nw_error_str),
							Util.onClickListner, Util.retryClickListner).show();
				} else

				{
					refresh = false;
					}

			}
		});
		mMediaPlayer
				.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

					@Override
					public void onCompletion(MediaPlayer mp) {

						if (lv.getAdapter() != null) {
							mAdapter.reset(false);
							mMediaPlayer.reset();
						}

					}
				});

		bInvite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				Intent a = new Intent(getActivity().getApplicationContext(),
						InvitePeople.class);
				Bundle bu = new Bundle();
				bu.putBoolean("go_back", true);
				bu.putBoolean("isInvite", true);
				a.putExtras(bu);
				startActivity(a);

			}
		});
		bAlarm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				Intent i = new Intent(getActivity().getApplicationContext(),
						CreateNewAlarmActivity.class);
				Bundle b = new Bundle();
				b.putBoolean("go_back", true);
				i.putExtras(b);
				startActivityForResult(i, ALARM_CREATE);

			}
		});
		bView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				Intent i = new Intent(getActivity().getApplicationContext(),
						MainActivity.class);
				startActivity(i);

			}
		});

		accept.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// Fragment
				if (!Util.isNetworkAvailable(getActivity())) {

					DialogFactory.getBeaconStreamDialog1(getActivity(),
							getResources().getString(R.string.nw_error_str),
							Util.onClickListner, Util.retryClickListner).show();
				} else

				{

					ParseQuery<ParseObject> not = ParseQuery.getQuery("alarms");
					not.getInBackground(a.getString("alarmid"),
							new GetCallback<ParseObject>() {
								@Override
								public void done(ParseObject object,
										com.parse.ParseException e) {
									object.addAllUnique("acceptedpeople",
											Arrays.asList(ParseUser
													.getCurrentUser()
													.getObjectId()));
									object.addAllUnique("notificationactions",
											Arrays.asList(ParseUser
													.getCurrentUser()
													.getObjectId()));
									object.saveInBackground();// saveEventually();();
									action.setVisibility(View.GONE);
									// Set alarm
									Intent intent = new Intent(getActivity(),
											 AlaramActivityNew.class);
									Bundle tempData = new Bundle();
									tempData.putString("alarm_category",
											object.getString("category"));
									tempData.putString("alarm_title",
											object.getString("title"));
									tempData.putBoolean("recorded",
											object.getBoolean("recorded"));
									tempData.putInt("alarmrequestcode", Integer
											.parseInt(object
													.getString("requestcode")));
									tempData.putString("alarmpath",
											object.getString("media"));
									tempData.putString("alarmfriends",
											object.getString("connectedlist"));
									intent.putExtras(tempData);
									PendingIntent pi = PendingIntent.getActivity(
											getActivity(),
											Integer.parseInt(object
													.getString("requestcode")),
											intent,
											PendingIntent.FLAG_CANCEL_CURRENT);
									AlarmManager am = (AlarmManager) getActivity()
											.getSystemService(
													Context.ALARM_SERVICE);
									am.set(AlarmManager.RTC_WAKEUP,
											object.getLong("alarmtimemilli"),
											pi);

									// Notification
									// New custom notification code
									ParseObject notification = new ParseObject(
											"Activity");
									notification.put("creator",
											ParseUser.getCurrentUser());
									notification.put("followers", Arrays
											.asList(object
													.getString("creatorplain")));
									notification.put("reference", object);
									notification.put("notificationtype", 7);
									notification.put("notificationDone", false);
									notification
											.put("readers", new JSONArray());
									notification.add("readers", ParseUser
											.getCurrentUser().getObjectId());
									notification.saveInBackground();// saveEventually();();

									// Download audio if required
									final String alarmPath = object
											.getString("media");
									Log.i("File path", alarmPath);
									if (object.getBoolean("recorded")) {
										ParseFile p = object
												.getParseFile("audioclip");
										if (p != null) {
											Log.i("File", "A");
											p.getDataInBackground(new GetDataCallback() {

												@Override
												public void done(
														byte[] data,
														com.parse.ParseException e) {
													if (e == null) {
														Log.i("File", "B");
														try {
															Log.i("File", "C");
															File newFile = new File(
																	Environment
																			.getExternalStorageDirectory()
																			+ _config
																					.getRecordAudioPath(),
																	alarmPath);
															OutputStream os = new FileOutputStream(
																	newFile);
															os.write(data);
															os.close();
														} catch (FileNotFoundException e1) {
															Log.i("ALARM FILE LOADED",
																	"FALSE");
															e1.printStackTrace();
														} catch (IOException e1) {
															e1.printStackTrace();
														}
													} else {
														Log.i("File",
																e.getMessage());
													}
												}
											});
										}
									}

								}
							});
				}

			}
		});

		setHasOptionsMenu(true);

		return view;
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    inflater.inflate(R.menu.action_home, menu);
	   
	    MenuItem item = menu.findItem(R.id.action_notification);
        LayerDrawable icon = (LayerDrawable) item.getIcon();
        BadgeUtils.setBadgeCount(getActivity(), icon, counter);
        
	    

 
	}
	 private void updateNotificationsBadge(int count) {
	        counter = count;
	        if(getActivity()!=null)
	        {
	        	getActivity().invalidateOptionsMenu(); 
	        }
	    }

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			return true;
		case R.id.action_notification:
			Intent notification = new Intent(getActivity(),NotificationActivityNew.class);
			getActivity().startActivity(notification);

			return true;
		case R.id.action_createalarm:
			Intent createalaram = new Intent(getActivity(),CreateNewAlarmActivity.class);
			getActivity().startActivity(createalaram);

			return true;
		case R.id.action_settings:
			Intent settings = new Intent(getActivity(),SettingsActivity.class);
			getActivity().startActivity(settings);

			return true;

		}
		return super.onOptionsItemSelected(item);
	}

	public void fetchFeed(int page, String action, Boolean isUpdate,
			String alarmid) {
		ParseUser currentUser = ParseUser.getCurrentUser();
		if (currentUser == null) {
			// User not logged in
			Intent i = new Intent(getActivity(), SplashActivity.class);
			startActivity(i);
		} else {
			if (action == "alarm") {

				// Fragment
				if (!Util.isNetworkAvailable(getActivity())) {

					DialogFactory.getBeaconStreamDialog1(getActivity(),
							getResources().getString(R.string.nw_error_str),
							Util.onClickListner, Util.retryClickListner).show();
				} else

				{

					isLoading = true;
					final Boolean isOld = isUpdate;
					ParseQuery<ParseObject> query = ParseQuery
							.getQuery("alarms");
					query.whereEqualTo("connected", ParseUser.getCurrentUser()
							.getObjectId().toString());
					query.whereEqualTo("status", true);
					query.whereNotEqualTo("isPrivateAlaramCheck", true);
					query.whereNotEqualTo("Hideit", ParseUser.getCurrentUser()
							.getObjectId().toString());
					query.whereNotEqualTo("flagged", ParseUser.getCurrentUser()
							.getObjectId().toString());
					if (alarmid != "") {
						query.whereEqualTo("objectId", alarmid);
					}
					query.include("creator");
					query.orderByDescending("createdAt");
					query.findInBackground(new FindCallback<ParseObject>() {
						@Override
						public void done(List<ParseObject> objects,
								ParseException e) {
							if (e == null) {
								try {
									NewsFeed list = null;
									for (ParseObject obj : objects) {
										String imageUrl = "image";
										p = obj.getParseObject("creator")
												.getParseFile(
														"profilethumbnail");
										if (p != null) {
											String url = p.getUrl();
											// Log.i("FB IMAGE",url);
											imageUrl = url;
										} else if (obj
												.getParseObject("creator").get(
														"facebookid") != null) {
											t = obj.getParseObject("creator")
													.get("facebookid")
													.toString();
											if ((t != null) || (t != "")) {
												imageUrl = "https://graph.facebook.com/"
														+ obj.getParseObject(
																"creator")
																.get("facebookid")
																.toString()
														+ "/picture/?type=square";
											}
										}
										boolean userLiked = false;
										if (obj.getList("likes") != null) {
											// Log.i("USER LIKES","YES");
											List<String> arr = obj
													.getList("likes");
											if (arr.contains(ParseUser
													.getCurrentUser()
													.getObjectId())) {
												// Log.i("USER LIKES","DONE");
												userLiked = true;
											}
										}
										int like_count = obj.getList("likes")
												.size();
										int comment_count = obj
												.getInt("comments");

										String alarm_repeat_type = "";
										if (obj.getString("alarm_repeat_type") != null)
											alarm_repeat_type = obj
													.getString("alarm_repeat_type");
										else
											alarm_repeat_type = "Once";

										list = new NewsFeed(
												obj.get("title").toString(),
												obj.get("category").toString(),
												obj.getParseObject("creator")
														.getString("fullname"),
												imageUrl,
												obj.getCreatedAt().toString(),
												obj.get("alarmTime").toString(),
												obj.get("connectedlist")
														.toString(),
												obj.get("connected").toString(),
												Integer.parseInt(obj
														.getString("requestcode")),
												like_count,
												comment_count,
												1,
												obj.getLong("alarmtimemilli"),
												userLiked,
												obj.getObjectId(),
												obj.get("media").toString(),
												obj.getBoolean("recorded"),
												false,
												obj.getParseObject("creator")
														.getObjectId(),
												obj.getBoolean("selfalarm"),
												obj.getString("media"),
												alarm_repeat_type,
												obj.getBoolean("isPrivateAlaramCheck"));
										nFeed.add(list);
										emptyfeed = false;
										if (isOld) {
											mAdapter.addNewFeed(list);
											mAdapter.notifyDataSetChanged();// added
																			// recently
										}
									}
									//pd.dismiss();
									pb.setVisibility(View.GONE);
									if (!nFeed.isEmpty()) {
										try {
											//pd.dismiss();
											nofeed.setVisibility(View.GONE);
											// Toast.makeText(getActivity(),"Not Empty.",Toast.LENGTH_LONG).show();
											mAdapter = new FeedAdapter(
													getActivity(),
													R.layout.feed_row, nFeed);
											ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(
													mAdapter);
											scaleInAnimationAdapter
													.setAbsListView(lv);

											lv.setAdapter(scaleInAnimationAdapter);
											mAdapter.notifyDataSetChanged();// added
																			// recently
											lv.setVisibility(View.VISIBLE);
											// used here
											if (refresh) {
												lv.onRefreshComplete();
											}

										} catch (Exception e2) {
											// TODO: handle exception
										}

									} else {
										// Toast.makeText(getActivity(),"Welcome, but there is nothing new.",Toast.LENGTH_LONG).show();
										emptyfeed = true;
										nofeed.setVisibility(View.VISIBLE);
										lv.setVisibility(View.GONE);
									}
									// if(!isOld){
									// if(!nFeed.isEmpty()){
									// try {
									// mAdapter = new
									// FeedAdapter(getActivity(),R.layout.feed_row,nFeed);
									// ScaleInAnimationAdapter
									// scaleInAnimationAdapter = new
									// ScaleInAnimationAdapter(mAdapter);
									// scaleInAnimationAdapter.setAbsListView(lv);
									//
									// lv.setAdapter(scaleInAnimationAdapter);
									// mAdapter.notifyDataSetChanged();//added
									// recently
									// lv.setVisibility(View.VISIBLE);
									// //used here
									// if (refresh) {
									// lv.onRefreshComplete();
									// }
									//
									// } catch (Exception e2) {
									// // TODO: handle exception
									// }
									//
									// }else{
									// Toast.makeText(getActivity(),"Welcome, but there is nothing new.",Toast.LENGTH_LONG).show();
									// emptyfeed = true;
									// nofeed.setVisibility(View.VISIBLE);
									// }
									// }
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							} else {
								// Toast.makeText(getActivity(),"No Feed found",
								// Toast.LENGTH_LONG).show();
								nofeed.setVisibility(View.VISIBLE);
							}
							isLoading = false;

							ll.setVisibility(View.GONE);
							pb.setVisibility(View.GONE);
						}
				
					});

				}
			}
		}

	}

	@Override
	public void onClick(View v) {

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// Log.i("SCROL>>>>>>>","SCROLLING");
		// Fragment
		if (!Util.isNetworkAvailable(getActivity())) {

			DialogFactory.getBeaconStreamDialog1(getActivity(),
					getResources().getString(R.string.nw_error_str),
					Util.onClickListner, Util.retryClickListner).show();
		} else

		{
			if (a != null) {
				try {
					if (firstTime) {
						fetchFeed(pageNumber, "alarm", false,
								a.getString("alarmid"));
						firstTime = false;

						// Check if active or not
						ParseQuery<ParseObject> not = ParseQuery
								.getQuery("alarms");
						// Toast.makeText(getActivity(),a.getString("alarmid"),Toast.LENGTH_LONG).show();
						not.getInBackground(a.getString("alarmid"),
								new GetCallback<ParseObject>() {

									@Override
									public void done(ParseObject object,
											ParseException e) {
										if (e == null) {
											// Toast.makeText(getActivity(),object.getString("title"),Toast.LENGTH_LONG).show();
											List<String> ap = object
													.getList("notificationactions");
											if (ap != null) {
												if (ap.contains(ParseUser
														.getCurrentUser()
														.getObjectId())) {
													action.setVisibility(View.GONE);
												} else {
													action.setVisibility(View.GONE);
																					
												}
											} else {
												action.setVisibility(View.GONE);
																				
											}
										}
									}

								});

					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				final int lastItem = firstVisibleItem + visibleItemCount;
				if (lastItem >= totalItemCount && !isLoading) {
					if (firstTime) {
						fetchFeed(pageNumber, "alarm", false, "");
						firstTime = false;
					} else {
						fetchFeed(pageNumber, "alarm", true, "");
					}
					pageNumber = pageNumber + 5;
				}
			}

		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}

	/* Media data */
	public static void startMedia(final Context c, int trackid, String track,
			boolean state, int aid) {
		try {
			// Toast.makeText(c.getApplicationContext(), "H",
			// Toast.LENGTH_LONG).show();
			mMediaPlayer.reset();
			if (state) {
				String fileName = "android.resource://" + c.getPackageName()
						+ "/" + trackid;
				mMediaPlayer.setDataSource(c.getApplicationContext(),
						Uri.parse(fileName));
				final AudioManager am = (AudioManager) c
						.getSystemService(Context.AUDIO_SERVICE);
				if (am.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
					mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
					float count = 100 * .01f;
					mMediaPlayer.setVolume(count, count);
					mMediaPlayer.prepare();
					mMediaPlayer.start();
				} else {
					mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
					float count = 100 * .01f;
					mMediaPlayer.setVolume(count, count);
					mMediaPlayer.prepare();
					mMediaPlayer.start();
				}
			} else {
				File mFile = new File(Environment.getExternalStorageDirectory()
						+ _config.getRecordAudioPath(), track);
				if (mFile.exists()) {
					// File Found
					mMediaPlayer.setDataSource(Environment
							.getExternalStorageDirectory()
							+ _config.getRecordAudioPath() + track);
					final AudioManager am = (AudioManager) c
							.getSystemService(Context.AUDIO_SERVICE);
					if (am.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
						mMediaPlayer
								.setAudioStreamType(AudioManager.STREAM_ALARM);
						float count = 100 * .01f;
						mMediaPlayer.setVolume(count, count);
						mMediaPlayer.prepare();
						mMediaPlayer.start();
					} else {
						mMediaPlayer
								.setAudioStreamType(AudioManager.STREAM_ALARM);
						float count = 100 * .01f;
						mMediaPlayer.setVolume(count, count);
						mMediaPlayer.prepare();
						mMediaPlayer.start();
					}
				} else {
					// Download audio
					isInternetActive = ic.check(c);
					if (isInternetActive) {
						startLoading("Downloading Audio", c);
						// ParseFile
						ParseQuery<ParseObject> query = ParseQuery
								.getQuery("alarms");
						query.whereEqualTo("requestcode", Integer.toString(aid));
						query.findInBackground(new FindCallback<ParseObject>() {

							@Override
							public void done(List<ParseObject> objects,
									ParseException e) {
								if (e == null) {
									try {
										for (int i = 0; i < objects.size(); i++) {
											final String alarmPath = objects
													.get(i).getString("media");
											if (objects.get(i).getBoolean(
													"recorded")) {
												// Find audio and download
												// silently
												Log.i("ALARM FILE", "A");
												ParseFile p = objects.get(i)
														.getParseFile(
																"audioclip");
												if (p != null) {
													Log.i("ALARM FILE", "B");
													p.getDataInBackground(new GetDataCallback() {

														@Override
														public void done(
																byte[] data,
																com.parse.ParseException e) {
															if (e == null) {
																try {
																	Log.i("ALARM FILE",
																			"C");
																	File newFile = new File(
																			Environment
																					.getExternalStorageDirectory()
																					+ _config
																							.getRecordAudioPath(),
																			alarmPath);
																	OutputStream os = new FileOutputStream(
																			newFile);
																	os.write(data);
																	os.close();
																	stopLoading();
																	mMediaPlayer
																			.setDataSource(Environment
																					.getExternalStorageDirectory()
																					+ _config
																							.getRecordAudioPath()
																					+ alarmPath);
																	final AudioManager am = (AudioManager) c
																			.getSystemService(Context.AUDIO_SERVICE);
																	if (am.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
																		mMediaPlayer
																				.setAudioStreamType(AudioManager.STREAM_ALARM);
																		float count = 100 * .01f;
																		mMediaPlayer
																				.setVolume(
																						count,
																						count);
																		mMediaPlayer
																				.prepare();
																		mMediaPlayer
																				.start();
																	} else {
																		mMediaPlayer
																				.setAudioStreamType(AudioManager.STREAM_ALARM);
																		float count = 100 * .01f;
																		mMediaPlayer
																				.setVolume(
																						count,
																						count);
																		mMediaPlayer
																				.prepare();
																		mMediaPlayer
																				.start();
																	}
																} catch (FileNotFoundException e1) {
																	// Log.i("ALARM FILE LOADED","FALSE");
																	Log.i("ALARM FILE",
																			"D");
																	e1.printStackTrace();
																} catch (IOException e1) {
																	Log.i("ALARM FILE",
																			"E");
																	e1.printStackTrace();
																}
															} else {
																stopLoading();
																Log.i("ALARM FILE",
																		"F");
																Log.i("File Error",
																		e.getMessage());
																Toast.makeText(
																		c,
																		"File Not Found.",
																		Toast.LENGTH_LONG)
																		.show();
															}
														}
													});
												}
											}
										}
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								} else {
									stopLoading();
									Toast.makeText(c, "Unable to Download",
											Toast.LENGTH_LONG).show();
								}
							}

						});
					} else {
						Toast.makeText(c, "No Internet Connectivity",
								Toast.LENGTH_LONG).show();
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void playMedia(Context context, String audioSource,
			int trackResource, int trackType, int alarmid) {
		if (trackType == 1) {
			startMedia(context, trackResource, audioSource, true, alarmid);
		} else {
			startMedia(context, trackResource, audioSource, false, alarmid);
		}
	}

	public static void resetMedia() {
		mMediaPlayer.reset();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (lv.getAdapter() != null) {
			mAdapter.reset(false);
			mMediaPlayer.reset();
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		EventBus.getDefault().removeStickyEvent(FeedEvent.class);
		if (lv.getAdapter() != null) {
			mAdapter.reset(false);
			mMediaPlayer.reset();
		}
	}

	protected static void setLoadingMessage(String msg) {
		if (proDialog != null) {
			proDialog.setMessage(msg);
		}
	}

	protected static void startLoading(String msg, Context c) {
		proDialog = new ProgressDialog(c);
		proDialog.setMessage(msg);
		proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		proDialog.setCancelable(false);
		proDialog.show();
	}

	protected static void stopLoading() {
		proDialog.dismiss();
		proDialog = null;
	}

	

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			nofeed.setVisibility(View.GONE);
			ll.setVisibility(View.VISIBLE);

			mAdapter.clear();
			fetchFeed(pageNumber, "alarm", false, "");
		} else {
		}
	}

}
