package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.ContactsSyncService;
import in.fastrack.app.m.fup.lib.NotifyingReceiver;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParsePush;
import com.parse.PushService;
import com.parse.SaveCallback;

public class FirstTime extends Application {
	
	private static final int PERIOD = 60000; // 900000 = 15 minutes
	private static final int INITIAL_DELAY = 60000; // 1 minutes
	
	public FirstTime(){
		
	}
	
	private AppConfig _config;
	
	@Override
	public void onCreate() {
		super.onCreate();
		_config = new AppConfig(this);
		String[] parseKeys = _config.getAppKeys("parse");
		//Parse.enableLocalDatastore(this);  
		Parse.initialize(this, parseKeys[0], parseKeys[1]);
	
		ParseFacebookUtils.initialize(getString(R.string.app_id));
	//commented by nabasree	
	/*	ParsePush.subscribeInBackground("", new SaveCallback() {
			  @Override
			  public void done(ParseException e) {
			    if (e != null) {
			      Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
			    } else {
			      Log.e("com.parse.push", "failed to subscribe for push", e);
			    }
			  }
			});*/
		
		
		
		
		
		//PushService.setDefaultPushCallback(this, HomeActivity.class);
		//ParseAnalytics.trackAppOpened(getIntent());
		
		 /*Intent myAlarm = new Intent(getApplicationContext(), NotifyingReceiver.class);
         myAlarm.putExtra("project_id", "some extra is always good"); //Put Extra if needed
         PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm, PendingIntent.FLAG_UPDATE_CURRENT);
         AlarmManager alarms = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
         alarms.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+INITIAL_DELAY, PERIOD, recurringAlarm);
         Log.d("FirstTime","Service");*/
		
		Log.d("When are you running", "running");
		/*Intent i= new Intent(getApplicationContext(), ContactsSyncService.class);
		i.putExtra("KEY1", "Value to be used by the service");
		getApplicationContext().startService(i); */
		
		Intent myAlarm = new Intent(getApplicationContext(), NotifyingReceiver.class);
        myAlarm.putExtra("project_id", "some extra is always good"); //Put Extra if needed
        PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarms = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarms.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+INITIAL_DELAY, PERIOD, recurringAlarm); 
	}

}
