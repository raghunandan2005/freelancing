package in.fastrack.app.m.fup;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentPage4 extends Fragment {

	private TextView create_alarm_tv,let_roll_tv,textView1,textView2,textView3;
	private ImageView big_image, heading_image,imageView1,imageView2,imageView3;
	private Animation fade_anim1, left_anim,fade_anim_image1,fade_anim_txt1,fade_anim_image2,fade_anim_txt2,fade_anim_image3,fade_anim_txt3;
	private TranslateAnimation animation;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState); 
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),"fonts/RobotoCondensed-Light.ttf");  
		//textfield.setTypeface(tf,Typeface.BOLD);
		 Spanned text = Html.fromHtml("News <b>Feed</b>"); 
		 Spanned text1 = Html.fromHtml("Select <b>Who</b>"); 
		 Spanned text2 = Html.fromHtml("<b>When</b> and");
		 Spanned text3 = Html.fromHtml("the <b>tune</b> you want them to hear"); 
	

		//imageView1= (ImageView) getView().findViewById(R.id.imageView1);
		//imageView2= (ImageView) getView().findViewById(R.id.imageView2);
		//imageView3= (ImageView) getView().findViewById(R.id.imageView3);
		create_alarm_tv = (TextView) getView().findViewById(R.id.create_alarm_textView);
		create_alarm_tv.setText(text);
		create_alarm_tv.setTypeface(tf);
		
		let_roll_tv = (TextView) getView().findViewById(R.id.let_roll_textView);
		let_roll_tv.setTypeface(tf);
		/*textView1 = (TextView) getView().findViewById(R.id.textView1);
		textView1.setText(text1);
		textView1.setTypeface(tf);
		textView2 = (TextView) getView().findViewById(R.id.textView2);
		textView2.setText(text2);
		textView2.setTypeface(tf);
		textView3 = (TextView) getView().findViewById(R.id.textView3);
		textView3.setText(text3);
		textView3.setTypeface(tf);*/
		

//		fade_anim1 = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fadein);
//		fade_anim1.setFillAfter(true);
//		
//		fade_anim_image1 = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fadein);
//		fade_anim_image1.setFillAfter(true);
//		
//		fade_anim_txt1 = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fadein);
//		fade_anim_txt1.setFillAfter(true);
//
//		fade_anim_image2 = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fadein);
//		fade_anim_image2.setFillAfter(true);
//		
//		fade_anim_txt2 = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fadein);
//		fade_anim_txt2.setFillAfter(true);
//		
//		fade_anim_image3 = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fadein);
//		fade_anim_image3.setFillAfter(true);
//		
//		fade_anim_txt3 = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fadein);
//		fade_anim_txt3.setFillAfter(true);
//
//		//left_anim = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.slideleft);
//		//left_anim.setFillAfter(true);
//        //Toast.makeText(getActivity(), ""+create_alarm_tv.getX(), Toast.LENGTH_LONG).show();
//		
//		Handler handler = new Handler();
//		handler.postDelayed(new Runnable() {
//			public void run() {
//				//starting big image animation
//				big_image.setScaleX(1.2f);
//				big_image.setScaleY(1.2f);
//				animate();
//				int centerXOnText=create_alarm_tv.getWidth()/2;
//			    int centerYOnText=create_alarm_tv.getHeight()/2;
//
//			    int centerXOfTextOnScreen=create_alarm_tv.getLeft()+centerXOnText-(create_alarm_tv.getWidth()/2)-60;
//			    int centerYOfTextOnScreen=create_alarm_tv.getTop()+centerYOnText-(create_alarm_tv.getHeight()/2)-60;
//			    animation = new TranslateAnimation(-1000,centerXOfTextOnScreen, 0, 0);
//				animation.setDuration(500);
//				animation.setFillAfter(true);
//				//Toast.makeText(getActivity(), ""+create_alarm_tv.getX(), Toast.LENGTH_LONG).show();
//				animation.setAnimationListener(new AnimationListener() {
//
//					@Override
//					public void onAnimationEnd(Animation arg0) {
//						// TODO Auto-generated method stub
//						
//						let_roll_tv.setVisibility(View.VISIBLE);
//						let_roll_tv.startAnimation(fade_anim1);
//					}
//
//					@Override
//					public void onAnimationRepeat(Animation arg0) {
//						// TODO Auto-generated method stub
//
//					}
//
//					@Override
//					public void onAnimationStart(Animation arg0) {
//						// TODO Auto-generated method stub
//
//					}
//
//				});
//			}
//		}, 1000);
//		
//	
//
//		fade_anim1.setAnimationListener(new AnimationListener() {
//
//			@Override
//			public void onAnimationEnd(Animation arg0) {
//				// TODO Auto-generated method stub
//				
//				float scalingFactor = 1.1f; // scale down to half the size
//				let_roll_tv.setScaleX(scalingFactor);
//				let_roll_tv.setScaleY(scalingFactor);
//				Handler handler = new Handler();
//				handler.postDelayed(new Runnable() {
//					public void run() {
//
//						float scalingFactor = 1f; // scale down to half the size
//						let_roll_tv.setScaleX(scalingFactor);
//						let_roll_tv.setScaleY(scalingFactor);
//						
//						
//						//imageView2.setVisibility(View.VISIBLE);
//						//imageView2.startAnimation(fade_anim_image1);
//						
//						//textView1.setVisibility(View.VISIBLE);						
//						//textView1.startAnimation(fade_anim_txt1);
//					}
//				}, 150);
//
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void onAnimationStart(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//		});
//		
//		
//		fade_anim_image1.setAnimationListener(new AnimationListener() {
//
//			@Override
//			public void onAnimationEnd(Animation arg0) {
//				// TODO Auto-generated method stub
//				
//				float scalingFactor = 1.1f; // scale down to half the size
//				imageView2.setScaleX(scalingFactor);
//				imageView2.setScaleY(scalingFactor);
//				Handler handler = new Handler();
//				handler.postDelayed(new Runnable() {
//					public void run() {
//
//						float scalingFactor = 1f; // scale down to half the size
//						imageView2.setScaleX(scalingFactor);
//						imageView2.setScaleY(scalingFactor);
//						textView1.setVisibility(View.VISIBLE);
//						textView1.startAnimation(fade_anim_txt1);
//					}
//				}, 150);
//
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void onAnimationStart(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//		});
//		fade_anim_txt1.setAnimationListener(new AnimationListener() {
//
//			@Override
//			public void onAnimationEnd(Animation arg0) {
//				// TODO Auto-generated method stub
//				
//				float scalingFactor = 1.1f; // scale down to half the size
//				textView1.setScaleX(scalingFactor);
//				textView1.setScaleY(scalingFactor);
//				Handler handler = new Handler();
//				handler.postDelayed(new Runnable() {
//					public void run() {
//
//						float scalingFactor = 1f; // scale down to half the size
//						textView1.setScaleX(scalingFactor);
//						textView1.setScaleY(scalingFactor);
//						imageView3.setVisibility(View.VISIBLE);
//						imageView3.startAnimation(fade_anim_image2);
//					}
//				}, 150);
//
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void onAnimationStart(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//		});
//		
//		fade_anim_image2.setAnimationListener(new AnimationListener() {
//
//			@Override
//			public void onAnimationEnd(Animation arg0) {
//				// TODO Auto-generated method stub
//				
//				float scalingFactor = 1.1f; // scale down to half the size
//				imageView3.setScaleX(scalingFactor);
//				imageView3.setScaleY(scalingFactor);
//				Handler handler = new Handler();
//				handler.postDelayed(new Runnable() {
//					public void run() {
//
//						float scalingFactor = 1f; // scale down to half the size
//						imageView3.setScaleX(scalingFactor);
//						imageView3.setScaleY(scalingFactor);
//						textView2.setVisibility(View.VISIBLE);
//						textView2.startAnimation(fade_anim_txt2);
//					}
//				}, 150);
//
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void onAnimationStart(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//		});
//		fade_anim_txt2.setAnimationListener(new AnimationListener() {
//
//			@Override
//			public void onAnimationEnd(Animation arg0) {
//				// TODO Auto-generated method stub
//				
//				float scalingFactor = 1.1f; // scale down to half the size
//				textView2.setScaleX(scalingFactor);
//				textView2.setScaleY(scalingFactor);
//				Handler handler = new Handler();
//				handler.postDelayed(new Runnable() {
//					public void run() {
//
//						float scalingFactor = 1f; // scale down to half the size
//						textView2.setScaleX(scalingFactor);
//						textView2.setScaleY(scalingFactor);
//						imageView1.setVisibility(View.VISIBLE);
//						imageView1.startAnimation(fade_anim_image3);
//					}
//				}, 150);
//
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void onAnimationStart(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//		});
//		fade_anim_image3.setAnimationListener(new AnimationListener() {
//
//			@Override
//			public void onAnimationEnd(Animation arg0) {
//				// TODO Auto-generated method stub
//				
//				float scalingFactor = 1.1f; // scale down to half the size
//				imageView1.setScaleX(scalingFactor);
//				imageView1.setScaleY(scalingFactor);
//				Handler handler = new Handler();
//				handler.postDelayed(new Runnable() {
//					public void run() {
//
//						float scalingFactor = 1f; // scale down to half the size
//						imageView1.setScaleX(scalingFactor);
//						imageView1.setScaleY(scalingFactor);
//						textView3.setVisibility(View.VISIBLE);
//						textView3.startAnimation(fade_anim_txt3);
//					}
//				}, 150);
//
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void onAnimationStart(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//		});
//		
//		fade_anim_txt3.setAnimationListener(new AnimationListener() {
//
//			@Override
//			public void onAnimationEnd(Animation arg0) {
//				// TODO Auto-generated method stub
//				
//				float scalingFactor = 1.1f; // scale down to half the size
//				textView3.setScaleX(scalingFactor);
//				textView3.setScaleY(scalingFactor);
//				Handler handler = new Handler();
//				handler.postDelayed(new Runnable() {
//					public void run() {
//
//						float scalingFactor = 1f; // scale down to half the size
//						textView3.setScaleX(scalingFactor);
//						textView3.setScaleY(scalingFactor);
//					
//					}
//				}, 150);
//
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void onAnimationStart(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//		});
//		left_anim.setAnimationListener(new AnimationListener() {
//
//			@Override
//			public void onAnimationEnd(Animation arg0) {
//				//starting let_roll_tv animation
//				let_roll_tv.setVisibility(View.VISIBLE);
//				let_roll_tv.startAnimation(fade_anim1);
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void onAnimationStart(Animation arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//		});

	}

//	public void animate() {
//		// This is to get the screen dimensions
//		final Point p = new Point();
//		getActivity().getWindowManager().getDefaultDisplay().getSize(p);
//
//		// First, define the starting values
//
//		final int startingWidth = big_image.getWidth();
//		final int startingHeight = big_image.getHeight();
//		// final int startingXValue = (int) mImageView.getX();
//
//		// Next, define the final values
//
//		// For this example, the final width will be half of screen-width
//		final int finalWidth = 50;// startingWidth / 2;// p.x / 2;
//
//		// Just an arbitrary final value for height. You will have to decide
//		// what the final height should be
//		final int finalHeight = 50;// (tv.getHeight() - tv.getPaddingTop());//
//									// p.y -
//									// mImageView.getTop()
//									// -
//									// 300;
//
//		// final `X` placement of the imageview
//		// int finalXValue = p.x / 2 - finalWidth / 2;
//
//		// ValueAnimator to generate changes in width
//		final ValueAnimator vaW = ValueAnimator.ofInt(startingWidth, finalWidth);
//
//		vaW.addUpdateListener(new AnimatorUpdateListener() {
//
//			@Override
//			public void onAnimationUpdate(ValueAnimator animation) {
//				// Get animated width value update
//				int newWidth = (Integer) vaW.getAnimatedValue();
//
//				// Get and update LayoutParams from imageview
//				RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) big_image
//						.getLayoutParams();
//				lp.width = newWidth;
//				big_image.setLayoutParams(lp);
//			}
//		});
//
//		// ValueAnimator to generate changes in height
//		final ValueAnimator vaH = ValueAnimator.ofInt(startingHeight,
//				finalHeight);
//
//		vaW.addUpdateListener(new AnimatorUpdateListener() {
//
//			@Override
//			public void onAnimationUpdate(ValueAnimator animation) {
//				// Get animated height value update
//				int newHeight = (Integer) vaH.getAnimatedValue();
//
//				// Get and update LayoutParams from imageview
//				RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) big_image
//						.getLayoutParams();
//				lp.height = newHeight;
//				big_image.setLayoutParams(lp);
//			}
//		});
//
//		// Used to provide translate animation
//		// ObjectAnimator oa = ObjectAnimator.ofFloat( mImageView, "X",
//		// startingXValue, finalXValue);
//
//		Log.i("....", "" + big_image.getTop());
//		Log.i("....", "" + big_image.getBottom());
//		ObjectAnimator y = ObjectAnimator.ofFloat(big_image, "Y", big_image.getTop(), create_alarm_tv.getY());
//
//		ObjectAnimator x = ObjectAnimator.ofFloat(big_image, "X", p.x / 2 - (big_image.getWidth() / 2), create_alarm_tv.getBottom() - 10);
//		// To play these 3 animators together
//		AnimatorSet as = new AnimatorSet();
//		as.playTogether(vaH, vaW);
//		as.setDuration(500);
//		as.addListener(new AnimatorListener() {
//
//			@Override
//			public void onAnimationCancel(Animator arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void onAnimationEnd(Animator arg0) {
//				// TODO Auto-generated method stub
//				//starting heading image animation
//				big_image.setVisibility(View.GONE);
//				heading_image.setVisibility(View.VISIBLE);
//				heading_image.startAnimation(animation);
//				
//			}
//
//			@Override
//			public void onAnimationRepeat(Animator arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void onAnimationStart(Animator arg0) {
//				// TODO Auto-generated method stub
//
//			}
//
//		});
//
//		as.start();
//	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment4, container, false);

		return v;
	}

	public ObjectAnimator tada(View view, final String check) {
		ObjectAnimator anim = tada(view, 1f);

		anim.addListener(new AnimatorListener() {

			@Override
			public void onAnimationCancel(Animator arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animator arg0) {
				// TODO Auto-generated method stub

				big_image.setVisibility(View.INVISIBLE);
				animation.setDuration(2000); // duartion in ms
				animation.setFillAfter(true);
				heading_image.startAnimation(animation);
				
				animation.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationEnd(Animation arg0) {
						// TODO Auto-generated method stub

						//tv2.startAnimation(anim1);

						
					}

					@Override
					public void onAnimationRepeat(Animation arg0) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationStart(Animation arg0) {
						// TODO Auto-generated method stub

					}

				});

			}

			@Override
			public void onAnimationRepeat(Animator arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationStart(Animator arg0) {
				// TODO Auto-generated method stub

			}

		});

		return anim;
	}

	public static ObjectAnimator tada(View view, float shakeFactor) {

		PropertyValuesHolder pvhScaleX = PropertyValuesHolder.ofKeyframe(
				View.SCALE_X, Keyframe.ofFloat(0f, 1f),
				Keyframe.ofFloat(.1f, .9f), Keyframe.ofFloat(.2f, .9f),
				Keyframe.ofFloat(.3f, 1.1f), Keyframe.ofFloat(.4f, 1.1f),
				Keyframe.ofFloat(.5f, 1.1f), Keyframe.ofFloat(.6f, 1.1f),
				Keyframe.ofFloat(.7f, 1.1f), Keyframe.ofFloat(.8f, 1.1f),
				Keyframe.ofFloat(.9f, 1.1f), Keyframe.ofFloat(1f, 1f));

		PropertyValuesHolder pvhScaleY = PropertyValuesHolder.ofKeyframe(
				View.SCALE_Y, Keyframe.ofFloat(0f, 1f),
				Keyframe.ofFloat(.1f, .9f), Keyframe.ofFloat(.2f, .9f),
				Keyframe.ofFloat(.3f, 1.1f), Keyframe.ofFloat(.4f, 1.1f),
				Keyframe.ofFloat(.5f, 1.1f), Keyframe.ofFloat(.6f, 1.1f),
				Keyframe.ofFloat(.7f, 1.1f), Keyframe.ofFloat(.8f, 1.1f),
				Keyframe.ofFloat(.9f, 1.1f), Keyframe.ofFloat(1f, 1f));

		return ObjectAnimator
				.ofPropertyValuesHolder(view, pvhScaleX, pvhScaleY)
				.setDuration(1000);
	}

	public static ObjectAnimator nope(View view) {
		int delta = 2;// view.getResources().getDimensionPixelOffset(R.dimen.spacing_medium);

		PropertyValuesHolder pvhTranslateX = PropertyValuesHolder.ofKeyframe(
				View.TRANSLATION_X, Keyframe.ofFloat(0f, 0),
				Keyframe.ofFloat(.10f, -delta), Keyframe.ofFloat(.26f, delta),
				Keyframe.ofFloat(.42f, -delta), Keyframe.ofFloat(.58f, delta),
				Keyframe.ofFloat(.74f, -delta), Keyframe.ofFloat(.90f, delta),
				Keyframe.ofFloat(1f, 0f));

		return ObjectAnimator.ofPropertyValuesHolder(view, pvhTranslateX)
				.setDuration(500);
	}

}