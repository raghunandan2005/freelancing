package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.adapter.FriendsAdapter;
import in.fastrack.app.m.fup.adapter.SurpriseAdapter;
import in.fastrack.app.m.fup.adapter.SurpriseAdapter.SurpriseListener;
import in.fastrack.app.m.fup.adapter.SurpriseAdapter;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.NewsFeed;
import in.fastrack.app.m.fup.model.People;
import in.fastrack.app.m.fup.model.UserFriendRelation;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class FriendAddFragmentNew extends Fragment implements SurpriseListener{

	private AppConfig _config;
	private Preloader preloader;
	ParseUser currentUser;
	int FB_REQUEST_ID = 900;
	static SurpriseAdapter mAdapter = null;
	ProgressBar mPbar;
	EditText mSearch;
	ListView lv = null;
	private JSONArray friends = new JSONArray();
	private JSONArray friendsid = new JSONArray();
	ArrayList<Friend> finalList = new ArrayList<Friend>();
	final ArrayList<People> sList = new ArrayList<People>();
	String friendsListString = null;
	NewsFeed alarmDetails;
	UserFriendRelation ufr = null;
	ArrayList<String> selectedFriendsid = new ArrayList<String>(); 
	public ParseUser getTheUser(String userid){
		ParseUser user = null;
		ParseQuery<ParseUser> query1 = ParseUser.getQuery();
		query1.whereEqualTo("objectId", userid);
		List<ParseUser> objects;
		try {
			objects = query1.find();
           // The query was successful.
       		for(ParseUser obj : objects) {
       			user = obj;
       			break;
       		}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.friends_layout, container, false);
		if(getActivity().getIntent().getStringExtra("friends") != null) { 
			try {
				friends = new JSONArray(getActivity().getIntent().getStringExtra("friends"));
				 for (int i = 0; i < friends.length(); i++) {
				        JSONObject explrObject = friends.getJSONObject(i);
				        selectedFriendsid.add(explrObject.getString("userid"));
				    }
				 System.out.println(selectedFriendsid.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		preloader = new Preloader();
		preloader.startPreloader(getActivity(),"Loading, Please wait...");
		
		
		configParse();
		
		mSearch = (EditText) root.findViewById(R.id.inputSearch);
		mPbar = (ProgressBar) root.findViewById(R.id.pbPhoneBook);
		lv = (ListView) root.findViewById(R.id.socialList);
///////////////////
TextView tv = new TextView(getActivity());
tv.setText(" No Phone Contacts");

mAdapter = new SurpriseAdapter(getActivity().getBaseContext(),R.layout.people_item_new,finalList,this, friends,ufr);;
lv.setEmptyView(tv);
lv.setAdapter(mAdapter);
/////////////////////
//		mSearch.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
//				String text = mSearch.getText().toString();
//				//mAdapter.search(text);
//				//ArrayList<Friend> searchedList = mAdapter.search(text);
//				//mAdapter.clear();
//				//mAdapter.addAll(searchedList);
//				 mAdapter.getFilter().filter(cs);
//                 mAdapter.notifyDataSetChanged();
//			}
//			
//			@Override
//			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
//					int arg3) {
//				
//			}
//			
//			@Override
//			public void afterTextChanged(Editable arg0) {
//				
//			}
//		});
		//Fragment
		if (!Util.isNetworkAvailable(getActivity())) {	
	      try {
			preloader.stopLoading();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	     DialogFactory.getBeaconStreamDialog1(getActivity(), getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
       } else 

       {
		final Boolean[] parms = {false};
		new LoadSocialAsyncFriend().execute(parms);
		  
       }
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
		query.whereEqualTo("user", ParseUser.getCurrentUser());
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if(e==null) {
				if(objects.size() > 0) {
					for(ParseObject obj : objects) { 
						ufr = new UserFriendRelation();						
						ufr.setConnectedFriends(obj.getJSONArray("connectedF"));						
						ufr.setConnectedFriendsS(obj.getJSONArray("connectedSF"));
						ufr.setUserId(ParseUser.getCurrentUser().getObjectId().toString());
						break;
					}
				}
			}
			}
		});
		
		
		return root;
	}

	
	
	public void configParse(){
		_config = new AppConfig(getActivity().getBaseContext());
		currentUser = ParseUser.getCurrentUser();
	}
	
	public class LoadSocialAsyncFriend extends AsyncTask<Boolean, Void, ArrayList<Friend>> {
		ArrayList<Friend> friendList = new ArrayList<Friend>();
		@Override
		protected ArrayList<Friend> doInBackground(Boolean... params) {
			
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
			query.whereEqualTo("user", ParseUser.getCurrentUser());
			try {
				List<ParseObject> objects = query.find();
				if(objects.size() > 0) {
					for(ParseObject obj : objects){
						JSONArray friendsList = obj.getJSONArray("connectedF");
						if(friendsList != null)  {
							for(int j = 0; j < friendsList.length(); j++) {
								try {
									String userid = friendsList.getString(j);
									if(userid != null) {
										ParseUser user = getTheUser(userid);
										
										Friend f = new Friend();
								        f.setName(user.getString("fullname"));
								        f.setUserId(userid);
								        String imageUrl = "image";
								        String t = null;
								        ParseFile p = user.getParseFile("profilethumbnail");
										if(p!=null){
											String url = p.getUrl();
											imageUrl = url;
										}else if(user.get("facebookid") != null){
											t = user.get("facebookid").toString();
											if((t!=null)||(t!="")){
												imageUrl = "https://graph.facebook.com/"
														+ user.get("facebookid").toString()
														+ "/picture/?type=square";
											}
										}
								        f.setUserImage(imageUrl);
								        if (selectedFriendsid.contains(userid)) {
								            f.setUserSelected(true);
								        } 
								        friendList.add(f);
									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
		
			return friendList;
		}

		@Override
		protected void onPostExecute(ArrayList<Friend> result) {
			super.onPostExecute(result);
		  // updateList(result);
			finalList.addAll(result);
			updateList(finalList);
			//finalList
			Log.i("list Friend", finalList.toString());
			//final Boolean[] parms = {false};
			//new LoadSocialAsyncSurpriseFriend().execute(parms);
			
		}
		
	}
	
	
	public class LoadSocialAsyncSurpriseFriend extends AsyncTask<Boolean, Void, ArrayList<Friend>> {
		ArrayList<Friend> friendList = new ArrayList<Friend>();
		@Override
		protected ArrayList<Friend> doInBackground(Boolean... params) {
			
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
			query.whereEqualTo("user", ParseUser.getCurrentUser());
			try {
				List<ParseObject> objects = query.find();
				if(objects.size() > 0) {
					for(ParseObject obj : objects){
						JSONArray friendsList = obj.getJSONArray("connectedSF");
						if(friendsList != null)  {
							for(int j = 0; j < friendsList.length(); j++) {
								try {
									String userid = friendsList.getString(j);
									if(userid != null) {
										ParseUser user = getTheUser(userid);
										
										Friend f = new Friend();
								        f.setName(user.getString("fullname"));
								        f.setUserId(userid);
								        String imageUrl = "image";
								        String t = null;
								        ParseFile p = user.getParseFile("profilethumbnail");
										if(p!=null){
											String url = p.getUrl();
											imageUrl = url;
										}else if(user.get("facebookid") != null){
											t = user.get("facebookid").toString();
											if((t!=null)||(t!="")){
												imageUrl = "https://graph.facebook.com/"
														+ user.get("facebookid").toString()
														+ "/picture/?type=square";
											}
										}
								        f.setUserImage(imageUrl);
								        if (selectedFriendsid.contains(userid)) {
								            f.setUserSelected(true);
								        } 
								        friendList.add(f);
									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}

			return friendList;
		}

		@Override
		protected void onPostExecute(ArrayList<Friend> result) {
			super.onPostExecute(result);
			//updateList(result);
			finalList.addAll(result);
			Log.i("list Surprise Friend", finalList.toString());
			updateList(finalList);
		}
		
	}
	
	
	
	
	
	
	public void updateList(ArrayList<Friend> res){
		if(res != null) {
			if(ufr != null && ufr.getUserId().equals(ParseUser.getCurrentUser().getObjectId().toString())) {
		
				JSONArray connectedFriend = ufr.getConnectedFriends();
				if(connectedFriend != null) {
					for(Friend fir : res) {
						for(int rf = 0; rf < connectedFriend.length(); rf++){
							try {
								if(fir.getUserId().equals(connectedFriend.get(rf))){
									
								//	fir.setStatus("Friend");
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
				}
				JSONArray connectedFriendS = ufr.getConnectedFriendsS();
				if(connectedFriendS != null) {
					for(Friend fir : res) {
						for(int rf = 0; rf < connectedFriendS.length(); rf++){
							try {
								if(fir.getUserId().equals(connectedFriendS.get(rf))){
									
								//  fir.setStatus("Friend.");
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
				}
				
			//	ArrayList<String> connectedListData = new ArrayList<String>();     
				ArrayList<String> newListData = new ArrayList<String>(); 
			//	ArrayList<String> connectedSupriseListData = new ArrayList<String>();     
				JSONArray jArray = (JSONArray)connectedFriendS; 
				
				JSONArray jConArray = (JSONArray)connectedFriend; 
				for(Friend fir : res) {
				
				
				if (jConArray != null) { 
				   for (int i=0;i<jConArray.length();i++){ 
				    try {
				    	
				    	if (jArray != null) { 
							   for (int j=0;j<jArray.length();j++){ 
							    try {
							    	if(jArray.get(j).toString().equals(jConArray.get(i).toString())){
							    		//newListData.add(jArray.get(j).toString());
							    		
							    		Log.i("jArray.get(j).toString()", "jArray.get(j).toString()"+jArray.get(j).toString());
							    		
							    		
							    		if(fir.getUserId().equals(jArray.get(j).toString())){
											
											Log.i("u r  inside"," u r inside");
											fir.setStatus("Surprise");
											}
							    		
							    	}
							    	//connectedSupriseListData.add(jArray.get(i).toString());
								} catch (JSONException e) {
									e.printStackTrace();
								}
							   } 
							}
				   }
				   
				    	
				    	//connectedListData.add(jConArray.get(i).toString());
					 catch (Exception e) {
						e.printStackTrace();
					}
				   } 
				}
				
				}
				System.out.println(newListData.toString());
				//System.out.println(connectedSupriseListData.toString());
				
				
				
				
				try {
					mAdapter = new SurpriseAdapter(getActivity().getBaseContext(),R.layout.people_item_new,res,this, friends,ufr);
					lv.setAdapter(mAdapter);
					lv.setVisibility(View.VISIBLE);
					mSearch.setVisibility(View.VISIBLE);
					preloader.stopLoading();
					Log.i("list", res.toString());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					preloader.stopLoading();
					e.printStackTrace();
				}
				
				
			}
		}
		
		
		
		try {
			preloader.stopLoading();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}	
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.action_next:
			if(friends != null){
				//friends found
				Intent exit = new Intent();
        		Bundle data = new Bundle();
        		data.putString("friendsid",friendsid.toString());
        		data.putString("friends",friends.toString());
        		exit.putExtras(data);
        		//setResult(RESULT_OK,exit);
        		//finish();
			}
		return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void getSurpriseAdded(JSONArray list, JSONArray friendsId) {
		Log.d("MyApp","Friends: "+list.toString());
		friends = list;
		friendsid = friendsId;
		someEventListener.someEvent(list, friendsId);
	}
	
	/*************INTERFACE********************/
	 public interface onSomeEventListener {
		 public void someEvent(JSONArray list, JSONArray friendsId);
	 }

	  onSomeEventListener someEventListener;

	  @Override
	  public void onAttach(Activity activity) {
	    super.onAttach(activity);
	        try {
	          someEventListener = (onSomeEventListener) activity;
	        } catch (ClassCastException e) {
	            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
	        }
	  }
	/*************INTERFACE********************/
	  
	  
	
}
