package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.FriendAddFragmentNew.onSomeEventListener;
import in.fastrack.app.m.fup.GroupsAddFragment.onSomeEventGroupListener;
import in.fastrack.app.m.fup.SurpriseFriendAddFragment.onSomeSurpriseEventListener;
import in.fastrack.app.m.fup.adapter.FriendGroupPagerAdapter;
import in.fastrack.app.m.fup.config.AppConfig;

import org.json.JSONArray;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.parse.ParseUser;

public class FriendGroupActivity extends Activity implements TabListener, onSomeEventListener, onSomeEventGroupListener{ //,onSomeSurpriseEventListener
	
	//public FragmentComm
	private ViewPager viewPager;
	private FriendGroupPagerAdapter mAdapter;
	private ActionBar bar;
	public Boolean isInvite = true;
	public AppConfig _config;
	Bundle extras = null;
	int FB_REQUEST_ID = 900;
	CheckConnectivity c = new CheckConnectivity();
	boolean isIntAv = false;
	private JSONArray friends = new JSONArray();
	private JSONArray friendsid = new JSONArray();
	private JSONArray groups = new JSONArray();
	private JSONArray groupsid = new JSONArray();
	private JSONArray surprisefriends = new JSONArray();
	private JSONArray surprisefriendsid = new JSONArray();
	//Custom Tabs
	private String[] tabs = {"Friends","Groups"}; //,"Surprise Friends"
	
	@Override
	protected void onResume() {
		super.onResume();
		checkActive();
	}
	
	public void checkActive(){
		ParseUser currentUser = ParseUser.getCurrentUser();
		if(currentUser == null) {
			//User not logged in
			Intent reset = new Intent(getApplicationContext(),SplashActivity.class);
			startActivity(reset);
		}
	}

	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
		
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction arg1) {
		viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
		
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.invite_friends);
		_config = new AppConfig(getApplicationContext());
		viewPager = (ViewPager) findViewById(R.id.pager);
		bar = getActionBar();
		mAdapter = new FriendGroupPagerAdapter(getFragmentManager());
		viewPager.setAdapter(mAdapter);
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		isIntAv = c.check(getApplicationContext());
		
		extras = getIntent().getExtras();
		
		if(extras != null){
			if(extras.getBoolean("go_back")){
				bar.setDisplayShowHomeEnabled(true);
				bar.setHomeButtonEnabled(true);
				bar.setDisplayHomeAsUpEnabled(true);
			}
			isInvite = extras.getBoolean("isInvite");
		}else{
			//TO-DO send back the user to first create alarm intent
		}
		
		bar.setTitle("Friends/Groups");
		
		for(String tab_name : tabs){
			bar.addTab(bar.newTab().setText(tab_name).setTabListener(this));
		}
		
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				bar.setSelectedNavigationItem(position);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
			}
		});
		
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.action_menu_select, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			Intent i = new Intent(getApplicationContext(), HomeActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			finish();
			break;
		case R.id.action_next:
			if(friends != null){
				//friends found
				System.out.println(friends.toString());
				System.out.println("custom interface");
				System.out.println(groups.toString());
				Intent exit = new Intent();
        		Bundle data = new Bundle();
        		data.putString("friendsid",friendsid.toString());
        		data.putString("friends",friends.toString());
        		data.putString("groups", groups.toString());
        	//	data.putString("surprisefriendsid",surprisefriendsid.toString());
        	//	data.putString("surprisefriends",surprisefriends.toString());
        		exit.putExtras(data);
        		setResult(RESULT_OK,exit);
        		finish();
			}
			return true;
		}
		return true;
	}
	
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//Toast.makeText(this, "Acitivy Return",Toast.LENGTH_LONG).show();
		if (resultCode != Activity.RESULT_OK) {
	        Log.d("Activity", "Error occured during linking");
	        return;
	    }
	}

	@Override
	public void someEvent(JSONArray list, JSONArray friendsId) {
		friends = list;
		friendsid = friendsId;
	}
	
	public void someEventGroup(JSONArray list, JSONArray groupsId) {
		groups = list;
		groupsid = groupsId;
	}
	/*public void someSurpriseEventGroup(JSONArray list, JSONArray surpriseid) {
		surprisefriends = list;
		surprisefriendsid = surpriseid;
	}
*/
	

	
	
}
