//
//package in.fastrack.app.m.fup;
//
//import in.fastrack.app.m.fup.adapter.FriendsAdapter;
//import in.fastrack.app.m.fup.adapter.FriendsAdapter.FriendListener;
//import in.fastrack.app.m.fup.config.AppConfig;
//import in.fastrack.app.m.fup.lib.DialogFactory;
//import in.fastrack.app.m.fup.lib.Preloader;
//import in.fastrack.app.m.fup.lib.Util;
//import in.fastrack.app.m.fup.model.Friend;
//import in.fastrack.app.m.fup.model.NewsFeed;
//import in.fastrack.app.m.fup.model.People;
//import in.fastrack.app.m.fup.model.UserFriendRelation;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import android.app.Activity;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.View;
//import android.widget.EditText;
//import android.widget.ListView;
//import android.widget.ProgressBar;
//
//import com.facebook.HttpMethod;
//import com.facebook.Request;
//import com.facebook.Response;
//import com.facebook.model.GraphObject;
//import com.parse.FindCallback;
//import com.parse.GetCallback;
//import com.parse.ParseException;
//import com.parse.ParseFacebookUtils;
//import com.parse.ParseObject;
//import com.parse.ParseQuery;
//import com.parse.ParseUser;
//
//public class Friends extends Activity implements FriendListener{
//	
//	private AppConfig _config;
//	ArrayList<Friend> fbList = new ArrayList<Friend>();
//	ArrayList<Friend> contactsList = new ArrayList<Friend>();
//	ArrayList<Friend> finalList = new ArrayList<Friend>();
//	
//	private Preloader preloader;
//	ParseUser currentUser;
//	static FriendsAdapter mAdapter = null;
//	ProgressBar mPbar;
//	EditText mSearch;
//	ListView lv = null;
//	private JSONArray friends = new JSONArray();
//	private JSONArray friendsid = new JSONArray();
//	
//	final ArrayList<People> sList = new ArrayList<People>();
//	String friendsListString = null;
//	NewsFeed alarmDetails;
//	ArrayList<String> selectedFriendsid = new ArrayList<String>(); 
//	
//	UserFriendRelation ufr = null;
//	
//	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.friends);
//		_config = new AppConfig(getApplicationContext());	
//		configParse();
//		
//		mSearch = (EditText) findViewById(R.id.inputSearch);
//		mPbar = (ProgressBar) findViewById(R.id.pbPhoneBook);
//		lv = (ListView) findViewById(R.id.socialList);
//		
//		mSearch.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
////				String text = mSearch.getText().toString();
////				ArrayList<Friend> searchedList = mAdapter.search(text);
////				mAdapter.clear();
////				mAdapter.addAll(searchedList);
//			}
//			
//			@Override
//			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
//					int arg3) {
//				
//			}
//			
//			@Override
//			public void afterTextChanged(Editable arg0) {
//				
//			}
//		});
//		
//		preloader = new Preloader();
//		preloader.startPreloader(this,"Fetching Your Friends");
//		//Activity
//		if (!Util.isNetworkAvailable(Friends.this)) {	
//			
//			DialogFactory.getBeaconStreamDialog1(Friends.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
//		} else 
//		
//		{
//		if(Util.isFbUser()){ 
//			final Boolean[] parms = {true};
//			new LoadSocialAsync().execute(parms);
//		} else {
//			//Get Contacts list of app users who are friends
//			new LoadContactsAsync().execute();
//		}
//		}
//		ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
//		query.whereEqualTo("user", ParseUser.getCurrentUser());
//		query.findInBackground(new FindCallback<ParseObject>() {
//			@Override
//			public void done(List<ParseObject> objects, ParseException e) {
//				if(e==null)
//				{
//				if(objects.size() > 0) {
//					for(ParseObject obj : objects) { 
//						ufr = new UserFriendRelation();
//						ufr.setPendingFriends(obj.getJSONArray("pendingF"));
//						ufr.setConnectedGroups(obj.getJSONArray("connectedG"));
//						ufr.setRequestedFriends(obj.getJSONArray("requestedF"));
//						ufr.setConnectedFriends(obj.getJSONArray("connectedF"));
//						ufr.setPendingFriendsS(obj.getJSONArray("pendingSF"));						
//						ufr.setRequestedFriendsS(obj.getJSONArray("requestedSF"));
//						ufr.setConnectedFriendsS(obj.getJSONArray("connectedSF"));
//						ufr.setUserId(ParseUser.getCurrentUser().getObjectId().toString());
//						break;
//					}
//				}
//			}
//			}
//		});
//		
//		
//	}
//	
//	
//	
//	public void configParse(){
//		_config = new AppConfig(this);
//		currentUser = ParseUser.getCurrentUser();
//	}
//	
//	
//	public class LoadSocialAsync extends AsyncTask<Boolean, Void, ArrayList<Friend>> {
//
//		@Override
//		protected ArrayList<Friend> doInBackground(Boolean... params) {
//			//Calling from server
//			String fqlQuery = "select uid, name,first_name, pic_square, is_app_user from user where is_app_user = 1 and uid in (select uid2 from friend where uid1 = me())";
//			Bundle ops = new Bundle();
//			ops.putString("q", fqlQuery);
//			Request r = new Request(ParseFacebookUtils.getSession(),"/fql",ops,HttpMethod.GET,new Request.Callback() {
//				
//				@Override
//				public void onCompleted(Response response) {
//					if (response.getError() == null) {
//						//Log.i("FB OP","Got results: "+ response.toString());
//						GraphObject gObject = response.getGraphObject();
//						JSONObject jObject = gObject.getInnerJSONObject();
//						try {
//							ParseQuery<ParseUser> query = ParseUser.getQuery();
//							JSONArray jArray = jObject.getJSONArray("data");
//							ArrayList<String> fblist = new ArrayList<String>();
//							for(int i = 0; i<jArray.length();i++){
//								JSONObject jObj2 = jArray.getJSONObject(i);
//								fblist.add(jObj2.getString("uid"));
//							}
//							query.whereContainedIn("facebookid",fblist);
//							query.whereNotEqualTo("objectId",ParseUser.getCurrentUser().getObjectId());
//							try {
//								List<ParseUser> objects = query.find();
//								Friend list = null;
//								if(objects.size() > 0){
//									for (int i = 0; i < objects.size(); i++) {
//										String fbid = objects.get(i).get("facebookid").toString();
//										
//										String fbImage = "https://graph.facebook.com/"+ fbid+ "/picture/";
//										String myObj = ParseUser.getCurrentUser().toString();
//										String userobj = objects.get(i).getObjectId().toString();
//										String userName = objects.get(i).get("fullname").toString();
//										//list = new People(myObj,userobj,fbid,userName,"facebook",fbImage,false,false);
//										list = new Friend(userobj, userName, "facebook", fbImage, false, false, "Add");
//										fbList.add(list);
//									}
//								}
//							} catch (ParseException e) {
//								e.printStackTrace();
//							}
//						}catch (JSONException e) {
//							e.printStackTrace();
//						}
//					}else{
//						//Log.i("MyApp",response.getError().toString());
//					}
//				}
//			});
//			Request.executeBatchAndWait(r);
//			return fbList;
//		}
//		
//		protected void onPostExecute(ArrayList<Friend> result) {
//			super.onPostExecute(result);
//			finalList.addAll(result);
//			if(result != null)
//			updateList(result);
//			//Activity
//			if (!Util.isNetworkAvailable(Friends.this)) {	
//				
//				DialogFactory.getBeaconStreamDialog1(Friends.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
//			} else 
//			
//			{
//			new LoadContactsAsync().execute();
//			}
//		}
//	}
//	
//	private class LoadContactsAsync extends AsyncTask<Void, Void, ArrayList<Friend>> {
//		@Override
//		protected ArrayList<Friend> doInBackground(Void... params) {
//		/*	ContentResolver cr = getContentResolver();
//	        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
//	 
//	         if (cur != null && cur.getCount() > 0) {
//	        	ParseQuery<ParseUser> query = ParseUser.getQuery();
//	            while (cur.moveToNext()) {
//	                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
//	                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//	                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
//	                    System.out.println("name : " + name + ", ID : " + id);
//	 
//	                    // get the phone number
//	                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
//	                                           ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
//	                                           new String[]{id}, null);
//	                    while (pCur.moveToNext()) {
//	                          String phoneNum = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("\\s","");
//	                          System.out.println("phone" + phoneNum);
//	                          if (phoneNum.length() > 8) {
//	  							try {
//	  								query.whereEqualTo("phone", phoneNum);
//	  								query.whereNotEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
//	  								Friend items = null;
//	  								List<ParseUser> qResult;
//	  								qResult = query.find();
//	  								for(int i=0;i<qResult.size();i++){
//	  									Log.i("INFOOOOO", qResult.get(i).getObjectId().toString() + " --- " + qResult.get(i).get("fullname"));
//	  									String imageUrl = "image";
//	  									ParseFile p;
//	  									p = qResult.get(i).getParseFile("profilethumbnail");
//	  									if(p!=null){
//	  										String url = p.getUrl();
//	  										imageUrl = url;
//	  									}
//	  									items = new Friend(qResult.get(i).getObjectId().toString(), qResult.get(i).getString("fullname"), "contact", imageUrl, false, false);
//	  									contactsList.add(items);
//	  								}
//	  							} catch (ParseException e) {
//	  								// TODO Auto-generated catch block
//	  								e.printStackTrace();
//	  							}
//	  						}
//	                    }
//	                    pCur.close();
//	                }
//	            }
//	            cur.close();
//	         }*/
//			return (contactsList);
//		}
//		
//		 protected void onPostExecute(ArrayList<Friend> result) {
// 			super.onPostExecute(result);
// 			finalList.addAll(result);
// 			
// 			if(finalList.size() > 0) {
// 				final JSONArray jsonFriendsArray;
// 				try {
// 					jsonFriendsArray = new JSONArray(finalList.toString());
// 					
// 					ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
// 					query.whereEqualTo("user", ParseUser.getCurrentUser());
// 					query.findInBackground(new FindCallback<ParseObject>() { 
// 						public void done(List<ParseObject> objects, ParseException e) {
// 							if(objects.size() > 0) {
// 								//edit
//	 							if(e == null){
//	 								for(ParseObject obj : objects){
//	 									obj.put("friendsList", jsonFriendsArray.toString());
//	 									obj.saveInBackground();//saveEventually();();
//	 								}
//	 							} else {
//	 									
//	 							}
// 							} else {
// 								//save
// 								ParseObject friends = new ParseObject("Friends");
// 			 					friends.put("user",ParseUser.getCurrentUser()); 
// 			 					friends.put("friendsList", jsonFriendsArray.toString());
// 			 					friends.saveInBackground();//saveEventually();(); 
// 							}
// 						}
// 					});
// 					//Log.d("FinalList", jsonFriendsArray.toString());
// 				} catch (JSONException e) {
// 					e.printStackTrace();
// 				}
// 			}
// 		}
//	}
//	
//	public void updateList(ArrayList<Friend> res){
//		try {
//			if(res != null) {
//				if(ufr != null && ufr.getUserId().equals(ParseUser.getCurrentUser().getObjectId().toString())) {
//					JSONArray pendingFriends = ufr.getPendingFriends();
//					if(pendingFriends != null) {
//						for(Friend fir : res) {
//							for(int pf = 0; pf < pendingFriends.length(); pf++){
//								try {
//									if(pendingFriends.get(pf).equals(fir.getUserId())){
//										//holder.mAction.setText("Accept");
//										fir.setStatus("Accept");
//									} 
//								} catch (JSONException e) {
//									e.printStackTrace();
//								}
//							}
//						}
//					}
//					
//					JSONArray requestedFriends = ufr.getRequestedFriends();
//					if(requestedFriends != null) {
//						for(Friend fir : res) {
//							for(int rf = 0; rf < requestedFriends.length(); rf++){
//								try {
//									if(fir.getUserId().equals(requestedFriends.get(rf))){
//										//holder.mAction.setText("Request Sent");
//										fir.setStatus("Request Sent");
//									}
//								} catch (JSONException e) {
//									e.printStackTrace();
//								}
//							}
//						}
//					}
//					JSONArray requestedFriendsS = ufr.getRequestedFriendsS();
//					if(requestedFriends != null) {
//						for(Friend fir : res) {
//							for(int rf = 0; rf < requestedFriendsS.length(); rf++){
//								try {
//									if(fir.getUserId().equals(requestedFriendsS.get(rf))){
//										//holder.mAction.setText("Request Sent");
//										fir.setStatus("Request Sent.");
//									}
//								} catch (JSONException e) {
//									e.printStackTrace();
//								}
//							}
//						}
//					}
//					JSONArray pendingFriendsS = ufr.getPendingFriendsS();
//					if(pendingFriendsS != null) {
//						for(Friend fir : res) {
//							for(int rf = 0; rf < pendingFriendsS.length(); rf++){
//								try {
//									if(fir.getUserId().equals(pendingFriendsS.get(rf))){
//										//holder.mAction.setText("Request Sent");
//										fir.setStatus("Accept.");
//									}
//								} catch (JSONException e) {
//									e.printStackTrace();
//								}
//							}
//						}
//					}
//					JSONArray connectedFriend = ufr.getConnectedFriends();
//					if(connectedFriend != null) {
//						for(Friend fir : res) {
//							for(int rf = 0; rf < connectedFriend.length(); rf++){
//								try {
//									if(fir.getUserId().equals(connectedFriend.get(rf))){
//										//holder.mAction.setText("Request Sent");
//										fir.setStatus("Friend");
//									}
//								} catch (JSONException e) {
//									e.printStackTrace();
//								}
//							}
//						}
//					}
//					JSONArray connectedFriendS = ufr.getConnectedFriendsS();
//					if(requestedFriends != null) {
//						for(Friend fir : res) {
//							for(int rf = 0; rf < connectedFriendS.length(); rf++){
//								try {
//									if(fir.getUserId().equals(connectedFriendS.get(rf))){
//										//holder.mAction.setText("Request Sent");
//										fir.setStatus("Friend.");
//									}
//								} catch (JSONException e) {
//									e.printStackTrace();
//								}
//							}
//						}
//					}
//					
//				}
//			}
//			
//			
//			try {
//				mAdapter = new FriendsAdapter(getApplicationContext(),R.layout.people_item,res,this, friends, ufr,"friends");
//				lv.setAdapter(mAdapter);
//				lv.setVisibility(View.VISIBLE);
//				mSearch.setVisibility(View.VISIBLE);
//				preloader.stopLoading();
//				//Log.i("list", res.toString());
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//	    MenuInflater inflater = getMenuInflater();
//	    return true;
//	}
//	
//	
//	@Override
//	public void getPeopleAdded(JSONArray list, JSONArray friendsId) {
//		//Log.d("MyApp","Friends: "+list.toString());
//		friends = list;
//		friendsid = friendsId;
//	}
//	
//
//
//
//}
//
///*public class Friends extends ActionBarActivity implements FriendListener{
//	private AppConfig _config;
//	ParseUser currentUser;
//	private Preloader preloader;
//	ArrayList<Friend> sList = new ArrayList<Friend>();
//	ArrayList<Friend> tList = new ArrayList<Friend>();
//	ArrayList<Friend> finalList = new ArrayList<Friend>();
//	static FriendsAdapter mAdapter = null;
//	ProgressBar mPbar;
//	EditText mSearch;
//	ListView lv = null;
//	private JSONArray friends = new JSONArray();
//	private JSONArray friendsid = new JSONArray();
//	String friends_id;
//
//	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		_config = new AppConfig(getApplicationContext());
//        ActionBar bar = getSupportActionBar();
//		bar.setTitle(this.getString(R.string.selectfriends));		
//		setContentView(R.layout.friends_layout);		
//		preloader = new Preloader();		
//		configParse();		
//		mSearch = (EditText) findViewById(R.id.inputSearch);
//		mPbar = (ProgressBar) findViewById(R.id.pbPhoneBook);
//		lv = (ListView) findViewById(R.id.socialList);
//		
//		mSearch.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
//				String text = mSearch.getText().toString();
//				mAdapter.search(text);
//			}
//			
//			@Override
//			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
//					int arg3) {
//				
//			}
//			
//			@Override
//			public void afterTextChanged(Editable arg0) {
//				
//			}
//		});
//		
//		preloader.startPreloader(this,"Fetching Your Friends");
//		if(Util.isFbUser()){ 
//			final Boolean[] parms = {true};
//			new LoadSocialAsync().execute(parms);
//		} else {
//			//Get Contacts list of app users who are friends
//			new LoadContactsAsync().execute();
//		}
//	}
//	
//	public void configParse(){
//		_config = new AppConfig(this);
//		currentUser = ParseUser.getCurrentUser();
//	}
//
//	public class LoadSocialAsync extends AsyncTask<Boolean, Void, ArrayList<Friend>> {
//
//		@Override
//		protected ArrayList<Friend> doInBackground(Boolean... params) {
//			//Calling from server
//			String fqlQuery = "select uid, name,first_name, pic_square, is_app_user from user where is_app_user = 1 and uid in (select uid2 from friend where uid1 = me())";
//			Bundle ops = new Bundle();
//			ops.putString("q", fqlQuery);
//			Request r = new Request(ParseFacebookUtils.getSession(),"/fql",ops,HttpMethod.GET,new Request.Callback() {
//				
//				@Override
//				public void onCompleted(Response response) {
//					if (response.getError() == null) {
//						Log.i("FB OP","Got results: "+ response.toString());
//						GraphObject gObject = response.getGraphObject();
//						JSONObject jObject = gObject.getInnerJSONObject();
//						try {
//							ParseQuery<ParseUser> query = ParseUser.getQuery();
//							JSONArray jArray = jObject.getJSONArray("data");
//							ArrayList<String> fblist = new ArrayList<String>();
//							for(int i = 0; i<jArray.length();i++){
//								JSONObject jObj2 = jArray.getJSONObject(i);
//								fblist.add(jObj2.getString("uid"));
//							}
//							query.whereContainedIn("facebookid",fblist);
//							query.whereNotEqualTo("objectId",ParseUser.getCurrentUser().getObjectId());
//							try {
//								List<ParseUser> objects = query.find();
//								Friend list = null;
//								if(objects.size() > 0){
//									for (int i = 0; i < objects.size(); i++) {
//										String fbid = objects.get(i).get("facebookid").toString();
//										
//										String fbImage = "https://graph.facebook.com/"+ fbid+ "/picture/";
//										String myObj = ParseUser.getCurrentUser().toString();
//										String userobj = objects.get(i).getObjectId().toString();
//										String userName = objects.get(i).get("fullname").toString();
//										//list = new People(myObj,userobj,fbid,userName,"facebook",fbImage,false,false);
//										list = new Friend(userobj, userName, "facebook", fbImage, false, false);
//										sList.add(list);
//									}
//								}
//							} catch (ParseException e) {
//								e.printStackTrace();
//							}
//						}catch (JSONException e) {
//							e.printStackTrace();
//						}
//					}else{
//						Log.i("MyApp",response.getError().toString());
//					}
//				}
//			});
//			Request.executeBatchAndWait(r);
//			return sList;
//		}
//		
//		protected void onPostExecute(ArrayList<Friend> result) {
//			super.onPostExecute(result);
//			finalList.addAll(result);
//			new LoadContactsAsync().execute();
//		}
//	}
//	
//	private class LoadContactsAsync extends AsyncTask<Void, Void, ArrayList<Friend>> {
//		@Override
//		protected ArrayList<Friend> doInBackground(Void... params) {
//			return (tList);	
//		}
//		
//		 protected void onPostExecute(ArrayList<Friend> result) {
// 			super.onPostExecute(result);
// 			finalList.addAll(result);
// 			updateList(finalList);
// 			
// 	
// 		}
//		
//		
//		
//		}
//	public void updateList(ArrayList<Friend> res){
//		mAdapter = new FriendsAdapter(getApplicationContext(),R.layout.people_item,res,Friends.this);
//		lv.setAdapter(mAdapter);
//		lv.setVisibility(View.VISIBLE);
//		mSearch.setVisibility(View.VISIBLE);
//		preloader.stopLoading();
//		Log.i("list", res.toString());
//	}
//
//	@Override
//	public void getFriendAdded(JSONArray list, JSONArray friendsId) {
//		Log.d("MyApp","Friends: "+list.toString()+"friendsId"+friendsId.toString());
//		friends = list;
//		friendsid = friendsId;
//		friends_id = friendsId.toString();
//
//	}
//	
//	
//
//}
//*/