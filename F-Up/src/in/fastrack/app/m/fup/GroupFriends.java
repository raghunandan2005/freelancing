package in.fastrack.app.m.fup;

public class GroupFriends {

	int tick,colorb;
	boolean chk;
	
	public int getColorb() {
		return colorb;
	}
	public void setColorb(int colorb) {
		this.colorb = colorb;
	}
	public int getTick() {
		return tick;
	}
	public void setTick(int tick) {
		this.tick = tick;
	}
	public boolean getGroupSelected() {
		return chk;
	}
	public void setGroupSelected(boolean chk) {
		this.chk = chk;
	}
	
}
