package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.adapter.GroupAddAdapter;
import in.fastrack.app.m.fup.adapter.GroupAddAdapter.GroupListener;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Group;
import in.fastrack.app.m.fup.model.NewsFeed;
import in.fastrack.app.m.fup.model.UserFriendRelation;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class GroupsAddFragment extends Fragment implements GroupListener{

	private AppConfig _config;
	private Preloader preloader;
	ParseUser currentUser;
	int FB_REQUEST_ID = 900;
	static GroupAddAdapter mAdapter = null;
	ProgressBar mPbar;
	EditText mSearch;
	ListView lv = null;
	private JSONArray friends = new JSONArray();
	private JSONArray friendsid = new JSONArray();
	private JSONArray groupss = new JSONArray();
	private JSONArray groupsid = new JSONArray();
	UserFriendRelation ufr = null;
	ArrayList<Group> groupList = new ArrayList<Group>();
	String friendsListString = null;
	NewsFeed alarmDetails;
	ArrayList<String> selectedFriendsid = new ArrayList<String>(); 
	ArrayList<GroupFriends> gropupfriendslist = new ArrayList<GroupFriends>(); 

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.groups, container, false);
		if(getActivity().getIntent().getStringExtra("group") != null) { 
			try {
				groupss = new JSONArray(getActivity().getIntent().getStringExtra("group"));
				 for (int i = 0; i < groupss.length(); i++) {
					 Group g = new Group();
					 
				        JSONObject explrObject = groupss.getJSONObject(i);
				        selectedFriendsid.add(explrObject.getString("userid"));
				    }
				 System.out.println(selectedFriendsid.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		preloader = new Preloader();
		preloader.startPreloader(getActivity(),"Loading, Please wait...");
		configParse();
		
		lv = (ListView) root.findViewById(R.id.groupList);
		
		//Fragment
		if (!Util.isNetworkAvailable(getActivity())) {	
			try {
				preloader.stopLoading();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	     DialogFactory.getBeaconStreamDialog1(getActivity(), getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
       } else 

       {
		final Boolean[] parms = {false};
		new LoadGroupsAsync().execute(parms);
       }
		
		
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Groups");
		query.whereEqualTo("user", ParseUser.getCurrentUser());
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if(e==null) {
				if(objects.size() > 0) {
					for(ParseObject obj : objects) { 
						ufr = new UserFriendRelation();						
					
						ufr.setUserId(ParseUser.getCurrentUser().getObjectId().toString());
						break;
					}
				}
			}
			}
		});
		
		return root;
	}

	public void configParse(){
		_config = new AppConfig(getActivity().getBaseContext());
		currentUser = ParseUser.getCurrentUser();
	}
	
	public class LoadGroupsAsync extends AsyncTask<Boolean, Void, ArrayList<Group>> {
		@Override
		protected ArrayList<Group> doInBackground(Boolean... params) {
			
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Groups");			
			query.whereEqualTo("creator", ParseUser.getCurrentUser());
			query.whereNotEqualTo("deleteG", ParseUser.getCurrentUser().getObjectId().toString());
			query.orderByDescending("createdAt");
			try {
				List<ParseObject> objects = query.find();
				if(objects.size() > 0) {
					for(ParseObject obj : objects){//
						Group g = new Group();
						g.setGroupObject(obj.getObjectId());
						g.setCretor(obj.get("creator").toString());
						g.setFriendList(obj.getString("friendsList"));
						g.setGroupSelected(obj.getBoolean("Gselected"));
						try {
							JSONArray jsonArray = null;
							if(obj.getString("friendsList") != null) {
								jsonArray = new JSONArray(obj.getString("friendsList"));
								for(int i=0;i<jsonArray.length();i++)//each friend
								{
								JSONObject jsonObject = jsonArray.getJSONObject(i);
								//GroupFriends gf = new GroupFriends();
//								g.setColorb(R.drawable.button);
//								g.setGroupSelected(false);
//								g.setTick(R.drawable.plus);
								//gropupfriendslist.add(gf);
							//	b.setBackgroundResource(R.drawable.button);
//								b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus, 0, 0, 0);
								}
								g.setMemberCount(jsonArray.length());
							} else {
								jsonArray = new JSONArray("[]");
								g.setMemberCount(0);
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
						ParseFile p = obj.getParseFile("groupProfileImage");
						if(p!=null){
							String url = p.getUrl();
							g.setGroupImagePath(url);
						}
						g.setName(obj.getString("groupName"));
						
						
						groupList.add(g);						
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return groupList;
		}

		@Override
		protected void onPostExecute(ArrayList<Group> result) {
			super.onPostExecute(result);
			updateList(result);
		}
		
	}
	
	public void updateList(ArrayList<Group> res) {
		
		try {
			mAdapter = new GroupAddAdapter(getActivity().getBaseContext(),R.layout.dummy,res,this, groupss);
			lv.setAdapter(mAdapter);
			lv.setVisibility(View.VISIBLE);
			preloader.stopLoading();
			Log.i("list", res.toString());
			lv.setOnItemClickListener(new OnItemClickListener() {
			     @Override
			     public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			      int itemPosition = position;
			      Group  itemValue = (Group) lv.getItemAtPosition(position);
			      Intent groupEachActivity = new Intent(getActivity().getBaseContext(),groupEach.class);
			      groupEachActivity.putExtra("group", itemValue);
				  startActivity(groupEachActivity);
			      Log.i("itemclick", "Position :"+itemPosition+"  ListItem : " +itemValue.toString() );   
			     }
			 });
		} catch (Exception e) {
			// TODO Auto-generated catch block
			try {
				preloader.stopLoading();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}    
	}

	@Override
	public void getGroupAdded(JSONArray arr, JSONArray groupsId) {
		Log.d("MyApp","Groups: "+arr.toString());
		friends = arr;
		friendsid = groupsId;
		someEventGroupListener.someEventGroup(arr, groupsId);
	}
	
	/*************INTERFACE********************/
	 public interface onSomeEventGroupListener {
		 public void someEventGroup(JSONArray arr, JSONArray groupsId);
	 }

	 onSomeEventGroupListener someEventGroupListener;

	  @Override
	  public void onAttach(Activity activity) {
	    super.onAttach(activity);
	        try {
	        	someEventGroupListener = (onSomeEventGroupListener) activity;
	        } catch (ClassCastException e) {
	            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
	        }
	  }
	/*************INTERFACE********************/

}
