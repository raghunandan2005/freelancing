package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.adapter.GroupAdapter;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Group;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class GroupsF extends Fragment {
	ListView lv = null;
	private AppConfig _config;
	private Preloader preloader;
	ParseUser currentUser;
	GroupAdapter mAdapter = null;
	ArrayList<Group> groupList = new ArrayList<Group>();
	Button addgroup;
	public void configParse(){
		_config = new AppConfig(getActivity().getBaseContext());
		currentUser = ParseUser.getCurrentUser();
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.groups, container, false);
		
		//preloader = new Preloader();
		configParse();
		
		lv = (ListView) root.findViewById(R.id.groupList);
		addgroup = (Button) root.findViewById(R.id.addgroup);
		addgroup.setVisibility(View.VISIBLE);
if (!Util.isNetworkAvailable(getActivity())) {	
			
			DialogFactory.getBeaconStreamDialog1(getActivity(), getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
		} else 
		
		{
		final Boolean[] parms = {false};
		new LoadGroupsAsync().execute(parms);
		}
		addgroup.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent addGroups = new Intent(getActivity(),AddGroups.class);
				startActivity(addGroups);
			}
		});
		return root;
	}
	/*@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.groups);
		
		ActionBar bar = getSupportActionBar();
		bar.setTitle("Groups");
		
		bar.setDisplayShowHomeEnabled(true);
		bar.setHomeButtonEnabled(true);
		bar.setDisplayHomeAsUpEnabled(true);
		
		lv = (ListView) findViewById(R.id.groupList);

		preloader = new Preloader();		
		configParse();
		preloader.startPreloader(this, "Loading Groups");
		final Boolean[] parms = {false};
		new LoadGroupsAsync().execute(parms);
	}*/
	
	public class LoadGroupsAsync extends AsyncTask<Boolean, Void, ArrayList<Group>> {
		@Override
		protected ArrayList<Group> doInBackground(Boolean... params) {
			
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Groups");
			query.whereEqualTo("creator", ParseUser.getCurrentUser());
			query.orderByDescending("createdAt");
			try {
				List<ParseObject> objects = query.find();
				if(objects.size() > 0) {
					for(ParseObject obj : objects){
						Group g = new Group();
						g.setGroupObject(obj.getObjectId());
						g.setCretor(obj.get("creator").toString());
						
						g.setFriendList(obj.getString("friendsList"));
						try {
							JSONArray jsonArray = null;
							if(obj.getString("friendsList") != null) {
								jsonArray = new JSONArray(obj.getString("friendsList"));
								g.setMemberCount(jsonArray.length());
							} else {
								jsonArray = new JSONArray("[]");
								g.setMemberCount(0);
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
						ParseFile p = obj.getParseFile("groupProfileImage");
						if(p!=null){
							String url = p.getUrl();
							g.setGroupImagePath(url);
						}
						g.setName(obj.getString("groupName"));
						groupList.add(g);						
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return groupList;
		}

		@Override
		protected void onPostExecute(ArrayList<Group> result) {
			super.onPostExecute(result);
			updateList(result);
		}
		
	}
	
	public void updateList(ArrayList<Group> res){
		try {
			mAdapter = new GroupAdapter(getActivity(),R.layout.group_item,res);
			lv.setAdapter(mAdapter);
			lv.setVisibility(View.VISIBLE);
			preloader.stopLoading();
			Log.i("list", res.toString());
			lv.setOnItemClickListener(new OnItemClickListener() {
			     @Override
			     public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			      int itemPosition = position;
			      Group  itemValue = (Group) lv.getItemAtPosition(position);
			      Intent groupEachActivity = new Intent(getActivity(),groupEach.class);
			      groupEachActivity.putExtra("group", itemValue);
				  startActivity(groupEachActivity);
			      Log.i("itemclick", "Position :"+itemPosition+"  ListItem : " +itemValue.toString() );   
			     }
			 });
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    
	}
	
	 

	
	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.action_groups, menu);
	    return true;
	}*/
	
	/*public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent i = new Intent(getActivity(), HomeActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			getActivity().finish();
			return true;
		case R.id.add_groups:
			Intent addGroups = new Intent(getActivity(),addGroups.class);
			startActivity(addGroups);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}*/
	
}
