package in.fastrack.app.m.fup;


import in.fastrack.app.m.fup.adapter.NavAdapter;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.model.NavList;
import in.fastrack.app.m.fup.model.UserFriendRelation;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

public class HomeActivity extends Activity {
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	private CharSequence mDrawerTitle;
	// used to store app title
	private CharSequence mTitle;
	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private ArrayList<NavList> navDrawerItems;
	private NavAdapter adapter;	
	private AppConfig _config;
	ParseUser currentUser;
	private Intent link;
	JSONArray connectedSurpriseF = new JSONArray();
	boolean createNotification;
    //For surprise Friend		
	private int notificationCount = 0;
	UserFriendRelation ufr = null;
	String selectedAlarmType,str;

	Bundle extras;	
	int d[] ={R.drawable.ic_home};
	//End For surprise Friend	
	ParseFile p,pb;
	String t = null;
	JSONObject json;
	 ParseObject parseOO;
	 int counter;
	public void configParse(){
		_config = new AppConfig(this);
		/*String[] parseKeys = _config.getAppKeys("parse");
		Parse.initialize(this, parseKeys[0], parseKeys[1]);
		ParseFacebookUtils.initialize(getString(R.string.app_id));*/
		currentUser = ParseUser.getCurrentUser();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.slidemenu_activity);
		counter = getIntent().getIntExtra("counter", 0);
		//Parse Push Notification
//		try {
//			Intent intent = getIntent();
//			 extras = intent.getExtras();
//			if (extras != null) {
//				  String action = intent.getAction();
//			      String channel = extras.getString("com.parse.Channel");
//			      if(intent.hasExtra("com.parse.Data")) 
//			      {
//			    	  json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
//			    	  int notificationtype = json.getInt("notificationtype");
//			    	  switch(notificationtype)
//			    	  {
//			    	  case 1:
//			    	//Someone created alarm
//			    	    Log.i("Parse"," Create ALarm");	
//			    	    Toast.makeText(HomeActivity.this,"Notification type 1", Toast.LENGTH_LONG).show();
//			    	 	link =new Intent(HomeActivity.this, NotificationActivityNew.class);	
//			    	 	
//						extras.putString("alarmid",json.getString("reference"));
//						extras.putBoolean("onlyone",true);														
//						link.putExtras(extras);
//						startActivity(link);
//			    	  
//			    	  
//			    	  break;
//			    	  case 2:
//			    	//Someone commented
//					Log.i("Parse", "Comment");
//					
//					Toast.makeText(HomeActivity.this,"2"+json.getString("reference"), Toast.LENGTH_LONG).show();
//					link = new Intent(HomeActivity.this, FeedComments.class);
//	
//					extras.putString("alarmid", json.getString("reference"));
//					extras.putInt("position", 0);
//					link.putExtras(extras);
//					startActivity(link); 
//			    	  break;
//			    	  case 3:			    	  
//			    	//Someone liked
//			    	   Log.i("Parse","Like");	
//			    	   Toast.makeText(HomeActivity.this,"Notification type 3", Toast.LENGTH_LONG).show();
//			    	   link =new Intent(HomeActivity.this, NotificationActivityNew.class);											
//						extras.putString("alarmid", json.getString("reference"));
//						extras.putBoolean("onlyone",true);
//						link.putExtras(extras);
//						startActivity(link);
//			    	  break;
//			    	  case 4:
//			    	   Log.i("Parse","Surprise Alarm");	
//			    	   Toast.makeText(HomeActivity.this,"Notification type 4", Toast.LENGTH_LONG).show();
//			    	   
//
//			   		if(ParseUser.getCurrentUser() != null){
//			   			ParseQuery<ParseObject> query = ParseQuery.getQuery("Activity");
//			   			query.whereEqualTo("followers",ParseUser.getCurrentUser().getObjectId());
//			   			//query.whereNotEqualTo("readers", ParseUser.getCurrentUser().getObjectId());
//			   			query.include("creator");
//			   			query.include("reference");			   			
//			   			query.orderByDescending("createdAt");
//			   			query.setLimit(10);
//			   			query.findInBackground(new FindCallback<ParseObject>() {
//			   				
//			   				@Override
//			   				public void done(List<ParseObject> objects, ParseException e) {
//			   					if(e==null){
//			   						try {
//			   							//Data found
//			   							Log.i("Parse","Data present");
//			   							int k = 1;
//			   							for(ParseObject o:objects){
//			   								final ParseObject parseO = o;
//			   								//notificationCount++;
//			   								int notificationType = o.getInt("notificationtype");
//			   								
//			   								createNotification = true;
//			   								Intent intent = null;
//			   								
//			   								
//			   								extras = new Bundle();
//								
//			   								switch(notificationType){
//			   								case 4:
//			   									
//			   									ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");			   									
//			   									query.whereEqualTo("user", ParseUser.getCurrentUser());
//			   									query.findInBackground(new FindCallback<ParseObject>() {
//			   										@Override
//			   										public void done(List<ParseObject> objects, ParseException e) {
//
//			   											if (e == null) {
//			   												try {
//			   													if (objects.size() > 0) {
//			   														for (ParseObject obj : objects) {														 
//			   															connectedSurpriseF = obj.getJSONArray("connectedSF");
//			   															//Log.i("Parse", "connectedSurpriseF"+connectedSurpriseF.toString());
//			   															break;
//			   														}
//			   														
//			   														if(connectedSurpriseF!=null)
//			   														{
//			   															//Log.i("File", "connected"+ connectedSurpriseF.toString());
//			   															if (connectedSurpriseF.length() > 0) {
//			   															for (int i = 0; i < connectedSurpriseF.length(); i++) {
//			   																
//			   																	//if (connectedSurpriseF != null) {
//			   																		Log.i("File", "in if \n" +connectedSurpriseF.get(i)+"\n" + parseO.getParseObject("reference").getString("creatorplain"));
//			   																		if (connectedSurpriseF.get(i).equals(parseO.getParseObject("reference").getString("creatorplain"))) {//ParseUser.getCurrentUser().getObjectId().toString()
//
//			   																			
//			   																			
//			   																			
//			   																			//started
//			   																			try {
//			   																		 
//			   																				createNotification = false;
//			   																				
//			   																				//parseO.addAllUnique("readers",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
//			   																				
//			   																				parseO.put("readers", Arrays.asList(ParseUser.getCurrentUser().getObjectId()));//ParseUser.getCurrentUser().getObjectId().toString()
//			   																				parseO.add("readers", parseO.getParseObject("reference").getString("creatorplain"));																				
//			   																				parseO.saveInBackground();
//			   																				
//			   																				ParseQuery<ParseObject> not = ParseQuery.getQuery("alarms");
//			   																				not.getInBackground(parseO.getParseObject("reference").getObjectId(), new GetCallback<ParseObject>() {
//			   																					@Override
//			   																					public void done(ParseObject object, com.parse.ParseException e) {
//			   																						object.addAllUnique("acceptedpeople",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
//			   																						object.addAllUnique("notificationactions",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
//			   																						object.saveInBackground();//saveEventually();();
//			   																						//Set alarm
//			   																						Intent intent = new Intent(HomeActivity.this,AlaramActivityNew.class);
//			   																						Bundle tempData = new Bundle();
//			   																						tempData.putString("alarm_category",object.getString("category"));
//			   																						tempData.putString("alarm_title", object.getString("title"));
//			   																						tempData.putBoolean("recorded",object.getBoolean("recorded"));
//			   																						tempData.putInt("alarmrequestcode",Integer.parseInt(object.getString("requestcode")));
//			   																						tempData.putString("alarmpath",object.getString("media"));
//			   																						tempData.putString("alarmfriends",object.getString("connectedlist"));
//			   																						if (object.getString("alarm_repeat_type") != null)
//			   																						tempData.putString("alarm_repeat_type",object.getString("alarm_repeat_type"));
//			   																						
//			   																						if (object.getString("alarm_repeat_type") != null)
//			   																						selectedAlarmType = object.getString("alarm_repeat_type");
//			   																					//Alarm for other surprise user
//			   																						Alarams al = new Alarams();
//			   																						al.setCategory(object.getString("category"));
//			   																						al.setTitle(object.getString("title"));
//			   																						if (object.getBoolean("recorded") == true) {
//			   																							al.setRecorded("true");
//			   																						} else {
//			   																							al.setRecorded("false");
//			   																						}
//			   																						al.setRequestcode(String.valueOf(object.getString("requestcode")));
//			   																						al.setPath(object.getString("media"));
//			   																						al.setFriends(object.getString("connectedlist"));
//			   																						JSONArray jArray = object.getJSONArray("connected");
//			   																						try {
//			   																							al.setFriendsid(jArray.getString(0));
//			   																						} catch (JSONException e3) {
//			   																							// TODO Auto-generated catch block
//			   																							e3.printStackTrace();
//			   																						}
//			   																						
//			   																						al.setRepeattype(selectedAlarmType);
//			   																						
//			   																						al.setMilli(String.valueOf(object.getLong("alarmtimemilli")));
//			   																						
//			   																						String time = object.getString("alarmTime");
//			   																						String split[] = time.split("\\s");
//			   																						String date = split[0];
//			   																						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault());
//			   																				
//			   																						//String cale = sdf.format(date);
//			   																						al.setAtime(date);
//			   																						Toast.makeText(HomeActivity.this,"millllllli" + object.getLong("alarmtimemilli")+date, Toast.LENGTH_LONG).show();
//
//			   																						DatabaseHandler db = new DatabaseHandler(HomeActivity.this);
//			   																						db.updateAlarm(al);
//			   																						//end
//			   																						intent.putExtras(tempData);
//			   																						PendingIntent pi = PendingIntent.getActivity(HomeActivity.this,Integer.parseInt(object.getString("requestcode")),intent,PendingIntent.FLAG_CANCEL_CURRENT);
//			   																						AlarmManager am = (AlarmManager)HomeActivity.this.getSystemService(Context.ALARM_SERVICE);
//			   																						am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
//			   																						
//			   																						Calendar calendar = Calendar.getInstance();
//			   																						calendar.setTimeInMillis(calendar.getTimeInMillis());
//			   																						calendar.add(Calendar.SECOND, 30);
//			   																						if (selectedAlarmType.equalsIgnoreCase("Once")) {
//			   																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
//			   																							
//			   																						} else if (selectedAlarmType.equalsIgnoreCase("Daily")) {
//
//			   																							calendar.add(Calendar.DAY_OF_WEEK, 1);
//			   																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
//			   																							// am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,calendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pi);
//			   																						} else if (selectedAlarmType.equalsIgnoreCase("Weekly")) {
//
//			   																							calendar.add(Calendar.WEEK_OF_MONTH, 1);
//			   																							am.set(AlarmManager.RTC_WAKEUP,
//			   																									object.getLong("alarmtimemilli"), pi);
//			   																						} else if (selectedAlarmType.equalsIgnoreCase("Monthly")) {
//
//			   																							calendar.add(Calendar.MONTH, 1);
//			   																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
//			   																						}
//			   																						
//			   																					
//			   																						
//			   																						//Download audio if required
//			   																						final String alarmPath = object.getString("media");
//			   																						Log.i("File path",alarmPath);
//			   																						if(object.getBoolean("recorded")){
//			   																							ParseFile p = object.getParseFile("audioclip");
//			   																							if(p!=null){
//			   																								Log.i("File","A");
//			   																								p.getDataInBackground(new GetDataCallback() {
//			   																									
//			   																									@Override
//			   																									public void done(byte[] data,
//			   																											com.parse.ParseException e) {
//			   																										if(e==null){
//			   																											Log.i("File","B");
//			   																											try {
//			   																												Log.i("File","C");
//			   																												File newFile = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),alarmPath);
//			   																												OutputStream os = new FileOutputStream(newFile);
//			   																												os.write(data);
//			   																												os.close();
//			   																											} catch (FileNotFoundException e1) {
//			   																												Log.i("ALARM FILE LOADED","FALSE");
//			   																												e1.printStackTrace();
//			   																											} catch(IOException e1){
//			   																												e1.printStackTrace();
//			   																											}
//			   																										}else{
//			   																											Log.i("File",e.getMessage());
//			   																										}
//			   																									}
//			   																								});
//			   																							}
//			   																						}
//			   																						
//			   																					}
//			   																				});
//			   																			} catch (Exception e10) {
//			   																				// TODO Auto-generated catch block
//			   																				e10.printStackTrace();
//			   																			}
//			   																			//ended
//			   																			
//			   																			
//			   																		} 
//			   																
//			   															}
//			   															
//			   															}
//			   															
//			   														}
//			   														
//			   													}
//			   												} catch (Exception e1) {
//			   													// TODO Auto-generated catch block
//			   													e1.printStackTrace();
//			   												}
//
//			   											} else {
//			   												Log.i("Parse", e.getMessage());
//			   											}
//
//			   										}
//			   									});
//			   								
//			   								
//			   								break;
//			   								
//			   								
//			   								}
//			   								
//			   							
//			   								
//			   							}
//			   							
//			   						} catch (Exception e14) {
//			   							// TODO Auto-generated catch block
//			   							e14.printStackTrace();
//			   						}
//			   					}else{
//			   						Log.i("Parse",e.getMessage());
//			   						//System.out.println(e.printStackTrace());
//			   					}
//			   				}
//			   			});
//			   		}
//			   		
//			    	  break;
//			      case 5:
//			    	//Friend request sent
//			        Log.i("Parse","Friend request");	  
//					Toast.makeText(HomeActivity.this,"Notification type 5", Toast.LENGTH_LONG).show();
//					link = new Intent(HomeActivity.this,NotificationActivityNew.class);
//					extras.putString("alarmid", json.getString("reference3"));
//					extras.putBoolean("onlyone", true);
//					link.putExtras(extras);
//					startActivity(link);
//					      
//			    	  break;
//			      case 6:
//			    	//Surprise Friend request sent
//			    	  Log.i("Parse","Surprise Friend request");	  
//						Toast.makeText(HomeActivity.this,"Notification type 6", Toast.LENGTH_LONG).show();
//						link = new Intent(HomeActivity.this,NotificationActivityNew.class);
//						extras.putString("alarmid", json.getString("reference4"));
//						extras.putBoolean("onlyone", true);
//						link.putExtras(extras);
//						startActivity(link);
//			    	  break;
//			      case 7:
//			    	//Accept alarm request
//			    	  Log.i("Parse","Acccept ALarm");	
//			    	    Toast.makeText(HomeActivity.this,"Notification type 7", Toast.LENGTH_LONG).show();
//			    	 	link =new Intent(HomeActivity.this, NotificationActivityNew.class);											
//						extras.putString("alarmid",json.getString("reference"));
//						extras.putBoolean("onlyone",true);														
//						link.putExtras(extras);
//						startActivity(link);
//			    	  
//			    	  break;
//			      case 8:
//			    	  //Edit the alarm
//			    	  Log.i("Parse","Edit ALarm");	
//			    	    Toast.makeText(HomeActivity.this,"Notification type 8", Toast.LENGTH_LONG).show();
//			    	 	link =new Intent(HomeActivity.this, NotificationActivityNew.class);											
//						extras.putString("alarmid",json.getString("reference"));
//						extras.putBoolean("onlyone",true);														
//						link.putExtras(extras);
//						startActivity(link);
//			    	  break;
//			      case 9:
//			    	  //Remove  alarm
//
//			    	  Log.i("Parse","Remove ALarm");	
//			    	    Toast.makeText(HomeActivity.this,"Notification type 9", Toast.LENGTH_LONG).show();
//			    	        link =new Intent(HomeActivity.this, NotificationActivityNew.class);											
//						   extras.putString("alarmid",json.getString("reference"));
//						   extras.putBoolean("onlyone",true);														
//						   link.putExtras(extras);
//						   startActivity(link);
//						   
//				   		if(ParseUser.getCurrentUser() != null){
//				   			ParseQuery<ParseObject> query = ParseQuery.getQuery("Activity");
//				   			query.whereEqualTo("followers",ParseUser.getCurrentUser().getObjectId());
//				   			//query.whereNotEqualTo("readers", ParseUser.getCurrentUser().getObjectId());
//				   			query.include("creator");
//				   			query.include("reference");			   			
//				   			query.orderByDescending("createdAt");
//				   			query.setLimit(10);
//				   			query.findInBackground(new FindCallback<ParseObject>() {
//				   				
//				   				@Override
//				   				public void done(List<ParseObject> objects, ParseException e) {
//				   					if(e==null){
//				   						try {
//				   							//Data found
//				   							Log.i("Parse","Data present");
//				   							int k = 1;
//				   							for(ParseObject o:objects){
//				   							parseOO = o;
//				   								//notificationCount++;
//				   								int notificationType = o.getInt("notificationtype");
//				   								
//				   								createNotification = true;
//				   								Intent intent = null;
//				   								
//				   								
//				   								extras = new Bundle();
//				   								
//				   							//Remove from DB
//												DatabaseHandler db = new DatabaseHandler(HomeActivity.this);
//												db.deleteAlarm(Integer.parseInt(parseOO.getParseObject("reference").getString("requestcode")));
//				   					    	  
//				   								switch(notificationType){
//				   								case 9:
//				   									try {
//					   									List<String> arr = parseOO.getParseObject("reference").getList("Hideit");
//					   									
//					   									if(arr != null){
//					   										Log.i("Actions","A"+arr.toString()+"request code is"+Integer.parseInt(parseOO.getParseObject("reference").getString("requestcode"))+"current user"+ParseUser.getCurrentUser().getObjectId().toString());
//					   										
//					   										if(arr.contains(ParseUser.getCurrentUser().getObjectId().toString())){
//					   											Intent intent1 = new Intent(HomeActivity.this, AlaramActivityNew.class);
//					   											PendingIntent pendingIntent = PendingIntent.getActivity(HomeActivity.this,Integer.parseInt(parseOO.getParseObject("reference").getString("requestcode")), intent1, PendingIntent.FLAG_CANCEL_CURRENT);
//					   											AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
//					   											alarmManager.cancel(pendingIntent);
//					   											
//					   											Log.i("Actions","B");
//					   										}
//					   									}
//					   								} catch (Exception e13) {
//					   									// TODO Auto-generated catch block
//					   									e13.printStackTrace();
//					   								}
//				   									
//				   								break;
//				   								
//				   								
//				   								}
//				   								
//				   							
//				   								
//				   							}
//				   							
//				   						} catch (Exception e14) {
//				   							// TODO Auto-generated catch block
//				   							e14.printStackTrace();
//				   						}
//				   					}else{
//				   						Log.i("Parse",e.getMessage());
//				   						//System.out.println(e.printStackTrace());
//				   					}
//				   				}
//				   			});
//				   		}
//						
//				
//			    	  break;
//			      case 10:
//			    	  //Added to group
//			    	    Log.i("Parse","Group");	
//			    	    Toast.makeText(HomeActivity.this,"Notification type 10", Toast.LENGTH_LONG).show();
//			    	 	link =new Intent(HomeActivity.this, NotificationActivityNew.class);											
//						extras.putString("alarmid",json.getString("reference5"));
//						extras.putBoolean("onlyone",true);														
//						link.putExtras(extras);
//						startActivity(link);
//			    	  break;
//			      case 11:
//			    	  //Accept Friend request
//			    	  Log.i("Parse","Accept Friend request");	  
//						Toast.makeText(HomeActivity.this,"Notification type 11", Toast.LENGTH_LONG).show();
//						link = new Intent(HomeActivity.this,NotificationActivityNew.class);
//						extras.putString("alarmid", json.getString("reference3"));
//						extras.putBoolean("onlyone", true);
//						link.putExtras(extras);
//						startActivity(link);
//			    	  
//			    	  break;
//			      case 12:
//			    	  //Accept Surprise Friend Request
//			    	  Log.i("Parse","Accept Surprise Friend request");	  
//						Toast.makeText(HomeActivity.this,"Notification type 12", Toast.LENGTH_LONG).show();
//						link = new Intent(HomeActivity.this,NotificationActivityNew.class);
//						extras.putString("alarmid", json.getString("reference4"));
//						extras.putBoolean("onlyone", true);
//						link.putExtras(extras);
//						startActivity(link);
//			    	  
//			    	  break;
//			      case 13:
//			    	  //Edit Surprise Friend Request
//			    	  Log.i("Parse","Edit Surprise Friend request");	  
//						Toast.makeText(HomeActivity.this,"Notification type 13", Toast.LENGTH_LONG).show();
//				   		if(ParseUser.getCurrentUser() != null){
//				   			ParseQuery<ParseObject> query = ParseQuery.getQuery("Activity");
//				   			query.whereEqualTo("followers",ParseUser.getCurrentUser().getObjectId());
//				   			//query.whereNotEqualTo("readers", ParseUser.getCurrentUser().getObjectId());
//				   			query.include("creator");
//				   			query.include("reference");			   			
//				   			query.orderByDescending("createdAt");
//				   			query.setLimit(10);
//				   			query.findInBackground(new FindCallback<ParseObject>() {
//				   				
//				   				@Override
//				   				public void done(List<ParseObject> objects, ParseException e) {
//				   					if(e==null){
//				   						try {
//				   							//Data found
//				   							Log.i("Parse","Data present");
//				   							int k = 1;
//				   							for(ParseObject o:objects){
//				   								final ParseObject parseO = o;
//				   								//notificationCount++;
//				   								int notificationType = o.getInt("notificationtype");
//				   								
//				   								createNotification = true;
//				   								Intent intent = null;
//				   								
//				   								
//				   								extras = new Bundle();
//									
//				   								switch(notificationType){
//				   								case 13:
//				   									
//				   									ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");			   									
//				   									query.whereEqualTo("user", ParseUser.getCurrentUser());
//				   									query.findInBackground(new FindCallback<ParseObject>() {
//				   										@Override
//				   										public void done(List<ParseObject> objects, ParseException e) {
//
//				   											if (e == null) {
//				   												try {
//				   													if (objects.size() > 0) {
//				   														for (ParseObject obj : objects) {														 
//				   															connectedSurpriseF = obj.getJSONArray("connectedSF");
//				   															//Log.i("Parse", "connectedSurpriseF"+connectedSurpriseF.toString());
//				   															break;
//				   														}
//				   														
//				   														if(connectedSurpriseF!=null)
//				   														{
//				   															//Log.i("File", "connected"+ connectedSurpriseF.toString());
//				   															if (connectedSurpriseF.length() > 0) {
//				   															for (int i = 0; i < connectedSurpriseF.length(); i++) {
//				   																
//				   																	//if (connectedSurpriseF != null) {
//				   																		Log.i("File", "in if \n" +connectedSurpriseF.get(i)+"\n" + parseO.getParseObject("reference").getString("creatorplain"));
//				   																		if (connectedSurpriseF.get(i).equals(parseO.getParseObject("reference").getString("creatorplain"))) {//ParseUser.getCurrentUser().getObjectId().toString()
//
//				   																			
//				   																			
//				   																			
//				   																			//started
//				   																			try {
//				   																		 
//				   																				createNotification = false;
//				   																				
//				   																				//parseO.addAllUnique("readers",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
//				   																				
//				   																				parseO.put("readers", Arrays.asList(ParseUser.getCurrentUser().getObjectId()));//ParseUser.getCurrentUser().getObjectId().toString()
//				   																				parseO.add("readers", parseO.getParseObject("reference").getString("creatorplain"));																				
//				   																				parseO.saveInBackground();
//				   																				
//				   																				ParseQuery<ParseObject> not = ParseQuery.getQuery("alarms");
//				   																				not.getInBackground(parseO.getParseObject("reference").getObjectId(), new GetCallback<ParseObject>() {
//				   																					@Override
//				   																					public void done(ParseObject object, com.parse.ParseException e) {
//				   																						object.addAllUnique("acceptedpeople",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
//				   																						object.addAllUnique("notificationactions",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
//				   																						object.saveInBackground();//saveEventually();();
//				   																						//Set alarm
//				   																						Intent intent = new Intent(HomeActivity.this,AlaramActivityNew.class);
//				   																						Bundle tempData = new Bundle();
//				   																						tempData.putString("alarm_category",object.getString("category"));
//				   																						tempData.putString("alarm_title", object.getString("title"));
//				   																						tempData.putBoolean("recorded",object.getBoolean("recorded"));
//				   																						tempData.putInt("alarmrequestcode",Integer.parseInt(object.getString("requestcode")));
//				   																						tempData.putString("alarmpath",object.getString("media"));
//				   																						tempData.putString("alarmfriends",object.getString("connectedlist"));
//				   																						if (object.getString("alarm_repeat_type") != null)
//				   																						tempData.putString("alarm_repeat_type",object.getString("alarm_repeat_type"));
//				   																						
//				   																						if (object.getString("alarm_repeat_type") != null)
//				   																						selectedAlarmType = object.getString("alarm_repeat_type");
//				   																					//Alarm for other surprise edit user
//				   																						Alarams al = new Alarams();
//				   																						al.setCategory(object.getString("category"));
//				   																						al.setTitle(object.getString("title"));
//				   																						if (object.getBoolean("recorded") == true) {
//				   																							al.setRecorded("true");
//				   																						} else {
//				   																							al.setRecorded("false");
//				   																						}
//				   																						al.setRequestcode(String.valueOf(object.getString("requestcode")));
//				   																						al.setPath(object.getString("media"));
//				   																						al.setFriends(object.getString("connectedlist"));
//				   																						JSONArray jArray = object.getJSONArray("connected");
//				   																						try {
//				   																							al.setFriendsid(jArray.getString(0));
//				   																						} catch (JSONException e3) {
//				   																							// TODO Auto-generated catch block
//				   																							e3.printStackTrace();
//				   																						}
//				   																						
//				   																						al.setRepeattype(selectedAlarmType);
//				   																						
//				   																						al.setMilli(String.valueOf(object.getLong("alarmtimemilli")));
//				   																						
//				   																						String time = object.getString("alarmTime");
//				   																						String split[] = time.split("\\s");
//				   																						String date = split[0];
//				   																						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault());
//				   																				
//				   																						//String cale = sdf.format(date);
//				   																						al.setAtime(date);
//				   																						Toast.makeText(HomeActivity.this,"millllllli" + object.getLong("alarmtimemilli")+date, Toast.LENGTH_LONG).show();
//
//				   																						DatabaseHandler db = new DatabaseHandler(HomeActivity.this);
//				   																						db.updateAlarm(al);
//				   																						//ends
//				   																						intent.putExtras(tempData);
//				   																						PendingIntent pi = PendingIntent.getActivity(HomeActivity.this,Integer.parseInt(object.getString("requestcode")),intent,PendingIntent.FLAG_CANCEL_CURRENT);
//				   																						AlarmManager am = (AlarmManager)HomeActivity.this.getSystemService(Context.ALARM_SERVICE);
//				   																						am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
//				   																						
//				   																						Calendar calendar = Calendar.getInstance();
//				   																						calendar.setTimeInMillis(calendar.getTimeInMillis());
//				   																						calendar.add(Calendar.SECOND, 30);
//				   																						if (selectedAlarmType.equalsIgnoreCase("Once")) {
//				   																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
//				   																							
//				   																						} else if (selectedAlarmType.equalsIgnoreCase("Daily")) {
//
//				   																							calendar.add(Calendar.DAY_OF_WEEK, 1);
//				   																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
//				   																							// am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,calendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pi);
//				   																						} else if (selectedAlarmType.equalsIgnoreCase("Weekly")) {
//
//				   																							calendar.add(Calendar.WEEK_OF_MONTH, 1);
//				   																							am.set(AlarmManager.RTC_WAKEUP,
//				   																									object.getLong("alarmtimemilli"), pi);
//				   																						} else if (selectedAlarmType.equalsIgnoreCase("Monthly")) {
//
//				   																							calendar.add(Calendar.MONTH, 1);
//				   																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
//				   																						}
//				   																						
//				   																					
//				   																						
//				   																						//Download audio if required
//				   																						final String alarmPath = object.getString("media");
//				   																						Log.i("File path",alarmPath);
//				   																						if(object.getBoolean("recorded")){
//				   																							ParseFile p = object.getParseFile("audioclip");
//				   																							if(p!=null){
//				   																								Log.i("File","A");
//				   																								p.getDataInBackground(new GetDataCallback() {
//				   																									
//				   																									@Override
//				   																									public void done(byte[] data,
//				   																											com.parse.ParseException e) {
//				   																										if(e==null){
//				   																											Log.i("File","B");
//				   																											try {
//				   																												Log.i("File","C");
//				   																												File newFile = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),alarmPath);
//				   																												OutputStream os = new FileOutputStream(newFile);
//				   																												os.write(data);
//				   																												os.close();
//				   																											} catch (FileNotFoundException e1) {
//				   																												Log.i("ALARM FILE LOADED","FALSE");
//				   																												e1.printStackTrace();
//				   																											} catch(IOException e1){
//				   																												e1.printStackTrace();
//				   																											}
//				   																										}else{
//				   																											Log.i("File",e.getMessage());
//				   																										}
//				   																									}
//				   																								});
//				   																							}
//				   																						}
//				   																						
//				   																					}
//				   																				});
//				   																			} catch (Exception e10) {
//				   																				// TODO Auto-generated catch block
//				   																				e10.printStackTrace();
//				   																			}
//				   																			//ended
//				   																			
//				   																			
//				   																		} 
//				   																
//				   															}
//				   															
//				   															}
//				   															
//				   														}
//				   														
//				   													}
//				   												} catch (Exception e1) {
//				   													// TODO Auto-generated catch block
//				   													e1.printStackTrace();
//				   												}
//
//				   											} else {
//				   												Log.i("Parse", e.getMessage());
//				   											}
//
//				   										}
//				   									});
//				   								
//				   								
//				   								break;
//				   								
//				   								
//				   								}
//				   								
//				   							
//				   								
//				   							}
//				   							
//				   						} catch (Exception e14) {
//				   							// TODO Auto-generated catch block
//				   							e14.printStackTrace();
//				   						}
//				   					}else{
//				   						Log.i("Parse",e.getMessage());
//				   						//System.out.println(e.printStackTrace());
//				   					}
//				   				}
//				   			});
//				   		}
//				   		
//			    	  break;
//			    	  
//			      case 14:
//			  		/////////////////////////
//			    	  //Edit Surprise Friend Request
//			    	  Log.i("Parse","Remove Surprise Friend request");	  
//						Toast.makeText(HomeActivity.this,"Notification type 14", Toast.LENGTH_LONG).show();
//				   		if(ParseUser.getCurrentUser() != null){
//				   			ParseQuery<ParseObject> query = ParseQuery.getQuery("Activity");
//				   			query.whereEqualTo("followers",ParseUser.getCurrentUser().getObjectId());
//				   			//query.whereNotEqualTo("readers", ParseUser.getCurrentUser().getObjectId());
//				   			query.include("creator");
//				   			query.include("reference");			   			
//				   			query.orderByDescending("createdAt");
//				   			query.setLimit(10);
//				   			query.findInBackground(new FindCallback<ParseObject>() {
//				   				
//				   				@Override
//				   				public void done(List<ParseObject> objects, ParseException e) {
//				   					if(e==null){
//				   						try {
//				   							//Data found
//				   							Log.i("Parse","Data present");
//				   							int k = 1;
//				   							for(ParseObject o:objects){
//				   							parseOO = o;
//				   								//notificationCount++;
//				   								int notificationType = o.getInt("notificationtype");
//				   								
//				   								createNotification = true;
//				   								Intent intent = null;
//				   								
//				   								
//				   								extras = new Bundle();
//				   								
//				   							//Remove from DB
//												DatabaseHandler db = new DatabaseHandler(HomeActivity.this);
//												db.deleteAlarm(Integer.parseInt(parseOO.getParseObject("reference").getString("requestcode")));
//				   					    	  
//				   								switch(notificationType){
//				   								case 14:
//				   									try {
//					   									List<String> arr = parseOO.getParseObject("reference").getList("Hideit");
//					   									
//					   									if(arr != null){
//					   										Log.i("Actions","A"+arr.toString()+"request code is"+Integer.parseInt(parseOO.getParseObject("reference").getString("requestcode"))+"current user"+ParseUser.getCurrentUser().getObjectId().toString());
//					   										
//					   										if(arr.contains(ParseUser.getCurrentUser().getObjectId().toString())){
//					   											Intent intent1 = new Intent(HomeActivity.this, AlaramActivityNew.class);
//					   											PendingIntent pendingIntent = PendingIntent.getActivity(HomeActivity.this,Integer.parseInt(parseOO.getParseObject("reference").getString("requestcode")), intent1, PendingIntent.FLAG_CANCEL_CURRENT);
//					   											AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
//					   											alarmManager.cancel(pendingIntent);
//					   											
//					   											Log.i("Actions","B");
//					   										}
//					   									}
//					   								} catch (Exception e13) {
//					   									// TODO Auto-generated catch block
//					   									e13.printStackTrace();
//					   								}
//				   									ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");			   									
//				   									query.whereEqualTo("user", ParseUser.getCurrentUser());
//				   									query.findInBackground(new FindCallback<ParseObject>() {
//				   										@Override
//				   										public void done(List<ParseObject> objects, ParseException e) {
//
//				   											if (e == null) {
//				   												try {
//				   													if (objects.size() > 0) {
//				   														for (ParseObject obj : objects) {														 
//				   															connectedSurpriseF = obj.getJSONArray("connectedSF");
//				   															//Log.i("Parse", "connectedSurpriseF"+connectedSurpriseF.toString());
//				   															break;
//				   														}
//				   														
//				   														if(connectedSurpriseF!=null)
//				   														{
//				   															//Log.i("File", "connected"+ connectedSurpriseF.toString());
//				   															if (connectedSurpriseF.length() > 0) {
//				   															for (int i = 0; i < connectedSurpriseF.length(); i++) {
//				   																
//				   																	//if (connectedSurpriseF != null) {
//				   																		Log.i("File", "in if \n" +connectedSurpriseF.get(i)+"\n" + parseOO.getParseObject("reference").getString("creatorplain"));
//				   																		if (connectedSurpriseF.get(i).equals(parseOO.getParseObject("reference").getString("creatorplain"))) {//ParseUser.getCurrentUser().getObjectId().toString()
//
//				   																			
//				   																			
//				   																			
//				   																			//started
//				   																			try {
//				   																		 
//				   																				createNotification = false;
//				   																				
//				   																				//parseOO.addAllUnique("readers",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
//				   																				
//				   																				parseOO.put("readers", Arrays.asList(ParseUser.getCurrentUser().getObjectId()));//ParseUser.getCurrentUser().getObjectId().toString()
//				   																				parseOO.add("readers", parseOO.getParseObject("reference").getString("creatorplain"));																				
//				   																				parseOO.saveInBackground();
//				   																			
//				   																			} catch (Exception e10) {
//				   																				// TODO Auto-generated catch block
//				   																				e10.printStackTrace();
//				   																			}
//				   																			//ended
//				   																			
//				   																			
//				   																		} 
//				   																		else{
//				   																		 	link =new Intent(HomeActivity.this, NotificationActivityNew.class);											
//				   																			extras.putString("alarmid",json.getString("reference"));
//				   																			extras.putBoolean("onlyone",true);														
//				   																			link.putExtras(extras);
//				   																			startActivity(link);
//				   																		}
//				   																
//				   															}
//				   															
//				   															}
//				   															else{
//				   															 	link =new Intent(HomeActivity.this, NotificationActivityNew.class);											
//				   																extras.putString("alarmid",json.getString("reference"));
//				   																extras.putBoolean("onlyone",true);														
//				   																link.putExtras(extras);
//				   																startActivity(link);
//				   															}
//				   															
//				   														}
//				   														else{
//				   														 	link =new Intent(HomeActivity.this, NotificationActivityNew.class);											
//				   															extras.putString("alarmid",json.getString("reference"));
//				   															extras.putBoolean("onlyone",true);														
//				   															link.putExtras(extras);
//				   															startActivity(link);
//				   														}
//				   														
//				   													}
//				   												} catch (Exception e1) {
//				   													// TODO Auto-generated catch block
//				   													e1.printStackTrace();
//				   												}
//
//				   											} else {
//				   												Log.i("Parse", e.getMessage());
//				   											}
//
//				   										}
//				   									});
//				   								
//				   								
//				   								break;
//				   								
//				   								
//				   								}
//				   								
//				   							
//				   								
//				   							}
//				   							
//				   						} catch (Exception e14) {
//				   							// TODO Auto-generated catch block
//				   							e14.printStackTrace();
//				   						}
//				   					}else{
//				   						Log.i("Parse",e.getMessage());
//				   						//System.out.println(e.printStackTrace());
//				   					}
//				   				}
//				   			});
//				   		}
//						///////////////////////////
//			    	  break;
//			      }
//			      
//			     
//			      Log.d("HomeActivity", "got action " + action + " on channel " + channel + " with:");
//			      
//			      Iterator itr = json.keys();
//			      while (itr.hasNext()) {
//			        String key = (String) itr.next();
//			        Log.d("HomeActivity", "..." + key + " => " + json.getString(key));
//			      }
//			}
//			}
//		    } catch (JSONException e) {
//		      Log.d("HomeActivity", "JSONException: " + e.getMessage());
//		    }
		configParse();
		if (currentUser == null) {
			// User not logged in
			Intent i = new Intent(this, SplashActivity.class);
			startActivity(i);
		} else {
		
		mTitle = mDrawerTitle = "F-Up";

		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// nav drawer icons from resources
		navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		navDrawerItems = new ArrayList<NavList>();

		// adding nav drawer items to array
		// Feed
		navDrawerItems.add(new NavList(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
		// Friends
		navDrawerItems.add(new NavList(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
	/*	// Groups
		navDrawerItems.add(new NavList(navMenuTitles[2], navMenuIcons
				.getResourceId(2, -1)));*/
		
		
		/*// Notifications
		navDrawerItems.add(new NavList(navMenuTitles[2], navMenuIcons
				.getResourceId(2, -1)));
		// Create
		navDrawerItems.add(new NavList(navMenuTitles[3], navMenuIcons
				.getResourceId(3, -1)));*/
		
		// Invite
		navDrawerItems.add(new NavList(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
		
	/*	// Settings
		navDrawerItems.add(new NavList(navMenuTitles[5], navMenuIcons
				.getResourceId(5, -1)));
		
		*/
		
		
		//Start Code for getting profile image
//		String imageUrl = "image";
//		p = ParseUser.getCurrentUser().getParseFile("profilethumbnail");
//		if(p!=null){
//			String url = p.getUrl();
//			//Log.i("FB IMAGE",url);
//			imageUrl = url;
//		}else{
//			if(ParseUser.getCurrentUser().get("facebookid") != null && ParseUser.getCurrentUser().get("facebookid") != "") {
//				t = ParseUser.getCurrentUser().get("facebookid").toString();
//				if((t!=null)||(t!="")){
//					imageUrl = "https://graph.facebook.com/"
//							+ ParseUser.getCurrentUser().get("facebookid").toString()
//							+ "/picture/?type=square";
//				}
//			}
//		}
//		Picasso.with(HomeActivity.this).load(imageUrl).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into((ImageView)findViewById(R.id.ivprofImage));
		//End Code for getting profile image

		
		// Profile
		navDrawerItems.add(new NavList(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
		//navDrawerItems.add(new NavList(navMenuTitles[3], imageUrl.toString());
		
		// Logout
				
		navDrawerItems.add(new NavList(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
		
		navDrawerItems.add(new NavList(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
		navDrawerItems.add(new NavList(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));
		navDrawerItems.add(new NavList(navMenuTitles[7], navMenuIcons.getResourceId(7, -1)));
		
		// Reset Password
		/*navDrawerItems.add(new NavList(navMenuTitles[7], navMenuIcons
						.getResourceId(4, -1)));*/
		
		/*// Pages
		navDrawerItems.add(new NavList(navMenuTitles[4], navMenuIcons
				.getResourceId(4, -1)));
		// What's hot, We will add a counter here
		navDrawerItems.add(new NavList(navMenuTitles[5], navMenuIcons
				.getResourceId(5, -1), true, "50+"));
	*/
		// Recycle the typed array
		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavAdapter(HomeActivity.this, navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}
		}
	}
	
	private void displayView(int position) {
		Bundle ext = new Bundle();
		ext = getIntent().getExtras();
		Fragment fragment = null;
        Bundle args = new Bundle();
		switch(position){
		case 0:
			fragment = new FeedFragment();
			Bundle bundle=new Bundle();

			if(ext != null){
				//We have some data to show
				//fragment = new FeedFragment(ext);
				//ext.setA
				//Log.i("Bundle",ext.getString("alarmid"));
				try {
					fragment.setArguments(ext);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		break;
		case 1:
			mDrawerLayout.closeDrawer(mDrawerList);
			Intent friends = new Intent(HomeActivity.this,FriendsGroup.class);
			startActivity(friends);
		break;
		/*case 2:
			mDrawerLayout.closeDrawer(mDrawerList);
			Intent groups = new Intent(HomeActivity.this,Groups.class);
			startActivity(groups);
		break;*/
		/*case 2:
			mDrawerLayout.closeDrawer(mDrawerList);
			Intent notification = new Intent(HomeActivity.this,NotificationActivityNew.class);
			startActivity(notification);
		break;
		case 3:
			mDrawerLayout.closeDrawer(mDrawerList);
			Intent i = new Intent(HomeActivity.this,CreateNewAlaramActivityNew.class);
			Bundle b = new Bundle();
			b.putBoolean("go_back",true);
			i.putExtras(b);
			startActivity(i);
		break;*/
		case 2:
			mDrawerLayout.closeDrawer(mDrawerList);
			Intent a = new Intent(HomeActivity.this,InvitePeople.class);
			Bundle bu = new Bundle();
			bu.putBoolean("go_back",true);
			bu.putBoolean("isInvite",true);
			a.putExtras(bu);
			startActivity(a);
		break;
		
		/*	
		  case 5:
		  mDrawerLayout.closeDrawer(mDrawerList);
			Intent settings = new Intent(HomeActivity.this,SettingsActivity.class);
			startActivity(settings);
		break;*/
		case 3:
			mDrawerLayout.closeDrawer(mDrawerList);
			Intent profile = new Intent(HomeActivity.this,ProfileActivity.class);
			profile.putExtra("userpath", "home");
			startActivity(profile);
		break;
		case 4:
			
		/*	
			
			ParseFacebookUtils.unlinkInBackground(currentUser, new SaveCallback() {
				  @Override
				  public void done(ParseException ex) {
				    if (ex == null) {
				      Log.d("MyApp", "The user is no longer associated with their Facebook account.");
				    }
				  }
				});
			
			*/
			
			if (ParseFacebookUtils.getSession() != null) {
				ParseFacebookUtils.getSession().closeAndClearTokenInformation();
			}
			 ParseUser.logOut();
			// ParseUser.getCurrentUser().logOut();
         //Commented
		/*	if (ServiceRunningCheck.isMyServiceRunning(HomeActivity.this,NotificationService.class) == true) {
				stopService(new Intent(getBaseContext(), NotificationService.class));
				
			}
			 */
			 
			 
			 
			 
			/*ParseFacebookUtils.unlinkInBackground(currentUser, new SaveCallback() {
				  @Override
				  public void done(ParseException ex) {
				    if (ex == null) {
				      Log.d("MyApp", "The user is no longer associated with their Facebook account.");
				    }
				  }
				});*/
			mDrawerLayout.closeDrawer(mDrawerList);
			Intent intent = new Intent(HomeActivity.this,SplashActivity.class);
			// Intent intent = new Intent(Intent.ACTION_MAIN);
			// intent.addCategory(Intent.CATEGORY_HOME);
			//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();
			/*
			 * Intent lg = getApplication().getBaseContext().getPackageManager()
			 * .getLaunchIntentForPackage(
			 * getApplication().getBaseContext().getPackageName());
			 * lg.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); startActivity(lg);
			 */
		break;
		/*case 7:
			Intent resetPassword = new Intent(HomeActivity.this,ResetPasswordActivity.class);
			startActivity(resetPassword);
		break;*/
		
		case 5:
			//Toast.makeText(HomeActivity.this,"Hello", Toast.LENGTH_LONG).show();
			mDrawerLayout.closeDrawer(mDrawerList);
			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
			    @Override
			    public void run() {
			    	startActivity(new Intent(HomeActivity.this,DemoActivity.class));
			    }
			}, 50);
			
			break;
		case 6:
			mDrawerLayout.closeDrawer(mDrawerList);
				Dialog d = new Dialog(this,android.R.style.Theme_Holo_Dialog);
				d.setContentView(R.layout.terms_dialog);
				d.setTitle("User Terms and Conditions");
				d.show();
				WebView web = (WebView) d.findViewById(R.id.webView);
				web.setWebViewClient(new WebViewClient());
			    web.loadUrl("http://fastrack.in/fup/tnc.html");
				Button accept = (Button) d.findViewById(R.id.button1);
				Button decline = (Button) d.findViewById(R.id.button2);
				accept.setVisibility(View.GONE);
				decline.setVisibility(View.GONE);
		
			break;
		case 7:
			mDrawerLayout.closeDrawer(mDrawerList);
			//timePickerDialog.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Light_Dialog);
			final Dialog dialog = new Dialog(this,android.R.style.Theme_Holo_Dialog);
			
			dialog.setContentView(R.layout.about);
			Button dismiss = (Button) dialog.findViewById(R.id.button1);
			dismiss.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
				
			});
			dialog.setTitle("About F-UP");
			dialog.show();
			
			break;
		
		}
		if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
 
            // update selected item and title, then close the drawer
            //mDrawerList.setItemChecked(position, true);
           // mDrawerList.setSelection(position);
            setTitle(navMenuTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
	}

	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
     
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {

//        case R.id.:
//        
//            return true;
        default:
            return false;
        }
    }

	/***
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		//menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
        //getActionBar().setTitle(mTitle);
    }
 
    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
 
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }
 
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    @Override
	protected void onDestroy() {
		super.onDestroy();
		/*FeedFragment ff = new FeedFragment();
		ff.stopRepeatingTask();
		try {
			ff.mStatusChecker.wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/
	}
    
    @Override
    protected void onResume() {
    	super.onResume();
    
    }
    
  
}
