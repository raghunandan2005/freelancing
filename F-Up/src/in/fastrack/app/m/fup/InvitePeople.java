package in.fastrack.app.m.fup;


import in.fastrack.app.m.fup.adapter.InvitePeopleAdapter;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.AlarmData;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class InvitePeople extends Activity implements TabListener {
	
	//public FragmentComm
	private ViewPager viewPager;
	private InvitePeopleAdapter mAdapter;
	private ActionBar bar;
	public Boolean isInvite = true;
	public AppConfig _config;
	Bundle extras = null;
	int FB_REQUEST_ID = 900;
	CheckConnectivity c = new CheckConnectivity();
	boolean isIntAv = false;
	
	AlarmData alarmData;
	
	public ArrayList<String> addList = null;
	
	JSONArray friendsdata = new JSONArray();
	
	//Custom Tabs
	private String[] tabs = {"Facebook","Phonebook"};
	
	@Override
	protected void onResume() {
		super.onResume();
		checkActive();
	}
	
	public void checkActive(){
		ParseUser currentUser = ParseUser.getCurrentUser();
		if(currentUser == null){
			//User not logged in
			Intent reset = new Intent(getApplicationContext(),SplashActivity.class);
			startActivity(reset);
		}
	}

	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
		
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction arg1) {
		viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
		
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.invite_friends);
		_config = new AppConfig(getApplicationContext());
		viewPager = (ViewPager) findViewById(R.id.pager);
		bar = getActionBar();
		mAdapter = new InvitePeopleAdapter(getFragmentManager());
		viewPager.setAdapter(mAdapter);
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		isIntAv = c.check(getApplicationContext());
		
		extras = getIntent().getExtras();
		if(extras != null){
			if(extras.getBoolean("go_back")){
				bar.setDisplayShowHomeEnabled(true);
				bar.setHomeButtonEnabled(true);
				bar.setDisplayHomeAsUpEnabled(true);
			}
			isInvite = extras.getBoolean("isInvite");
		}else{
			//TO-DO send back the user to first create alarm intent
		}
		_config.setInviteMode(isInvite);
		if(isInvite){
			bar.setTitle("Invite Friends");
		}else{
			bar.setTitle("Add Friends");
		} 
		for(String tab_name : tabs){
			bar.addTab(bar.newTab().setText(tab_name).setTabListener(this));
		}
		
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int position) {
				bar.setSelectedNavigationItem(position);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if(!isInvite){
		  getMenuInflater().inflate(R.menu.action_home, menu);
		  return super.onCreateOptionsMenu(menu);
		}
		return false;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch(item.getItemId()){
		
		case android.R.id.home:
			Intent i = new Intent(getApplicationContext(), HomeActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			finish();
			return true;
		case R.id.action_next:
			if (!Util.isNetworkAvailable(InvitePeople.this)) {	
				
				DialogFactory.getBeaconStreamDialog1(InvitePeople.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			} else 
			
			{

			
			if(!isInvite){
				FacebookInviteFragment.pullList();
				Log.d("FULL LIST",FacebookInviteFragment.facebookList.toString());
				friendsdata = FacebookInviteFragment.facebookList;
				long alarmParams = extras.getLong("alarmid"); //alarmid
				new CreateAlarmAsync().execute(alarmParams);
				Intent a = new Intent(getApplicationContext(),HomeActivity.class);
				a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(a);
			}
			}
			return true;
		 default:
	            return super.onOptionsItemSelected(item);
		}

	}
	
	public class CreateAlarmAsync extends AsyncTask<Long, Void, Boolean>{

		@SuppressWarnings("deprecation")
		@Override
		protected Boolean doInBackground(Long... params) {
			//Create the alarm silently
			alarmData = AlarmData.findById(AlarmData.class,params[0]);
			Random rand = new Random();
			int random = rand.nextInt(9999);
			final String requestcode = Long.toString(random)+Long.toString(params[0]);
			alarmData.setAlarmrequestcode(Integer.parseInt(requestcode));
			String friends = FacebookInviteFragment.facebookList.toString();
			if(friends.length() > 0){
				alarmData.setFriends(friends);
			}
			alarmData.save();
			
			
			final AlarmData d = AlarmData.findById(AlarmData.class,params[0]);
			Log.i("ALARM Log",d.toString());
			//Do all the setup
			//AlarmData d = AlarmData.findById(AlarmData.class,params[0]);
			//AlarmData alarmData = new AlarmData(getApplicationContext());
			Bundle bundle = new Bundle();
			bundle.putString("alarm_title",d.getAlarmtitle());
			bundle.putString("alarm_category",d.getAlarmcategory());
			bundle.putBoolean("recorded",d.isRecorded());
			bundle.putString("alarmpath",d.getAlarmpath());
			bundle.putString("alarmfriends",friends);
			bundle.putLong("alarmid",d.getId());
			bundle.putInt("alarmrequestcode",d.getAlarmrequestcode());
			Intent intent = new Intent(getApplicationContext(), AlaramActivityNew.class);
			intent.putExtras(bundle);
			Log.i("YEST","I AM IN");
			
			//Log.i("SELECTED DATE",d.getAlarmtime());
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss",Locale.getDefault());
				Date date = sdf.parse(d.getAlarmtime());
				long milli = date.getTime();
				Log.d("ALARM DATE",Long.toString(date.getTime()));
				PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),d.getAlarmrequestcode(),intent,PendingIntent.FLAG_CANCEL_CURRENT);
				AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
				am.set(AlarmManager.RTC_WAKEUP,milli, pi);
				//Save to parse server
				final ParseObject saveAlarm = new ParseObject("AlarmStore");
				saveAlarm.put("userid",ParseUser.getCurrentUser().getObjectId());
				saveAlarm.put("alarmtitle",d.getAlarmtitle());
				saveAlarm.put("alarmcategory",d.getAlarmcategory());
				saveAlarm.put("friends",friends);
				saveAlarm.put("alarmrequestid",d.getAlarmrequestcode());
				saveAlarm.put("alarmtime",d.getAlarmtime());
				saveAlarm.put("longtime",milli);
				saveAlarm.put("alarmpath",d.getAlarmpath());
				saveAlarm.put("recorded",d.isRecorded());
				saveAlarm.put("comments",0);
				saveAlarm.put("likes",0);
				saveAlarm.put("author",ParseUser.getCurrentUser());
				addList = FacebookInviteFragment.pullArray();
				final ArrayList<String> tempList = new ArrayList<String>();
				//tempList = addList;
				if(addList !=null){
					Log.i("LIST",addList.toString());
					addList.add(ParseUser.getCurrentUser().getObjectId());
					saveAlarm.put("userconnected",addList);
				}else{
					addList = new ArrayList<String>();
					addList.add(ParseUser.getCurrentUser().getObjectId());
					saveAlarm.put("userconnected",addList);
				}
				/*ParseRelation<ParseObject> relation = saveAlarm.getRelation("connected");
				ArrayList<String> ds = FacebookInviteFragment.pullObjects();
				for(int i=0;i<addList.size();i++){
					Log.i("ACCESS",addList.get(i));
					saveAlarm.put("authorset",ParseObject.createWithoutData("User",addList.get(i)));
				}*/
				saveAlarm.saveInBackground(new SaveCallback() {
					
					@Override
					public void done(com.parse.ParseException e) {
						if(e == null){
						//Save to news feed
						final ParseObject saveFeed = new ParseObject("NewsFeed");
						saveFeed.put("action","alarm");
						saveFeed.put("actionid",saveAlarm.getObjectId());
						//if(addList !=null){
							saveFeed.put("userconnected",addList);
						//}
						saveFeed.put("feedcreatedby",ParseUser.getCurrentUser().getObjectId());
						saveFeed.put("linkto",saveAlarm);
						saveFeed.saveInBackground(new SaveCallback() {
							
							@Override
							public void done(com.parse.ParseException e) {
								//Upload Audio
								if(d.isRecorded()){
									File mFile = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),d.getAlarmpath());
									try {
										Log.i("AUDIO RECORD","TRUE");
										//ParseQuery q = new ParseQuery<ParseObject>(subclass)
										int size = (int) mFile.length();
										byte[] bytes = new byte[size];
										BufferedInputStream buf = new BufferedInputStream(new FileInputStream(mFile));
										buf.read(bytes,0,bytes.length);	
										buf.close();
										ParseFile audio = new ParseFile(d.getAlarmpath(),bytes);
										audio.saveInBackground();
										saveAlarm.put("audioclip",audio);
										saveAlarm.saveInBackground();
										
									} catch (FileNotFoundException e1) {
										e1.printStackTrace();
									}catch (IOException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
									
								}
							}
						});
						JSONObject friendsobject = new JSONObject();
						if(friendsdata.length() > 0){
							ParseQuery pushQuery = ParseInstallation.getQuery();
							ParsePush push = new ParsePush();
							int set = 0;
							
							for(int k = 0; k<friendsdata.length();k++){
								try {
									friendsobject = friendsdata.getJSONObject(k);
									Log.i("FRIENDS",friendsobject.getString("userid"));
									pushQuery.whereEqualTo("myuser",friendsobject.getString("userid"));
									//tempList.add(friendsobject.getString("userid"));
									ParseObject notifications = new ParseObject("Notifications");
									notifications.put("fromid",ParseUser.getCurrentUser()); 
									notifications.put("toid",friendsobject.getString("userid"));
									notifications.put("alarmaccepted","pending");
									notifications.put("notificationtype","alarm");
									notifications.put("actionref",saveAlarm);
									notifications.put("notificationDone",false);
									notifications.saveInBackground();
									set++;
								} catch (JSONException en) {
									en.printStackTrace();
								}
							}
							if(set > 0){
								//Some data found. Now we can push and also add them to notification
								/*ParseObject notifications = new ParseObject("Notifications");
								notifications.put("fromid",ParseUser.getCurrentUser()); 
								notifications.put("toid",tempList);
								notifications.put("notificationtype","alarm");
								notifications.put("actionref",saveAlarm);
								notifications.saveInBackground();*/
								push.setQuery(pushQuery);
								//push.setMessage("You have a alarm from your friend");
								JSONObject tempAlarmData = new JSONObject();
								try {
									tempAlarmData.put("alarmid",extras.getLong("alarmid"));
									tempAlarmData.put("creatorid",ParseUser.getCurrentUser().getObjectId());
									tempAlarmData.put("alarmrequestcode",requestcode);
									tempAlarmData.put("title",d.getAlarmcategory());
									tempAlarmData.put("alert","An alarm has been created by "+ParseUser.getCurrentUser().getString("name"));
									tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivity");
									push.setData(tempAlarmData);
									//
									push.sendInBackground();
								} catch (JSONException ex) {
									ex.printStackTrace();
								}
								
							}
						}
					}
					}
				});
				
				//Send push to client users only when internet is available
				if(isIntAv){
					//Internet Available
					//Send to Users
						
				}
			} catch (ParseException e) {
				Log.d("DATE CONVERSTION","ERROR");
				e.printStackTrace();
			}
			return false;
		}
		
	}
	
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//Toast.makeText(this, "Acitivy Return",Toast.LENGTH_LONG).show();
		if (resultCode != Activity.RESULT_OK) {
	        Log.d("Activity", "Error occured during linking");
	        return;
	    }
	    if (requestCode == FB_REQUEST_ID) {
	        if (ParseFacebookUtils.getSession() != null) {
	            Log.d("Facebook", "Linking");
	           ParseFacebookUtils.finishAuthentication(requestCode,resultCode, data);
	           //Update FB ID
	           final ParseUser currentUser = ParseUser.getCurrentUser();
	           Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
	   				new Request.GraphUserCallback() {

	   					@Override
	   					public void onCompleted(GraphUser user, Response response) {
	   						if(user !=null){
	   							currentUser.put("fbid",user.getId());
	   							currentUser.saveInBackground();
	   						}
	   					}
	           });
	           request.executeAsync();
	        }
	    }
	}
	
	
}
