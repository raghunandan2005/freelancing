package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.Util;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.mobsandgeeks.saripaar.annotation.TextRule;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class LoginActivity extends Activity implements OnClickListener,ValidationListener {
	
	private AppConfig _config;
	private Preloader loader;
	
	@Required(order = 1,message="Enter a valid Username")
	@TextRule(order = 2,minLength = 4,message="Username is more than 4 characters")
	private EditText mUserName;
	
	@Required(order = 3,message="Enter your password")
	@TextRule(order = 4,minLength = 4,message="Password is more than 4 chanracters")
	private EditText mPassword;
	
	Validator validate;
	private Button mButton;
	private TextView mForgotPassword;
	
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_layout);
		
		final ActionBar bar = getActionBar();
		bar.setTitle("Login");
		
		mUserName = (EditText) findViewById(R.id.etUserName);
		mPassword = (EditText) findViewById(R.id.etPassword);
		
		mButton = (Button) findViewById(R.id.bLoginAccount);
		mButton.setOnClickListener(this);
		
		mForgotPassword = (TextView) findViewById(R.id.bForgotPassword);
		mForgotPassword.setOnClickListener(this);
		
		validate = new Validator(this);
		validate.setValidationListener(this);
		
		loader = new Preloader();
	}

	
	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		String message = failedRule.getFailureMessage();
		if(failedView instanceof EditText){
			failedView.requestFocus();
			((EditText) failedView).setError(message);
		}else{
			Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onValidationSucceeded() {
		//Check from parse
			loader.startPreloader(LoginActivity.this,"Logging IN");
			ParseUser.logInInBackground(mUserName.getText().toString(), mPassword.getText().toString(),new LogInCallback() {
				
				@Override
				public void done(ParseUser user, ParseException e) {
					if(user!=null){
						Intent i = new Intent(getApplicationContext(),HomeActivity.class);
						startActivity(i);
						finish();
					}else{
						Toast.makeText(getApplicationContext(),"Invalid Username or Password", Toast.LENGTH_LONG).show();
					}
					loader.stopLoading();
				}
			});
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.bLoginAccount:
				if (!Util.isNetworkAvailable(LoginActivity.this)) {				
					DialogFactory.getBeaconStreamDialog1(LoginActivity.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
				} else 
				
				{
		
				validate.validate();
				}
			break;
			case R.id.bForgotPassword:
				Intent i = new Intent(getApplicationContext(),ResetPasswordActivity.class);
				startActivity(i);
				finish();
			break;
		}	
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(LoginActivity.this,SplashActivity.class);
		startActivity(intent);
		finish();
	}

}
