package in.fastrack.app.m.fup;





import java.util.ArrayList;
import java.util.List;
import in.fastrack.app.m.fup.R;

import com.viewpagerindicator.CirclePageIndicator;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

public class MainActivity extends FragmentActivity {
    static final int NUM_ITEMS = 10;
    static String[] GENRES = new String[] {
    	    "Action", "Adventure", "Animation", "Children", "Comedy",
    	"Documentary", "Drama",
    	    "Foreign", "History", "Independent", "Romance", "Sci-Fi",
    	"Television", "Thriller"
    	};
    String s=null;
    public List<String> fragments= new ArrayList<String>();
    
    MyAdapter mAdapter;

    ViewPager mPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_pager);
        fragments.add( FragmentPage1.class.getName());
        fragments.add( FragmentPage2.class.getName());
        fragments.add(FragmentPage3.class.getName());
        fragments.add(FragmentPage4.class.getName());
        mAdapter = new MyAdapter(getSupportFragmentManager());

        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        CirclePageIndicator mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);
//        GradientDrawable shape =  new GradientDrawable();
//        shape.setCornerRadius(20);
//       
//        shape.setColor(Color.parseColor("#FFB770"));
//        UnderlinePageIndicator indicator = (UnderlinePageIndicator)findViewById(R.id.indicator);
//       // mIndicator = indicator;
//        indicator.setViewPager(mPager);
//        indicator.setBackgroundDrawable(shape);
//        indicator.setFades(false);
        //indicator.setSelectedColor(Color.GREEN);
        //indicator.setBackgroundColor(#FFC184);
        //indicator.setFadeDelay(1000);
        //indicator.setFadeLength(1000);
       

        // Watch for button clicks.
//        Button button = (Button)findViewById(R.id.goto_first);
//        button.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                mPager.setCurrentItem(0);
//            }
//        });
//        button = (Button)findViewById(R.id.goto_last);
//        button.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                mPager.setCurrentItem(NUM_ITEMS-1);
//            }
//        });
    }
    class MyAdapter extends FragmentPagerAdapter {
        public List<String> fragmentsA; 

        public MyAdapter(FragmentManager fm) {
            super(fm);
            fragmentsA = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            //return MyFragment.newInstance();
        //   TheTask t = new TheTask();
           Fragment f = Fragment.instantiate(MainActivity.this, fragmentsA.get(position));
//           if(fragmentsA.get(position).equals("com.example.fragmentpageradapter.FragmentPage1"))
//           {
//           Bundle args = new Bundle();
//           args.putString("key","value");
//           f.setArguments(args);
//           }
//           if(fragmentsA.get(position).equals("com.example.fragmentpageradapter.FragmentPage2"))
//           {
//           Bundle args = new Bundle();
//           args.putString("key","value2");
//           f.setArguments(args);
//           }
            return f;

        }

        @Override
        public CharSequence getPageTitle(int position) {
            //return CONTENT[position % CONTENT.length].toUpperCase();
            return fragmentsA.get(position).toUpperCase();
        }

        @Override
        public int getCount() {
           // return CONTENT.length;
            return fragmentsA.size();
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }
    class TheTask extends AsyncTask<Void,Void,Void>
    {

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			return null;
		}
    	
    }
}
