package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.adapter.FriendsAdapter;
import in.fastrack.app.m.fup.adapter.PhoneBookAdapter;
import in.fastrack.app.m.fup.adapter.FriendsAdapter.FriendListener;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.NewsFeed;
import in.fastrack.app.m.fup.model.People;
import in.fastrack.app.m.fup.model.UserFriendRelation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.MenuItem.OnActionExpandListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.ParseFacebookUtils.Permissions;

public class NewFacebookInviteFragment extends Fragment implements FriendListener{
	private AppConfig _config;
	Button mFb,mFbLogin;
	CheckConnectivity c = new CheckConnectivity();
	boolean isInternetActive = false;
	boolean loadable = false;
	ProgressBar mPhoneBookPreloader;
	//added
	ArrayList<Friend> fbList = new ArrayList<Friend>();
	ArrayList<Friend> contactsList = new ArrayList<Friend>();
	ArrayList<Friend> finalList = new ArrayList<Friend>();
	
	private Preloader preloader;
	ParseUser currentUser;
	private FriendsAdapter mAdapter ;
	ProgressBar mPbar;
	//EditText mSearch;
	ListView lv = null;
	private JSONArray friends = new JSONArray();
	private JSONArray friendsid = new JSONArray();
	
	final ArrayList<People> sList = new ArrayList<People>();
	String friendsListString = null;
	NewsFeed alarmDetails;
	ArrayList<String> selectedFriendsid = new ArrayList<String>(); 
	
	UserFriendRelation ufr = null;
	//added
	int FB_REQUEST_ID = 900;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.new_fragment_invite_layout, container,false);
		this.setHasOptionsMenu(true);
		_config = new AppConfig(getActivity().getBaseContext());
		String[] parseKeys = _config.getAppKeys("parse");
		Parse.initialize(getActivity().getBaseContext(), parseKeys[0],parseKeys[1]);
		ParseFacebookUtils.initialize(getString(R.string.app_id));
	    currentUser = ParseUser.getCurrentUser();
		isInternetActive = c.checkAndToast(getActivity(), "No Internet Connection");
		lv = (ListView)  root.findViewById(R.id.socialList);
		
		
		TextView tv = new TextView(getActivity());
		tv.setText(" No Phone Contacts");
		
		mAdapter = new FriendsAdapter(getActivity(),R.layout.people_item,finalList,this, friends, ufr,"friends");
		lv.setEmptyView(tv);
		lv.setAdapter(mAdapter);
		
		mFb = (Button) root.findViewById(R.id.mFb);
		mFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	//Fragment
				if (!Util.isNetworkAvailable(getActivity())) {	
			
			     DialogFactory.getBeaconStreamDialog1(getActivity(), getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
		       } else 
		
		       {
            	sendRequestDialog();
		       }
            }
        });
		mFbLogin = (Button) root.findViewById(R.id.mFbLogin);
		mFbLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	
            	
            	
            	
            	//Fragment
				if (!Util.isNetworkAvailable(getActivity())) {	
			
			     DialogFactory.getBeaconStreamDialog1(getActivity(), getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
		       } else 
		
		       {
            	
            	//sendRequestDialog();
           	//myFbLogin();
             	/*Toast.makeText(getActivity(), "FB Button Click",
				Toast.LENGTH_LONG).show();*/
		if (!ParseFacebookUtils.isLinked(currentUser)) {
			// Current User not linked, so link it
			/*Toast.makeText(getActivity(), "FB Not Linked",
					Toast.LENGTH_LONG).show();*/
			ParseFacebookUtils.link(currentUser, Arrays.asList("email","user_birthday", Permissions.Friends.ABOUT_ME,Permissions.Friends.BIRTHDAY), getActivity(),FB_REQUEST_ID, new SaveCallback() {

						@Override
						public void done(ParseException e) {
							if (e == null) {
								// Toast.makeText(getActivity(),
								// "FB Connected",Toast.LENGTH_LONG).show();
								//checkFbEnabled();
								
								
								if(Util.isFbUser()){ 
									final Boolean[] parms = {true};
									new LoadSocialAsync().execute(parms);
									mFb.setVisibility(View.VISIBLE);
									mFbLogin.setVisibility(View.GONE);
									//Log.i("list", "u r in iffffffffffffffffffffffff");
								} else {
									//Get Contacts list of app users who are friends
								//	new LoadContactsAsync().execute();
									try {
										preloader.stopLoading();
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
									mFb.setVisibility(View.GONE);
									mFbLogin.setVisibility(View.VISIBLE);
									//Log.i("list", "u r in elseeeeeeeeeeeeeeeeeeee");
								}
								
								
								
							} else {
								e.printStackTrace();
							}
						}
					});
		} else {
			/*Toast.makeText(getActivity(), "FB Already Linked",
					Toast.LENGTH_LONG).show();*/
		}
		
            	
            }  	
            }

			
        });
		//checkFbEnabled();
			preloader = new Preloader();
			preloader.startPreloader(getActivity(),"Fetching Your Friends");
		

		mPbar = (ProgressBar)  root.findViewById(R.id.pbPhoneBook);

	
		
		if (!Util.isNetworkAvailable(getActivity())) {				
			DialogFactory.getBeaconStreamDialog1(getActivity(), getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
		} else 
		
		{

		
		if(Util.isFbUser()){ 
			final Boolean[] parms = {true};
			new LoadSocialAsync().execute(parms);
			mFb.setVisibility(View.VISIBLE);
			mFbLogin.setVisibility(View.GONE);
			//Log.i("list", "u r in iffffffffffffffffffffffff");
		} else {
			//Get Contacts list of app users who are friends
		//	new LoadContactsAsync().execute();
			try {
				preloader.stopLoading();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mFb.setVisibility(View.GONE);
			mFbLogin.setVisibility(View.VISIBLE);
			//Log.i("list", "u r in elseeeeeeeeeeeeeeeeeeee");
		}
		
		
		
		
		}
		
		
		
		try {
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
			query.whereEqualTo("user", ParseUser.getCurrentUser());
			query.findInBackground(new FindCallback<ParseObject>() {
				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					if(e==null)
					{
					if(objects.size() > 0) {
						
						for(ParseObject obj : objects) { 
							ufr = new UserFriendRelation();
							ufr.setPendingFriends(obj.getJSONArray("pendingF"));
							ufr.setConnectedGroups(obj.getJSONArray("connectedG"));
							ufr.setRequestedFriends(obj.getJSONArray("requestedF"));
							ufr.setConnectedFriends(obj.getJSONArray("connectedF"));						
							ufr.setUserId(ParseUser.getCurrentUser().getObjectId().toString());
							break;
						}
						}
						else{
							
						}
					}
				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				preloader.stopLoading();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		//added
		
		
		
		
		
		return root;
	}
/*	
	private void myFbLogin() {
		// TODO Auto-generated method stub
		

		
		if (!Util.isNetworkAvailable(getActivity())) {				
			DialogFactory.getBeaconStreamDialog1(getActivity(), getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
		} else 
		
		{

	
		
		
		preloader.startPreloader(getActivity(),"Connecting Facebook");
		ParseFacebookUtils.logIn(Arrays.asList("email", "user_birthday","user_friends"),getActivity(),new LogInCallback() {
							
							@Override
							public void done(ParseUser user, ParseException e) {
								//ll.setVisibility(View.GONE);
								
								if (user == null) {
									Log.d("MyApp","Uh oh. The user cancelled the Facebook login.");
									Toast.makeText(getActivity(),e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
									preloader.stopLoading();
								} else if (user.isNew()) {
									Log.d("MyApp","User signed up and logged in through Facebook!");
									facebookData(true);
									preloader.stopLoading();
								} else {
									Log.d("MyApp","User logged in through Facebook!");
									facebookData(false);
									preloader.stopLoading();
								}
							}
						});
		}
	
	}*/
	public void checkFbEnabled() {
		Session fbUser = ParseFacebookUtils.getSession();
		if (fbUser != null) {
			loadable = true;
			//mPhoneBookPreloader.setVisibility(View.VISIBLE);
			mFb.setVisibility(View.VISIBLE);
			mFbLogin.setVisibility(View.GONE);
			final Boolean[] parms = {true};
			new LoadSocialAsync().execute(parms);
		} else {
			//mPhoneBookPreloader.setVisibility(View.GONE);
			//mLinkFb.setVisibility(View.VISIBLE);
			mFb.setVisibility(View.GONE);
			mFbLogin.setVisibility(View.GONE);
		}
	}
	
	private void sendRequestDialog() {
	    Bundle params = new Bundle();
	    params.putString("title", "App Invite");
	    params.putString("message", "Hi, Check the F-Up App in the Play Store. http://fastrack.in/ " +
                 "Download now on your ANDROID device!");

	    WebDialog requestsDialog = (
	        new WebDialog.RequestsDialogBuilder(getActivity(),
	        		ParseFacebookUtils.getSession(),
	            params))
	            .setOnCompleteListener(new OnCompleteListener() {

	                @Override
	                public void onComplete(Bundle values,
	                    FacebookException error) {
	                    if (error != null) {
	                        if (error instanceof FacebookOperationCanceledException) {
	                            Toast.makeText(getActivity().getApplicationContext(), 
	                                "Request cancelled", 
	                                Toast.LENGTH_SHORT).show();
	                        } else {
	                            Toast.makeText(getActivity(), 
	                                "Network Error", 
	                                Toast.LENGTH_SHORT).show();
	                        }
	                    } else {
	                        final String requestId = values.getString("request");
	                        if (requestId != null) {
	                            Toast.makeText(getActivity(), 
	                                "Request sent",  
	                                Toast.LENGTH_SHORT).show();
	                        } else {
	                            Toast.makeText(getActivity(), 
	                                "Request cancelled", 
	                                Toast.LENGTH_SHORT).show();
	                        }
	                    }   
	                }

	            })
	            .build();
	    requestsDialog.show();
	}
	
	//added
	public class LoadSocialAsync extends AsyncTask<Boolean, Void, ArrayList<Friend>> {

		@Override
		protected ArrayList<Friend> doInBackground(Boolean... params) {
			//Calling from server
			String fqlQuery = "select uid, name,first_name, pic_square, is_app_user from user where is_app_user = 1 and uid in (select uid2 from friend where uid1 = me())";
			Bundle ops = new Bundle();
			ops.putString("q", fqlQuery);
			Request r = new Request(ParseFacebookUtils.getSession(),"/fql",ops,HttpMethod.GET,new Request.Callback() {
				
				@Override
				public void onCompleted(Response response) {
					if (response.getError() == null) {
						//Log.i("FB OP","Got results: "+ response.toString());
						GraphObject gObject = response.getGraphObject();
						JSONObject jObject = gObject.getInnerJSONObject();
						try {
							ParseQuery<ParseUser> query = ParseUser.getQuery();
							JSONArray jArray = jObject.getJSONArray("data");
							ArrayList<String> fblist = new ArrayList<String>();
							for(int i = 0; i<jArray.length();i++){
								JSONObject jObj2 = jArray.getJSONObject(i);
								fblist.add(jObj2.getString("uid"));
							}
							query.whereContainedIn("facebookid",fblist);
							query.whereNotEqualTo("objectId",ParseUser.getCurrentUser().getObjectId());
							try {
								List<ParseUser> objects = query.find();
								Friend list = null;
								if(objects.size() > 0){
									for (int i = 0; i < objects.size(); i++) {
										String fbid = objects.get(i).get("facebookid").toString();
										
										String fbImage = "https://graph.facebook.com/"+ fbid+ "/picture/";
										String myObj = ParseUser.getCurrentUser().toString();
										String userobj = objects.get(i).getObjectId().toString();
										String userName = objects.get(i).get("fullname").toString();
										//list = new People(myObj,userobj,fbid,userName,"facebook",fbImage,false,false);
										list = new Friend(userobj, userName, "facebook", fbImage, false, false, "Add");
										fbList.add(list);
									}
								}
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}catch (JSONException e) {
							e.printStackTrace();
						}
					}else{
						//Log.i("MyApp",response.getError().toString());
					}
				}
			});
			Request.executeBatchAndWait(r);
			return fbList;
		}
		
		protected void onPostExecute(ArrayList<Friend> result) {
			super.onPostExecute(result);
			finalList.addAll(result);
			if(result != null)
			updateList(result);
			new LoadContactsAsync().execute();
		}
	}
	
	private class LoadContactsAsync extends AsyncTask<Void, Void, ArrayList<Friend>> {
		@Override
		protected ArrayList<Friend> doInBackground(Void... params) {
		/*	ContentResolver cr = getContentResolver();
	        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
	 
	         if (cur != null && cur.getCount() > 0) {
	        	ParseQuery<ParseUser> query = ParseUser.getQuery();
	            while (cur.moveToNext()) {
	                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
	                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
	                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
	                    System.out.println("name : " + name + ", ID : " + id);
	 
	                    // get the phone number
	                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
	                                           ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
	                                           new String[]{id}, null);
	                    while (pCur.moveToNext()) {
	                          String phoneNum = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("\\s","");
	                          System.out.println("phone" + phoneNum);
	                          if (phoneNum.length() > 8) {
	  							try {
	  								query.whereEqualTo("phone", phoneNum);
	  								query.whereNotEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
	  								Friend items = null;
	  								List<ParseUser> qResult;
	  								qResult = query.find();
	  								for(int i=0;i<qResult.size();i++){
	  									Log.i("INFOOOOO", qResult.get(i).getObjectId().toString() + " --- " + qResult.get(i).get("fullname"));
	  									String imageUrl = "image";
	  									ParseFile p;
	  									p = qResult.get(i).getParseFile("profilethumbnail");
	  									if(p!=null){
	  										String url = p.getUrl();
	  										imageUrl = url;
	  									}
	  									items = new Friend(qResult.get(i).getObjectId().toString(), qResult.get(i).getString("fullname"), "contact", imageUrl, false, false);
	  									contactsList.add(items);
	  								}
	  							} catch (ParseException e) {
	  								// TODO Auto-generated catch block
	  								e.printStackTrace();
	  							}
	  						}
	                    }
	                    pCur.close();
	                }
	            }
	            cur.close();
	         }*/
			return (contactsList);
		}
		
		 protected void onPostExecute(ArrayList<Friend> result) {
 			super.onPostExecute(result);
 			finalList.addAll(result);
 			
 			if(finalList.size() > 0) {
 				final JSONArray jsonFriendsArray;
 				try {
 					jsonFriendsArray = new JSONArray(finalList.toString());
 					
 					ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
 					query.whereEqualTo("user", ParseUser.getCurrentUser());
 					query.findInBackground(new FindCallback<ParseObject>() { 
 						public void done(List<ParseObject> objects, ParseException e) {
 							if(objects.size() > 0) {
 								//edit
	 							if(e == null){
	 								for(ParseObject obj : objects){
	 									obj.put("friendsList", jsonFriendsArray.toString());
	 									obj.saveInBackground();//saveEventually();();
	 								}
	 							} else {
	 									
	 							}
 							} else {
 								//save
 								ParseObject friends = new ParseObject("Friends");
 			 					friends.put("user",ParseUser.getCurrentUser()); 
 			 					friends.put("friendsList", jsonFriendsArray.toString());
 			 					friends.saveInBackground();//saveEventually();(); 
 							}
 						}
 					});
 					//Log.d("FinalList", jsonFriendsArray.toString());
 				} catch (JSONException e) {
 					e.printStackTrace();
 				}
 			}
 		}
	}
	
	public void updateList(ArrayList<Friend> res){
		if(res != null) {
			if(ufr != null && ufr.getUserId().equals(ParseUser.getCurrentUser().getObjectId().toString())) {
				JSONArray pendingFriends = ufr.getPendingFriends();
				if(pendingFriends != null) {
					for(Friend fir : res) {
						for(int pf = 0; pf < pendingFriends.length(); pf++){
							try {
								if(pendingFriends.get(pf).equals(fir.getUserId())){
									//holder.mAction.setText("Accept");
									fir.setStatus("Accept");
									//fir.setVisibility(View.VISIBLE);
									fir.setmActionVisibility(View.GONE);
								} 
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
				}
				
				JSONArray requestedFriends = ufr.getRequestedFriends();
				if(requestedFriends != null) {
					for(Friend fir : res) {
						for(int rf = 0; rf < requestedFriends.length(); rf++){
							try {
								if(fir.getUserId().equals(requestedFriends.get(rf))){
									//holder.mAction.setText("Request Sent");
									fir.setStatus("Request Sent");
									fir.setVisibility(View.GONE);
									fir.setmActionVisibility(View.GONE);
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
				}
				
				JSONArray connectedFriend = ufr.getConnectedFriends();
				if(connectedFriend != null) {
					for(Friend fir : res) {
						for(int rf = 0; rf < connectedFriend.length(); rf++){
							try {								
								if(fir.getUserId().equals(connectedFriend.get(rf))){
									//holder.mAction.setText("Request Sent");
									fir.setStatus("Friend");
									fir.setVisibility(View.GONE);
									fir.setmActionVisibility(View.GONE);
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
				}
			
				
			}
		}
		
		
		try {
			mAdapter = new FriendsAdapter(getActivity(),R.layout.people_item,res,this, friends, ufr,"friends");
			lv.setAdapter(mAdapter);
			lv.setVisibility(View.VISIBLE);
			//mSearch.setVisibility(View.VISIBLE);
			preloader.stopLoading();
			//Log.i("list", res.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	@Override
	public void getPeopleAdded(JSONArray list, JSONArray friendsId) {
		//Log.d("MyApp","Friends: "+list.toString());
		friends = list;
		friendsid = friendsId;
	}
	



	//added
/*
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if (requestCode == com.facebook.Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE)
		  ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}
	
	
	public void facebookData(final boolean firstTime){
		Request request = Request.newMeRequest(ParseFacebookUtils.getSession(), new Request.GraphUserCallback() {
			
			@Override
			public void onCompleted(GraphUser user, Response response) {
				if(user !=null){
					Toast.makeText(getActivity(),"A", Toast.LENGTH_LONG).show();
					currentUser = ParseUser.getCurrentUser();
					currentUser.put("fullname",user.getName());
					currentUser.put("dob",user.getBirthday());
					currentUser.put("email", user.asMap().get("email"));
					currentUser.put("facebookid",user.getId());
					currentUser.put("facebookConnected",true);
					Log.e("MyApp","-"+user.getName()+"-"+user.getBirthday()+"-"+user.asMap().get("email")+"-"+user.getId());
     				currentUser.saveInBackground(new SaveCallback() {
						@Override
						public void done(ParseException e) {
							if(e==null){
								//All safe, enable push and go to next intent
								installPush();
								preloader.stopLoading();
								moveNext();
							}else{
		     					Log.e("MyApp",e.toString());
								preloader.stopLoading();
							}					
						}						
					});
				}else if (response.getError() != null){
					Toast.makeText(getActivity(),"B", Toast.LENGTH_LONG).show();
					if ((response.getError().getCategory() == FacebookRequestError.Category.AUTHENTICATION_RETRY)
							|| (response.getError().getCategory() == FacebookRequestError.Category.AUTHENTICATION_REOPEN_SESSION)) {
						Log.d("MyApp","The facebook session was invalidated.");
					} else {
						Log.d("MyApp", "Some other error: "+ response.getError().getErrorMessage());
					}
					preloader.stopLoading();
				//	ll.setVisibility(View.VISIBLE);
				
				}else{
					Toast.makeText(getActivity(),"C", Toast.LENGTH_LONG).show();
					installPush();
					preloader.stopLoading();
					//moveNext();
				}
		}
		});
		request.executeAsync();
	}
	
	public void installPush(){
		ParseInstallation installation = ParseInstallation.getCurrentInstallation();
		installation.put("user",currentUser);
	}
	private void moveNext() {
		try {
			ParseInstallation installation = ParseInstallation.getCurrentInstallation();
			installation.put("user",ParseUser.getCurrentUser());
			installation.put("myuser",ParseUser.getCurrentUser().getObjectId().toString());
			installation.saveInBackground();
			Intent i = new Intent(getActivity(),HomeActivity.class);
			startActivity(i);
			//finish();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		/*Toast.makeText(getActivity(), "Acitivy Return", Toast.LENGTH_LONG)
				.show();*/
		if (resultCode != Activity.RESULT_OK) {
			Log.d("Activity", "Error occured during linking");
			return;
		}
		if (requestCode == FB_REQUEST_ID) {
			if (ParseFacebookUtils.getSession() != null) {
				Log.d("Facebook", "Linking");
				ParseFacebookUtils.finishAuthentication(requestCode,
						resultCode, data);
			}
		}
	}
	
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.result_menu, menu);
		// get my MenuItem with placeholder submenu

	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		super.onPrepareOptionsMenu(menu);
		MenuItem searchMenuItem = menu.findItem(R.id.search1);
	
		searchMenuItem.expandActionView(); // Expand the search menu item in order to show by default the query
		SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
		
		SearchView searchView = (SearchView) searchMenuItem.getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
		searchView.setQueryHint("Search Friends");
		//searchView.setIconifiedByDefault(false);
		
		searchMenuItem.setOnActionExpandListener(new OnActionExpandListener() {

					@Override
					public boolean onMenuItemActionCollapse(MenuItem item) {
						// TODO Auto-generated method stub
						mAdapter.setData();
						mAdapter.notifyDataSetChanged();

						return true;
					}

					@Override
					public boolean onMenuItemActionExpand(MenuItem item) {
						// TODO Auto-generated method stub
						return true;
					}

				});

		SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextChange(String newText) {
				// this is your adapter that will be filtered
				mAdapter.getFilter().filter(newText);
				return true;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				// this is your adapter that will be filtered
				mAdapter.getFilter().filter(query);
				mAdapter.notifyDataSetChanged();

				return true;
			}
		};
		searchView.setOnQueryTextListener(textChangeListener);
	}
}
