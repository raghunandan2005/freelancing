package in.fastrack.app.m.fup;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;



import in.fastrack.app.m.fup.adapter.FriendsAdapterF;
import in.fastrack.app.m.fup.adapter.SurpriseAdapter;
import in.fastrack.app.m.fup.adapter.FriendsAdapterF.FriendListener;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.NewsFeed;
import in.fastrack.app.m.fup.model.People;
import in.fastrack.app.m.fup.model.UserFriendRelation;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.MenuItem.OnActionExpandListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class NewFacebookInviteFragmentF extends Fragment implements
		FriendListener {
	private AppConfig _config;
	Button mFb;
	CheckConnectivity c = new CheckConnectivity();
	boolean isInternetActive = false;
	boolean loadable = false;
	ProgressBar mPhoneBookPreloader;
	// added
	ArrayList<Friend> fbList = new ArrayList<Friend>();
	ArrayList<Friend> contactsList = new ArrayList<Friend>();
	ArrayList<Friend> finalList = new ArrayList<Friend>();

	// private Preloader preloader;
	ParseUser currentUser;
	static FriendsAdapterF mAdapter = null;
	ProgressBar mPbar;
	//EditText mSearch;
	ListView lv = null;
	private JSONArray friends = new JSONArray();
	private JSONArray friendsid = new JSONArray();

	final ArrayList<People> sList = new ArrayList<People>();
	String friendsListString = null;
	NewsFeed alarmDetails;
	private ProgressDialog pd;
	ArrayList<String> selectedFriendsid = new ArrayList<String>();

	UserFriendRelation ufr = null;

	// added
	public ParseUser getTheUser(String userid) {

		ParseUser user = null;
		ParseQuery<ParseUser> query1 = ParseUser.getQuery();
		query1.whereEqualTo("objectId", userid);
		List<ParseUser> objects;
		try {
			objects = query1.find();
			// The query was successful.
			for (ParseUser obj : objects) {
				user = obj;
				break;
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.friends_layout, container, false);
		this.setHasOptionsMenu(true);
		_config = new AppConfig(getActivity().getBaseContext());
		String[] parseKeys = _config.getAppKeys("parse");
		Parse.initialize(getActivity().getBaseContext(), parseKeys[0],
				parseKeys[1]);
		ParseFacebookUtils.initialize(getString(R.string.app_id));
		final ParseUser currentUser = ParseUser.getCurrentUser();
		isInternetActive = c.checkAndToast(getActivity(),
				"No Internet Connection");

		pd = new ProgressDialog(getActivity());
		pd.setTitle("Please Wait");
		pd.setMessage("Loading...");
		// added
		
		mPbar = (ProgressBar) root.findViewById(R.id.pbPhoneBook);
		lv = (ListView) root.findViewById(R.id.socialList);
		// /////////////////
		TextView tv = new TextView(getActivity());
		tv.setText(" No Phone Contacts");

		mAdapter = new FriendsAdapterF(getActivity(), R.layout.people_item,
				finalList, this, friends, ufr, "surprisefriends");
		lv.setEmptyView(tv);
		lv.setAdapter(mAdapter);
		// ///////////////////
//		mSearch.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence cs, int arg1, int arg2,
//					int arg3) {
//				try {
//					// String text = mSearch.getText().toString();
//					// ArrayList<Friend> searchedList = mAdapter.search(text);
//					// mAdapter.clear();
//					// mAdapter.addAll(searchedList);
//					mAdapter.getFilter().filter(cs);
//					mAdapter.notifyDataSetChanged();
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//
//			@Override
//			public void beforeTextChanged(CharSequence arg0, int arg1,
//					int arg2, int arg3) {
//
//			}
//
//			@Override
//			public void afterTextChanged(Editable arg0) {
//
//			}
//		});

		// preloader = new Preloader();
		// preloader.startPreloader(getActivity(),"Fetching Your Friends");

		try {
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
			query.whereEqualTo("user", ParseUser.getCurrentUser());
			query.findInBackground(new FindCallback<ParseObject>() {
				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					if (e == null) {
						if (objects.size() > 0) {
							for (ParseObject obj : objects) {
								ufr = new UserFriendRelation();
								ufr.setPendingFriendsS(obj
										.getJSONArray("pendingSF"));
								ufr.setRequestedFriendsS(obj
										.getJSONArray("requestedSF"));
								ufr.setConnectedFriendsS(obj
										.getJSONArray("connectedSF"));
								ufr.setUserId(ParseUser.getCurrentUser()
										.getObjectId().toString());
								break;
							}
						}
					} else {

					}
				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				// preloader.stopLoading();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		// added

		if (!Util.isNetworkAvailable(getActivity())) {
			DialogFactory.getBeaconStreamDialog1(getActivity(),
					getResources().getString(R.string.nw_error_str),
					Util.onClickListner, Util.retryClickListner).show();
		} else

		{

			final Boolean[] parms = { false };
			new LoadSocialAsync().execute(parms);
		}

		return root;
	}

	public class LoadSocialAsync extends
			AsyncTask<Boolean, Void, ArrayList<Friend>> {
		ArrayList<Friend> friendList = new ArrayList<Friend>();

		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd.show();
		}

		@Override
		protected ArrayList<Friend> doInBackground(Boolean... params) {

			ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
			query.whereEqualTo("user", ParseUser.getCurrentUser());
			try {
				List<ParseObject> objects = query.find();
				if (objects.size() > 0) {
					for (ParseObject obj : objects) {
						JSONArray friendsList = obj.getJSONArray("connectedF");
						if (friendsList != null) {
							for (int j = 0; j < friendsList.length(); j++) {
								try {
									String userid = friendsList.getString(j);
									if (userid != null) {
										ParseUser user = getTheUser(userid);

										Friend f = new Friend();
										f.setName(user.getString("fullname"));
										f.setUserId(userid);
										String imageUrl = "image";
										String t = null;
										ParseFile p = user
												.getParseFile("profilethumbnail");
										if (p != null) {
											String url = p.getUrl();
											imageUrl = url;
										} else if (user.get("facebookid") != null) {
											t = user.get("facebookid")
													.toString();
											if ((t != null) || (t != "")) {
												imageUrl = "https://graph.facebook.com/"
														+ user.get("facebookid")
																.toString()
														+ "/picture/?type=square";
											}
										}
										f.setUserImage(imageUrl);
										if (selectedFriendsid.contains(userid)) {
											f.setUserSelected(true);
										}
										friendList.add(f);
									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}

			return friendList;
		}

		@Override
		protected void onPostExecute(ArrayList<Friend> result) {
			super.onPostExecute(result);
			pd.dismiss();
			finalList.addAll(result);
			updateList(finalList);
		}

	}

	public void updateList(ArrayList<Friend> res) {
		if (res != null) {
			if (ufr != null
					&& ufr.getUserId()
							.equals(ParseUser.getCurrentUser().getObjectId()
									.toString())) {
				JSONArray requestedFriendsS = ufr.getRequestedFriendsS();
				if (requestedFriendsS != null) {
					for (Friend fir : res) {
						for (int rf = 0; rf < requestedFriendsS.length(); rf++) {
							try {
								if (fir.getUserId().equals(
										requestedFriendsS.get(rf))) {
									fir.setStatus("RequestSent");
									fir.setVisibility(View.VISIBLE);
									fir.setmActionVisibility(View.GONE);
									// fir.setmRemoveVisibility(View.GONE);
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
				}
				JSONArray pendingFriendsS = ufr.getPendingFriendsS();
				if (pendingFriendsS != null) {
					for (Friend fir : res) {
						for (int rf = 0; rf < pendingFriendsS.length(); rf++) {
							try {
								if (fir.getUserId().equals(
										pendingFriendsS.get(rf))) {
									fir.setStatus("Accept");
									fir.setVisibility(View.VISIBLE);
									fir.setmActionVisibility(View.GONE);
									// fir.setmRemoveVisibility(View.GONE);
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
				}
				JSONArray connectedFriendS = ufr.getConnectedFriendsS();
				if (connectedFriendS != null) {
					for (Friend fir : res) {
						for (int rf = 0; rf < connectedFriendS.length(); rf++) {
							try {
								if (fir.getUserId().equals(
										connectedFriendS.get(rf))) {
									fir.setStatus("SurpriseFriend");
									fir.setVisibility(View.VISIBLE);
									fir.setmActionVisibility(View.GONE);
									// fir.setmRemoveVisibility(View.VISIBLE);
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
				}

			}
		}

		try {
			mAdapter = new FriendsAdapterF(getActivity(), R.layout.people_item,
					res, this, friends, ufr, "surprisefriends");
			lv.setAdapter(mAdapter);
			lv.setVisibility(View.VISIBLE);
			//mSearch.setVisibility(View.VISIBLE);
			// preloader.stopLoading();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

	}

	@Override
	public void getPeopleAdded(JSONArray list, JSONArray friendsId) {
		// Log.d("MyApp","Friends: "+list.toString());
		friends = list;
		friendsid = friendsId;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.result_menu, menu);
		// get my MenuItem with placeholder submenu

	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		super.onPrepareOptionsMenu(menu);
		MenuItem searchMenuItem = menu.findItem(R.id.search1);
		searchMenuItem.expandActionView(); // Expand the search menu item in order to show by default the query
		SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
		
		SearchView searchView = (SearchView) searchMenuItem.getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
		searchView.setQueryHint("Search Friends");
		//searchView.setIconifiedByDefault(false);
		
		searchMenuItem.setOnActionExpandListener(new OnActionExpandListener() {

					@Override
					public boolean onMenuItemActionCollapse(MenuItem item) {
						// TODO Auto-generated method stub
						mAdapter.setData();
						mAdapter.notifyDataSetChanged();

						return true;
					}

					@Override
					public boolean onMenuItemActionExpand(MenuItem item) {
						// TODO Auto-generated method stub
						return true;
					}

				});

		SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextChange(String newText) {
				// this is your adapter that will be filtered
				mAdapter.getFilter().filter(newText);
				return true;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				// this is your adapter that will be filtered
				mAdapter.getFilter().filter(query);
				mAdapter.notifyDataSetChanged();

				return true;
			}
		};
		searchView.setOnQueryTextListener(textChangeListener);
	}

//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// TODO Auto-generated method stub
//		switch (item.getItemId()) {
//		case R.id.search:
//			MenuItem searchMenuItem = item;
//			searchMenuItem.expandActionView(); // Expand the search menu item in order to show by default the query
//			SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
//			
//			SearchView searchView = (SearchView) searchMenuItem.getActionView();
//			searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
//			searchView.setQueryHint("Search Friends");
//			searchView.setIconifiedByDefault(false);
//			
//			searchMenuItem.setOnActionExpandListener(new OnActionExpandListener() {
//
//						@Override
//						public boolean onMenuItemActionCollapse(MenuItem item) {
//							// TODO Auto-generated method stub
//							mAdapter.setData();
//							mAdapter.notifyDataSetChanged();
//
//							return true;
//						}
//
//						@Override
//						public boolean onMenuItemActionExpand(MenuItem item) {
//							// TODO Auto-generated method stub
//							return true;
//						}
//
//					});
//
//			SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
//				@Override
//				public boolean onQueryTextChange(String newText) {
//					// this is your adapter that will be filtered
//					mAdapter.getFilter().filter(newText);
//					return true;
//				}
//
//				@Override
//				public boolean onQueryTextSubmit(String query) {
//					// this is your adapter that will be filtered
//					mAdapter.getFilter().filter(query);
//					mAdapter.notifyDataSetChanged();
//
//					return true;
//				}
//			};
//			searchView.setOnQueryTextListener(textChangeListener);
//			break;
//		}
//		return super.onOptionsItemSelected(item);
//	}
}
