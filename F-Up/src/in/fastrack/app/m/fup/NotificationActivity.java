package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.adapter.NotificationAdapter;
import in.fastrack.app.m.fup.model.Notification;

import java.util.ArrayList;
import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class NotificationActivity extends Activity {
	
	//private ArrayAdapter<Notification> items;
	private ListView lv;
	private ProgressBar mPreloader;
	private TextView notificationMessage;
	private CheckConnectivity c = new CheckConnectivity();
	private Notification list = null;
	private ArrayList<Notification> mNotifications = new ArrayList<Notification>();
	NotificationAdapter mAdapter;
	ParseFile p;
	String t = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notifications);
		
		final ActionBar bar = getActionBar();
		bar.setTitle("Recent Notifications");
		
		bar.setDisplayShowHomeEnabled(true);
		bar.setHomeButtonEnabled(true);
		bar.setDisplayHomeAsUpEnabled(true);
		
		lv = (ListView) findViewById(R.id.lvNotifications);
		mPreloader = (ProgressBar) findViewById(R.id.pLoader);
		
		notificationMessage = (TextView) findViewById(R.id.tvNotificationMessage);
		
		checkAndLoadData();
	}
	
	private void checkAndLoadData() {
		if(c.check(getApplicationContext())){
			//Internet Available
			//new LoadNotifications().execute(10);
			loadNotifications();
		}
	}
	
	private void loadNotifications(){
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Notifications");
		//query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
		query.whereEqualTo("toid",ParseUser.getCurrentUser().getObjectId());
		query.include("actionref");
		query.include("fromid");
		query.orderByDescending("createdAt");
		query.setLimit(10);
		query.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if(e == null){
					Log.i("Data for Notification","TRUE");
					for(ParseObject obj: objects){
						Log.i("Notification Data",obj.toString());
						Log.i("NOTIFICATION TYPE",obj.get("notificationtype").toString());
						if(obj.get("notificationtype").toString().equals("alarm")){
							Log.i("NOTIFICATION TYPE 2","ALARM");
							String imageUrl = "image";
							p = obj.getParseObject("fromid").getParseFile("profilethumbnail");
							if(p!=null){
								String url = p.getUrl();
								Log.i("FB IMAGE",url);
								imageUrl = url;
							}else{
								if(obj.getParseObject("fromid").get("facebookid") != null) {
									t = obj.getParseObject("fromid").get("facebookid").toString();
									if((t!=null)||(t!="")){
										imageUrl = "https://graph.facebook.com/"
												+ obj.getParseObject("fromid").get("facebookid").toString()
												+ "/picture/?type=square";
									}
								}
							}
							Log.d("obj.getObjectId().toString()", obj.getObjectId().toString());
							//Log.d("obj.getParseObject(actionref).get(alarmrequestid).toString()", obj.getParseObject("actionref").get("requestcode").toString());
							//Log.d("obj.getParseObject(actionref).getString(alarmtitle)", obj.getParseObject("actionref").getString("title") );
							//Log.d("obj.getParseObject(actionref).getString(alarmcategory)", obj.getParseObject("actionref").getString("category"));
							//Log.d("obj.getParseObject(actionref).getString(alarmtime)", obj.getParseObject("actionref").getString("alarmTime"));
							Log.d("obj.getParseObject(fromid).getString(name)", obj.getParseObject("fromid").getString("fullname"));
							Log.d("obj.getParseObject(fromid).getObjectId()", obj.getParseObject("fromid").getObjectId());
							Log.d("imageUrl",imageUrl);
							Log.d("obj.getString(alarmaccepted)",obj.getString("alarmaccepted"));
							if(obj.getParseObject("actionref") != null) {
								list = new Notification(obj.getObjectId().toString(),obj.getParseObject("actionref").get("requestcode").toString(),obj.getParseObject("actionref").getString("title"), obj.getParseObject("actionref").getString("category"), obj.getParseObject("actionref").getString("alarmTime"), obj.getParseObject("fromid").getString("fullname"),obj.getParseObject("fromid").getObjectId(),imageUrl,"alarm", obj.getString("alarmaccepted"));
								//list = new Notification("2","abc","def","123","Raghu", "alarm","pending");
								mNotifications.add(list);
							}
						}
					}
					if(!mNotifications.isEmpty()){
						mAdapter = new NotificationAdapter(getApplicationContext(),R.layout.notification_row,mNotifications);
						lv.setAdapter(mAdapter);
						lv.setVisibility(View.VISIBLE);
						mPreloader.setVisibility(View.GONE);
						notificationMessage.setVisibility(View.GONE);
					}else{
						mPreloader.setVisibility(View.GONE);
						notificationMessage.setText("No recent notifications found.");
						notificationMessage.setVisibility(View.VISIBLE);
					}
				}else{
					mPreloader.setVisibility(View.GONE);
					notificationMessage.setText("No recent notifications found.");
					notificationMessage.setVisibility(View.VISIBLE);
				}
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			Intent i = new Intent(getApplicationContext(), HomeActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			finish();
			return true;
		}
		return false;
	}

	

	
	
}
