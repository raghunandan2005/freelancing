package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.adapter.NotificationActivityAdapter;
import in.fastrack.app.m.fup.adapter.NotificationAdapter;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.FeedEvent;
import in.fastrack.app.m.fup.model.FeedObject;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.Notification;
import in.fastrack.app.m.fup.model.NotificationModel;
import in.fastrack.app.m.fup.model.UserFriendRelation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import de.greenrobot.event.EventBus;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class NotificationActivityNew extends Activity {

	private ListView lv;
	private ProgressBar mPreloader;
	private TextView notificationMessage;
	private CheckConnectivity c = new CheckConnectivity();
	private NotificationModel list = null;
	private ArrayList<NotificationModel> mNotifications = new ArrayList<NotificationModel>();
	NotificationActivityAdapter mAdapter;
	ParseFile p;
	String t = null;
	//LinearLayout nodata;
	String notification_type;
	JSONArray connected = new JSONArray();
	JSONArray declined = new JSONArray();
	String friend_obj_id;
	boolean accepted = false;
	boolean showButtons = true;
	String prefix, message = null, suffix, postfix, fromName, toName, fromId,
			toId, title = "", creator_fullname, crearor_ref_obj_id, creator_id,
			creator_time, imageUrl, notify_type;
	int notificationType;
	String selectedAlarmType;
	private AppConfig _config;
	ParseUser currentUser;
	UserFriendRelation ufr = null;
	private TextView tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notifications);

		_config = new AppConfig(getApplicationContext());
		configParse();

		final ActionBar bar = getActionBar();
		tv = (TextView) this.findViewById(R.id.textView1);
		bar.setTitle("Recent Notifications");
		bar.setDisplayShowHomeEnabled(true);
		bar.setHomeButtonEnabled(true);
		bar.setDisplayHomeAsUpEnabled(true);
		lv = (ListView) findViewById(R.id.lvNotifications);
		mPreloader = (ProgressBar) findViewById(R.id.pLoader);
		// notificationMessage = (TextView)
		// findViewById(R.id.tvNotificationMessage);
		//nodata = (LinearLayout) findViewById(R.id.nofeed);

		if (!Util.isNetworkAvailable(NotificationActivityNew.this)) {

			DialogFactory.getBeaconStreamDialog1(NotificationActivityNew.this,
					getResources().getString(R.string.nw_error_str),
					Util.onClickListner, Util.retryClickListner).show();
		} else

		{

			loadNotification();

		}
	}

	public void configParse() {
		_config = new AppConfig(getApplicationContext());
		currentUser = ParseUser.getCurrentUser();
	}

	public void loadNotification() {
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Activity");
		query.whereEqualTo("followers", ParseUser.getCurrentUser()
				.getObjectId());
		query.include("creator");
		query.include("reference");
		query.include("reference.creator");
		query.include("reference2");
		query.include("reference3");
		query.include("reference4");
		query.include("reference5");
		query.orderByDescending("createdAt");
		query.setLimit(50);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					// Data found
					Log.i("Parse", "Data present");
					int k = 1;
					for (ParseObject o : objects) {
						notificationType = o.getInt("notificationtype");
						// System.out.println(o.getParseObject("reference").getString("creatorplain")
						// + "----------------------------");
						// String prefix,message =
						// null,suffix,postfix,fromName,toName,fromId,toId,title
						// = ""; //updated by nabasree
						switch (notificationType) {
						case 1:

							try {
								// Normal alarm notification
								Log.i("Parse", "ALarm");
								if (o.getParseObject("creator").getString(
										"fullname") != null)
									prefix = o.getParseObject("creator")
											.getString("fullname") + " ";
								title = "An alarm request from " + prefix;
								suffix = "added you to '"
										+ o.getParseObject("reference")
												.getString("title") + "' ";
								postfix = "for category '"
										+ o.getParseObject("reference")
												.getString("category") + "' ";
								message = prefix + suffix + postfix;
								// List<String> arr = o.getList("followers");
							} catch (Exception e3) {
								// TODO Auto-generated catch block
								e3.printStackTrace();
							}

							break;
						case 2:
							try {
								// Someone commented
								Log.i("Parse", "Comment");
								prefix = o.getParseObject("creator").getString(
										"fullname")
										+ " ";
								// if(o.getParseObject("creator").getObjectId()
								// ==
								// o.getParseObject("reference").get("creator")
								title = prefix
										+ " commented on '"
										+ o.getParseObject("reference")
												.getString("title") + "'";
								suffix = "commented on '"
										+ o.getParseObject("reference2")
												.getString("comment") + "'";
								postfix = "";
								message = suffix + postfix;
							} catch (Exception e3) {
								// TODO Auto-generated catch block
								e3.printStackTrace();
							}
							break;
						case 3:
							try {
								// Someone liked
								Log.i("Parse", "Liked");
								prefix = o.getParseObject("creator").getString(
										"fullname")
										+ " ";
								// if(o.getParseObject("creator").getObjectId()
								// ==
								// o.getParseObject("reference").get("creator")
								title = "F-Up";
								suffix = "liked an alarm '"
										+ o.getParseObject("reference")
												.getString("title") + "'";
								postfix = "";
								message = suffix + postfix;
							} catch (Exception e3) {
								// TODO Auto-generated catch block
								e3.printStackTrace();
							}
							break;
						case 4:
							// Alarm updated
							break;
						case 5:
							try {
								// Friend notification
								Log.i("Parse", "Friend");
								prefix = o.getParseObject("creator").getString(
										"fullname")
										+ " ";
								title = "An friend request from " + prefix;
								// suffix =
								// "requested to be your friend '"+o.getParseObject("reference3").getString("title")+"' ";
								suffix = "requested to be your friend ";
								postfix = "";
								message = prefix + suffix + postfix;
								// List<String> arr = o.getList("followers");
							} catch (Exception e3) {
								// TODO Auto-generated catch block
								e3.printStackTrace();
							}

							break;
						case 6:
							try {
								// Surprise Friend notification
								Log.i("Parse", "Surprise Friend");
								prefix = o.getParseObject("creator").getString(
										"fullname")
										+ " ";
								title = "An surprise friend request from "
										+ prefix;
								// suffix =
								// "added you to a friend '"+o.getParseObject("reference3").getString("title")+"' ";
								suffix = "requested to be your surprise friend and can set alarm for you at any time";
								postfix = "";
								message = prefix + suffix + postfix;
								// List<String> arr = o.getList("followers");
							} catch (Exception e3) {
								// TODO Auto-generated catch block
								e3.printStackTrace();
							}

							break;
						case 7:
							try {
								// Accept alarm notification
								Log.i("Parse", "ALarm Accept");
								prefix = o.getParseObject("creator").getString(
										"fullname")
										+ " ";
								title = "Alarm request was accepted by"
										+ prefix;
								suffix = "accepted your request for '"
										+ o.getParseObject("reference")
												.getString("title") + "' ";
								postfix = "of category '"
										+ o.getParseObject("reference")
												.getString("category") + "' ";

								message = prefix + suffix + postfix;
								// List<String> arr = o.getList("followers");
							} catch (Exception e3) {
								// TODO Auto-generated catch block
								e3.printStackTrace();
							}
							break;
						case 8:
							try {
								// Edit alarm notification
								Log.i("Parse", "Edit alarm");
								prefix = o.getParseObject("creator").getString(
										"fullname")
										+ " ";
								title = "Alarm was edited by" + prefix;
								suffix = "Alarm was edited for '"
										+ o.getParseObject("reference")
												.getString("title") + " '";
								postfix = "of category '"
										+ o.getParseObject("reference")
												.getString("category") + "' ";
								message = prefix + suffix + postfix;
								// List<String> arr = o.getList("followers");
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}

							break;
						case 9:
							try {
								// Remove alarm notification
								Log.i("Parse", "Remove alarm");
								prefix = o.getParseObject("creator").getString(
										"fullname")
										+ " ";
								title = "Alarm was removed by" + prefix;
								suffix = "removed the alarm for '"
										+ o.getParseObject("reference")
												.getString("title") + " '";
								postfix = "";
								message = prefix + suffix + postfix;
								// List<String> arr = o.getList("followers");
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}
							break;

						case 10:
							try {
								// Group notification

								Log.i("Parse", "Add group");
								prefix = o.getParseObject("creator").getString(
										"fullname")
										+ " ";
								title = "Added you to Group by" + prefix;
								suffix = "added you to group '"
										+ o.getParseObject("reference5")
												.getString("groupName") + "'";
								postfix = "";
								message = prefix + suffix + postfix;

								// List<String> arr = o.getList("followers");
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}

							break;
						case 11:
							try {
								// Friend notification
								Log.i("Parse", "Friend");
								prefix = o.getParseObject("creator").getString(
										"fullname")
										+ " ";
								title = "Alarm request was accepted by"
										+ prefix;
								// suffix =
								// "'"+o.getParseObject("reference3").getString("title")+"' ";
								suffix = "accepted your friend request";
								postfix = "";
								message = prefix + suffix + postfix;
								// List<String> arr = o.getList("followers");
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}

							break;
						case 12:
							try {
								// Friend notification
								Log.i("Parse", "Friend");
								prefix = o.getParseObject("creator").getString(
										"fullname")
										+ " ";
								title = "Alarm request was accepted by"
										+ prefix;
								// suffix =
								// " '"+o.getParseObject("reference3").getString("title")+"' ";
								suffix = "accepted your surprise friend request";
								postfix = "";
								message = prefix + suffix + postfix;
								// List<String> arr = o.getList("followers");
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}

							break;
						case 13:
							break;
						case 14:
							break;
						}
						if (o.getParseObject("creator").getObjectId() != null)
							if (o.getParseObject("creator")
									.getObjectId()
									.toString()
									.equals(ParseUser.getCurrentUser()
											.getObjectId().toString())) {
								// if(o.getParseObject("reference").getString("creatorplain").equals(ParseUser.getCurrentUser().getObjectId())){
								Log.i("Parse",
										"self user, so dont show activity");
							} else {
								imageUrl = "image";
								p = o.getParseObject("creator").getParseFile(
										"profilethumbnail");
								if (p != null) {
									String url = p.getUrl();
									imageUrl = url;
								} else {
									if (o.getParseObject("creator").get(
											"facebookid") != null) {
										t = o.getParseObject("creator")
												.get("facebookid").toString();
										if ((t != null) || (t != "")) {
											imageUrl = "https://graph.facebook.com/"
													+ o.getParseObject(
															"creator")
															.get("facebookid")
															.toString()
													+ "/picture/?type=square";
										}
									}
								}

								if ((notificationType == 1)) {
									try {
										Log.i("Actions", "1");
										crearor_ref_obj_id = o.getParseObject(
												"reference").getObjectId();
										creator_time = o.getParseObject(
												"reference").getString(
												"alarmTime");
										notify_type = "notifyalarm";
										creator_id = o
												.getParseObject("creator")
												.getObjectId();

										List<String> arr = o.getParseObject(
												"reference").getList(
												"acceptedpeople");
										List<String> arr2 = o.getParseObject(
												"reference").getList(
												"notificationactions");

										if (arr != null) {
											Log.i("Actions", "A");
											accepted = false;
											if (arr.contains(ParseUser
													.getCurrentUser()
													.getObjectId())) {
												accepted = true;
												Log.i("Actions", "B");
											}
										}
										if (arr2 != null) {
											Log.i("Actions", "C");
											if (arr2.contains(ParseUser
													.getCurrentUser()
													.getObjectId())) {
												showButtons = false;
												Log.i("Actions", "D");
											}
										}
										if (ParseUser
												.getCurrentUser()
												.getObjectId()
												.equals(o.getParseObject(
														"creator")
														.getObjectId())) {
											accepted = true;
											showButtons = false;
											Log.i("Actions", "E");
										}

									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								}

								else if (notificationType == 5) {
									try {
										Log.i("Actions", "5");
										notify_type = "notifyfriend";
										creator_time = "";
										crearor_ref_obj_id = o.getParseObject(
												"reference3").getObjectId();
										creator_id = o
												.getParseObject("creator")
												.getObjectId();
										declined = o.getParseObject(
												"reference3").getJSONArray(
												"declinedF");
										connected = o.getParseObject(
												"reference3").getJSONArray(
												"connectedF");

										if ((declined != null)
												&& (declined.length() > 0)) {
											for (int i = 0; i < declined
													.length(); i++) {
												try {
													if (declined
															.get(i)
															.equals(ParseUser
																	.getCurrentUser()
																	.getObjectId()
																	.toString())) {
														Log.i("u r here",
																"in connected");
														accepted = true;
														showButtons = false;
													}
												} catch (JSONException e1) {
													// TODO Auto-generated catch
													// block
													e1.printStackTrace();
												}
											}
										}
										if ((connected != null)
												&& (connected.length() > 0)) {
											for (int i = 0; i < connected
													.length(); i++) {
												try {
													if (connected
															.get(i)
															.equals(ParseUser
																	.getCurrentUser()
																	.getObjectId()
																	.toString())) {
														accepted = true;
														showButtons = false;
													}
												} catch (JSONException e1) {
													// TODO Auto-generated catch
													// block
													e1.printStackTrace();
												}
											}
										}

									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								}

								else if (notificationType == 6) {
									try {
										Log.i("Actions", "6");
										notify_type = "notifysurprisefriend";
										creator_time = "";
										crearor_ref_obj_id = o.getParseObject(
												"reference4").getObjectId();
										creator_id = o
												.getParseObject("creator")
												.getObjectId();
										declined = o.getParseObject(
												"reference4").getJSONArray(
												"declinedSF");
										connected = o.getParseObject(
												"reference4").getJSONArray(
												"connectedSF");
										// accepted = true;
										// showButtons = false;
										if (declined != null) {
											for (int i = 0; i < declined
													.length(); i++) {
												try {
													if (declined
															.get(i)
															.equals(ParseUser
																	.getCurrentUser()
																	.getObjectId()
																	.toString())) {
														Log.i("u r here",
																"in connected");
														accepted = true;
														showButtons = false;
													}
												} catch (JSONException e1) {
													// TODO Auto-generated catch
													// block
													e1.printStackTrace();
												}
											}
										}
										if (connected != null) {
											for (int i = 0; i < connected
													.length(); i++) {
												try {
													if (connected
															.get(i)
															.equals(ParseUser
																	.getCurrentUser()
																	.getObjectId()
																	.toString())) {
														accepted = true;
														showButtons = false;
													}
												} catch (JSONException e1) {
													// TODO Auto-generated catch
													// block
													e1.printStackTrace();
												}
											}
										}
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								}

								else if ((notificationType == 8)) {
									try {
										Log.i("Actions", "8");
										crearor_ref_obj_id = o.getParseObject(
												"reference").getObjectId();
										creator_time = o.getParseObject(
												"reference").getString(
												"alarmTime");
										notify_type = "notifyalarm";
										creator_id = o
												.getParseObject("creator")
												.getObjectId();
										List<String> arr3 = o.getParseObject(
												"reference").getList(
												"acceptedpeopleedit");
										List<String> arr4 = o.getParseObject(
												"reference").getList(
												"noficationsedit");

										if (arr3 != null) {
											Log.i("Actions", "A8" + creator_id);
											accepted = true;
											if (arr3.size() == 1) {
												accepted = false;
												Log.i("Actions", "B8");
											}
										}
										if (arr4 != null) {
											showButtons = false;
											Log.i("Actions", "C8");
											if (arr4.size() == 1) {
												showButtons = true;
												Log.i("Actions", "D8");
											}
										}

										if (ParseUser
												.getCurrentUser()
												.getObjectId()
												.equals(o.getParseObject(
														"creator")
														.getObjectId())) {
											accepted = true;
											showButtons = false;
											Log.i("Actions", "E8");
										}
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								}

								else if (notificationType == 11) {
									try {
										Log.i("Actions", "11");
										notify_type = "notifyfriend";
										creator_time = "";
										crearor_ref_obj_id = o.getParseObject(
												"reference3").getObjectId();
										creator_id = o
												.getParseObject("creator")
												.getObjectId();
										declined = o.getParseObject(
												"reference3").getJSONArray(
												"declinedF");
										connected = o.getParseObject(
												"reference3").getJSONArray(
												"connectedF");
										accepted = true;
										showButtons = false;
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
									/*
									 * if (declined != null) { for (int i = 0; i
									 * < declined.length(); i++) { try { if
									 * (declined
									 * .get(i).equals(ParseUser.getCurrentUser
									 * ().getObjectId().toString())) {
									 * Log.i("u r here","in connected");
									 * accepted = true; showButtons = false; } }
									 * catch (JSONException e1) { // TODO
									 * Auto-generated catch block
									 * e1.printStackTrace(); } } } if (connected
									 * != null) { for (int i = 0; i <
									 * connected.length(); i++) { try { if
									 * (connected
									 * .get(i).equals(ParseUser.getCurrentUser
									 * ().getObjectId().toString())) { accepted
									 * = true; showButtons = false; } } catch
									 * (JSONException e1) { // TODO
									 * Auto-generated catch block
									 * e1.printStackTrace(); } } }
									 */
								} else if (notificationType == 12) {
									try {
										Log.i("Actions", "12");
										notify_type = "notifysurprisefriend";
										creator_time = "";
										crearor_ref_obj_id = o.getParseObject(
												"reference4").getObjectId();
										creator_id = o
												.getParseObject("creator")
												.getObjectId();
										declined = o.getParseObject(
												"reference4").getJSONArray(
												"declinedSF");
										connected = o.getParseObject(
												"reference4").getJSONArray(
												"connectedSF");
										accepted = true;
										showButtons = false;
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
									/*
									 * if (declined != null) { for (int i = 0; i
									 * < declined.length(); i++) { try { if
									 * (declined
									 * .get(i).equals(ParseUser.getCurrentUser
									 * ().getObjectId().toString())) {
									 * Log.i("u r here","in connected");
									 * accepted = true; showButtons = false; } }
									 * catch (JSONException e1) { // TODO
									 * Auto-generated catch block
									 * e1.printStackTrace(); } } } if (connected
									 * != null) { for (int i = 0; i <
									 * connected.length(); i++) { try { if
									 * (connected
									 * .get(i).equals(ParseUser.getCurrentUser
									 * ().getObjectId().toString())) { accepted
									 * = true; showButtons = false; } } catch
									 * (JSONException e1) { // TODO
									 * Auto-generated catch block
									 * e1.printStackTrace(); } } }
									 */
								} else {
									try {
										accepted = true;
										showButtons = false;
										Log.i("Actions", "else");
										crearor_ref_obj_id = o.getParseObject(
												"reference").getObjectId();
										creator_time = o.getParseObject(
												"reference").getString(
												"alarmTime");
										creator_id = o
												.getParseObject("creator")
												.getObjectId();
										notify_type = "";
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								}

								try {
									Log.i("Actions", "" + accepted
											+ showButtons);
									list = new NotificationModel(o
											.getParseObject("creator")
											.getString("fullname"), message,
											imageUrl, crearor_ref_obj_id,
											creator_time, notificationType,
											accepted, showButtons, notify_type,
											creator_id);
									mNotifications.add(list);
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}

							}

					}
					// Set notification count if the app is open
					// FeedObject object2= new
					// FeedObject(3,objects.size(),false,0,0);
					// EventBus.getDefault().postSticky(new FeedEvent(object2));

					if (!mNotifications.isEmpty()) {
						try {
							mAdapter = new NotificationActivityAdapter(
									NotificationActivityNew.this,
									R.layout.notification_row, mNotifications);
							// mAdapter = new
							// NotificationAdapter(getApplicationContext(),R.layout.notification_row,mNotifications);
							lv.setAdapter(mAdapter);
							lv.setVisibility(View.VISIBLE);
							mPreloader.setVisibility(View.GONE);
						
							//nodata.setVisibility(View.GONE);
							// notificationMessage.setVisibility(View.GONE);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					} else {
						mPreloader.setVisibility(View.GONE);
						tv.setText("No Notifications to display");
						lv.setEmptyView(tv);
						// notificationMessage.setText("No recent notifications found.");
						// notificationMessage.setVisibility(View.VISIBLE);
						//nodata.setVisibility(View.VISIBLE);
					}

				} else {
					Log.i("Parse", e.getMessage());
					// System.out.println(e.printStackTrace());
					mPreloader.setVisibility(View.GONE);
					/*
					 * notificationMessage.setText("No recent notifications found."
					 * ); notificationMessage.setVisibility(View.VISIBLE);
					 */
					//nodata.setVisibility(View.VISIBLE);
				}
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent i = new Intent(getApplicationContext(), HomeActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			finish();
			return true;
		}
		return false;
	}

}
