package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.FriendAddFragment.LoadSocialAsync;
import in.fastrack.app.m.fup.adapter.AlarmFriendsAdapter;
import in.fastrack.app.m.fup.adapter.ImageAdapter;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.AlarmFriend;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.UserFriendRelation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SendCallback;
import com.squareup.picasso.Picasso;

public class ProfileActivity extends Activity implements AnimationListener {
	

	
	 /** Called when the user touches the button */
    public void openEditProfile(View view) {
        // Do something in response to button click
    	Intent intent = new Intent(this, EditProfileActivity.class);
        startActivity(intent);
    }
    public  ArrayList<AlarmFriend> alarmFriends = new ArrayList<AlarmFriend>();
    public  ArrayList<AlarmFriend> alarmFriends1 = new ArrayList<AlarmFriend>();

    int position;
    ImageAdapter imageAdapter;
	private String profilBg;
    ImageView   imageViewFull,ivProfImage;
    private boolean bool1,bool2;
    public  ArrayList<AlarmFriend> friendList= new ArrayList<AlarmFriend>();
    public  ArrayList<AlarmFriend> friendList1 = new ArrayList<AlarmFriend>();
	private JSONArray friends_count = new JSONArray();
	private JSONArray surprise_friends_count = new JSONArray();
	private AppConfig _config;
	ParseUser currentUser;
	  ImageButton btnfrndR;
	String name, city = null;
	String myImageUrl;
	ParseFile p,pb;
	String t = null;
	int totalAlarmsCount;
	ParseObject reference_id;
	TextView nameText;
	TextView cityText;
	TextView myAlarmsCount,myFriendsCount,mySurpriseFriendsCount;
	boolean showImageOne = true, showImageAll = true ;
	ListView profileList;
	Bundle extras = null;
	Boolean viewExtra = false;
	Boolean self = true;
	String noOfAlarms,noOfFriend,noOfSurprise = null;
	
	String u_username,u_email,u_city,u_fullname,u_name,u_facebookid;
	ParseFile u_profilethumbnail;
	String imageUrl = "image";
	Intent intent;
	String userpath;
	
	ParseUser user;
	int friendCount = 0;
	/*
	String[] web = {
			"0 Friends", "0 Surprise Friends", "Edit Profile", "Settings"
	};
	Integer[] imageId = {
      R.drawable.ic_friends,
      R.drawable.ic_surprise,
      R.drawable.ic_edit_profile,
      R.drawable.ic_settings,
	};*/
	
	String[] web = {
	  "Edit Profile", "Settings"
	};
	Integer[] imageId = {
      R.drawable.ic_edit_profile,
      R.drawable.ic_settings,
	};
	  
	  public void configParse(){
		_config = new AppConfig(this);
	  }
	  
	  @Override
	  public void onResume() {
		super.onResume();
		checkActive();
	  }
		
	  public void checkActive(){
		currentUser = ParseUser.getCurrentUser();
		if(currentUser == null){
			//User not logged in
			Intent i = new Intent(getApplicationContext(),SplashActivity.class);
			startActivity(i);
		}
	  }
	  
	  public ParseUser getTheUser(){
			ParseUser user = null;
			ParseQuery<ParseUser> query1 = ParseUser.getQuery();
			query1.whereEqualTo("objectId", Util.getCurrent_profile_id());
			List<ParseUser> objects;
			try {
				objects = query1.find();
	           // The query was successful.
	       		for(ParseUser obj : objects) {
	       			user = obj;
	       			break;
	       		}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return user;
		}
	  
	  public ParseUser getTheUserString(String userid){
			ParseUser user = null;
			ParseQuery<ParseUser> query1 = ParseUser.getQuery();
			query1.whereEqualTo("objectId", userid);
			List<ParseUser> objects;
			try {
				objects = query1.find();
	           // The query was successful.
	       		for(ParseUser obj : objects) {
	       			user = obj;
	       			break;
	       		}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return user;
		}
	  
	  
	  @Override
	  protected void onCreate(Bundle savedInstanceState) {
		    super.onCreate(savedInstanceState);
		    setContentView(R.layout.profile_activity);
		    
		    intent = getIntent();
			userpath = intent.getStringExtra("userpath");
			   
		    configParse();
		    checkActive();
		    //myFriendsCoun
		    
		    ActionBar bar = getActionBar();
			bar.setTitle("Profile");
			bar.setDisplayShowHomeEnabled(true);
			bar.setHomeButtonEnabled(true);
			bar.setDisplayHomeAsUpEnabled(true);
			
			if(getIntent().getExtras().getString("userpath").equals("adapter")) {
				user = getTheUser();
			} else {
				user = ParseUser.getCurrentUser();
			}
			
			
			if(user.get("fullname") != null) {
				name = user.get("fullname").toString();
			} else {
				name = user.get("name").toString();
			}
			
			
			if(user.get("city") != null)
				city = user.get("city").toString();
			
			
			///////////////////
			//Flagging the profile pic
			//Hiding the image of flagged user for 1
		ParseQuery<ParseObject> flagg = ParseQuery.getQuery("FlagUserImage");
		flagg.whereEqualTo("flagProfileImg",ParseUser.getCurrentUser().getObjectId().toString());
		flagg.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					           try {
						                   // Data found
					        	   		   Log.i("Parse", "Data present........................");
					        	   		   if (objects != null) 
					        	   		    {
					        	   			   if (objects.size() != 0)
					        	   			   {
					        	   				   Log.i("Parse","Data present........................in if"+ objects.toString());
					        	   				   for (ParseObject o : objects)
					        	   				   {
					        	   					   Log.i("Parse","Data present........................ in for");
					        	   					 
					        	   					//showImage
					        	   					  if(o.getBoolean("profImgFrom")) 
					        	   					  {
					        	   						 Log.i("Parse","Data present........................ in boolean");
					        	   						showImageOne = true ;
					        	   					  }
					        	   					  else
					        	   					  {
					        	   						 Log.i("Parse","Data present........................ out boolean");
					        	   						showImageOne = false ;
					        	   					  }
					        	   						if(showImageOne)
					        	   						{
					        	   							/*****************SET THE Profile Image*************************/
								        	   				String imageUrl = "image";
								        	   				p = user.getParseFile("profilethumbnail");
								        	   				if(p!=null){
								        	   					String url = p.getUrl();
								        	   					//Log.i("FB IMAGE",url);
								        	   					imageUrl = url;
								        	   				}else{
								        	   					if(user.get("facebookid") != null && user.get("facebookid") != "") {
								        	   						t = user.get("facebookid").toString();
								        	   						if((t!=null)||(t!="")){
								        	   							imageUrl = "https://graph.facebook.com/"+ user.get("facebookid").toString()+ "/picture/?type=square";
								        	   						}
								        	   					}
								        	   				}
								        	   				Picasso.with(getApplicationContext()).load(imageUrl).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into((ImageView)findViewById(R.id.ivprofImage));
								        	   				/**************************************************************/
					        	   						}
					        	   						else
					        	   						{
					        	   							/*****************SET THE Profile Image*************************/
								        	   				String imageUrl = "image";
								        	   				
								        	   				Picasso.with(getApplicationContext()).load(imageUrl).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into((ImageView)findViewById(R.id.ivprofImage));
								        	   				/**************************************************************/
					        	   						}

					        	   				   }
					        	   			   } else
					        	   			   
					        	   			   {
					        	   				   Log.i("Parse","Data present........................in if else");
					        	   				
					        	   				/*****************SET THE Profile Image*************************/
					        	   				String imageUrl = "image";
					        	   				p = user.getParseFile("profilethumbnail");
					        	   				if(p!=null){
					        	   					String url = p.getUrl();
					        	   					//Log.i("FB IMAGE",url);
					        	   					imageUrl = url;
					        	   				}else{
					        	   					if(user.get("facebookid") != null && user.get("facebookid") != "") {
					        	   						t = user.get("facebookid").toString();
					        	   						if((t!=null)||(t!="")){
					        	   							imageUrl = "https://graph.facebook.com/"
					        	   									+ user.get("facebookid").toString()
					        	   									+ "/picture/?type=square";
					        	   						}
					        	   					}
					        	   				}
					        	   				Picasso.with(getApplicationContext()).load(imageUrl).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into((ImageView)findViewById(R.id.ivprofImage));
					        	   				/**************************************************************/
					        	   				
					        	   				
					        	   			   }
					        	   		    }

					           		} catch (Exception e1) {
						e1.printStackTrace();
					}

				}
			}

		});
			//////////////////////
			
		//Hiding the image of flagged user for all
		ParseQuery<ParseObject> flagg1 = ParseQuery.getQuery("FlagUserImage");
		flagg1.whereEqualTo("userIDStr",ParseUser.getCurrentUser().getObjectId().toString());
		flagg1.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					           try {
						                   // Data found
					        	   		   Log.i("Parse", "Data present........................");
					        	   		   if (objects != null) 
					        	   		    {
					        	   			   if (objects.size() != 0)
					        	   			   {
					        	   				   Log.i("Parse","Data present........................in if"+ objects.toString());
					        	   				   for (ParseObject o : objects)
					        	   				   {
					        	   					   Log.i("Parse","Data present........................ in for");
					        	   					 
					        	   					//showImage
					        	   					  if(o.getBoolean("profImgTo")) 
					        	   					  {
					        	   						 Log.i("Parse","Data present........................ in boolean");
					        	   						showImageAll = true ;
					        	   					  }
					        	   					  else
					        	   					  {
					        	   						 Log.i("Parse","Data present........................ out boolean");
					        	   						showImageAll = false ;
					        	   					  }
					        	   						if(showImageAll)
					        	   						{
					        	   							/*****************SET THE Profile Image*************************/
								        	   				String imageUrl = "image";
								        	   				p = user.getParseFile("profilethumbnail");
								        	   				if(p!=null){
								        	   					String url = p.getUrl();
								        	   					//Log.i("FB IMAGE",url);
								        	   					imageUrl = url;
								        	   				}else{
								        	   					if(user.get("facebookid") != null && user.get("facebookid") != "") {
								        	   						t = user.get("facebookid").toString();
								        	   						if((t!=null)||(t!="")){
								        	   							imageUrl = "https://graph.facebook.com/"+ user.get("facebookid").toString()+ "/picture/?type=square";
								        	   						}
								        	   					}
								        	   				}
								        	   				Picasso.with(getApplicationContext()).load(imageUrl).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into((ImageView)findViewById(R.id.ivprofImage));
								        	   				/**************************************************************/
					        	   						}
					        	   						else
					        	   						{
					        	   							/*****************SET THE Profile Image*************************/
								        	   				String imageUrl = "image";
								        	   				
								        	   				Picasso.with(getApplicationContext()).load(imageUrl).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into((ImageView)findViewById(R.id.ivprofImage));
								        	   				/**************************************************************/
					        	   						}

					        	   				   }
					        	   			   } else
					        	   			   
					        	   			   {
					        	   				   Log.i("Parse","Data present........................in if else");
					        	   				
					        	   				/*****************SET THE Profile Image*************************/
					        	   				String imageUrl = "image";
					        	   				p = user.getParseFile("profilethumbnail");
					        	   				if(p!=null){
					        	   					String url = p.getUrl();
					        	   					//Log.i("FB IMAGE",url);
					        	   					imageUrl = url;
					        	   				}else{
					        	   					if(user.get("facebookid") != null && user.get("facebookid") != "") {
					        	   						t = user.get("facebookid").toString();
					        	   						if((t!=null)||(t!="")){
					        	   							imageUrl = "https://graph.facebook.com/"
					        	   									+ user.get("facebookid").toString()
					        	   									+ "/picture/?type=square";
					        	   						}
					        	   					}
					        	   				}
					        	   				Picasso.with(getApplicationContext()).load(imageUrl).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into((ImageView)findViewById(R.id.ivprofImage));
					        	   				/**************************************************************/
					        	   				
					        	   				
					        	   			   }
					        	   		    }

					           		} catch (Exception e1) {
						e1.printStackTrace();
					}

				}
			}

		});
	////////////////////////////////////		
			
			
			if(name != "" && name != null) {
				nameText = (TextView) findViewById(R.id.tvUserProfName);
				nameText.setText(toTitleCase(name));
			}
			
			if(city != "" && city != null && city.length()!=0) {
				cityText = (TextView) findViewById(R.id.tvUserProfLocation);
				cityText.setText(toTitleCase(city));
			}
			
			//Form the String Array for the ProfileListviewActivity
			/*web[0] = getFriendsCount() + " Friends";
			web[1] = getSupriseFriendsCount() + " Surprise Friends";*/
			web[0] = "Edit Profile";
			web[1] = "Settings";
						
			extras = getIntent().getExtras();
			if(extras != null){
				//Fetch data of other profile
				self = false;
			}else{
				//Self data
				self = true;
				updateData(name,city,noOfAlarms,noOfFriend,noOfSurprise);
			}
            if (!Util.isNetworkAvailable(ProfileActivity.this)) {	
				
				DialogFactory.getBeaconStreamDialog1(ProfileActivity.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			} else 
			
			{

			
			//collceting the friends
			 Boolean[] parms = {false};
			new LoadSocialAsync().execute(parms);
			

			
			//collceting the  surprise friends
			
			 Boolean[] parms1 = {false};
			new LoadSocialAsync1().execute(parms1);
			
}
		    // List view
		    ProfileListviewActivity adapter = new ProfileListviewActivity(ProfileActivity.this, web, imageId);
		    profileList=(ListView)findViewById(R.id.profileList);
		    profileList.setAdapter(adapter);
		    profileList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	                @Override
	                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	                	switch(position){
	            			/*case 0:
	            				// Friends Count
	            			break;
	            			case 1:
	            				// Surprise Friends Count
	            			break;*/
	            			case 0:
	            				// Edit Profile
	            				Intent editprofile = new Intent(getApplicationContext(),EditProfileActivity.class);
	    	        			startActivity(editprofile);
	            			break;
	            			case 1:
	            				// Settings
	            				Intent settings = new Intent(getApplicationContext(),SettingsActivity.class);
	    	        			startActivity(settings);
	            			break;
	                	}
	                    //Toast.makeText(ProfileActivity.this, "You Clicked at " +web[+ position], Toast.LENGTH_SHORT).show();
	                }
	            });
		    
		    
		    //*******************Alarm Count***********************//
		    myAlarmsCount = (TextView) findViewById(R.id.myAlarmsCount);
		    ParseQuery<ParseObject> query = ParseQuery.getQuery("alarms");
		    query.whereEqualTo("creator", user);
		    currentUser = ParseUser.getCurrentUser();
			if(currentUser == null){
				 query.whereNotEqualTo("isPrivateAlaramCheck", true);
			}
		    
		    query.findInBackground(new FindCallback<ParseObject>() {
		        public void done(List<ParseObject> objects, ParseException e) {
		            if (e == null) {
		                // all good
		                totalAlarmsCount = objects.size();
		                Log.d("totalAlarmsCount", String.valueOf(totalAlarmsCount));
		                myAlarmsCount.setText(String.valueOf(totalAlarmsCount) + " Alarms");
		            } else {
		            	myAlarmsCount.setText(String.valueOf(0));
		            }
		        }
		    });

	        
	        // image view full layout and animation
	        imageViewFull = (ImageView) findViewById(R.id.imageViewFull);
	        RelativeLayout profileRelative = (RelativeLayout) findViewById(R.id.profileRelative);
	        
	        //resizing the image
	        Display display = getWindowManager().getDefaultDisplay();
	        DisplayMetrics outMetrics = new DisplayMetrics();
	        display.getMetrics(outMetrics);
	        float scWidth = outMetrics.widthPixels;
	        imageViewFull.getLayoutParams().width = (int) scWidth;
	        imageViewFull.getLayoutParams().height = (int) (scWidth * 0.5f);
	        profileRelative.getLayoutParams().height = (int) (scWidth * 0.5f);

			//saving the image in server
			/*String imageUrlb = "imageb";
			pb = ParseUser.getCurrentUser().getParseFile("profilebg");
			if(pb!=null){
				String url = pb.getUrl();
				imageUrlb = url;
			}
			Picasso.with(getApplicationContext()).load(imageUrlb).fit().placeholder(R.drawable.cover).error(R.drawable.cover).into((ImageView)findViewById(R.id.imageViewFull));
	*/
	        
	        imageAdapter = new ImageAdapter(this);
	        if(user.get("profileBg") != null)
	        profilBg = 	user.get("profileBg").toString();
			
			Log.i("name == null", "profileBG" + profilBg);
			
			if(profilBg!=null){
				
				if(profilBg.equalsIgnoreCase("0"))
				{
					position = 0;
				}
				else if(profilBg.equalsIgnoreCase("1"))
				{
					position = 1;
				}
				else if(profilBg.equalsIgnoreCase("2"))
				{
					position = 2;
				}
				else if(profilBg.equalsIgnoreCase("3"))
				{
					position = 3;
				}
				else if(profilBg.equalsIgnoreCase("4"))
				{
					position = 4;
				}
				if(position < 5)
				{
					
					Log.i("name == null", "profileBG" + profilBg +position);
				    imageViewFull.setImageResource(imageAdapter.mThumbIds[position]);
				}
				else
				{
					Log.i("name == null", "profileBG" + profilBg +position);
					imageViewFull.setImageDrawable(getResources().getDrawable(R.drawable.cover_options_1));
				}
					
			}
				
		ivProfImage = (ImageView) findViewById(R.id.ivprofImage);
		if (getIntent().getExtras().getString("userpath").equals("adapter")) {
			ivProfImage.setEnabled(true);
			ivProfImage.setFocusable(true);
			ivProfImage.setClickable(true);
		} else {
			ivProfImage.setEnabled(false);
			ivProfImage.setFocusable(false);
			ivProfImage.setClickable(false);
		}
		        
			
			ivProfImage.setOnClickListener(new OnClickListener(
					) {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					Toast.makeText(ProfileActivity.this, "here u r.................", Toast.LENGTH_SHORT).show();
					
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
					alertDialogBuilder.setMessage("Do you want to flag the Profile Pic?");
					alertDialogBuilder.setIcon(R.drawable.alarm);
					alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.cancel();
						}
					});
					alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface dialog, int id) {
	                    	
							ParseQuery<ParseObject> flagg = ParseQuery.getQuery("FlagUserImage");				   			
							//flagg.whereEqualTo("userIDStr",ParseUser.getCurrentUser().getObjectId());
							//flagg.include("userID");
							//flagg.setLimit(10);
							flagg.findInBackground(new FindCallback<ParseObject>() {
				   				
				   				@Override
				   				public void done(List<ParseObject> objects, ParseException e) {
				   					if(e==null){
				   						try {
				   							//Data found
			
				   							Log.i("Parse","Data present........................");	
				   							
				   							if(objects!=null ){
				   							if( objects.size()!=0){
				   								Log.i("Parse","Data present........................in if"+objects.toString());
				   							
				   							for(ParseObject o:objects){
				   								Log.i("Parse","Data present........................ in for");	
				   							final ParseObject parseO = o;
				   								
				   							if(o.get("flagProfileImg") != null) {
				   								Log.i("Parse","Data present........................in if");	
												final String[] peopleFlaggedTheImg = parseO.get("flagProfileImg").toString().split(",");
												Log.d("list", Boolean.toString(Arrays.asList(peopleFlaggedTheImg).contains(ParseUser.getCurrentUser().getObjectId().toString())));
												Log.d("list", Arrays.asList(peopleFlaggedTheImg).toString());
												Log.d("list", ParseUser.getCurrentUser().getObjectId().toString());
												parseO.addAllUnique("flagProfileImg", Arrays.asList(ParseUser.getCurrentUser().getObjectId().toString()));
												parseO.put("userID",user);//user pointer
												parseO.put("userIDStr",user.getObjectId().toString());//user string
												parseO.put("profImgTo", true);
												parseO.put("profImgFrom", false);
												parseO.saveInBackground(new SaveCallback() {
													public void done(com.parse.ParseException e) {
														if(peopleFlaggedTheImg.length == 4) {
															parseO.put("profImgTo", false);
															parseO.saveInBackground();
														}
													}
												});
											
												Toast.makeText(ProfileActivity.this, "The Alarm is now flagged in iffffffffff", Toast.LENGTH_SHORT).show();
											} else {
												Log.i("Parse","Data present........................in else");	
												parseO.add("flagProfileImg", ParseUser.getCurrentUser().getObjectId().toString());
												parseO.put("userID",user);//user pointer
												parseO.put("userIDStr",user.getObjectId().toString());//user string
												parseO.put("profImgTo", true);
												parseO.put("profImgFrom", false);
												parseO.saveInBackground();
												Toast.makeText(ProfileActivity.this, "The Alarm is now flagged in elseeeeeeee", Toast.LENGTH_SHORT).show();
												
											}
				   								
				   								
				   							}
				   						    }
				   							else{
				   								Log.i("Parse","Data present........................in if else");	
				   								//If table is null
				   								ParseObject flagUser = new ParseObject("FlagUserImage");
				   								flagUser.add("flagProfileImg", ParseUser.getCurrentUser().getObjectId().toString());
				   								flagUser.put("userID",user);//user pointer
				   								flagUser.put("userIDStr",user.getObjectId().toString());//user string
				   								flagUser.put("profImgTo", true);//true
				   								flagUser.put("profImgFrom", false);
												flagUser.saveInBackground();
				   							}
				   							}
				   							
				   						
				   					} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
							
				   					}
				   				    }
				   				    
				   			        });
							
		                    }
	                });
					AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();  
				}
			});
			
	        final FrameLayout profileDrawer = (FrameLayout) findViewById(R.id.profileDrawer);
	        ImageButton btnOpenProfDrawer = (ImageButton) findViewById(R.id.btnOpenProfDrawer);
	        
	        if(getIntent().getExtras().getString("userpath").equals("adapter")) {
	        	btnOpenProfDrawer.setVisibility(View.GONE);
			} else {
				btnOpenProfDrawer.setVisibility(View.VISIBLE);
			}
	        
	        
            btnfrndR = (ImageButton) findViewById(R.id.btnfrndR);
	        
	      btnfrndR.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				

				if (!Util.isNetworkAvailable(ProfileActivity.this)) {	
					
					DialogFactory.getBeaconStreamDialog1(ProfileActivity.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
				} else 
				
				{

				try {

					ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
					query.whereEqualTo("user", ParseUser.getCurrentUser());
					query.findInBackground(new FindCallback<ParseObject>() {
						public void done(List<ParseObject> objects,
								ParseException e) {
							if (objects.size() > 0) {

								// edit
								if (e == null) {
									for (ParseObject obj : objects) {
										obj.addAllUnique("requestedF", Arrays.asList(user.getObjectId()));
										obj.saveInBackground();//saveEventually();();
										
										reference_id = obj;
									}
									if(user.getString("fullname")!=null)
									Toast.makeText(ProfileActivity.this, "Friend request sent"+user.getString("fullname"), Toast.LENGTH_SHORT).show();
								} else {
								}
							} else {
								
							}
						}
					});
					ParseQuery<ParseUser> query2 = ParseUser.getQuery();
					query2.whereEqualTo("user", user.getObjectId());
					query2.getInBackground(user.getObjectId(),
							new GetCallback<ParseUser>() {
								@Override
								public void done(ParseUser object,ParseException e) {
									//frienduser = object;
									//frienduser.getObjectId();
									ParseQuery<ParseObject> query3 = ParseQuery.getQuery("Friends");
									query3.whereEqualTo("user", user);
									query3.findInBackground(new FindCallback<ParseObject>() {
										public void done(List<ParseObject> objects,ParseException e) {
											
											if (objects.size() > 0) {
												// edit
												if (e == null) {
													for (ParseObject obj : objects) {
														obj.addAllUnique("pendingF",Arrays.asList(ParseUser.getCurrentUser().getObjectId().toString()));
														obj.saveInBackground();//saveEventually();();
													}
												} else {
												}
											} else {
												
											}
																						
												
											
											
											

											try {
											
												ParseQuery pushQuery = ParseInstallation.getQuery();
												ParsePush push = new ParsePush();
												pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId()); // not me
												pushQuery.whereEqualTo("myuser",user.getObjectId());// push notification to other users
												push.setQuery(pushQuery);
												JSONObject tempAlarmData = new JSONObject();
												
												if (reference_id != null)
													tempAlarmData.put("reference3",reference_id);
													tempAlarmData.put("notificationtype", 5);
												    tempAlarmData.put("alert","The friend request is by "+ParseUser.getCurrentUser().getString("fullname"));
													tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivityNew");												
													push.setData(tempAlarmData);
													push.sendInBackground(new SendCallback() {
														   public void done(ParseException e) {
															     if (e == null) {
															    	 Toast.makeText(ProfileActivity.this, "Friend request sent", Toast.LENGTH_LONG).show();
															    	 //Shifted to here
															    	 // New custom notification code
																		final ParseObject notification = new ParseObject("Activity");
																		notification.put("creator",ParseUser.getCurrentUser());
																		notification.put("followers",Arrays.asList(user.getObjectId()));
																		if (reference_id != null)
																	    notification.put("reference3",reference_id);
																		notification.put("notificationtype", 5);
																		notification.put("notificationDone", true);
																		notification.put("readers",Arrays.asList(user.getObjectId()));//changed from `new JSONArray()` to Arrays.asList(user.getObjectId())
																		notification.add("readers", ParseUser.getCurrentUser().getObjectId());
																		notification.saveInBackground();// saveEventually();();
															    	   // notification.put("notificationDone", true);
															       Log.d("push", "success!");
															     } else {
															       Log.d("push", "failure");
															     }
															   }
															 });
											
											
											
											
											
											} catch (Exception e1) {
												// TODO Auto-generated catch block
												e1.printStackTrace();
											}
											
											
										
											
										}										
									});
								}
							});
				} catch (Exception e) {
				}
				
				}
			}
		});
	        

	        
	        
	        if (savedInstanceState == null) {
				// on first time display view for first nav item
				displayView(0);
			}
	       
		myFriendsCount = (TextView) findViewById(R.id.myFriendsCount);
		ParseQuery<ParseObject> query1 = ParseQuery.getQuery("Friends");
		query1.whereEqualTo("user", user);
		query1.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if (objects.size() > 0) {
					for (ParseObject obj : objects) {
						Log.e("u r", "here");
						friends_count = obj.getJSONArray("connectedF");

						break;
					}
					if (friends_count != null)
						myFriendsCount.setText(String.valueOf(friends_count.length()) + " Friends");

					if (getIntent().getExtras().getString("userpath").equals("adapter")) {

						if (friends_count != null) {
							if (friends_count.length() != 0) {
								boolean show = false;

								for (int rf = 0; rf < friends_count.length(); rf++) {
									try {
										Log.e("u r", "here in try "+friends_count.get(rf)+".."+Util.getCurrent_profile_id());
										
										if (friends_count.get(rf).equals(ParseUser.getCurrentUser().getObjectId().toString())) {
											show = false;
											Log.e("u r", "here in if");
											//btnfrndR.setVisibility(View.GONE);
										}

										else {
											Log.e("u r", "here in else");
											show = true;
											//btnfrndR.setVisibility(View.VISIBLE);
										}

									} catch (Exception r) {
										r.printStackTrace();
									}
								}

								if (show) {
									btnfrndR.setVisibility(View.VISIBLE);
								}

							}
						}

					} else {
						btnfrndR.setVisibility(View.GONE);
					}
				} else {
					myFriendsCount.setText(String.valueOf(0) + " Friends");

				}

			}
		});
			mySurpriseFriendsCount = (TextView) findViewById(R.id.mySurpriseFriendsCount);
			 ParseQuery<ParseObject> query2 = ParseQuery.getQuery("Friends");
				query2.whereEqualTo("user", user);
				query2.findInBackground(new FindCallback<ParseObject>() {
					@Override
					public void done(List<ParseObject> objects, ParseException e) {
						if(objects.size() > 0) {
							for(ParseObject obj : objects) { 
									Log.e("u r", "here");
									surprise_friends_count = obj.getJSONArray("connectedSF");
								
								break;
							}
							if(surprise_friends_count!=null)
							mySurpriseFriendsCount.setText(String.valueOf(surprise_friends_count.length()) + " Surprise Friends");
						}
						
						
						else{
					    mySurpriseFriendsCount.setText(String.valueOf(0) + " Surprise Friends");
						}
						
					
						//Log.e("surprise_friends_count is", "surprise_friends_count"+surprise_friends_count.length());
					}
				});
			
				myFriendsCount.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						
						
						if (!Util.isNetworkAvailable(ProfileActivity.this)) {	
							
							DialogFactory.getBeaconStreamDialog1(ProfileActivity.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
						} else 
						
						{
						if(bool1==true)
						{
						if (friends_count != null) {
							if (friends_count.length() != 0) {
								
								Dialog dialog = new Dialog(ProfileActivity.this);
				                dialog.setContentView(R.layout.alarm_friends_layout);
				                dialog.setTitle("Friends List");
				                ListView lv = (ListView) dialog.findViewById(R.id.socialList); 
				                lv.setVisibility(View.VISIBLE);
				                AlarmFriendsAdapter mAdapter = new AlarmFriendsAdapter(ProfileActivity.this,R.layout.people_item,alarmFriends);			                            
				                lv.setAdapter(mAdapter);
				                dialog.show();   
							}
							else{
								
							}
						}
						
						}else{
							Toast.makeText(ProfileActivity.this, "Loading Friends List", Toast.LENGTH_SHORT).show();
						}
						}
					}
				});
				
				mySurpriseFriendsCount.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if (!Util.isNetworkAvailable(ProfileActivity.this)) {	
							
							DialogFactory.getBeaconStreamDialog1(ProfileActivity.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
						} else 
						
						{
						if(bool2==true)
						{
						if (surprise_friends_count != null) {
							if (surprise_friends_count.length() != 0) {

								Dialog dialog = new Dialog(ProfileActivity.this);
				                dialog.setContentView(R.layout.alarm_friends_layout);
				                dialog.setTitle("Surprise Friends List");
				                ListView lv = (ListView) dialog.findViewById(R.id.socialList); 
				                lv.setVisibility(View.VISIBLE);
				                AlarmFriendsAdapter mAdapter = new AlarmFriendsAdapter(ProfileActivity.this,R.layout.people_item,alarmFriends1);			                            
				                lv.setAdapter(mAdapter);
				                dialog.show();   
							}
							else{
								
							}
						}
					}else
					{
						Toast.makeText(ProfileActivity.this, "Loading Friends List", Toast.LENGTH_SHORT).show();
					}
						}
					}
				});
			
	  }
	  
	  
	  private void displayView(int position) {
			Fragment profilefragment = null;
	        Bundle args = new Bundle();
	        profilefragment = new ProfileFeedFragment();
	        if (profilefragment != null) {
	            FragmentManager fragmentManager = getFragmentManager();
	            fragmentManager.beginTransaction()
	                    .replace(R.id.profile_frame_container, profilefragment).commit();
	        }
	  }
	  
	
	public void updateData(String n,String l, String ac,String fc,String sc){
		
	}
	
	/*  
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_home, menu);
        return true;
    }*/

	@Override
	public void onAnimationEnd(Animation arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationRepeat(Animation arg0) {
		// TODO Auto-generated method stub
	
		
	}

	@Override
	public void onAnimationStart(Animation arg0) {
		// TODO Auto-generated method stub
		
	}
	
	//Capatilize Text
	public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < arr.length; i++) {
        sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
        }          
      return sb.toString().trim();
    }
	
	//current user friends count
	private String getFriendsCount() {
		//get Friends Count
		return String.valueOf(friendCount);
	}
	
	//current user suprise friends count
	private String getSupriseFriendsCount() {
		//get Friends Count
		int supriseFriendCount = 0;
		return String.valueOf(supriseFriendCount);
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			Intent i = new Intent(getApplicationContext(), HomeActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			finish();
			break;
		}
		return true;
	}
	
	
	public class LoadSocialAsync extends AsyncTask<Boolean, Void, ArrayList<AlarmFriend>> {
		
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			bool1=false;
		}

		@Override
		protected ArrayList<AlarmFriend> doInBackground(Boolean... params) {
			Log.d("user", "userid is d result"+ user);
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
			query.whereEqualTo("user", user);
			try {
				List<ParseObject> objects = query.find();
				if(objects.size() > 0) {
					for(ParseObject obj : objects){
						JSONArray friendsList = obj.getJSONArray("connectedF");
						Log.d("friendList", "connectedF result"+ friendList.toString() + friendList);
						if(friendsList != null)  {
							for(int j = 0; j < friendsList.length(); j++) {
								try {
									String userid = friendsList.getString(j);
									if(userid != null) {
										ParseUser user = getTheUserString(userid);
										AlarmFriend alarmfriend = new AlarmFriend();
										//Friend f = new Friend();
										Log.d("user.getString(fullname)", "user.getString(fullname) result"+ user.getString("fullname"));
										alarmfriend.setName(user.getString("fullname"));
										Log.d("userid", "userid is d result"+ userid);
										alarmfriend.setUserId(userid);
								        String imageUrl = "image";
								        String t = null;
								        ParseFile p = user.getParseFile("profilethumbnail");
										if(p!=null){
											String url = p.getUrl();
											imageUrl = url;
										}else if(user.get("facebookid") != null){
											t = user.get("facebookid").toString();
											if((t!=null)||(t!="")){
												imageUrl = "https://graph.facebook.com/"
														+ user.get("facebookid").toString()
														+ "/picture/?type=square";
											}
										}
										alarmfriend.setUserImage(imageUrl);								       
								        friendList.add(alarmfriend);
									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Log.d("friendList", "this is d result"+ friendList.toString() + friendList);
			return friendList;
		}

		@Override
		protected void onPostExecute(ArrayList<AlarmFriend> result) {
			super.onPostExecute(result);
		//	updateList(result);
			bool1=true;
			Log.d("friendList", "this is d result"+ result.toString() + result);
			for(int f = 0; f <friendList.size(); f++){
				AlarmFriend alarmfriend = new AlarmFriend();				
				String friend_name = friendList.get(f).getName();
				String friend_image = friendList.get(f).getUserImage();
				String friend_id = friendList.get(f).getUserId();
				alarmfriend.setName(friend_name);
				alarmfriend.setUserImage(friend_image);
				alarmfriend.setUserId(friend_id);
				alarmFriends.add(alarmfriend);
				
			}
		}
		
	}	
public class LoadSocialAsync1 extends AsyncTask<Boolean, Void, ArrayList<AlarmFriend>> {
		
	
		@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		bool2=false;
	}

		@Override
		protected ArrayList<AlarmFriend> doInBackground(Boolean... params) {
			
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
			query.whereEqualTo("user", user);
			try {
				List<ParseObject> objects = query.find();
				if(objects.size() > 0) {
					for(ParseObject obj : objects){
						JSONArray friendsList = obj.getJSONArray("connectedSF");
						if(friendsList != null)  {
							for(int j = 0; j < friendsList.length(); j++) {
								try {
									String userid = friendsList.getString(j);
									if(userid != null) {
										ParseUser user = getTheUserString(userid);
										AlarmFriend alarmfriend = new AlarmFriend();
										//Friend f = new Friend();
										alarmfriend.setName(user.getString("fullname"));
										alarmfriend.setUserId(userid);
								        String imageUrl = "image";
								        String t = null;
								        ParseFile p = user.getParseFile("profilethumbnail");
										if(p!=null){
											String url = p.getUrl();
											imageUrl = url;
										}else if(user.get("facebookid") != null){
											t = user.get("facebookid").toString();
											if((t!=null)||(t!="")){
												imageUrl = "https://graph.facebook.com/"
														+ user.get("facebookid").toString()
														+ "/picture/?type=square";
											}
										}
										alarmfriend.setUserImage(imageUrl);								       
								        friendList1.add(alarmfriend);
									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		
			return friendList1;
		}

		@Override
		protected void onPostExecute(ArrayList<AlarmFriend> result) {
			super.onPostExecute(result);
		//	updateList(result);
			bool2=true;
			for(int f = 0; f <friendList1.size(); f++){
			AlarmFriend alarmfriend = new AlarmFriend();				
			String friend_name = friendList1.get(f).getName();
			String friend_image = friendList1.get(f).getUserImage();
			String friend_id = friendList1.get(f).getUserId();
			alarmfriend.setName(friend_name);
			alarmfriend.setUserImage(friend_image);
			alarmfriend.setUserId(friend_id);
			alarmFriends1.add(alarmfriend);
			}
		}
		
	}	


  
}
