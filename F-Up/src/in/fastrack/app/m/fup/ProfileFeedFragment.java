package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.adapter.FeedAdapter;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.NewsFeed;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nhaarman.listviewanimations.swinginadapters.prepared.ScaleInAnimationAdapter;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class ProfileFeedFragment extends Fragment implements OnClickListener,OnScrollListener {

	private static AppConfig _config;
	private ProgressBar pb;
	protected static ProgressDialog proDialog;
	ListView lv = null;
	static CheckConnectivity ic = new CheckConnectivity();
	static boolean isInternetActive = false;
	ArrayList<NewsFeed> nFeed;
	FeedAdapter mAdapter = null;
	ParseFile p = null;
	String t = null;
	int pageNumber = 0;
	Boolean firstTime = true;
	Boolean isLoading = false;
	static MediaPlayer mMediaPlayer = new MediaPlayer();
	static float count=100*.01f;
	
	private int mInterval = 30000; //30 Seconds
	private Handler mHandler;
	ParseUser user;
	
	@Override
	public void onResume() {
		super.onResume();
		checkActive();		
	}
	
	public void checkActive(){
		ParseUser currentUser = ParseUser.getCurrentUser();
		if(currentUser == null){
			//User not logged in
			Intent i = new Intent(getActivity(),SplashActivity.class);
			startActivity(i);
		}
	}
	
	public ProfileFeedFragment(){
	}
	
	private Context mContext;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.profile_news_feed, container, false);
		checkActive();
		mContext = getActivity();
		if(getActivity().getIntent().getExtras().getString("userpath").equals("adapter")) {
			user = getTheUser();
		} else {
			user = ParseUser.getCurrentUser();
		}
		
		pb = (ProgressBar) view.findViewById(R.id.prLoader);

		_config = new AppConfig(getActivity());
		lv = (ListView) view.findViewById(R.id.profeedList);
		lv.setOnTouchListener(new OnTouchListener() {
    	    // Setting on Touch Listener for handling the touch inside ScrollView
    	    @Override
    	    public boolean onTouch(View v, MotionEvent event) {
    	    // Disallow the touch request for parent scroll on touch of child view
    	    v.getParent().requestDisallowInterceptTouchEvent(true);
    	    return false;
    	    }
    	});
		
		lv.setOnScrollListener(this);
		mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				if(lv.getAdapter() != null) {
					mAdapter.reset(false);
					mMediaPlayer.reset();
				}
			}
		});
		
		//setHasOptionsMenu(true);
		return view;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	   //inflater.inflate(R.menu.action_home, menu);
	}
	
	public ParseUser getTheUser(){
		ParseUser user = null;
		ParseQuery<ParseUser> query1 = ParseUser.getQuery();
		query1.whereEqualTo("objectId", Util.getCurrent_profile_id());
		List<ParseUser> objects;
		try {
			objects = query1.find();
           // The query was successful.
       		for(ParseUser obj : objects) {
       			user = obj;
       			break;
       		}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}
		   
		
	
	
	public void fetchFeed(int page,String action,Boolean isUpdate){
		
		
		//Fragment
		if (!Util.isNetworkAvailable(getActivity())) {	
	
	     DialogFactory.getBeaconStreamDialog1(getActivity(), getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
       } else 

       {
		if(action == "alarm" && user != null){
			
			
			
			
			
			
			
			
					
			
			
			
			isLoading = true;
			nFeed = new ArrayList<NewsFeed>();
			final Boolean isOld = isUpdate;
			ParseQuery<ParseObject> query = ParseQuery.getQuery("alarms");		
			query.whereEqualTo("creator", user);
			ParseUser currentUser = ParseUser.getCurrentUser();
			if(currentUser == null){
				query.whereNotEqualTo("isPrivateAlaramCheck", true);
			}
			//added
			query.whereEqualTo("connected",ParseUser.getCurrentUser().getObjectId().toString());
			query.whereEqualTo("status", true);
			query.whereNotEqualTo("Hideit", ParseUser.getCurrentUser().getObjectId().toString());
			query.whereNotEqualTo("flagged", ParseUser.getCurrentUser().getObjectId().toString());			
			query.include("creator");
			//added
			query.orderByDescending("createdAt");
			query.setLimit(5);
			query.setSkip(page);
	     	query.findInBackground(new FindCallback<ParseObject>() {
				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					if(e == null){
						NewsFeed list = null;
						for(ParseObject obj : objects){
							String imageUrl = "image";
						
							p = obj.getParseObject("creator").getParseFile("profilethumbnail");
							if(p!=null){
								String url = p.getUrl();
								imageUrl = url;
							}else if(obj.getParseObject("creator").get("facebookid") != null){
								t = obj.getParseObject("creator").get("facebookid").toString();
								if((t!=null)||(t!="")){
									imageUrl = "https://graph.facebook.com/"
											+ obj.getParseObject("creator").get("facebookid").toString()
											+ "/picture/?type=square";
								}
							}
							boolean userLiked = false;
							if(obj.getList("likes")!=null){
								List<String> arr = obj.getList("likes");
								if(arr.contains(ParseUser.getCurrentUser().getObjectId())){
									userLiked  =true;
								}
							}
							Log.i("ALARM TITLE",obj.get("title").toString());
							int like_count = obj.getList("likes").size();
							int comment_count = obj.getInt("comments");
							String alarm_repeat_type = "";
							if(obj.getString("alarm_repeat_type") != null)
							alarm_repeat_type = obj.getString("alarm_repeat_type");
							else
								alarm_repeat_type = "Once";
							
							Log.d("MyApp","PQRS: "+obj.get("connectedlist").toString());
							list = new NewsFeed(obj.get("title").toString(), obj.get("category").toString(), 
									obj.getParseObject("creator").getString("fullname"),imageUrl, 
									obj.getCreatedAt().toString(),obj.get("alarmTime").toString(), 
									obj.get("connectedlist").toString(),obj.get("connected").toString(),Integer.parseInt(obj.getString("requestcode")), 
									like_count,comment_count, 
									1,obj.getLong("alarmtimemilli"),userLiked,obj.getObjectId(),obj.get("media").toString(),obj.getBoolean("recorded"),false, obj.getParseObject("creator").getObjectId(),obj.getBoolean("selfalarm"),obj.getString("media"),alarm_repeat_type,obj.getBoolean("isPrivateAlaramCheck"));
							nFeed.add(list);
							if(isOld){
								mAdapter.addNewFeed(list);
							}
						}
						pb.setVisibility(View.GONE);
						if(!isOld){
							if(!nFeed.isEmpty()){
								try {
									mAdapter = new FeedAdapter(getActivity(),R.layout.feed_row,nFeed);
									ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(mAdapter);
									scaleInAnimationAdapter.setAbsListView(lv);
									lv.setAdapter(scaleInAnimationAdapter);
									/*mAdapter = new FeedAdapter(getActivity(),R.layout.feed_row,nFeed);
									lv.setAdapter(mAdapter);*/
									lv.setVisibility(View.VISIBLE);
								} catch (Exception e2) {
									
								}
							}else{
								//Toast.makeText(mContext,"Welcome, but there is nothing new.",Toast.LENGTH_LONG).show();
							}
						}
					}else{
						//Toast.makeText(getActivity(),"No Feed found", Toast.LENGTH_LONG).show();
					}
					isLoading = false;
				}
			});
		}
       }
	}
	
	
	
	private void addNewData(){
		
	}

	@Override
	public void onClick(View v) {
		
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		final int lastItem = firstVisibleItem + visibleItemCount;
		if(lastItem >= totalItemCount && !isLoading){
			Log.i("PAGE NUMBER",Integer.toString(pageNumber));
			if(firstTime){
				fetchFeed(pageNumber,"alarm",false);
				firstTime = false;
			}else{
				fetchFeed(pageNumber,"alarm",true);
				//startRepeatingTask();
			}
			pageNumber = pageNumber + 5;
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		
	}
	
	/* Media data */
	public static void startMedia(final Context c,int trackid,String track,boolean state, int aid){
		try {
			//Toast.makeText(c.getApplicationContext(), "H", Toast.LENGTH_LONG).show();
			mMediaPlayer.reset();
			if(state){
				String fileName = "android.resource://" + c.getPackageName() + "/" +trackid;
				mMediaPlayer.setDataSource(c.getApplicationContext(),Uri.parse(fileName));
				final AudioManager am = (AudioManager) c.getSystemService(Context.AUDIO_SERVICE);
				if(am.getStreamVolume(AudioManager.STREAM_ALARM) != 0){
					mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
					float count=100*.01f;
					mMediaPlayer.setVolume(count,count);
					mMediaPlayer.prepare();
					mMediaPlayer.start();
				}else{
					mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
					float count=100*.01f;
					mMediaPlayer.setVolume(count,count);
					mMediaPlayer.prepare();
					mMediaPlayer.start();
				}
			}else{
				File mFile = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),track);
				if(mFile.exists()){
					//File Found
					mMediaPlayer.setDataSource(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath()+track);
					final AudioManager am = (AudioManager) c.getSystemService(Context.AUDIO_SERVICE);
					if(am.getStreamVolume(AudioManager.STREAM_ALARM) != 0){
						mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
						float count=100*.01f;
						mMediaPlayer.setVolume(count,count);
						mMediaPlayer.prepare();
						mMediaPlayer.start();
					}else{
						mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
						float count=100*.01f;
						mMediaPlayer.setVolume(count,count);
						mMediaPlayer.prepare();
						mMediaPlayer.start();
					}
				}else{
					//Download audio
					isInternetActive = ic.check(c);
					if(isInternetActive){
						startLoading("Downloading Audio",c);
						//ParseFile
						ParseQuery<ParseObject> query = ParseQuery.getQuery("alarms");
						query.whereEqualTo("requestcode",aid);
						query.findInBackground(new FindCallback<ParseObject>() {

							@Override
							public void done(List<ParseObject> objects,
									ParseException e) {
								if(e==null){
									for(int i=0;i<objects.size();i++){
										final String alarmPath = objects.get(i).getString("media");
										if(objects.get(i).getBoolean("recorded")){
											//Find audio and download silently
											ParseFile p = objects.get(i).getParseFile("audioclip");
											if(p!=null){
												p.getDataInBackground(new GetDataCallback() {
													
													@Override
													public void done(byte[] data,
															com.parse.ParseException e) {
														if(e==null){
															try {
																File newFile = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),alarmPath);
																OutputStream os = new FileOutputStream(newFile);
																os.write(data);
																os.close();
																stopLoading();
																mMediaPlayer.setDataSource(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath()+alarmPath);
																final AudioManager am = (AudioManager) c.getSystemService(Context.AUDIO_SERVICE);
																if(am.getStreamVolume(AudioManager.STREAM_ALARM) != 0){
																	mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
																	float count=100*.01f;
																	mMediaPlayer.setVolume(count,count);
																	mMediaPlayer.prepare();
																	mMediaPlayer.start();
																}else{
																	mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
																	float count=100*.01f;
																	mMediaPlayer.setVolume(count,count);
																	mMediaPlayer.prepare();
																	mMediaPlayer.start();
																}
															} catch (FileNotFoundException e1) {
																Log.i("ALARM FILE LOADED","FALSE");
																e1.printStackTrace();
															} catch(IOException e1){
																e1.printStackTrace();
															}
														}else{
															stopLoading();
															Toast.makeText(c,"File Not Found.",Toast.LENGTH_LONG).show();
														}
													}
												});
											}
										}
									}
								}else{
									stopLoading();
									Toast.makeText(c,"Unable to Download",Toast.LENGTH_LONG).show();
								}
							}
							
						});
					}else{
						Toast.makeText(c,"No Internet Connectivity",Toast.LENGTH_LONG).show();
					}
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void playMedia(Context context,String audioSource,int trackResource,int trackType,int alarmid ){
		if(trackType == 1){
			startMedia(context,trackResource,audioSource,true,alarmid);
		}else{
			startMedia(context,trackResource,audioSource,false,alarmid);
		}
	}
	
	public static void resetMedia(){
		mMediaPlayer.reset();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		if(lv.getAdapter() != null){
			mAdapter.reset(false);
			mMediaPlayer.reset();
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if(lv.getAdapter() != null){
			mAdapter.reset(false);
			mMediaPlayer.reset();
		}
	}
	
	
	protected static void setLoadingMessage(String msg){
		if(proDialog != null){
			proDialog.setMessage(msg);
		}
	}
	
	protected static void startLoading(String msg,Context c) {
	    proDialog = new ProgressDialog(c);
	    proDialog.setMessage(msg);
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

	protected static void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	
	/*Runnable mStatusChecker = new Runnable() {
	    @Override 
	    public void run() {
	    	mAdapter.notifyDataSetChanged();
	    	Log.d("Looping", "IT");
	    	mHandler.postDelayed(mStatusChecker, mInterval);
	    }
	  };

	  void startRepeatingTask() {
	    mStatusChecker.run(); 
	  }

	  void stopRepeatingTask() {
	    mHandler.removeCallbacks(mStatusChecker);
	  }*/
}
