package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.Util;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

public class ResetPasswordActivity extends Activity implements OnClickListener,ValidationListener {
	
	private Preloader loader;
	Validator validate;
	private Button rstSubmitButton;
	
	@Required(order = 1)
	@Email(order = 2, message = "Enter a valid E-Mail address.")
	private EditText rstEmail;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reset_password_activity);
		
		final ActionBar bar = getActionBar();
		bar.setTitle("Reset Password");
		
		rstEmail = (EditText) findViewById(R.id.rstEmail);
		
		rstSubmitButton = (Button) findViewById(R.id.rstSubmit);
		rstSubmitButton.setOnClickListener(this);
		
		validate = new Validator(this);
		validate.setValidationListener(this);
		
		loader = new Preloader();
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.rstSubmit:
				if (!Util.isNetworkAvailable(ResetPasswordActivity.this)) {				
					DialogFactory.getBeaconStreamDialog1(ResetPasswordActivity.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
				} else 
				
				{
		
				validate.validate();
				}
			break;
		}
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		String message = failedRule.getFailureMessage();
		if(failedView instanceof EditText){
			failedView.requestFocus();
			((EditText) failedView).setError(message);
		}else{
			Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
		}
		
	}

	@Override
	public void onValidationSucceeded() {
		loader.startPreloader(ResetPasswordActivity.this,"Sending Mail");
		String emailID = rstEmail.getText().toString();
		Log.d("Mail", emailID);
		if(emailID.length() > 0) {
			ParseUser.requestPasswordResetInBackground(emailID, new RequestPasswordResetCallback() {
				public void done(ParseException e) {
					if (e == null) {
						//Toast.makeText(getApplicationContext(), "Reset Password Mail Sent.", Toast.LENGTH_LONG).show();
						/*Intent i = new Intent(getApplicationContext(),LoginActivity.class);
						startActivity(i);
						finish();	*/
						
						
						DialogFactory.getBeaconStreamDialog2(ResetPasswordActivity.this, getResources().getString(R.string.success_msg), new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Dialog dialog = (Dialog) v.getTag();
								dialog.dismiss();
								Intent i = new Intent(getApplicationContext(),LoginActivity.class);
								startActivity(i);
								finish();	
								
							}
						}).show();
					} else {
						Toast.makeText(getApplicationContext(), "Please Enter a Valid Email ID", Toast.LENGTH_LONG).show();
					}
				}
			});
		} else {
			Toast.makeText(getApplicationContext(), "Please Enter a Valid Email ID", Toast.LENGTH_LONG).show();
		}
		loader.stopLoading();
	}

}
