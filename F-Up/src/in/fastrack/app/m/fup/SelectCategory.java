package in.fastrack.app.m.fup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class SelectCategory extends Activity{
	
	private String selectedCategory = "Alarm";
	
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.select_category);
	}
	
	public void openSelectCategory(View v){
		switch(v.getId()){
			case R.id.birthday_orange:
				selectedCategory = "Birthday Party";
			break;
			case R.id.wedding_orange:
				selectedCategory = "Wedding Party";
			break;
			case R.id.run_orange:
				selectedCategory = "Run";
			break;
			case R.id.party_time_orange:
				selectedCategory = "Party Time";
			break;
			case R.id.movie_night_orange:
				selectedCategory = "Movie Night";
			break;
			case R.id.lunch_orange:
				selectedCategory = "Lunch";
			break;
			case R.id.let_ride_orange:
				selectedCategory = "Ride";
			break;
			case R.id.dinner_orange:
				selectedCategory = "Dinner";
			break;
			case R.id.brunch_orange:
				selectedCategory = "Brunch";
			break;
			case R.id.breakfast_orange:
				selectedCategory = "Breakfast";
			break;
		}
		Intent exit = new Intent();
		Bundle data = new Bundle();
		data.putString("selectedCategory",selectedCategory);
		exit.putExtras(data);
		setResult(RESULT_OK,exit);
		finish();
	}

}
