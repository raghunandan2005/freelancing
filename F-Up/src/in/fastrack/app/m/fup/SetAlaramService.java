package in.fastrack.app.m.fup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.fastrack.app.m.fup.database.DatabaseHandler;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

public class SetAlaramService extends IntentService {

	public SetAlaramService() {
		super("Set Alarm Service");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent arg0) {
		// TODO Auto-generated method stub
		DatabaseHandler db = new DatabaseHandler(this);
		List<Alarams> listAlarams = db.getAllAlarams();

		// db.deleteAlarmmilli(time);

		for (int i = 0; i < listAlarams.size(); i++) {
			long time = System.currentTimeMillis();
			if (Long.parseLong(listAlarams.get(i).getMilli()) < time) {
				// not doing anything for alarm times before the current time
			}
			else
			{
				Intent intent = new Intent(this, AlaramActivityNew.class);
				Bundle tempData = new Bundle();
				tempData.putString("alarm_category", listAlarams.get(i)
						.getCategory());
				tempData.putString("alarm_title", listAlarams.get(i).getTitle());
				Log.i("...................Time in Service........", ""
						+ listAlarams.get(i).getMilli());

				if (listAlarams.get(i).getRecorded().equals("true")) {
					tempData.putBoolean("recorded", true);
				} else {
					tempData.putBoolean("recorded", false);
				}

				tempData.putInt("alarmrequestcode",
						Integer.parseInt(listAlarams.get(i).getRequestcode()));
				tempData.putString("alarmpath", listAlarams.get(i).getPath());
				tempData.putString("alarmfriends", listAlarams.get(i)
						.getFriends());// changed
				tempData.putString("alarmfriendsid", listAlarams.get(i)
						.getFriendsid());// added
				// tempData.putString("alarm_repeat_type","Once");

				tempData.putString("alarm_repeat_type", listAlarams.get(i)
						.getRepeattype());
				intent.putExtras(tempData);

				PendingIntent pi = PendingIntent.getActivity(this,
						Integer.parseInt(listAlarams.get(i).getRequestcode()),
						intent, PendingIntent.FLAG_CANCEL_CURRENT);
				AlarmManager am = (AlarmManager) this
						.getSystemService(Context.ALARM_SERVICE);
				am.set(AlarmManager.RTC_WAKEUP,
						Long.parseLong(listAlarams.get(i).getMilli()), pi);
				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat(
						"dd-MM-yyyy HH:mm:ss", Locale.getDefault());

				Date date;
				/*
				 * try { date = sdf.parse(listAlarams.get(i).getAtime());
				 * //calendar.setTime(date); } catch (ParseException e) { //
				 * TODO Auto-generated catch block e.printStackTrace(); }
				 */

				if (listAlarams.get(i).getRepeattype().equalsIgnoreCase("Once")) {
					am.set(AlarmManager.RTC_WAKEUP,
							Long.parseLong(listAlarams.get(i).getMilli()), pi);

				} else if (listAlarams.get(i).getRepeattype()
						.equalsIgnoreCase("Daily")) {

					calendar.add(Calendar.DAY_OF_WEEK, 1);
					am.set(AlarmManager.RTC_WAKEUP,
							Long.parseLong(listAlarams.get(i).getMilli()), pi);
					// am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,calendar.getTimeInMillis()
					// + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY,
					// pi);
				} else if (listAlarams.get(i).getRepeattype()
						.equalsIgnoreCase("Weekly")) {

					calendar.add(Calendar.WEEK_OF_MONTH, 1);
					am.set(AlarmManager.RTC_WAKEUP,
							Long.parseLong(listAlarams.get(i).getMilli()), pi);
				} else if (listAlarams.get(i).getRepeattype()
						.equalsIgnoreCase("Monthly")) {

					calendar.add(Calendar.MONTH, 1);
					am.set(AlarmManager.RTC_WAKEUP,
							Long.parseLong(listAlarams.get(i).getMilli()), pi);
				}

			}
		}
	}
}
