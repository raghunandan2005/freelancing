package in.fastrack.app.m.fup;


import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;


public class SettingsFragment extends PreferenceFragment   implements OnSharedPreferenceChangeListener {

	public static final String KEY_MEDIA_DOWNLOAD_PREFERENCE = "mediadownloads";
	public static final String KEY_ALARM_SNOOZE_PREFERENCE = "snooze";
	private ListPreference mMediaDownloads,mSnooze;

	public static String snooze_interval;
	
	  @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);

	        // Load the preferences from an XML resource
	        addPreferencesFromResource(R.xml.settings);
	        myCustomPreference();
	    }
	
	private void myCustomPreference(){
		
		mMediaDownloads = (ListPreference) getPreferenceScreen().findPreference(KEY_MEDIA_DOWNLOAD_PREFERENCE);
		mSnooze = (ListPreference) getPreferenceScreen().findPreference(KEY_ALARM_SNOOZE_PREFERENCE);
	}

//	@Override
//	protected void onResume() {
//		super.onResume();
//		
//		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
//
//		
//		if(mMediaDownloads.getEntry() == null){
//			mMediaDownloads.setValueIndex(1);
//		}
//		
//		mMediaDownloads.setSummary(mMediaDownloads.getEntry());
//		
//		if(mSnooze.getEntry() == null){
//			mSnooze.setValueIndex(1);
//		}
//		
//		mSnooze.setSummary(mSnooze.getEntry());
//		snooze_interval = mSnooze.getEntry().toString();
//		Log.i("mSnooze", "mSnooze.getEntry()"+mSnooze.getEntry());
//		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
//	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		Preference pref = findPreference(key);

		
		if(mMediaDownloads.getEntry() == null){
			mMediaDownloads.setValueIndex(1);
		}
		
		mMediaDownloads.setSummary(mMediaDownloads.getEntry());
		
		if(mSnooze.getEntry() == null){
			mSnooze.setValueIndex(1);
		}
		
		mSnooze.setSummary(mSnooze.getEntry());
		snooze_interval = mSnooze.getEntry().toString();
		//Log.i("mSnooze", "mSnooze.getEntry()"+mSnooze.getEntry());
		
		
		
		if (pref instanceof ListPreference) {
			ListPreference listPref = (ListPreference) pref;
            pref.setSummary(listPref.getEntry());
		}
		
	}



}
