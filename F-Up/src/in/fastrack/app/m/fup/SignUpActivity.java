package in.fastrack.app.m.fup;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Locale;

import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.dialogs.DatePickerFragment;
import in.fastrack.app.m.fup.dialogs.DatePickerFragment.DatePickedListener;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.lib.Util;

import com.kbeanie.imagechooser.api.ImageChooserManager;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NumberRule;
import com.mobsandgeeks.saripaar.annotation.NumberRule.NumberType;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.mobsandgeeks.saripaar.annotation.TextRule;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class SignUpActivity extends Activity implements ValidationListener,ImageChooserListener,android.view.View.OnClickListener,DatePickedListener {
	
private EditText mCity,mDob;

private AppConfig _config;
	
	protected ProgressDialog proDialog;
	
	private Preloader loader;
	
	private Button mSignUp;
	
	private int choiceType;
	
	private ImageChooserManager chooserManager;
	
	private String filePath,fileName = null;
	
	File f;
	
	Validator validator;
	
	@Required(order = 1)
	@TextRule(order = 2,minLength = 4,message="Enter atleast 4 characters.")
	private EditText mUserName;
	
	@Required(order = 3)
	@TextRule(order = 4,minLength = 4,message="Enter atleast 4 characters.")
	private EditText mPassword;
	
	@Required(order = 5)
	@TextRule(order = 6,minLength = 4,message="Enter atleast 4 characters.")
	private EditText mFullName;
	
	@Required(order = 7)
	@Email(order=8,message="Enter a valid E-Mail address.")
	private EditText mEmail;
	
	
	@NumberRule(order = 8,message="Enter phone number in numeric",type=NumberType.LONG)
	@TextRule(order=9,minLength=10,maxLength=14,message="Enter a valid phone number")
	private EditText mPhone;
	
	ImageView mUserImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		ActionBar bar = getActionBar();
		bar.setTitle(this.getString(R.string.signup));
		
		setContentView(R.layout.signup_layout);
		
		loader = new Preloader();
		
		_config = new AppConfig(this);
		String[] parseKeys = _config.getAppKeys("parse");
		Parse.initialize(this,parseKeys[0],parseKeys[1]);
		
		
		mUserName = (EditText) findViewById(R.id.etUsername);
		mEmail = (EditText) findViewById(R.id.etEmail);
		mPassword = (EditText) findViewById(R.id.etPassword);
		mFullName = (EditText) findViewById(R.id.etName);
		mCity = (EditText) findViewById(R.id.etCity);
		mDob = (EditText) findViewById(R.id.etDob);
		mPhone = (EditText) findViewById(R.id.etMobile);
		
		mUserImage = (ImageView) findViewById(R.id.ivUserProfile);
		mUserImage.setOnClickListener(this);
		
		mSignUp = (Button) findViewById(R.id.bSignUp);
		mSignUp.setOnClickListener(this);
		
		mDob.setInputType(InputType.TYPE_NULL);
		mDob.setClickable(true);
		mDob.setOnClickListener(this);
		
		
		mUserName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus){
					if(mUserName.getText().length() >= 4){
						//Validate if user name is present. Lets do this later
						
					}
				}
			}
		});
		
		mDob.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(hasFocus){
					DialogFragment df = new DatePickerFragment();
					df.show(getFragmentManager(),"datePicker");
				}
			}
		});
		
		validator = new Validator(this);
		validator.setValidationListener(this);
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK
				&& (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
			loader.stopLoading();
			if (chooserManager == null) {
				reinitializeImageChooser();
			}
			chooserManager.submit(requestCode, data);
		} else {
			loader.stopLoading();
			//pbar.setVisibility(View.GONE);
		}
	}
	
	private void reinitializeImageChooser() {
		chooserManager = new ImageChooserManager(this, choiceType,"Fup/temp", true);
		chooserManager.setImageChooserListener(this);
		chooserManager.reinitialize(filePath);
	}

	@Override
	public void onError(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onImageChosen(final ChosenImage image) {
		runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						if(image != null){
							loader.startPreloader(SignUpActivity.this,"Capturing Image..");
							f = new File(image.getFileThumbnail());	
							Picasso.with(getApplicationContext()).load(Uri.fromFile(f)).fit().centerCrop().transform(new RoundedTransform()).into(mUserImage,new Callback() {
								
								@Override
								public void onSuccess() {
									Long tsLong = System.currentTimeMillis()/1000;
									fileName = tsLong.toString();
									loader.stopLoading();
								}
								
								@Override
								public void onError() {
									loader.stopLoading();
								}
							});
						}
					}
				});
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		String message = failedRule.getFailureMessage();
		if(failedView instanceof EditText){
			failedView.requestFocus();
			((EditText) failedView).setError(message);
		}else{
			showToast(message,true);
		}
	}

	@Override
	public void onValidationSucceeded() {
		//showToast("SUCCESS");
		ParseUser user = new ParseUser();
		user.setUsername(mUserName.getText().toString());
		user.setPassword(mPassword.getText().toString());
		if(mEmail.getText().length() >0){
			user.setEmail(mEmail.getText().toString());
		}
		user.put("name",mFullName.getText().toString());
		user.put("fullname",mFullName.getText().toString());
		user.put("phone",mPhone.getText().toString());
		user.put("city", mCity.getText().toString());
		user.put("dob",mDob.getText().toString());
		
		loader.startPreloader(SignUpActivity.this,"Creating account");
		//startLoading("Signing Up..");
		user.signUpInBackground(new SignUpCallback() {
			
			@Override
			public void done(ParseException e) {
				if(e==null){
					//TODO Create local user info
					ParseUser currentUser = ParseUser.getCurrentUser();
					String parseId = currentUser.getObjectId();
					ParseInstallation installation = ParseInstallation.getCurrentInstallation();
					installation.put("user",ParseUser.getCurrentUser());
					installation.put("myuser",ParseUser.getCurrentUser().getObjectId().toString());
					installation.saveInBackground();
					if(fileName !=null){
						new ParseUser();
						ParseUser user2 = ParseUser.getCurrentUser();
						Bitmap bmp = BitmapFactory.decodeFile(f.toString());
						ByteArrayOutputStream stream = new ByteArrayOutputStream();
				        // Compress image to lower quality scale 1 - 100
				        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
				        byte[] images = stream.toByteArray();
				        ParseFile file = new ParseFile(fileName+".png",images);
						file.saveInBackground();
						user2.put("profilethumbnail",file);	
						user2.saveInBackground();
					}

					loader.stopLoading();
					Intent i = new Intent(getApplicationContext(), HomeActivity.class);
					startActivity(i);
					finish();
				}else{
					switch(e.getCode()){
					case ParseException.EMAIL_TAKEN:
						showToast("Email alredy being used.",false);
						loader.stopLoading();
					break;
					case ParseException.USERNAME_TAKEN:
						showToast("Username not available.",false);
						loader.stopLoading();
					break;
					}
				}
			}
		});
	}

	@Override
	public void onDatePicked(int year, int month, int day) {
		String dob = String.format(Locale.getDefault(),"%02d/%02d/%d",day,month+1,year);
		mDob.setText(dob);
	}
	
	public void openSelectionDialog(){
		String[] selections = new String[]{"Gallery","Camera"};
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle("Choose Method");
		dialog.setItems(selections, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(which == 0){
					//Gallery
					Toast.makeText(getApplicationContext(),"G",Toast.LENGTH_LONG).show();
					chooseImage();
				}else if(which == 1){
					//Camera
					Toast.makeText(getApplicationContext(),"Camera",Toast.LENGTH_LONG).show();
					takeImage();
				}
			}
		});
		
		dialog.setNegativeButton("cancel",new android.content.DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		
		dialog.show();
	}
	
	//To manage image
		public void chooseImage(){
			Toast.makeText(getApplicationContext(),"K",Toast.LENGTH_LONG).show();
			choiceType = ChooserType.REQUEST_PICK_PICTURE;
			chooserManager = new ImageChooserManager(this,choiceType,"Fup/temp",true);
			chooserManager.setImageChooserListener(this);
			try {
				loader.startPreloader(this,"Fetching Data..");
				filePath = chooserManager.choose();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public void takeImage(){
			choiceType = ChooserType.REQUEST_CAPTURE_PICTURE;
			chooserManager = new ImageChooserManager(this,choiceType,"Fup/temp",true);
			chooserManager.setImageChooserListener(this);
			try {
				loader.startPreloader(this,"Fetching Data..");
				filePath = chooserManager.choose();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}


	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.ivUserProfile:
			openSelectionDialog();
		break;
		case R.id.bSignUp:
			if (!Util.isNetworkAvailable(SignUpActivity.this)) {				
				DialogFactory.getBeaconStreamDialog1(SignUpActivity.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			} else 
			
			{
			validator.validate();
			}
		break;
		case R.id.etDob:
			DialogFragment df = new DatePickerFragment();
			df.show(getFragmentManager(),"datePicker");
		break;
		}
	}
	
	public void showToast(String message,boolean top){
		Toast toast = Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG);
		if(top){
			toast.setGravity(Gravity.TOP, 0, 0);
		}
		toast.show();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(this,SplashActivity.class);
		startActivity(intent);
		finish();
	}
	
}
