package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.Util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookRequestError;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.parse.CountCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;

public class SplashActivity extends Activity implements AnimationListener,
		OnClickListener {
	private static int SPLASH_TIME_OUT = 2000;
	private ProgressDialog pd;
	// private Preloader loader = new Preloader();
	private ImageView imgLogo;
	private TextView mLogin, mAlready, tvWalkthrough;
	private LinearLayout ll;
	private Button mRegister, mFacebook, mGooglePlus;
	private AppConfig _config;
	RelativeLayout rlWalkthrough;
	ParseUser currentUser;
	// added by nabasree
	public String login_type = null;
	Animation animMove, animFadein;
	private GestureDetector gestureDetector;
	View.OnTouchListener gestureListener;
	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_MAX_OFF_PATH = 250;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;
	RelativeLayout splash_ll;


	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
		setContentView(R.layout.splash_layout);
		

   
		pd = new ProgressDialog(this);
		pd.setTitle("Connecting");
		pd.setMessage("Connecting to FaceBook");
		TextView walkthrough = (TextView) this.findViewById(R.id.tvWalkthrough);
		walkthrough.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SplashActivity.this,
						MainActivity.class);
				startActivity(intent);
			}
		});
		// added by nabasree
		// PushService.setDefaultPushCallback(this,
		// NotificationActivityNew.class);
		_config = new AppConfig(getApplicationContext());
		configParse();
		Typeface tf = Typeface.createFromAsset(SplashActivity.this.getAssets(),
				"fonts/RobotoCondensed-Light.ttf");
		Spanned text = Html.fromHtml("<b>Login</b>");
		// textfield.setTypeface(tf,Typeface.BOLD);
		// Gesture detection
		// gestureDetector = new GestureDetector(this, new MyGestureDetector());
		// gestureListener = new View.OnTouchListener() {
		// public boolean onTouch(View v, MotionEvent event) {
		// return gestureDetector.onTouchEvent(event);
		// }
		// };
		imgLogo = (ImageView) findViewById(R.id.imgLogo);
		mLogin = (TextView) findViewById(R.id.tvLogin);
		mAlready = (TextView) findViewById(R.id.tvAlready);
		tvWalkthrough = (TextView) findViewById(R.id.tvWalkthrough);
		rlWalkthrough = (RelativeLayout) findViewById(R.id.rl);
		mLogin.setText(text);
		mLogin.setTypeface(tf);
		mAlready.setTypeface(tf);
		tvWalkthrough.setTypeface(tf);
		mLogin.setOnClickListener(this);

		mFacebook = (Button) findViewById(R.id.bFacebookLogin);
		mFacebook.setOnClickListener(this);
		mRegister = (Button) findViewById(R.id.bRegister);
		mRegister.setOnClickListener(this);

		mFacebook.setTypeface(tf);
		mRegister.setTypeface(tf);
		// calling gesture detector for relative layout
		// splash_ll.setOnClickListener(this);
		// splash_ll.setOnTouchListener(gestureListener);
		imgLogo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Intent i = new
				// Intent(SplashActivity.this,FlipHorizontalLayoutActivity.class);
				// startActivity(i);

			}
		});
		rlWalkthrough.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivity(new Intent(SplashActivity.this,
						MainActivity.class));
			}
		});
		if (currentUser != null) {
			// User is logged in, send him to activity feed
			installPush();
			moveNext();
		} else {
			// Do animation and show login buttons
			// animMove =
			// AnimationUtils.loadAnimation(getApplicationContext(),R.anim.move);
			// animMove.setAnimationListener(this);
			// autoLoad();
		}
	}

	// class MyGestureDetector extends SimpleOnGestureListener {
	// @Override
	// public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
	// float velocityY) {
	// try {
	// if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
	// return false;
	// // right to left swipe
	// if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) >
	// SWIPE_THRESHOLD_VELOCITY) {
	// Toast.makeText(SplashActivity.this, "Left Swipe",
	// Toast.LENGTH_SHORT).show();
	// Intent i = new
	// Intent(SplashActivity.this,FlipHorizontalLayoutActivity.class);
	// startActivity(i);
	// } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE &&
	// Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	// Toast.makeText(SplashActivity.this, "Right Swipe",
	// Toast.LENGTH_SHORT).show();
	// }
	// } catch (Exception e) {
	// // nothing
	// }
	// return false;
	// }
	//
	// @Override
	// public boolean onDown(MotionEvent e) {
	// return true;
	// }
	// }

	public void configParse() {
		_config = new AppConfig(this);
		/*
		 * String[] parseKeys = _config.getAppKeys("parse");
		 * Parse.initialize(this, parseKeys[0], parseKeys[1]);
		 * ParseFacebookUtils.initialize(getString(R.string.app_id));
		 */
		currentUser = ParseUser.getCurrentUser();
	}

	public void installPush() {
		ParseInstallation installation = ParseInstallation
				.getCurrentInstallation();
		installation.put("user", currentUser);
	}

	@Override
	public void onAnimationEnd(Animation arg0) {
		ll.setVisibility(View.VISIBLE);
	}

	private void autoLoad() {
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// After timeout do animation
				imgLogo.startAnimation(animMove);
			}
		}, SPLASH_TIME_OUT);
	}

	@Override
	public void onAnimationRepeat(Animation arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onAnimationStart(Animation arg0) {
		// TODO Auto-generated method stub
	}

	// added by nabasree
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bRegister:
			login_type = "register";
			if (!_config.getTNC()) {
				// SHOW TNC of the app
				showTnc();
			}
			break;
		case R.id.bFacebookLogin:
			// User clicked facebook login
			login_type = "fbregister";
			if (!_config.getTNC()) {
				// SHOW TNC of the app
				showTnc();
			}
			break;
		case R.id.tvLogin:
			Intent i = new Intent(getApplicationContext(), LoginActivity.class);
			startActivity(i);
			finish();
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if (requestCode ==
		// com.facebook.Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE)
		ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}

	public void facebookData(final boolean firstTime) {
		Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
				new Request.GraphUserCallback() {
					@Override
					public void onCompleted(GraphUser user, Response response) {
						if (user != null) {
							// Toast.makeText(getApplicationContext(),"A",
							// Toast.LENGTH_LONG).show();
							currentUser = ParseUser.getCurrentUser();
							currentUser.put("fullname", user.getName());
							currentUser.put("dob", user.getBirthday());
							currentUser.put("email", user.asMap().get("email"));
							currentUser.put("facebookid", user.getId());
							currentUser.put("facebookConnected", true);
							currentUser.saveInBackground(new SaveCallback() {
								@Override
								public void done(ParseException e) {
									if (e == null) {
										// All safe, enable push and go to next
										// intent
										installPush();
										// loader.stopLoading();

										try {
											PackageInfo info = getPackageManager()
													.getPackageInfo(
															"in.fastrack.app.m.fup",
															PackageManager.GET_SIGNATURES);
											for (Signature signature : info.signatures) {
												MessageDigest md = MessageDigest
														.getInstance("SHA");
												md.update(signature
														.toByteArray());
												Log.d("KeyHash:", Base64
														.encodeToString(
																md.digest(),
																Base64.DEFAULT));
											}
										} catch (NameNotFoundException e1) {
											Log.d("KeyHash:", "error");

										} catch (NoSuchAlgorithmException e2) {
											Log.d("KeyHash:", "error1");

										}
										if (!Util
												.isNetworkAvailable(SplashActivity.this)) {
											DialogFactory
													.getBeaconStreamDialog1(
															SplashActivity.this,
															getResources()
																	.getString(
																			R.string.nw_error_str),
															Util.onClickListner,
															Util.retryClickListner)
													.show();
										} else {
											moveNext();
										}
									} else {
										// Toast.makeText(getApplicationContext(),"D",
										// Toast.LENGTH_LONG).show();
										/*
										 * if
										 * (!ParseFacebookUtils.isLinked(currentUser
										 * )) {
										 * ParseFacebookUtils.link(currentUser,
										 * SplashActivity.this, new
										 * SaveCallback() {
										 * 
										 * @Override public void
										 * done(ParseException ex) { if
										 * (ParseFacebookUtils
										 * .isLinked(currentUser)) {
										 * Log.d("MyApp",
										 * "Woohoo, user logged in with Facebook!"
										 * ); } } }); }
										 */
										Log.e("MyApp", e.toString());
										// loader.stopLoading();
									}
								}

							});
						} else if (response.getError() != null) {
							Toast.makeText(getApplicationContext(), "B",
									Toast.LENGTH_LONG).show();
							if ((response.getError().getCategory() == FacebookRequestError.Category.AUTHENTICATION_RETRY)
									|| (response.getError().getCategory() == FacebookRequestError.Category.AUTHENTICATION_REOPEN_SESSION)) {
								Log.d("MyApp",
										"The facebook session was invalidated.");
							} else {
								Log.d("MyApp", "Some other error: "
										+ response.getError().getErrorMessage());
							}
							// loader.stopLoading();
							ll.setVisibility(View.VISIBLE);
						} else {
							// Toast.makeText(getApplicationContext(),"C",
							// Toast.LENGTH_LONG).show();
							// installPush();
							// stopLoading();
							// moveNext();
						}
					}
				});
		request.executeAsync();
	}

	private void moveNext() {
		try {
			ParseInstallation installation = ParseInstallation
					.getCurrentInstallation();
			installation.put("user", ParseUser.getCurrentUser());
			installation.put("myuser", ParseUser.getCurrentUser().getObjectId()
					.toString());
			installation.saveInBackground();
			Intent i = new Intent(this, HomeActivity.class);
			
			startActivity(i);
			finish();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// added by nabasree
	private void showTnc() {
		final Dialog d = new Dialog(this);
		d.setContentView(R.layout.terms_dialog);
		d.setTitle("User Terms and Conditions");
		d.show();
		WebView web = (WebView) d.findViewById(R.id.webView);
		web.setWebViewClient(new WebViewClient());
		web.loadUrl("http://fastrack.in/fup/tnc.html");
		Button accept = (Button) d.findViewById(R.id.button1);
		Button decline = (Button) d.findViewById(R.id.button2);
		accept.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (login_type.equalsIgnoreCase("register")) {
					Intent r = new Intent(getApplicationContext(),
							SignUpActivity.class);
					startActivity(r);
					finish();
				} else if (login_type.equalsIgnoreCase("fbregister")) {
					if (!Util.isNetworkAvailable(SplashActivity.this)) {
						DialogFactory
								.getBeaconStreamDialog1(
										SplashActivity.this,
										getResources().getString(
												R.string.nw_error_str),
										Util.onClickListner,
										Util.retryClickListner).show();
					} else {
						pd.show();
						// loader.startPreloader(SplashActivity.this,"Connecting Facebook");
						ParseFacebookUtils.logIn(Arrays.asList("email",
								"user_birthday", "user_friends"),
								SplashActivity.this, new LogInCallback() {
									@Override
									public void done(ParseUser user,
											ParseException e) {
										// ll.setVisibility(View.GONE);
										if (user == null) {
											Log.d("MyApp",
													"Uh oh. The user cancelled the Facebook login.");
											Toast.makeText(
													getApplicationContext(),
													e.getLocalizedMessage(),
													Toast.LENGTH_LONG).show();
											// loader.stopLoading();
											pd.dismiss();
											finish();
										} else if (user.isNew()) {
											Log.d("MyApp",
													"User signed up and logged in through Facebook!");
											facebookData(true);
										} else {
											Log.d("MyApp",
													"User logged in through Facebook!");
											facebookData(false);
										}
									}
								});
					}
				}
				d.dismiss();
			}
		});
		decline.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				d.dismiss();
			}
		});
		// String title = this.getString(R.string.tnctitle);
		// String message =
		// this.getString(R.string.tncmessagepara1)+"\n\n"+this.getString(R.string.tncmessagepara2);
		// AlertDialog.Builder builder = new
		// AlertDialog.Builder(this).setTitle(title).setMessage(message).setPositiveButton("Accept",new
		// DialogInterface.OnClickListener() {
		// @Override
		// public void onClick(DialogInterface d, int arg1) {
		// // _config.setTNC(true);
		//
		// if(login_type.equalsIgnoreCase("register")){
		// Intent r = new Intent(getApplicationContext(),SignUpActivity.class);
		// startActivity(r);
		// finish();
		// }
		// else if(login_type.equalsIgnoreCase("fbregister")){
		//
		// if (!Util.isNetworkAvailable(SplashActivity.this)) {
		// DialogFactory.getBeaconStreamDialog1(SplashActivity.this,
		// getResources().getString(R.string.nw_error_str),Util.onClickListner,
		// Util.retryClickListner).show();
		// } else
		//
		// {
		//
		//
		//
		//
		// loader.startPreloader(SplashActivity.this,"Connecting Facebook");
		// ParseFacebookUtils.logIn(Arrays.asList("email",
		// "user_birthday","user_friends"),SplashActivity.this,new
		// LogInCallback() {
		//
		// @Override
		// public void done(ParseUser user, ParseException e) {
		// ll.setVisibility(View.GONE);
		//
		// if (user == null) {
		// Log.d("MyApp","Uh oh. The user cancelled the Facebook login.");
		// Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),
		// Toast.LENGTH_LONG).show();
		// loader.stopLoading();
		// finish();
		// } else if (user.isNew()) {
		// Log.d("MyApp","User signed up and logged in through Facebook!");
		// facebookData(true);
		//
		// } else {
		// Log.d("MyApp","User logged in through Facebook!");
		// facebookData(false);
		// }
		// }
		// });
		// }
		// }
		//
		// d.dismiss();
		// }
		// })
		// .setNegativeButton("Decline",new DialogInterface.OnClickListener() {
		//
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// dialog.dismiss();
		// }
		// });
		// builder.create().show();
	}

}
