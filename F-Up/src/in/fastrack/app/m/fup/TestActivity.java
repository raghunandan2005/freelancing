package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.config.AppConfig;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.PushService;

public class TestActivity extends Activity implements OnClickListener {
	
	private AppConfig _config;
	Button push;
	ParseUser currentUser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test_activity);
		_config = new AppConfig(getApplicationContext());
		configParse();
		
		PushService.setDefaultPushCallback(this, TestActivity.class);

		ParseInstallation.getCurrentInstallation().saveInBackground();
		push = (Button)findViewById(R.id.senPushB);
		push.setOnClickListener(this);
	}
	
	public void configParse(){
		_config = new AppConfig(this);
		currentUser = ParseUser.getCurrentUser();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.action_menu_createalarm, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		JSONObject obj;
		try {
			obj =new JSONObject();
			obj.put("channel", "customchannel");
			obj.put("action","in.fasttrack.app.m.fup.pushnotification.UPDATE_STATUS");
			obj.put("customdata","My  Custom Notification Message");

			ParsePush push = new ParsePush();
			ParseQuery query = ParseInstallation.getQuery();
			
			// Notification for Android users
			//query.whereEqualTo("deviceType", "android");
			query.whereEqualTo("user", "Sfmg2nILSl");
			push.setQuery(query);
			push.setData(obj);
			push.setChannel("customchannel");
			PushService.subscribe(getApplicationContext(), "customchannel", HomeActivity.class);
			PushService.setDefaultPushCallback(this, HomeActivity.class);
			push.sendInBackground(); 
			Log.d("asdfsda", "asdfasasadd");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
