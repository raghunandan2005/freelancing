package in.fastrack.app.m.fup.adapter;

import in.fastrack.app.m.fup.ProfileActivity;
import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.AlarmFriend;
import in.fastrack.app.m.fup.model.NewsFeed;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

public class AlarmFriendsAdapter extends ArrayAdapter<AlarmFriend> {

	private Context context;
	private int resource;
	private ArrayList<AlarmFriend> list;
	String userid = "", user_id;
	private LayoutInflater mInflater;

	public AlarmFriendsAdapter(Context context, int resource,
			ArrayList<AlarmFriend> list) {
		super(context, resource, list);
		this.context = context;
		this.resource = resource;
		this.list = list;
		// this.list.addAll(list);
		mInflater = LayoutInflater.from(context);

	}

	static class ViewHolder {
		TextView mName;
		ImageView mUserImage;
		Button mAction;
		LinearLayout ll;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		

		if (convertView == null) {

			convertView = mInflater.inflate(resource, parent, false);
			holder = new ViewHolder();
			holder.mName = (TextView) convertView.findViewById(R.id.tvPhoneName);
			holder.mUserImage = (ImageView) convertView.findViewById(R.id.ivUserImage);
			holder.mAction = (Button) convertView.findViewById(R.id.bInviteUser);
			holder.ll = (LinearLayout) convertView.findViewById(R.id.ll);
			holder.mAction.setVisibility(View.INVISIBLE);
			convertView.setTag(holder);
		} else {
			
				holder =(ViewHolder) convertView.getTag();
			
		}
		
		AlarmFriend rowItem = (AlarmFriend) list.get(position);
		holder.mName.setText(rowItem.getName());
		convertView.setTag(R.string.tag,rowItem);
		//holder.ll.setTag(rowItem);

		userid = rowItem.getUserId();
//		Picasso.with(context).load(rowItem.getUserImage()).fit()
//		.centerCrop().transform(new RoundedTransform())
//		.placeholder(R.drawable.friend_nopic)
//		.error(R.drawable.friend_nopic).into(holder.mUserImage);
  	

		if (rowItem.getUserImage() != null) {
			Picasso.with(context).load(rowItem.getUserImage()).fit()
					.centerCrop().transform(new RoundedTransform())
					.placeholder(R.drawable.friend_nopic)
					.error(R.drawable.friend_nopic).into(holder.mUserImage);
		} else {
			holder.mUserImage.setImageResource(R.drawable.friend_nopic);
		}

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				

				AlarmFriend a = (AlarmFriend) v.getTag(R.string.tag);
				Toast.makeText(context, "Clicked"+a.getName(), Toast.LENGTH_LONG).show();
				// TODO Auto-generated method stub

				user_id = a.getUserId();
				Intent profile = new Intent(context, ProfileActivity.class);
					//	.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				// profile.putExtra("userpath", "adapter");
				// Util.setCurrent_profile_id(ff.getCreator());

				if (user_id.equals(ParseUser.getCurrentUser().getObjectId()
						.toString())) {
					profile.putExtra("userpath", "home");
					Util.setCurrent_profile_id(ParseUser.getCurrentUser()
							.getObjectId().toString());
				} else {
					profile.putExtra("userpath", "adapter");
					Util.setCurrent_profile_id(user_id);
				}

				context.startActivity(profile);
				Log.i("list", ">>>>" + userid + "-----" + a.getUserId());
			}
		});

		return convertView;
	}

	public void addNewFeed(AlarmFriend data) {
		this.list.add(data);
		notifyDataSetChanged();
	}

	public void addNew(AlarmFriend data) {
		// Toast.makeText(getContext(),"NEW DATA INSERT",Toast.LENGTH_LONG).show();
		this.list.add(0, data);
		notifyDataSetChanged();
	}

}
