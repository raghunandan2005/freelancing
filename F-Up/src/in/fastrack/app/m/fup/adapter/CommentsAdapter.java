package in.fastrack.app.m.fup.adapter;


import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Comment;
import in.fastrack.app.m.fup.model.FeedEvent;
import in.fastrack.app.m.fup.model.FeedObject;
import java.util.ArrayList;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import de.greenrobot.event.EventBus;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
//import android.widget.Toast;
import in.fastrack.app.m.fup.R;
public class CommentsAdapter extends ArrayAdapter<Comment> {

	private Context context;
	private int layoutId;
	private ArrayList<Comment> list;
	private String commentId,alarmId;
	private ParseObject alarmObject = null;
	private ViewHolder holder = null;
	private ParseObject parse;
	private  int pos;
	LayoutInflater  mInflater;

	public CommentsAdapter(Context context, int resource, ArrayList<Comment> objectsl) {
		super(context, resource, objectsl);
		this.context = context;
		this.layoutId = resource;
		this.list =objectsl;
		mInflater = LayoutInflater.from(context);
	}
	

	private class ViewHolder{
		
		ImageView mUserProfile;
		TextView mComment,mUserName;
		Button mDelete;
		
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
	
	     Comment item = list.get(position);

		if(convertView == null){
			
			convertView = mInflater.inflate(layoutId,parent,false);
			holder = new ViewHolder();
			holder.mComment = (TextView) convertView.findViewById(R.id.tvUserName);
			holder.mUserName = (TextView) convertView.findViewById(R.id.tvComment);
			holder.mUserProfile = (ImageView)convertView.findViewById(R.id.ivUserImage);
			holder.mDelete = (Button)convertView.findViewById(R.id.bDelete);
			convertView.setTag(holder);
			
		}else{
			
			holder = (ViewHolder) convertView.getTag();
			
		}
		if(item.getAlarmCreator()!=null){
			 Log.i("item.getAlarmCreator()","have"+item.getAlarmCreator()+"\n"+ParseUser.getCurrentUser().getObjectId().toString());	
			
		if(item.getAlarmCreator().equals(ParseUser.getCurrentUser().getObjectId().toString())){
			holder.mDelete.setVisibility(View.VISIBLE);
		}
		else{
			holder.mDelete.setVisibility(View.GONE);
		}
			holder.mComment.setTag(item);
			holder.mDelete.setTag(item);
			
			
			holder.mDelete.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					//Fragment
					if (!Util.isNetworkAvailable(context)) {	
				
				     DialogFactory.getBeaconStreamDialog1(context, context.getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			       } else 
			
			       {
	
				  Comment f = (Comment) v.getTag();
			//	  Log.i("f","have"+f.getComment()+"\n"+f.getCommentId());
				  
				  
					 commentId = f.getCommentId();
					 alarmId = f.getAlarmId();
					 
					// Log.i("item","have"+f.getComment()+"\n"+f.getCommentId()+"\n"+f.getAlarmId());
					 
					 
					pos = position;
					AlertDialog.Builder alertDialogBuilderDisconnect = new AlertDialog.Builder(context);
					alertDialogBuilderDisconnect.setMessage("Do you want to remove ?");
					alertDialogBuilderDisconnect.setIcon(R.drawable.alarm);
					alertDialogBuilderDisconnect.setNegativeButton("NO", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.cancel();
						}
					});
					alertDialogBuilderDisconnect.setPositiveButton("YES", new DialogInterface.OnClickListener() {
	                    public void onClick(final DialogInterface dialog, int id) {
	                    	ParseQuery<ParseObject> like = ParseQuery.getQuery("Comments");
	                    	dialog.cancel();
	                    	if(commentId!=null){
							like.getInBackground(commentId, new GetCallback<ParseObject>() {
								@Override
								public void done( final ParseObject object, com.parse.ParseException e) {
									if(e == null){
									//parse = object;
									
									ParseQuery<ParseObject> q = ParseQuery.getQuery("alarms");
									q.getInBackground(alarmId,new GetCallback<ParseObject>() {
										
										@Override
										public void done(ParseObject obj, ParseException e) {
											if(e == null){
												//alarmObject = object;
												
												
												
												try {
													object.put("deleteC", (obj.get("connected")));//ParseUser.getCurrentUser().getObjectId().toString()
													object.saveInBackground();
													list.remove(pos);
													notifyDataSetChanged();
												} catch (Exception e1) {
													// TODO Auto-generated catch block
													e1.printStackTrace();
												}
												
												try {
													obj.increment("comments", -1);
													notifyDataSetChanged();
													FeedObject object2= new FeedObject(2,Integer.parseInt(obj.getString("requestcode")),true,position,obj.getInt("comments"));
													EventBus.getDefault().postSticky(new FeedEvent(object2));
													obj.saveInBackground();
												} catch (NumberFormatException e1) {
													// TODO Auto-generated catch block
													e1.printStackTrace();
												}
												
											}
										}
									});
									
									
									
								}
								}
   
							});
	                    	}
							
							
							//Toast.makeText(context, "Removed from the Comment", Toast.LENGTH_SHORT).show();
	                    }
	                });
					AlertDialog alertDialogDisconnect = alertDialogBuilderDisconnect.create();
					alertDialogDisconnect.show(); 
					  
					
				}	
				}
				

			});
			
			
		
		
		
		
		
		
		holder.mComment.setText(item.getComment());
		holder.mUserName.setText(item.getCommentUserName());
		//Log.i("COMMENT IMAGE",item.getCommentUserProfile());
		Picasso.with(context).load(item.getCommentUserProfile()).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into(holder.mUserProfile);
		
		}
		return convertView;
	}

//	public void setComment(Comment cmt){
//		this.list.add(cmt);
//		notifyDataSetChanged();
//	}
//	
//
//	public void setNew(Comment data){
//		//Toast.makeText(getContext(),"NEW DATA INSERT",Toast.LENGTH_LONG).show();
//		this.list.add(0,data);
//		notifyDataSetChanged();
//	}
	
	
}

/*ParseQuery<ParseObject> q = ParseQuery.getQuery("alarms");
q.getInBackground(alarmId,new GetCallback<ParseObject>() {
	
	@Override
	public void done(ParseObject objs, ParseException e) {
		if(e == null){
			//alarmObject = object;
			
			try {
				if( objs.getString("creatorplain").equals(ParseUser.getCurrentUser().getObjectId().toString())){
					holder.mDelete.setVisibility(View.VISIBLE);
               	}
				
				else{
					holder.mDelete.setVisibility(View.GONE);
				}
			
				
			} catch (NumberFormatException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
	}
});
*/

