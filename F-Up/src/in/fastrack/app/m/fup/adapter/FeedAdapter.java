package in.fastrack.app.m.fup.adapter;

import in.fastrack.app.m.fup.AlaramActivityNew;
import in.fastrack.app.m.fup.AlarmFriends;
import in.fastrack.app.m.fup.FeedComments;
import in.fastrack.app.m.fup.FeedFragment;
import in.fastrack.app.m.fup.ProfileActivity;
import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.alarm.CreateNewAlarmActivity;
import in.fastrack.app.m.fup.database.DatabaseHandler;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.AlarmFriend;
import in.fastrack.app.m.fup.model.NewsFeed;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SendCallback;
import com.squareup.picasso.Picasso;

public class FeedAdapter extends ArrayAdapter<NewsFeed> {
	
	private static Context context;
	private ArrayList<NewsFeed> feed;
	int layoutResourceId;
	public static ArrayList<AlarmFriend> answers = new ArrayList<AlarmFriend>();
	private int seconds,min,hours,days,years;
	private String ampmText;
	private static String[] nDays = {"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
	private static String[] nMonths = {"January","febuary","March","April","May","June","July","August","September","October","November","December"};
	
	private static final int SECOND_MILLIS = 1000;
	private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
	private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
	private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
	
	//private JSONArray friendsId = null;
	private JSONArray arraynew = null;
	private JSONArray jarraydifferent =null;
	private JSONArray jarraydifferentunique =null;
	private JSONArray jarraysimilar =null;
	
	ViewHolder holder;
	
	public FeedAdapter(Context context, int layoutResourceId,ArrayList<NewsFeed> feed) {
		super(context, layoutResourceId,feed);
		this.context = context;
		this.feed = new ArrayList<NewsFeed>();
		this.layoutResourceId = layoutResourceId;
		this.feed.addAll(feed);
	}
	
	private class ViewHolder{
		ImageView mUserProfileImage,mPreview,mCategoryIcon;
		TextView mAuthor,mCategory,mTitle,mCreatedTime,mDay,mMonth,mFriends,mTime,ampmText;
		Button mLikes,mComments;
		ImageButton mDelete;
		LinearLayout mProfile_page;
	}
	
	public String stringtolower(String myString){
		return myString.replaceAll(" ","_").toLowerCase(Locale.getDefault());
	}
	
	

	@Override
	public int getCount() {
		return feed.size();
	}

	@Override
	public NewsFeed getItem(int position) {
		return feed.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public void reset(boolean state){
		ArrayList<NewsFeed> n = new ArrayList<NewsFeed>();
		n = feed;
		for(NewsFeed nf:n){
			nf.setPlaying(state);
		}
		//holder.mPreview.setBackgroundResource(R.drawable.created_play_icon);
		notifyDataSetChanged();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		holder = null;
		NewsFeed item = feed.get(position);
		Log.i("item","have"+item.toString());
		if(convertView == null){
			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(layoutResourceId,parent,false);
			holder = new ViewHolder();
			holder.mAuthor = (TextView) convertView.findViewById(R.id.tvUserName);
			holder.mCreatedTime = (TextView) convertView.findViewById(R.id.tvCreatedTime);
			holder.mTitle = (TextView) convertView.findViewById(R.id.tvAlarmTitle);
			holder.mFriends = (TextView) convertView.findViewById(R.id.tvFriends);
			holder.mTime = (TextView) convertView.findViewById(R.id.tvAlarmTime);
			holder.mDay = (TextView) convertView.findViewById(R.id.tvAlarmDay);
			holder.ampmText = (TextView) convertView.findViewById(R.id.ampmtext);
			holder.mLikes = (Button) convertView.findViewById(R.id.bLikes);
			holder.mComments = (Button) convertView.findViewById(R.id.bComment);
			holder.mUserProfileImage = (ImageView) convertView.findViewById(R.id.ivUserImage);
			holder.mPreview = (ImageView) convertView.findViewById(R.id.ivPreview);
			holder.mCategoryIcon = (ImageView) convertView.findViewById(R.id.ivCategoryImage);
			holder.mCategory = (TextView) convertView.findViewById(R.id.tvAlarmListens);
			holder.mDelete = (ImageButton) convertView.findViewById(R.id.deleteit);
			holder.mProfile_page = (LinearLayout) convertView.findViewById(R.id.profile_page);
			
			convertView.setTag(holder);
		}else
		{
			holder = (ViewHolder) convertView.getTag();
		}
			
			
			holder.mProfile_page.setOnClickListener(new OnClickListener() {	
				@Override
				public void onClick(View v) {
					final NewsFeed ff = (NewsFeed) v.getTag();
					Intent profile = new Intent(context,ProfileActivity.class);
					  Log.e("ff.getCreator() is", " "+ff.getCreator()+"ParseUser.getCurrentUser().getObjectId().toString() is"+ParseUser.getCurrentUser().getObjectId().toString());
					if(ff.getCreator().equals(ParseUser.getCurrentUser().getObjectId().toString())) 
					{
						profile.putExtra("userpath", "home");
						Util.setCurrent_profile_id(ParseUser.getCurrentUser().getObjectId().toString());
					}
					else
					{
						profile.putExtra("userpath", "adapter");
						Util.setCurrent_profile_id(ff.getCreator());
					}
					context.startActivity(profile);
				}
			});

			holder.mDelete.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					
					
					
					
					//Fragment
					if (!Util.isNetworkAvailable(context)) {	
				
				     DialogFactory.getBeaconStreamDialog1(context, context.getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			       } else 
			
			       {


			   		ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
			   		query.whereEqualTo("user", ParseUser.getCurrentUser());
			   		query.findInBackground(new FindCallback<ParseObject>() {
			   			@Override
			   			public void done(List<ParseObject> objects, com.parse.ParseException e) {
			   				if(e==null){
			   				if(objects.size() > 0) {
			   					for(ParseObject obj : objects) { 
			   						
			   						arraynew = obj.getJSONArray("connectedSF");
			   						
			   						break;
			   					}
			   				}
			   				}
			   			}

			   		
			   		});
					
					final NewsFeed f = (NewsFeed) v.getTag();
					final int pos = position;
					AlertDialog.Builder dialogbuilder=new AlertDialog.Builder(context);
					
					final ArrayList<String> actions = new ArrayList<String>();
					actions.add("Flag");
					actions.add("Remove");
					
					if(f.getCreator().equals(ParseUser.getCurrentUser().getObjectId().toString())) {
						actions.add("Edit");
					}
					
					final CharSequence[] items = actions.toArray(new CharSequence[actions.size()]);
					//final CharSequence[] items={"Flag","Removed","Edit"};
					
					dialogbuilder.setItems(items, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							switch (which) {
							case 0:
								//Flag
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
								alertDialogBuilder.setMessage("Flag '" + f.getAlarmTitle() + "' ?");
								alertDialogBuilder.setIcon(R.drawable.alarm);
								alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,int id) {
										dialog.cancel();
									}
								});
								alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				                    public void onClick(DialogInterface dialog, int id) {
				                    	ParseQuery<ParseObject> like = ParseQuery.getQuery("alarms");
										like.getInBackground(f.getAlarmid(), new GetCallback<ParseObject>() {
											
											@Override
											public void done(final ParseObject object, com.parse.ParseException e) {
												if(object.get("flagged") != null) {
													final String[] peopleFlaggedTheAlarm = object.get("flagged").toString().split(",");
													Log.d("list", Boolean.toString(Arrays.asList(peopleFlaggedTheAlarm).contains(ParseUser.getCurrentUser().getObjectId().toString())));
													Log.d("list", Arrays.asList(peopleFlaggedTheAlarm).toString());
													Log.d("list", ParseUser.getCurrentUser().getObjectId().toString());
													object.addAllUnique("flagged", Arrays.asList(ParseUser.getCurrentUser().getObjectId().toString()));
													
													object.saveInBackground(new SaveCallback() {
														public void done(com.parse.ParseException e) {
															if(peopleFlaggedTheAlarm.length == 2) {
																object.put("status", false);
																object.saveInBackground();
															}
														}
													});
													feed.remove(pos);
													notifyDataSetChanged();
													Toast.makeText(context, "The Alarm is now flagged", Toast.LENGTH_SHORT).show();
												} else {
													object.add("flagged", ParseUser.getCurrentUser().getObjectId().toString());
													object.saveInBackground();
													Toast.makeText(context, "The Alarm is now flagged", Toast.LENGTH_SHORT).show();
													feed.remove(pos);
													notifyDataSetChanged();
												}
												
												Intent intent = new Intent(context,  AlaramActivityNew.class);
												PendingIntent pendingIntent = PendingIntent.getActivity(context, f.getAlarmrequestid(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
												AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
												alarmManager.cancel(pendingIntent);
												Toast.makeText(context, "The Alarm is now flagged", Toast.LENGTH_SHORT).show();
												feed.remove(pos);
												notifyDataSetChanged();
											}
										});
				                    }
				                });
								AlertDialog alertDialog = alertDialogBuilder.create();
								alertDialog.show();  
								break;
							case 1:
								//Disconnect
							
								AlertDialog.Builder alertDialogBuilderDisconnect = new AlertDialog.Builder(context);
								alertDialogBuilderDisconnect.setMessage("Remove '" + f.getAlarmTitle() + "' ?");
								alertDialogBuilderDisconnect.setIcon(R.drawable.alarm);
								alertDialogBuilderDisconnect.setNegativeButton("NO", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,int id) {
										dialog.cancel();
									}
								});
								alertDialogBuilderDisconnect.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				                    public void onClick(DialogInterface dialog, int id) {
				                    	ParseQuery<ParseObject> like = ParseQuery.getQuery("alarms");
										like.getInBackground(f.getAlarmid(), new GetCallback<ParseObject>() {
											@Override
											public void done(final ParseObject object, com.parse.ParseException e) {
												try {
												 if(f.getCreator().equals(ParseUser.getCurrentUser().getObjectId().toString()))  { 
													 Toast.makeText(context, "Removed from the Alarm in if", Toast.LENGTH_SHORT).show();
													    object.put("Hideit", object.get("connected"));
														object.saveInBackground();
														feed.remove(pos);
														notifyDataSetChanged();
														
															//Earlier here
															/*ParseObject notification = new ParseObject("Activity");
															notification.put("creator",ParseUser.getCurrentUser());
															notification.put("followers",object.get("connected"));
															notification.put("reference",object);
															notification.put("notificationtype",9);
															notification.put("notificationDone",false);
															notification.put("readers",new JSONArray());
															notification.add("readers", ParseUser.getCurrentUser().getObjectId());
															notification.saveInBackground();//saveEventually();();
*/                                                            															
															// parse push implementation
															
													
														
															ArrayList<String> jarray =  (ArrayList<String>) object.get("connected");
															//Added newly
															//friendsId = (JSONArray) object.get("connected");
															jarraydifferent = new JSONArray();
															jarraysimilar = new JSONArray();
															jarraydifferentunique  = new JSONArray();

															if (jarray != null) {
																Log.i("friendsId", "friendsId............."+jarray.toString());
																for (int i = 0; i < jarray.size(); i++) {
																	try {

																		if (arraynew != null && arraynew.length()!=0) {
																			Log.i("arraynew", "arraynew.............."+arraynew.toString());
																			for (int j = 0; j < arraynew.length(); j++) {
																				try {
																					if (arraynew.get(j).toString().equals(jarray.get(i).toString())) {

																						jarraysimilar.put(jarray.get(i).toString());

																					}
																					else{
																						
																						jarraydifferent.put(jarray.get(i).toString());
																					}

																				} catch (Exception e4) {
																					e.printStackTrace();
																				}
																			}
																		}
																		else{
																			System.out.println("in else......................"+jarraydifferent.toString());
																			jarraydifferent.put(jarray.get(i).toString());
																			
																		}
																		
																		System.out.println("111111......................"+jarraydifferent.toString()+"222222222222------------------------\n"+jarraysimilar.toString());
																	}

																	catch (Exception e5) {
																		e.printStackTrace();
																	}
																}
															}

															
															if (jarraydifferent != null) {
																
																try {
																	for (int j = 0; j < jarraydifferent.length(); j++) {
																		
																			if (jarraydifferent.get(j).toString().equals(ParseUser.getCurrentUser().getObjectId().toString())) {

																			}
																			else{
																				
																				jarraydifferentunique.put(jarraydifferent.get(j).toString());
																			}

																			
																	
													}
																	
																	System.out.println("33333333333......................"+jarraydifferentunique.toString());
																} catch (Exception e6) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																}
															
															}
															
															
															
															
															

										                     if(jarraysimilar!=null){
										                    	                    	 
										                    		try {
										        						 
										                               // parse push implementation
										            						
										            						for (int j = 0; j < jarraysimilar.length(); j++) {
										            							 Log.d("friendsId.get(j).toString()", "--"+jarraysimilar.get(j).toString()+"ParseUser.getCurrentUser().getObjectId().toString())"+"--"+ParseUser.getCurrentUser().getObjectId().toString());
										            							if(!jarraysimilar.get(j).toString().equals(ParseUser.getCurrentUser().getObjectId().toString())){
										            								
										            								ParseQuery pushQuery = ParseInstallation.getQuery();
																					ParsePush push = new ParsePush();
																					//pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId().toString()); // not me
																					pushQuery.whereEqualTo("myuser",jarray.get(j).toString());
																					push.setQuery(pushQuery);
																					JSONObject tempAlarmData = new JSONObject();	

																						if(object != null)
																						tempAlarmData.put("reference",object);													
																						tempAlarmData.put("notificationtype", 14);						
																						
																						tempAlarmData.put("title","");
										            									tempAlarmData.put("alert","");
																						tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivityNew");												
																						push.setData(tempAlarmData);
																						push.sendInBackground(new SendCallback() {
																							   public void done(com.parse.ParseException e) {
																								     if (e == null) {
																								    	 Toast.makeText(context, "Alaram removed sucessfully", Toast.LENGTH_LONG).show();
																								    	//Shifted to here
																											ParseObject notification = new ParseObject("Activity");
																											notification.put("creator",ParseUser.getCurrentUser());
																											notification.put("followers",jarraysimilar);
																											notification.put("reference",object);
																											notification.put("notificationtype",14);
																											notification.put("notificationDone",false);
																											notification.put("readers",jarraysimilar);//changed from `new JSONArray()` to object.get("connected")
																											notification.add("readers", ParseUser.getCurrentUser().getObjectId());
																											notification.saveInBackground();//saveEventually();();
																								       Log.d("push", "success!");
																								     } else {
																								       Log.d("push", "failure");
																								     }
																								   }
																							
																								 });
										            							}
										            							}
										            							
										            					        
										            					
										            					
										            					
										            					}catch (Exception e1) {
										            							// TODO Auto-generated catch block
										            							e1.printStackTrace();
										            						} 
										            						
										            						
										            						
										                    	 
										                    	 
										                   	 
										                     }
										                     
										                     if(jarraydifferentunique!=null){                    	 
										                   		try {
										        						 
										            					
										            					
										            					// parse push implementation
										            						
										            						for (int j = 0; j < jarraydifferentunique.length(); j++) {
										            							 Log.d("jarraydifferentunique.get(j).toString()", "--"+jarraydifferentunique.get(j).toString()+"ParseUser.getCurrentUser().getObjectId().toString())"+"--"+ParseUser.getCurrentUser().getObjectId().toString());
										            							if(!jarraydifferentunique.get(j).toString().equals(ParseUser.getCurrentUser().getObjectId().toString())){
										            								
										            								ParseQuery pushQuery = ParseInstallation.getQuery();
																					ParsePush push = new ParsePush();
																					//pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId().toString()); // not me
																					pushQuery.whereEqualTo("myuser",jarray.get(j).toString());
																					push.setQuery(pushQuery);
																					JSONObject tempAlarmData = new JSONObject();	

																						if(object != null)
																						tempAlarmData.put("reference",object);													
																						tempAlarmData.put("notificationtype", 9);						
																						tempAlarmData.put("alert","The alarm is removed by "+ParseUser.getCurrentUser().getString("fullname"));
																						tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivityNew");												
																						push.setData(tempAlarmData);
																						push.sendInBackground(new SendCallback() {
																							   public void done(com.parse.ParseException e) {
																								     if (e == null) {
																								    	 Toast.makeText(context, "Alaram removed sucessfully", Toast.LENGTH_LONG).show();
																								    	//Shifted to here
																											ParseObject notification = new ParseObject("Activity");
																											notification.put("creator",ParseUser.getCurrentUser());
																											notification.put("followers",jarraydifferentunique);
																											notification.put("reference",object);
																											notification.put("notificationtype",9);
																											notification.put("notificationDone",false);
																											notification.put("readers",jarraydifferentunique);//changed from `new JSONArray()` to object.get("connected")
																											notification.add("readers", ParseUser.getCurrentUser().getObjectId());
																											notification.saveInBackground();//saveEventually();();
																								       Log.d("push", "success!");
																								     } else {
																								       Log.d("push", "failure");
																								     }
																								   }
																							
																								 });
										            							}
										            							}
										            							
										            					        
										            					
										            					
										            					
										            					}catch (Exception e1) {
										            							// TODO Auto-generated catch block
										            							e1.printStackTrace();
										            						} 
										            						
										            						
										            						
										                    	 
										                    	 
										                     }
										                     
										                     
										                     
										                     if(jarraydifferent!=null && jarraydifferent.length() == 1 ){                    	 
										                    		try {
										              					// parse push implementation
										             						
										             						for (int j = 0; j < jarraydifferent.length(); j++) {
										             							 Log.d("jarraydifferent.get(j).toString()", "--"+jarraydifferent.get(j).toString()+"ParseUser.getCurrentUser().getObjectId().toString())"+"--"+ParseUser.getCurrentUser().getObjectId().toString());
										             							if(!jarraydifferent.get(j).toString().equals(ParseUser.getCurrentUser().getObjectId().toString())){
										             								
										             								ParseQuery pushQuery = ParseInstallation.getQuery();
																					ParsePush push = new ParsePush();
																					//pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId().toString()); // not me
																					pushQuery.whereEqualTo("myuser",jarray.get(j).toString());
																					push.setQuery(pushQuery);
																					JSONObject tempAlarmData = new JSONObject();	

																						if(object != null)
																						tempAlarmData.put("reference",object);													
																						tempAlarmData.put("notificationtype", 9);						
																						tempAlarmData.put("alert","The alarm is removed by "+ParseUser.getCurrentUser().getString("fullname"));
																						tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivityNew");												
																						push.setData(tempAlarmData);
																						push.sendInBackground(new SendCallback() {
																							   public void done(com.parse.ParseException e) {
																								     if (e == null) {
																								    	 Toast.makeText(context, "Alaram removed sucessfully", Toast.LENGTH_LONG).show();
																								    	//Shifted to here
																											ParseObject notification = new ParseObject("Activity");
																											notification.put("creator",ParseUser.getCurrentUser());
																											notification.put("followers",jarraydifferent);
																											notification.put("reference",object);
																											notification.put("notificationtype",9);
																											notification.put("notificationDone",false);
																											notification.put("readers",jarraydifferent);//changed from `new JSONArray()` to object.get("connected")
																											notification.add("readers", ParseUser.getCurrentUser().getObjectId());
																											notification.saveInBackground();//saveEventually();();
																								       Log.d("push", "success!");
																								     } else {
																								       Log.d("push", "failure");
																								     }
																								   }
																							
																								 });
										             								
										             							}
										             							}
										             							
										              					
										             					}catch (Exception e1) {
										             							// TODO Auto-generated catch block
										             							e1.printStackTrace();
										             						} 
										             						
										             						
										             						
										                     	 
										                     	 
										                      }
															
															//Added newly ends
															
														
														
											 }
												 else{
													 Toast.makeText(context, "Removed from the Alarm else", Toast.LENGTH_SHORT).show();
													    object.add("Hideit", ParseUser.getCurrentUser().getObjectId().toString());
														object.saveInBackground();
														feed.remove(pos);
														notifyDataSetChanged();
												 }
												} catch (Exception e1) {
													// TODO Auto-generated catch block
													e1.printStackTrace();
												}
											
											}
										});
										
										
										
										Intent intent = new Intent(context, AlaramActivityNew.class);
										PendingIntent pendingIntent = PendingIntent.getActivity(context, f.getAlarmrequestid(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
										AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
										alarmManager.cancel(pendingIntent);
										//Remove from DB
										DatabaseHandler db = new DatabaseHandler(context);
										db.deleteAlarm(f.getAlarmrequestid());
										Toast.makeText(context, "Removed from the Alarm", Toast.LENGTH_SHORT).show();
				                    }
				                });
								AlertDialog alertDialogDisconnect = alertDialogBuilderDisconnect.create();
								alertDialogDisconnect.show();  
								
								
								/*

								AlertDialog.Builder alertDialogBuilderDisconnect = new AlertDialog.Builder(context);
								alertDialogBuilderDisconnect.setMessage("Remove '" + f.getAlarmTitle() + "' ?");
								alertDialogBuilderDisconnect.setIcon(R.drawable.alarm);
								alertDialogBuilderDisconnect.setNegativeButton("NO", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,int id) {
										dialog.cancel();
									}
								});
								alertDialogBuilderDisconnect.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				                    public void onClick(DialogInterface dialog, int id) {
				                    	ParseQuery<ParseObject> like = ParseQuery.getQuery("alarms");
										like.getInBackground(f.getAlarmid(), new GetCallback<ParseObject>() {
											@Override
											public void done(final ParseObject object, com.parse.ParseException e) {
												
												 if(f.getCreator().equals(ParseUser.getCurrentUser().getObjectId().toString()))  { 
													
													    object.put("Hideit", object.get("connected"));
														object.saveInBackground();
														feed.remove(pos);
														notifyDataSetChanged();
														
														
												
												 }
												 else{
													    object.add("Hideit", ParseUser.getCurrentUser().getObjectId().toString());
														object.saveInBackground();
														feed.remove(pos);
														notifyDataSetChanged();
												 }
												
											
											}
										});
										
										
										
										Intent intent = new Intent(context, AlaramActivityNew.class);
										PendingIntent pendingIntent = PendingIntent.getActivity(context, f.getAlarmrequestid(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
										AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
										alarmManager.cancel(pendingIntent);
										
										Toast.makeText(context, "Removed from the Alarm", Toast.LENGTH_SHORT).show();
				                    }
				                });
								AlertDialog alertDialogDisconnect = alertDialogBuilderDisconnect.create();
								alertDialogDisconnect.show(); */ 
								break;
							case 2:
								//Edit
								Intent i = new Intent(context, CreateNewAlarmActivity.class);
								i.putExtra("alarm_details", f);
								i.putExtra("alarm_action", "edit");
								context.startActivity(i);
								((Activity) context).finish();
								break;
							}
						}
					});
					dialogbuilder.show();
				}
				
				/*@Override
				public void onClick(View v) {
					final NewsFeed f = (NewsFeed) v.getTag();
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
					alertDialogBuilder.setMessage("Delete '" + f.getAlarmTitle() + "' ?");
					alertDialogBuilder.setIcon(R.drawable.alarm);
					alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.cancel();
						}
					});
					alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface dialog, int id) {
	                    	ParseQuery<ParseObject> like = ParseQuery.getQuery("alarms");
							like.getInBackground(f.getAlarmid(), new GetCallback<ParseObject>() {
								
								@Override
								public void done(ParseObject object, com.parse.ParseException e) {
									object.deleteInBackground();
									
								}
							});
	                    }
	                });
	                
					AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.show();  
				}*/
				}
			});
			
			holder.mPreview.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					//Fragment
					if (!Util.isNetworkAvailable(context)) {	
				
				     DialogFactory.getBeaconStreamDialog1(context, context.getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			       } else 
			
			       {

					ImageButton iv = (ImageButton) v;
					NewsFeed r = (NewsFeed) v.getTag();
					if(r.isPlaying()){
						//Track is playing so stop
						reset(false);
						r.setPlaying(false);
						FeedFragment.resetMedia();
					}else{
						//Track is not playing so start
						reset(false);
						r.setPlaying(true);
						iv.setBackgroundResource(R.drawable.created_stop_icon);
						if(!r.recorded){
							String trackName = stringtolower(r.getAlarmpath());
							int trackId = context.getResources().getIdentifier(trackName,"raw",context.getPackageName());
							//If 1 - APP else folder
							FeedFragment.playMedia(context,trackName, trackId,1,r.getAlarmrequestid());
						}else{
							//Check from folder then load from server
							FeedFragment.playMedia(context,r.getAlarmpath(),0,2,r.getAlarmrequestid());
						}
					}
				}
				}
			});
			
			holder.mFriends.setOnClickListener(new View.OnClickListener() {
				
				@SuppressWarnings("unchecked")
				@Override
				public void onClick(View v) {
					//Fragment
					if (!Util.isNetworkAvailable(context)) {	
				
				     DialogFactory.getBeaconStreamDialog1(context, context.getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			       } else 
			
			       {

					final NewsFeed feeed = (NewsFeed) v.getTag();
					String friend_name = "";
					String friend_image = "";
					String friend_id = "";
					JSONObject friendsobject = new JSONObject();
					JSONArray friendsArray;
					AlarmFriend alarmfriend;
					try {
						friendsArray = new JSONArray(feeed.getAlarmFriends());
						for(int f = 0; f < friendsArray.length(); f++){
							alarmfriend = new AlarmFriend();
							friendsobject = friendsArray.getJSONObject(f);
							Log.i("friendsobject", ">>>>"+friendsobject.toString());
							friend_name = friendsobject.getString("name");
							friend_image = friendsobject.getString("userImage");
							friend_id = friendsobject.getString("userid");
							alarmfriend.setName(friend_name);
							alarmfriend.setUserImage(friend_image);
							alarmfriend.setUserId(friend_id);
							answers.add(alarmfriend);
							
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					Intent  alarmfriends_intent = new Intent(context, AlarmFriends.class);	
					context.startActivity(alarmfriends_intent);					
			       }	
				}
			});

			holder.mComments.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					//Fragment
					if (!Util.isNetworkAvailable(context)) {	
				
				     DialogFactory.getBeaconStreamDialog1(context, context.getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			       } else 
			
			       {

					Button b = (Button) v;
					NewsFeed a = (NewsFeed) v.getTag();
					
					Log.i("a","have"+a.toString());
					Bundle extras = new Bundle();
					extras.putString("alarmid",a.getAlarmid());
					extras.putInt("position", position);
					Intent i = new Intent(context,FeedComments.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					i.putExtras(extras);
					context.startActivity(i);
					//showDialog(a.getAlarmid(),position);
			       }
				}
			});
			
			holder.mLikes.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					
					//Fragment
					if (!Util.isNetworkAvailable(context)) {	
				
				     DialogFactory.getBeaconStreamDialog1(context, context.getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			       } else 
			
			       {

					Button mLikeClick = (Button) v;
					NewsFeed f = (NewsFeed) v.getTag();
					boolean isLiked = f.isLiked();
					
					//notifyDataSetChanged();
					if(!isLiked){
						//Like feed
						int value = f.getLikes();
						value = value + 1;
						f.setLikes(value);
						mLikeClick.setText(Integer.toString(value)+ " Likes");
						mLikeClick.setCompoundDrawablesWithIntrinsicBounds(R.drawable.like_icon_pressed, 0, 0, 0);
						f.setLiked(true);
						//notifyDataSetChanged();
						ParseQuery<ParseObject> like = ParseQuery.getQuery("alarms");
						like.getInBackground(f.getAlarmid(), new GetCallback<ParseObject>() {
							
							@Override
							public void done(final ParseObject object, com.parse.ParseException e) {
								
								try {
								object.addAllUnique("likes",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
								//object.increment("likes");
								object.saveInBackground();
								//Earlier here
								   /* ParseObject notification = new ParseObject("Activity");
								    notification.put("creator",ParseUser.getCurrentUser());
									notification.put("followers",object.get("connected"));
									notification.put("reference",object);
									notification.put("notificationtype",3);
									notification.put("notificationDone",false);
									notification.put("readers",new JSONArray());
									notification.add("readers", ParseUser.getCurrentUser().getObjectId());
									notification.saveInBackground();//saveEventually();();
*/                                									
    								// parse push implementation									
									ArrayList<String> jarray =  (ArrayList<String>) object.get("connected");
									Log.i("jarray", "isssssssss"+jarray.toString());
									for (int j = 0; j < jarray.size(); j++) {
										 Log.d("friendsId.get(j).toString()", "--"+jarray.get(j).toString()+"ParseUser.getCurrentUser().getObjectId().toString())"+"--"+ParseUser.getCurrentUser().getObjectId().toString());
										if(!jarray.get(j).toString().equals(ParseUser.getCurrentUser().getObjectId().toString())){
											
											ParseQuery pushQuery = ParseInstallation.getQuery();
											ParsePush push = new ParsePush();
											pushQuery.whereEqualTo("myuser",jarray.get(j).toString());
											//
											push.setQuery(pushQuery);
											 JSONObject tempAlarmData = new JSONObject();	
											 if(object != null)
												tempAlarmData.put("reference",object);													
												tempAlarmData.put("notificationtype", 3);						
												tempAlarmData.put("alert","The alarm is liked by "+ParseUser.getCurrentUser().getString("fullname"));
												tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivityNew");												
												push.setData(tempAlarmData);
												push.sendInBackground(new SendCallback() {
													   public void done(com.parse.ParseException e) {
														     if (e == null) {
														    	 Toast.makeText(context, "User liked the alarm feed", Toast.LENGTH_LONG).show();
														    	//Shifted to here
																    ParseObject notification = new ParseObject("Activity");
																    notification.put("creator",ParseUser.getCurrentUser());
																	notification.put("followers",object.get("connected"));
																	notification.put("reference",object);
																	notification.put("notificationtype",3);
																	notification.put("notificationDone",false);
																	notification.put("readers",object.get("connected"));//changed from `new JSONArray()` to object.get("connected")
																	notification.add("readers", ParseUser.getCurrentUser().getObjectId());
																	notification.saveInBackground();//saveEventually();();
														       Log.d("push", "success!");
														     } else {
														       Log.d("push", "failure");
														     }
														   }
													
														 });
										}
										}
	
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									} 
									
							}
						});
					}else{
						//Unlike feed
						int value = f.getLikes();
						value = value - 1;
						f.setLikes(value);
						mLikeClick.setText(Integer.toString(value)+ " Likes");
						mLikeClick.setCompoundDrawablesWithIntrinsicBounds(R.drawable.like_icon, 0, 0, 0);
						f.setLiked(false);
						ParseQuery<ParseObject> like = ParseQuery.getQuery("alarms");
						like.getInBackground(f.getAlarmid(), new GetCallback<ParseObject>() {
							
							@Override
							public void done(ParseObject object, com.parse.ParseException e) {
								//object.addAllUnique("userlikes",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
								object.removeAll("likes",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
								//object.increment("likes",-1);
								object.saveInBackground();
							}
						});
					}
			       }
				}
			});
			
		
		
	    // item = feed.get(position);
		holder.mTitle.setText(item.getAlarmTitle());
		holder.mAuthor.setText(item.getUserName());
		holder.mLikes.setTag(item);
		holder.mComments.setTag(item);
		holder.mPreview.setTag(item);
		holder.mCategory.setText(item.getAlarmCategory());
		holder.mDelete.setTag(item);
		holder.mProfile_page.setTag(item);
		holder.mFriends.setTag(item);
		
		if(item.getAlarmCategory().equals("Alarm")) {
			holder.mCategoryIcon.setBackgroundResource(R.drawable.alarm);
		} else if (item.getAlarmCategory().equals("Birthday Party")) {
			holder.mCategoryIcon.setBackgroundResource(R.drawable.birthday);
		} else if (item.getAlarmCategory().equals("Wedding Party")) {
			holder.mCategoryIcon.setBackgroundResource(R.drawable.wedding);
		} else if (item.getAlarmCategory().equals("Run")) {
			holder.mCategoryIcon.setBackgroundResource(R.drawable.run);
		} else if (item.getAlarmCategory().equals("Party Time")) {
			holder.mCategoryIcon.setBackgroundResource(R.drawable.party_time);
		} else if (item.getAlarmCategory().equals("Movie Night")) {
			holder.mCategoryIcon.setBackgroundResource(R.drawable.movie_night);
		} else if (item.getAlarmCategory().equals("Lunch")) {
			holder.mCategoryIcon.setBackgroundResource(R.drawable.lunch);
		} else if (item.getAlarmCategory().equals("Ride")) {
			holder.mCategoryIcon.setBackgroundResource(R.drawable.let_ride);
		} else if (item.getAlarmCategory().equals("Dinner")) {
			holder.mCategoryIcon.setBackgroundResource(R.drawable.dinner);
		} else if (item.getAlarmCategory().equals("Brunch")) {
			holder.mCategoryIcon.setBackgroundResource(R.drawable.brunch);
		} else if (item.getAlarmCategory().equals("Breakfast")) {
			holder.mCategoryIcon.setBackgroundResource(R.drawable.breakfast);
		} else {
			holder.mCategoryIcon.setBackgroundResource(R.drawable.alarm);
		}
		
		if(item.isPlaying()){
			holder.mPreview.setBackgroundResource(R.drawable.created_stop_icon);
		}else{
			holder.mPreview.setBackgroundResource(R.drawable.created_play_icon);
		}
		if(item.isLiked()){
			holder.mLikes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.like_icon_pressed, 0, 0, 0);
		}else{
			holder.mLikes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.like_icon, 0, 0, 0);
		}
		
		//String tempImage = "image";
		//Log.i("USER PROFILE PIC",tempImage);
		//if (item.getUserProfileImage().startsWith("https://")){
			String tempImage = item.getUserProfileImage();
			//Log.i("USER PROFILE PIC",tempImage);
			Picasso.with(context).load(tempImage).fit().centerInside().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into(holder.mUserProfileImage);
		/*}else{
			Uri tempImage = Uri.parse(item.getUserProfileImage());
			Picasso.with(context).load(tempImage).fit().centerCrop().transform(new RoundedTransform(100,0)).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into(holder.mUserProfileImage);
		}*/
		
		String likes = Integer.toString(item.getLikes())+" Likes";
		String comments = Integer.toString(item.getComments())+" Comments";
		holder.mLikes.setText(likes);
		holder.mComments.setText(comments);	
		
		try {
			String friends = "";
			JSONObject friendsobject = new JSONObject();
			JSONArray friendsArray = new JSONArray(item.getAlarmFriends());
			//Log.d("MyApp","Friends on Feed: "+friendsArray);
			for(int f = 0; f < friendsArray.length(); f++){
				friendsobject = friendsArray.getJSONObject(f);
				friends += "+"+friendsobject.getString("name")+" ";
			}
			if(friends != ""){
				friends = "With "+friends;
			}
			holder.mFriends.setText(friends);
			String[] data_res = dateconverstion(item.getUserCreatedTime(),item.getLongtime());
			if(data_res != null){
				holder.mCreatedTime.setText(data_res[0]);
				holder.mTime.setText(data_res[1]);
				holder.mDay.setText(data_res[2]);
				holder.ampmText.setText(data_res[3]);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		//button click for comments and likes
		
		
		
		return convertView;
	}
	
	private String[] dateconverstion(String milliseconds,long longtime){
		//Log.i("DATE",milliseconds);
		SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy",Locale.getDefault());
		Date mDate,nDate;
		try {
			mDate = sdf.parse(milliseconds);
			long timeinMilli = mDate.getTime();
			nDate = new Date(longtime);
			min = nDate.getMinutes();
			hours = nDate.getHours();
			int month = nDate.getMonth();
			int day = nDate.getDay();
			int dateofAlarm = nDate.getDate();
			String time = (String) DateUtils.getRelativeTimeSpanString(timeinMilli,System.currentTimeMillis(),0);
			String hour_min = String.format(Locale.getDefault(),"%02d:%02d",hours,min);
			String day_month = String.format(Locale.getDefault(),"%s | %s  %s",nDays[day],dateofAlarm,nMonths[month]);
			SimpleDateFormat ft = new SimpleDateFormat ("a");
			ampmText = ft.format(nDate);
			String[] a = {time,hour_min,day_month,ampmText};
			return a;
			//return time;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void addNewFeed(NewsFeed data){
		this.feed.add(data);
		notifyDataSetChanged();
	}
	
	public void addNew(NewsFeed data){
		//Toast.makeText(getContext(),"NEW DATA INSERT",Toast.LENGTH_LONG).show();
		this.feed.add(0,data);
		notifyDataSetChanged();
	}
	
	public void updateRow(int comment,int resourceid){
		ArrayList<NewsFeed> n = new ArrayList<NewsFeed>();
		n = feed;
		for(NewsFeed nf:n){
			//Log.i("MyApp",nf.toString());
			//Log.i("MyApp",nf.getAlarmrequestid()+"="+resourceid);
			if(nf.getAlarmrequestid() == resourceid){
				//Toast.makeText(getContext(),"DS B",Toast.LENGTH_LONG).show();
				nf.setComments(comment);
				notifyDataSetChanged();
				break;
			}
		}
		//holder.mPreview.setBackgroundResource(R.drawable.created_play_icon);
		//notifyDataSetChanged();
	}
	
	//public void updateC(int f,int c){
	//	feed.get(f).setComments(c);
	//	notifyDataSetChanged();
	//}
	
	//protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		//super.onActivityResult(requestCode, resultCode, data);
	//	Toast.makeText(context,"Activity Connectivity",Toast.LENGTH_LONG).show();
	//	if(requestCode == 1){
			/*if(mAdapter != null){
				Bundle d = data.getExtras();
				mAdapter.updateC(d.getInt("position"),12);
			}*/
	//	}
	//}
	
	/*
	void showDialog(String alarmid, int position) {
		FragmentActivity activity = (FragmentActivity)(context);
	    FragmentManager ft = activity.getSupportFragmentManager();
	    DialogFragment newFragment = MyDialogFragment.newInstance(alarmid, position);
	    newFragment.show(ft, "dialog");
	}
	
	public static class MyDialogFragment extends DialogFragment {
		
		private EditText mCommentEdit;
		private ImageView mSubmit;
		private Bundle extras = null;
		private String alarmid = null;
		private ProgressBar mBar;
		private ArrayList<Comment> row = new ArrayList<Comment>();
		ListView lv;
		ParseFile p;
		String t = null;
		String myImageUrl;
		Comment comment = null;
		int pos = 0;
		
		private CommentsAdapter mAdapter = null;
		ParseObject alarmObject = null;
		
	    static MyDialogFragment newInstance(String alarmid, int position) {
	        MyDialogFragment f = new MyDialogFragment();
	        
	        Bundle args = new Bundle();
	        args.putString("alarmid", alarmid);
	        args.putInt("position", position);
	        f.setArguments(args);
	        
	        return f;
	    }
	    
	    public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	            // the back key was pressed so do something?
	        	Log.d("backkk","backd");
	            return true;
	        }
	        return true;
	    }

	    
	    public Dialog onCreateDialog(final Bundle savedInstanceState) {
	    	 
	        // the content
	        final RelativeLayout root = new RelativeLayout(getActivity());
	        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
	 
	        // creating the fullscreen dialog
	        final Dialog dialog = new Dialog(getActivity());
	        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	        dialog.setContentView(root);
	        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.YELLOW));
	        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
	        
	        return dialog;
	    }

	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	        View v = inflater.inflate(R.layout.activity_comments, container, false);
	       
	        mCommentEdit = (EditText) v.findViewById(R.id.etComment);
			mSubmit = (ImageView) v.findViewById(R.id.bSubmitComment);
			lv = (ListView) v.findViewById(R.id.commentsList);
			mBar = (ProgressBar) v.findViewById(R.id.pLoader);
			
			mSubmit.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
					if(mCommentEdit.length() > 0 && alarmObject!=null){
						String comments = mCommentEdit.getText().toString();
						ParseObject a = new ParseObject("Comments");
						a.put("comment",comments);
						a.put("alarmid",alarmObject.getObjectId());
						a.put("alarm",alarmObject);
						a.put("user",ParseUser.getCurrentUser());
						//InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
						//inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
						a.saveInBackground(new SaveCallback() {
							
							
							public void done(com.parse.ParseException e) {
								//Update the comments count in the server
								ParseQuery<ParseObject> commentcount = ParseQuery.getQuery("alarms");
								commentcount.getInBackground(alarmObject.getObjectId(), new GetCallback<ParseObject>() {
									
									@Override
									public void done(ParseObject object, com.parse.ParseException e) {
										object.increment("comments");
										object.saveInBackground();
									}
								});
							}
						});
						
						Log.d("alarmObject.getObjectId()", alarmObject.getObjectId());
						Log.d("comments", comments);
						Log.d("ParseUser.FULLNAME", ParseUser.getCurrentUser().get("fullname").toString());
						Log.d("ParseUser.ObjectID", ParseUser.getCurrentUser().getObjectId());
						comment = new Comment(alarmObject.getObjectId(),"1",comments,myImageUrl, ParseUser.getCurrentUser().get("fullname").toString(), ParseUser.getCurrentUser().getObjectId(),"Just Now",true);
						if(lv.getAdapter() == null){
							//List view is not set
							row.add(comment);
							mAdapter = new CommentsAdapter(context ,R.layout.comment_row,row);
							lv.setAdapter(mAdapter);
							lv.setVisibility(View.VISIBLE);
						}else{
							mAdapter.setComment(comment);
						}
						mAdapter.notifyDataSetChanged();
						mCommentEdit.setText("");
						
					}
	            }
			});
			
			
			//mSubmit.setOnClickListener(this);
			
			if(getArguments().getString("alarmid") != null){
				alarmid = getArguments().getString("alarmid");
				pullComments(alarmid,0,"insert");
				ParseQuery<ParseObject> q = ParseQuery.getQuery("alarms");
				q.getInBackground(alarmid,new GetCallback<ParseObject>() {
					
					public void done(ParseObject object, com.parse.ParseException e) {
						if(e == null){
							alarmObject = object;
							myImageUrl = "image";
							p = ParseUser.getCurrentUser().getParseFile("profilethumbnail");
							if(p!=null){
								String url = p.getUrl();
								Log.i("FB IMAGE",url);
								myImageUrl = url;
							}else{
								if( ParseUser.getCurrentUser().get("facebookid") != null ) {
									t = ParseUser.getCurrentUser().get("facebookid").toString();
									if((t!=null)||(t!="")){
										myImageUrl = "https://graph.facebook.com/"+ ParseUser.getCurrentUser().get("facebookid").toString()+ "/picture/?type=square";
									}
								}
							}
						}
					}

					
				});
			}
			
			
	        return v;
	    }
	    
	    private void pullComments(String id, int page,String type) {
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Comments");
			query.whereEqualTo("alarmid",id);
			query.include("alarm");
			query.include("user");
			query.setLimit(15);
			query.setSkip(page);
			query.findInBackground(new FindCallback<ParseObject>() {
				
				public void done(List<ParseObject> objects, com.parse.ParseException e) {
					if(e == null){
						
						for(ParseObject obj: objects){
							Log.i("COMMENT ALARM ID",obj.getParseObject("alarm").getObjectId());
							Log.i("COMMENT TEXT",obj.get("comment").toString());
							Log.i("COMMENT USERNAME",obj.getParseUser("user").get("fullname").toString());
							Log.i("COMMENT USERID",obj.getParseUser("user").getObjectId());
							Log.i("COMMENT CREATED AT",obj.getCreatedAt().toString());
							String imageUrl = "image";
							p = obj.getParseObject("user").getParseFile("profilethumbnail");
							if(p!=null){
								String url = p.getUrl();
								Log.i("FB IMAGE",url);
								imageUrl = url;
							}else{
								if(obj.getParseObject("user").get("facebookid") != null && obj.getParseObject("user").get("facebookid") != "") {
									t = obj.getParseObject("user").get("facebookid").toString();
									if((t!=null)||(t!="")){
										imageUrl = "https://graph.facebook.com/"
												+ obj.getParseObject("user").get("facebookid").toString()
												+ "/picture/?type=square";
									}
								}
							}
							comment = new Comment(obj.getParseObject("alarm").getObjectId(),obj.getObjectId(),obj.get("comment").toString(),imageUrl, obj.getParseUser("user").get("fullname").toString(),obj.getParseUser("user").getObjectId(), obj.getCreatedAt().toString(), true);
							row.add(comment);
						}
						if(!row.isEmpty()){
							mAdapter = new CommentsAdapter(context,R.layout.comment_row,row);
							lv.setAdapter(mAdapter);
							lv.setVisibility(View.VISIBLE);
							mBar.setVisibility(View.GONE);
						}else{
							Toast.makeText(context,"No Comments Found",Toast.LENGTH_LONG).show();
							mBar.setVisibility(View.GONE);
						}
					}
				}

				
			});
			
		}
	    
	    public void onBackPressed() {
	    	Log.d("Back is pressed", "yes");
	    }
	}*/
public void updateArray() {

		

		

	}

}
