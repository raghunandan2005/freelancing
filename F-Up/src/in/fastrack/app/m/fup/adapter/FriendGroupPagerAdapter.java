package in.fastrack.app.m.fup.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import in.fastrack.app.m.fup.FriendAddFragmentNew;
import in.fastrack.app.m.fup.GroupsAddFragment;


public class FriendGroupPagerAdapter extends FragmentPagerAdapter {

	public FriendGroupPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {
		switch (index) {
		case 0:
			// return new FacebookInviteFragment();
			return new FriendAddFragmentNew();
		case 1:
			return new GroupsAddFragment();
		/*case 2:
			return new SurpriseFriendAddFragment();
		*/
		}
		return null;
	}

	@Override
	public int getCount() {
		return 2;
	}

}