package in.fastrack.app.m.fup.adapter;


import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.PhoneBook;
import in.fastrack.app.m.fup.model.UserFriendRelation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SendCallback;
import com.squareup.picasso.Picasso;

public class FriendsAdapter extends BaseAdapter implements Filterable {

	private Context context;
	private int resource;
	private ArrayList<Friend> list, origin;
	private FriendListener mListen;
	private JSONArray friends;
	private UserFriendRelation ufr;
	private ArrayList<String> friendsId = new ArrayList<String>();
	private ArrayList<String> friendsIds = new ArrayList<String>();
	private String friends_id;// ,nami;
	private String friends_ids;
	private ParseUser frienduser;
	private ParseObject reference_id;
	private ParseObject reference_ids;
	private String from;
	LayoutInflater vi;;
	  @Override
	    public Filter getFilter() {
	        return new Filter() {
	            @SuppressWarnings("unchecked")
				@Override
	            protected void publishResults(CharSequence constraint, FilterResults results) {
	            	
	            	list = (ArrayList<Friend>) results.values;
	                notifyDataSetInvalidated();
	            }



	           @Override
	            protected FilterResults performFiltering(CharSequence constraint) {
	                FilterResults Friend = new FilterResults();
	                if(TextUtils.isDigitsOnly(constraint)) {
	                    ArrayList<Friend> list = new ArrayList<Friend>(origin);
	                    Friend.values = list;
	                    Friend.count = list.size();
	                } else {
	                    ArrayList<Friend> newValues = new ArrayList<Friend>();
	                    for(int i = 0; i < origin.size(); i++) {
	                        String team1 = origin.get(i).getName().toLowerCase(Locale.ENGLISH);
	                    
	                        if(team1.startsWith(constraint.toString().toLowerCase(Locale.ENGLISH)))  {
	                            newValues.add(origin.get(i));

	                        }
	                      
	                    }
	                    if(newValues.size()>0)
	                    {
	                    Friend.values = newValues;
	                    Friend.count = newValues.size();
	                    }else
	                    {
	                    	Friend.values = origin;
	                        Friend.count = origin.size();
	                    }
	                }  

	                return Friend;
	            }
	        };
	    }

	    public void setData() { 
	    	//list.clear();
	           list = origin;
	       }
	public FriendsAdapter(Context context, int resource,
			ArrayList<Friend> list, FriendListener mListen, JSONArray friends,
			UserFriendRelation ufr, String from) {
	
		this.context = context;
		this.resource = resource;
	    this.list = list;
		this.origin = list;
		this.mListen = mListen;
		this.friends = friends;
		this.ufr = ufr;
		this.from = from;
		vi = LayoutInflater.from(context);
	}

	private class ViewHolder {
		TextView mName, mStatus;
		ImageView mUserImage;
		Button mAction, mSupriseFriend, currentUserImage;
		LinearLayout mSupriseFriendll;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		Friend rowItem = list.get(position);

		if (convertView == null) {
			convertView = vi.inflate(resource, parent,false);
			holder = new ViewHolder();
			holder.mName = (TextView) convertView.findViewById(R.id.tvPhoneName);
			holder.mUserImage = (ImageView) convertView.findViewById(R.id.ivUserImage);
			holder.mAction = (Button) convertView.findViewById(R.id.bInviteUser);
			holder.mStatus = (TextView) convertView.findViewById(R.id.bstatus);
			holder.mSupriseFriend = (Button) convertView.findViewById(R.id.bSupriseFriend);
			holder.mSupriseFriendll = (LinearLayout) convertView.findViewById(R.id.bSupriseFriendll);
			holder.mSupriseFriendll.setVisibility(View.GONE);
			convertView.setTag(holder);
			
		} else {
			holder = (ViewHolder) convertView.getTag();//
		}
		/* Set data to views */
	     holder.mAction.setTag(rowItem);
	     holder.mAction.setVisibility(rowItem.getmActionVisibility());
	     //Toast.makeText(context,""+holder.mAction.getVisibility(), Toast.LENGTH_LONG).show();
	     if(holder.mAction.getVisibility() == 0)
	     {
	    	 holder.mStatus.setVisibility(View.GONE);
	     }else
	     {

	    	 holder.mStatus.setVisibility(View.VISIBLE);
	    	 holder.mStatus.setText(rowItem.getStatus());
	     }
	     
		 holder.mSupriseFriend.setTag(rowItem);
		 //holder.mAction.setTag(holder);
		holder.mName.setText(rowItem.getName());
		// holder.mSupriseFriend.setTag(rowItem);
		if (rowItem.getUserImage() != null) {
			Picasso.with(context).load(rowItem.getUserImage()).fit()
					.centerCrop().transform(new RoundedTransform())
					.placeholder(R.drawable.friend_nopic)
					.error(R.drawable.friend_nopic).into(holder.mUserImage);
		} else {
			holder.mUserImage.setImageResource(R.drawable.friend_nopic);
		}

	
		holder.mAction.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Button b = (Button) v;
				final Friend p = (Friend) v.getTag();
				Log.d("", "" + b.getText().toString());
				p.setStatus("Request Sent");
				p.setmActionVisibility(View.GONE);
				notifyDataSetChanged();
				friends_id = p.getUserId();
				Log.i("friends_id state", "Found" + friends_id);
			
				//showSelected();
				try
				{
				JSONArray myArray = new JSONArray();
				JSONArray myFriendsId = new JSONArray();
				JSONObject object = new JSONObject();
				object.put("userid", p.getUserId());
				object.put("name", p.getName());
				object.put("source", p.getSource());
				object.put("userImage", p.getUserImage());
				myArray.put(object);
				myFriendsId.put(p.getUserId());
				friendsId.add(p.getUserId());
				/// push by Raghu
			
				}catch(Exception e)
				{
					e.printStackTrace();
				}

				try {

					ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
					query.whereEqualTo("user", ParseUser.getCurrentUser());
					query.findInBackground(new FindCallback<ParseObject>() {
						public void done(List<ParseObject> objects,
								ParseException e) {
							if (objects.size() > 0) {

								// edit
								if (e == null) {
									for (ParseObject obj : objects) {
										obj.addAllUnique("requestedF",Arrays.asList(friends_id));
										obj.saveInBackground();// saveEventually();();

										reference_id = obj;
										ParseQuery pushQuery = ParseInstallation.getQuery();
										ParsePush push = new ParsePush();
										pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId()); // not me
										pushQuery.whereEqualTo("myuser",p.getUserId());// push notification to other users
										push.setQuery(pushQuery);
										JSONObject tempAlarmData = new JSONObject();
										
										if (reference_id != null)
											try {
												tempAlarmData.put("reference3",reference_id);
											
											tempAlarmData.put("notificationtype", 5);
										    tempAlarmData.put("alert","The friend request is by "+ParseUser.getCurrentUser().getString("fullname"));
											tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivityNew");												
											push.setData(tempAlarmData);
											push.sendInBackground(new SendCallback() {
												   public void done(ParseException e) {
													     if (e == null) {
													    	 Toast.makeText(context, "Friend request sent", Toast.LENGTH_LONG).show();
													    	 //Shifted to here
													    	 // New custom notification code
																final ParseObject notification = new ParseObject("Activity");
																notification.put("creator",ParseUser.getCurrentUser());
																notification.put("followers",Arrays.asList(p.getUserId()));
																if (reference_id != null)
															    notification.put("reference3",reference_id);
																notification.put("notificationtype", 5);
																notification.put("notificationDone", true);
																notification.put("readers",Arrays.asList(p.getUserId()));//changed from `new JSONArray()` to Arrays.asList(friends_id)
																notification.add("readers", ParseUser.getCurrentUser().getObjectId());
																notification.saveInBackground();// saveEventually();();
													    	   // notification.put("notificationDone", true);
													       Log.d("push", "success!");
													     } else {
													       Log.d("push", "failure");
													     }
													   }
													 });
											} catch (JSONException e1) {
												// TODO Auto-generated catch block
												e1.printStackTrace();
											}
									
									}
								} else {
								}
							} else {
								// save
								// ParseObject friends = new ParseObject("Friends");
								// friends.put("user",ParseUser.getCurrentUser());
								// friends.addAllUnique("requestedF",friendsId);
								// friends.saveInBackground();//saveEventually();();
							}
						}
					});
					ParseQuery<ParseUser> query2 = ParseUser.getQuery();
					query2.whereEqualTo("user", friends_id);
					query2.getInBackground(friends_id,
							new GetCallback<ParseUser>() {
								@Override
								public void done(ParseUser object,ParseException e) {
									frienduser = object;
									frienduser.getObjectId();
									ParseQuery<ParseObject> query3 = ParseQuery.getQuery("Friends");
									query3.whereEqualTo("user", frienduser);
									query3.findInBackground(new FindCallback<ParseObject>() {
										public void done(
												List<ParseObject> objects,
												ParseException e) {

											if (objects.size() > 0) {
												// edit
												if (e == null) {
													for (ParseObject obj : objects) {
														obj.addAllUnique("pendingF",Arrays.asList(ParseUser.getCurrentUser().getObjectId().toString()));
														obj.saveInBackground();// saveEventually();();
													}
												} else {
												}
											} else {
												// save commented on 10-10-2014
												// ParseObject friends = new ParseObject("Friends");
												// friends.put("user", frienduser);
												// friends.addAllUnique("pendingF", Arrays.asList(ParseUser.getCurrentUser().getObjectId().toString()));
												// friends.saveInBackground();//
												// saveEventually();();
											}
											
											
											try {
												//Earlier here
												// New custom notification code
											/*	final ParseObject notification = new ParseObject("Activity");
												notification.put("creator",ParseUser.getCurrentUser());
												notification.put("followers",Arrays.asList(friends_id));
												if (reference_id != null)
											    notification.put("reference3",reference_id);
												notification.put("notificationtype", 5);
												notification.put("notificationDone", false);
												notification.put("readers",new JSONArray());
												notification.add("readers", ParseUser.getCurrentUser().getObjectId());
												notification.saveInBackground();// saveEventually();();
                                                									*/			
												
												
												
												// parse push implementation
//												ParseQuery pushQuery = ParseInstallation.getQuery();
//												ParsePush push = new ParsePush();
//												pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId()); // not me
//												pushQuery.whereEqualTo("myuser",friends_id);// push notification to other users
//												push.setQuery(pushQuery);
//												JSONObject tempAlarmData = new JSONObject();
//												
//												if (reference_id != null)
//													tempAlarmData.put("reference3",reference_id);
//													tempAlarmData.put("notificationtype", 5);
//												    tempAlarmData.put("alert","The friend request is by "+ParseUser.getCurrentUser().getString("fullname"));
//													tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivityNew");												
//													push.setData(tempAlarmData);
//													push.sendInBackground(new SendCallback() {
//														   public void done(ParseException e) {
//															     if (e == null) {
//															    	 Toast.makeText(context, "Push Was Successfull", Toast.LENGTH_LONG).show();
//															    	 //Shifted to here
//															    	 // New custom notification code
//																		final ParseObject notification = new ParseObject("Activity");
//																		notification.put("creator",ParseUser.getCurrentUser());
//																		notification.put("followers",Arrays.asList(friends_id));
//																		if (reference_id != null)
//																	    notification.put("reference3",reference_id);
//																		notification.put("notificationtype", 5);
//																		notification.put("notificationDone", true);
//																		notification.put("readers",Arrays.asList(friends_id));//changed from `new JSONArray()` to Arrays.asList(friends_id)
//																		notification.add("readers", ParseUser.getCurrentUser().getObjectId());
//																		notification.saveInBackground();// saveEventually();();
//															    	   // notification.put("notificationDone", true);
//															       Log.d("push", "success!");
//															     } else {
//															       Log.d("push", "failure");
//															     }
//															   }
//															 });
//											
											
											
											} catch (Exception e1) {
												// TODO Auto-generated catch block
												e1.printStackTrace();
											}
										}
									});
								}
							});
				} catch (Exception e) {
				}
				//p.setStatus("Request Sent");
				//p.setmActionVisibility(View.GONE);
				notifyDataSetChanged();
			}
		});

		if (!rowItem.getUserSelected()) {
			holder.mAction.setBackgroundResource(R.drawable.button);
			holder.mAction.setCompoundDrawablesWithIntrinsicBounds(
					R.drawable.plus, 0, 0, 0);
		} else {
			holder.mAction.setBackgroundColor(context.getResources().getColor(
					R.color.gray_button));
			holder.mAction.setCompoundDrawablesWithIntrinsicBounds(
					R.drawable.tick, 0, 0, 0);
		}

		holder.mSupriseFriend.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Button b = (Button) v;
				Friend ps = (Friend) v.getTag();
				Log.d("", "" + b.getText().toString());

				friends_ids = ps.getUserId();
				Log.i("friends_ids state", "Found" + friends_ids);
				if (!ps.getSupriseFriend()) {
					// Not selected so select them
					ps.setSupriseFriend(true);
					b.setBackgroundColor(context.getResources().getColor(
							R.color.gray_button));
					b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick,
							0, 0, 0);

				} else {
					// Already selected, so unselect
					ps.setSupriseFriend(false);
					b.setBackgroundResource(R.drawable.button);
					b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus,
							0, 0, 0);
				}
				//showSelectedS();

				try {

					ParseQuery<ParseObject> query = ParseQuery
							.getQuery("Friends");
					query.whereEqualTo("user", ParseUser.getCurrentUser());
					query.findInBackground(new FindCallback<ParseObject>() {
						public void done(List<ParseObject> objects,
								ParseException e) {
							if (objects.size() > 0) {

								// edit
								if (e == null) {
									for (ParseObject obj : objects) {
										obj.addAllUnique("requestedSF",
												Arrays.asList(friends_ids));
										obj.saveInBackground();// saveEventually();();

										reference_ids = obj;
									}
								} else {
								}
							} else {
								// save
								// ParseObject friends = new ParseObject("Friends");
								// friends.put("user",ParseUser.getCurrentUser());
								// friends.addAllUnique("requestedSF",friendsId);
								// friends.saveInBackground();//saveEventually();();
							}
						}
					});
					ParseQuery<ParseUser> query2 = ParseUser.getQuery();
					query2.whereEqualTo("user", friends_ids);
					query2.getInBackground(friends_ids,
							new GetCallback<ParseUser>() {
								@Override
								public void done(ParseUser object,
										ParseException e) {
									frienduser = object;
									frienduser.getObjectId();
									ParseQuery<ParseObject> query3 = ParseQuery
											.getQuery("Friends");
									query3.whereEqualTo("user", frienduser);
									query3.findInBackground(new FindCallback<ParseObject>() {
										public void done(
												List<ParseObject> objects,
												ParseException e) {

											if (objects.size() > 0) {
												// edit
												if (e == null) {
													for (ParseObject obj : objects) {
														obj.addAllUnique(
																"pendingSF",
																Arrays.asList(ParseUser
																		.getCurrentUser()
																		.getObjectId()
																		.toString()));
														obj.saveInBackground();// saveEventually();();
													}
												} else {
												}
											} else {
												// save
												// ParseObject friends = new
												// ParseObject("Friends");
												// friends.put("user",
												// frienduser);
												// friends.addAllUnique("pendingSF",Arrays.asList(ParseUser.getCurrentUser().getObjectId().toString()));
												// friends.saveInBackground();//saveEventually();();
											}

											// New custom notification code
											ParseObject notification = new ParseObject("Activity");
											notification.put("creator",ParseUser.getCurrentUser());
											notification.put("followers",Arrays.asList(friends_ids));
											notification.put("reference4",reference_ids);
											notification.put("notificationtype", 6);
											notification.put("notificationDone", false);
											notification.put("readers",new JSONArray());
											notification.saveInBackground();// saveEventually();();
										}
									});
								}
							});
				} catch (Exception e) {
				}
				notifyDataSetChanged();
			}
		});

		if (!rowItem.getSupriseFriend()) {
			holder.mSupriseFriend.setBackgroundResource(R.drawable.button);
			holder.mSupriseFriend.setCompoundDrawablesWithIntrinsicBounds(
					R.drawable.plus, 0, 0, 0);
		} else {
			holder.mSupriseFriend.setBackgroundColor(context.getResources()
					.getColor(R.color.gray_button));
			holder.mSupriseFriend.setCompoundDrawablesWithIntrinsicBounds(
					R.drawable.tick, 0, 0, 0);
		}

		// /////////////////////////ends

		return convertView;
	}

	public int getCount() {
		return list.size();
	}

	@SuppressLint("DefaultLocale")
	public ArrayList<Friend> search(String query) {
		query = query.toLowerCase();
		Log.i("Search", query);
		list.clear();
		if (query.length() == 0) {
			list.addAll(origin);
		} else {
			for (Friend mList : origin) {
				Log.i("Search res", mList.getName().toLowerCase());
				if (mList.getName().toLowerCase().contains(query)) {
					Log.i("Search state", "Found");
					list.add(mList);
				} else {
					Log.i("Search state", "Not Found");
				}
			}
			notifyDataSetChanged();
		}
		return list;
	}

	public static interface FriendListener {
		public void getPeopleAdded(JSONArray arr, JSONArray friendsId);
	}

	public void showSelected() {
		JSONArray myArray = new JSONArray();
		JSONArray myFriendsId = new JSONArray();
		ArrayList<Friend> myList = new ArrayList<Friend>();
		myList = list;
		for (Friend s : myList) {
			if (s.getUserSelected()) {
				try {
					JSONObject object = new JSONObject();
					object.put("userid", s.getUserId());
					object.put("name", s.getName());
					object.put("source", s.getSource());
					object.put("userImage", s.getUserImage());
					myArray.put(object);
					myFriendsId.put(s.getUserId());
					friendsId.add(s.getUserId());
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		Log.d("MyApp", "Data: " + myList.toString());
		mListen.getPeopleAdded(myArray, myFriendsId);
		Log.d("MyApp friendsId", "" + friendsId.toString());
	}

	public void showSelectedS() {
		JSONArray myArray = new JSONArray();
		JSONArray myFriendsId = new JSONArray();
		ArrayList<Friend> myList = new ArrayList<Friend>();
		myList = list;
		for (Friend s : myList) {
			if (s.getSupriseFriend()) {
				try {
					JSONObject object = new JSONObject();
					object.put("userid", s.getUserId());
					object.put("name", s.getName());
					object.put("source", s.getSource());
					object.put("userImage", s.getUserImage());
					myArray.put(object);
					myFriendsId.put(s.getUserId());
					friendsIds.add(s.getUserId());
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		Log.d("MyApp", "Data: " + myList.toString());
		mListen.getPeopleAdded(myArray, myFriendsId);
		Log.d("MyApp friendsId", "" + friendsIds.toString());
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
