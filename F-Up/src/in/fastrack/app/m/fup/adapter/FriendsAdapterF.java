package in.fastrack.app.m.fup.adapter;

import in.fastrack.app.m.fup.FeedComments;
import in.fastrack.app.m.fup.FeedFragment;
import in.fastrack.app.m.fup.HomeActivity;
import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.UserFriendRelation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SendCallback;
import com.squareup.picasso.Picasso;

public class FriendsAdapterF extends BaseAdapter implements Filterable {
	
	private Context context;
	private int resource;
	private ArrayList<Friend> list,origin;
	private FriendListener mListen;
	private JSONArray friends;
	private UserFriendRelation ufr;
	ArrayList<String> friendsId = new ArrayList<String>();
	ArrayList<String> friendsIds = new ArrayList<String>();
	private JSONArray friendsJ = new JSONArray();
	private JSONArray friendssJ = new JSONArray();
	JSONArray list1 = new JSONArray();     
	JSONArray list2 = new JSONArray();    
	String friends_id;//,nami;
	String friends_ids;
	ParseUser frienduser;
	ParseObject reference_id;
	ParseObject reference_ids;
    String from;
    LayoutInflater vi;
    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
			@Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
            	
            	list = (ArrayList<Friend>) results.values;
                notifyDataSetInvalidated();
            }



           @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                if(TextUtils.isDigitsOnly(constraint)) {
                    ArrayList<Friend> list = new ArrayList<Friend>(origin);
                    result.values = list;
                    result.count = list.size();
                } else {
                    ArrayList<Friend> newValues = new ArrayList<Friend>();
                    for(int i = 0; i < origin.size(); i++) {
                        String team1 = origin.get(i).getName().toString();
                        if(team1.startsWith(constraint.toString())) {
                            newValues.add(origin.get(i));

                        }
                      
                    }
                    if(newValues.size()>0)
                    {
                    result.values = newValues;
                    result.count = newValues.size();
                    }else
                    {
                    	result.values = origin;
                        result.count = origin.size();
                    }
                }  

                return result;
            }
        };
    }

    public void setData() { 
    	//list.clear();
           list = origin;
       }

	public FriendsAdapterF(Context context, int resource, ArrayList<Friend> list,FriendListener mListen, JSONArray friends, UserFriendRelation ufr, String from) {
		
		this.context = context;
		this.resource = resource;
		this.list =list;
		this.origin =list;
		this.mListen = mListen;
		this.friends = friends;
		this.ufr = ufr;
		this.from = from;
		vi = LayoutInflater.from(context);
	}
	
	private class ViewHolder{
		TextView mName,mStatus,mStatusf;
		ImageView mUserImage,mRemovesurprise;
		Button mAction,mSupriseFriend,currentUserImage;
		LinearLayout mSupriseFriendll,mAddFriendll,mllRemovesurprise;
	}
	
	@Override
	public View getView(int position,View convertView,ViewGroup parent){
		 ViewHolder holder = null;
		Friend rowItem = getItem(position);
		if(convertView == null){
			convertView = vi.inflate(resource,parent,false);
			holder = new ViewHolder();
			holder.mName = (TextView) convertView.findViewById(R.id.tvPhoneName);
			holder.mUserImage = (ImageView) convertView.findViewById(R.id.ivUserImage);
			holder.mAction = (Button) convertView.findViewById(R.id.bInviteUser);
			holder.mStatus = (TextView) convertView.findViewById(R.id.bstatus);
			holder.mStatusf = (TextView) convertView.findViewById(R.id.bstatusf);
			holder.mSupriseFriend = (Button) convertView.findViewById(R.id.bSupriseFriend);
			holder.mSupriseFriendll = (LinearLayout) convertView.findViewById(R.id.bSupriseFriendll);
			holder.mAddFriendll = (LinearLayout) convertView.findViewById(R.id.bAddFriendll);
			
			holder.mRemovesurprise = (ImageView) convertView.findViewById(R.id.bRemovesurprise);
			holder.mllRemovesurprise = (LinearLayout) convertView.findViewById(R.id.llRemovesurprise);
			
			holder.mAddFriendll.setVisibility(View.GONE);
			holder.mSupriseFriendll.setVisibility(View.VISIBLE);
			//holder.mRemovesurprise.setVisibility(View.GONE);
			holder.mllRemovesurprise.setVisibility(View.GONE);
			//holder.currentUserImage.setOnClickListener(includeMeButtonListener);
			convertView.setTag(holder);
		}else
		{
			holder = (ViewHolder) convertView.getTag();
		}
         	
			
			////////////////////////////starts
			
			

			if(rowItem.getStatus()!=null){
			 if (rowItem.getStatus().equals("SurpriseFriend")) {
				holder.mllRemovesurprise.setVisibility(View.VISIBLE);
				holder.mSupriseFriendll.setVisibility(View.GONE);

				}
			 else if (rowItem.getStatus().equals("Accept")) {
				holder.mllRemovesurprise.setVisibility(View.GONE);
				holder.mSupriseFriendll.setVisibility(View.VISIBLE);
				}
			 else if (rowItem.getStatus().equals("RequestSent")) {
					holder.mllRemovesurprise.setVisibility(View.GONE);
					holder.mSupriseFriendll.setVisibility(View.VISIBLE);
				}
				}
			else{
				holder.mllRemovesurprise.setVisibility(View.GONE);
				holder.mSupriseFriendll.setVisibility(View.VISIBLE);
			}
			
			
		
		

		// Initial status of views
		
		holder.mStatusf.setText(rowItem.getStatus());
		holder.mStatusf.setVisibility(rowItem.getVisibility());
		holder.mSupriseFriend.setTag(rowItem);
	    holder.mSupriseFriend.setVisibility(rowItem.getmActionVisibility());	   
	    holder.mRemovesurprise.setTag(rowItem);	    	
		holder.mName.setText(rowItem.getName());
		
		if(rowItem.getUserImage() != null){
			Picasso.with(context).load(rowItem.getUserImage()).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into(holder.mUserImage);
		}else{
			holder.mUserImage.setImageResource(R.drawable.friend_nopic);
		}

		holder.mRemovesurprise.setOnClickListener( new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Fragment
				if (!Util.isNetworkAvailable(context)) {	
			
			     DialogFactory.getBeaconStreamDialog1(context, context.getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
		       } else 
		
		       {
				ImageView b = (ImageView) v;
			    Friend p = (Friend) v.getTag();
				//Log.d("", ""+b.getText().toString());	
			    
				friends_id =p.getUserId();
				Log.i("friends_id state","Found"+friends_id);
				
				AlertDialog.Builder alertDialogBuilderDisconnect = new AlertDialog.Builder(context);
				alertDialogBuilderDisconnect.setMessage("Do you want to unfriend !!");
				alertDialogBuilderDisconnect.setIcon(R.drawable.alarm);
				alertDialogBuilderDisconnect.setNegativeButton("NO", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.cancel();
					}
				});
				alertDialogBuilderDisconnect.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    	
    					ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
    							query.whereEqualTo("user", ParseUser.getCurrentUser());
    							query.findInBackground(new FindCallback<ParseObject>() {
    								public void done(List<ParseObject> objects,ParseException e) {
    									if (objects.size() > 0) {

    										// edit
    										if (e == null) {
    											for (ParseObject obj : objects) {
    												friendsJ = obj.getJSONArray("connectedSF");
    											}
    											if(friendsJ!=null)
    											for(int i= 0; i < friendsJ.length(); i++) {
    												try {
    													if(friendsJ.get(i).equals(friends_id)){															
    														//friendsJ.remove(i);																																																
    													}
    													else{
															list1.put(friendsJ.get(i));
														}
    													
    												} catch (JSONException e1) {
    													// TODO Auto-generated catch block
    													e1.printStackTrace();
    												}
    											}
    											for (ParseObject obj : objects) {
    												obj.put("connectedSF" ,list1);
    												obj.saveInBackground();//saveEventually();();
    											}
    											
    										} else {
    										}
    									}
    								}
    							});

    							ParseQuery<ParseUser> query2 = ParseUser.getQuery();
    							query2.whereEqualTo("user", friends_id);
    							query2.getInBackground(friends_id,new GetCallback<ParseUser>() {
    										@Override
    										public void done(ParseUser object,ParseException e) {
    											
    											
    											ParseQuery<ParseObject> query3 = ParseQuery.getQuery("Friends");
    											query3.whereEqualTo("user", object);
    											query3.findInBackground(new FindCallback<ParseObject>() {
    												public void done(List<ParseObject> objects,ParseException e) {
    													
    													if (objects.size() > 0) {
    														// edit
    														if (e == null) {
    															for (ParseObject obj : objects) {
    																friendssJ = obj.getJSONArray("connectedSF");
    															}
    															if(friendssJ!=null)
    															for(int i= 0; i < friendssJ.length(); i++) {
    																try {
    																	if(friendssJ.get(i).equals(ParseUser.getCurrentUser().getObjectId().toString())){															
    																		//friendssJ.remove(i);																																																
    																	}
    																	else{
    																		list2.put(friendssJ.get(i));
    																	}
    																} catch (JSONException e1) {
    																	// TODO Auto-generated catch block
    																	e1.printStackTrace();
    																}
    															}
    															
    															for (ParseObject obj : objects) {
    																obj.put("connectedSF", list2);
    																obj.saveInBackground();//saveEventually();();
    															}
    															
    															}
    														} else {
    														}
    													
    													
    													notifyDataSetChanged();																													
    													Intent i = new Intent(context,HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);												
    													context.startActivity(i);
    													
    													}
    																								
    																																				
    																						
    											});
    										}
    									});		
                    	
                    	
                    }
                });
				AlertDialog alertDialogDisconnect = alertDialogBuilderDisconnect.create();
				alertDialogDisconnect.show();  
				
				
		       }		
			}
		});
		
	        holder.mAction.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					//Fragment
					if (!Util.isNetworkAvailable(context)) {	
				
				     DialogFactory.getBeaconStreamDialog1(context, context.getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			       } else 
			
			       {
			    	   
					Button b = (Button) v;
				    Friend p = (Friend) v.getTag();
					Log.d("", ""+b.getText().toString());
					
					
					
					friends_id =p.getUserId();
					Log.i("friends_id state","Found"+friends_id);
					if(!p.getUserSelected()){
						//Not selected so select them
						p.setUserSelected(true);
						b.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
						b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
						

						
					}else{
						//Already selected, so unselect
						p.setUserSelected(false);
						b.setBackgroundResource(R.drawable.button);
						b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus, 0, 0, 0);
					}
					showSelected();
				
					try {

						ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
						query.whereEqualTo("user", ParseUser.getCurrentUser());
						query.findInBackground(new FindCallback<ParseObject>() {
							public void done(List<ParseObject> objects,
									ParseException e) {
								if (objects.size() > 0) {

									// edit
									if (e == null) {
										for (ParseObject obj : objects) {
											obj.addAllUnique("requestedF", Arrays.asList(friends_id));
											obj.saveInBackground();//saveEventually();();
											
											reference_id = obj;
										}
									} else {
									}
								} else {
									/*// save
									ParseObject friends = new ParseObject("Friends");
									friends.put("user", ParseUser.getCurrentUser());
									friends.addAllUnique("requestedF", friendsId);
									friends.saveInBackground();//saveEventually();();*/
								}
							}
						});
						ParseQuery<ParseUser> query2 = ParseUser.getQuery();
						query2.whereEqualTo("user", friends_id);
						query2.getInBackground(friends_id,
								new GetCallback<ParseUser>() {
									@Override
									public void done(ParseUser object,ParseException e) {
										frienduser = object;
										frienduser.getObjectId();
										ParseQuery<ParseObject> query3 = ParseQuery.getQuery("Friends");
										query3.whereEqualTo("user", frienduser);
										query3.findInBackground(new FindCallback<ParseObject>() {
											public void done(List<ParseObject> objects,ParseException e) {
												
												if (objects.size() > 0) {
													// edit
													if (e == null) {
														for (ParseObject obj : objects) {
															obj.addAllUnique("pendingF",Arrays.asList(ParseUser.getCurrentUser().getObjectId().toString()));
															obj.saveInBackground();//saveEventually();();
														}
													} else {
													}
												} else {
													/*// save
													ParseObject friends = new ParseObject("Friends");
													friends.put("user", frienduser);
													friends.addAllUnique("pendingF",Arrays.asList(ParseUser.getCurrentUser().getObjectId().toString()));
													friends.saveInBackground();//saveEventually();();*/
												}
																							
												//New custom notification code
												ParseObject notification = new ParseObject("Activity");
												notification.put("creator",ParseUser.getCurrentUser());
												notification.put("followers",Arrays.asList(friends_id));
												notification.put("reference3",reference_id);
												notification.put("notificationtype",5);
												notification.put("notificationDone",false);
												notification.put("readers",new JSONArray());
												notification.add("readers", ParseUser.getCurrentUser().getObjectId());
												notification.saveInBackground();//saveEventually();();																								
											}										
										});
									}
								});
					} catch (Exception e) {
					}
					notifyDataSetChanged();
				
				}
				}
			});
			
			if(!rowItem.getUserSelected()){
				holder.mAction.setBackgroundResource(R.drawable.button);
				holder.mAction.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus, 0, 0, 0);
			}else{
				holder.mAction.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
				holder.mAction.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
			}
			
			
			
			
			holder.mSupriseFriend.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					//Fragment
					if (!Util.isNetworkAvailable(context)) {	
				
				     DialogFactory.getBeaconStreamDialog1(context, context.getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			       } else 
			
			       {
					Button b = (Button) v;
				    Friend ps = (Friend) v.getTag();
					Log.d("", ""+b.getText().toString());
					ps.setStatus("RequestSent");
					ps.setmActionVisibility(View.GONE);
                  //  ps.setmRemoveVisibility(View.VISIBLE);					
					notifyDataSetChanged();										
					friends_ids = ps.getUserId();
					Log.i("friends_ids state","Found"+friends_ids);
//					if(!ps.getSupriseFriend()){
//						//Not selected so select them
//						ps.setSupriseFriend(true);
//						b.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
//						b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
//						b.setText("Invited");
//						b.setFocusable(false);
//						b.setEnabled(false);
//
//						
//					}else{
//						//Already selected, so unselect
//						ps.setSupriseFriend(false);
//						b.setBackgroundResource(R.drawable.button);
//						b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus, 0, 0, 0);
//					}
					showSelectedS();
				
					try {	

						ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
						query.whereEqualTo("user", ParseUser.getCurrentUser());
						query.findInBackground(new FindCallback<ParseObject>() {
							public void done(List<ParseObject> objects,
									ParseException e) {
								if (objects.size() > 0) {

									// edit
									if (e == null) {
										for (ParseObject obj : objects) {
											obj.addAllUnique("requestedSF",Arrays.asList(friends_ids));
											obj.saveInBackground();//saveEventually();();
											
											reference_ids = obj;//for creator
											
										}
									} else {
									}
								} else {
									/*// save
									ParseObject friends = new ParseObject("Friends");
									friends.put("user", ParseUser.getCurrentUser());
									friends.addAllUnique("requestedSF", friendsId);
									friends.saveInBackground();//saveEventually();();*/
								}
							}
						});
						ParseQuery<ParseUser> query2 = ParseUser.getQuery();
						query2.whereEqualTo("user", friends_ids);
						query2.getInBackground(friends_ids,
								new GetCallback<ParseUser>() {
									@Override
									public void done(ParseUser object,ParseException e) {
										frienduser = object;
										frienduser.getObjectId();
										ParseQuery<ParseObject> query3 = ParseQuery.getQuery("Friends");
										query3.whereEqualTo("user", frienduser);
										query3.findInBackground(new FindCallback<ParseObject>() {
											public void done(List<ParseObject> objects,ParseException e) {
												
												if (objects.size() > 0) {
													// edit
													if (e == null) {
														for (ParseObject obj : objects) {
															obj.addAllUnique("pendingSF",Arrays.asList(ParseUser.getCurrentUser().getObjectId().toString()));
															obj.saveInBackground();//saveEventually();();
															
															//reference_ids = obj;//friend
														// nami = obj.getString("fullname");
															
														}
													} else {
													}
												} else {
												/*	// save
													ParseObject friends = new ParseObject("Friends");
													friends.put("user", frienduser);
													friends.addAllUnique("pendingSF",Arrays.asList(ParseUser.getCurrentUser().getObjectId().toString()));
													friends.saveInBackground();//saveEventually();();*/
												}
												try {											
											
											 //Earlier here
											 //New custom notification code
									          /*  ParseObject notification = new ParseObject("Activity");
												notification.put("creator",ParseUser.getCurrentUser());
												notification.put("followers",Arrays.asList(friends_ids));
												if(reference_ids != null)
												notification.put("reference4",reference_ids);
												notification.put("notificationtype",6);
												notification.put("notificationDone",false);
												notification.put("readers",new JSONArray());
												notification.add("readers", ParseUser.getCurrentUser().getObjectId());
												notification.saveInBackground();//saveEventually();();	
											*/	
													
													//if(nami != null)
												//Toast.makeText(getContext(), " Surprise Friend request sent" + nami, Toast.LENGTH_SHORT).show();
												
												
												
													// parse push implementation
													ParseQuery pushQuery = ParseInstallation.getQuery();
													ParsePush push = new ParsePush();
													pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId()); // not me
													//pushQuery.whereEqualTo("followers",Arrays.asList(friends_id));  
													pushQuery.whereEqualTo("myuser",friends_ids);// push notification to other users
													push.setQuery(pushQuery);
													JSONObject tempAlarmData = new JSONObject();
														if (reference_ids != null)
														tempAlarmData.put("reference4",reference_ids);		
														tempAlarmData.put("notificationtype", 6);	
														tempAlarmData.put("alert","The  surprise friend request is by "+ParseUser.getCurrentUser().getString("fullname"));
														tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivityNew");												
														push.setData(tempAlarmData);
														push.sendInBackground(new SendCallback() {
															   public void done(ParseException e) {
																     if (e == null) {
																    	 Toast.makeText(context, "Suprise request sent", Toast.LENGTH_LONG).show();
																    	// Shifted to here
																    	//New custom notification code
																            ParseObject notification = new ParseObject("Activity");
																			notification.put("creator",ParseUser.getCurrentUser());
																			notification.put("followers",Arrays.asList(friends_ids));
																			if(reference_ids != null)
																			notification.put("reference4",reference_ids);
																			notification.put("notificationtype",6);
																			notification.put("notificationDone",true);
																			notification.put("readers",Arrays.asList(friends_ids));// changed from `new JSONArray()` to 
																			notification.add("readers", ParseUser.getCurrentUser().getObjectId());
																			notification.saveInBackground();//saveEventually();();	
																       Log.d("push", "success!");
																     } else {
																       Log.d("push", "failure");
																     }
																   }
																 });
												
												
												
												} catch (Exception e1) {
													// TODO Auto-generated catch block
													e1.printStackTrace();
												}
												
												
												
												
												
											}										
										});
									}
								});
				
					/*	ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
						query.whereEqualTo("user", ParseUser.getCurrentUser());
						query.findInBackground(new FindCallback<ParseObject>() {
							public void done(List<ParseObject> objects,
									ParseException e) {
								if (objects.size() > 0) {

									// edit
									if (e == null) {
										for (ParseObject obj : objects) {
											obj.addAllUnique("connectedSF",Arrays.asList(friends_ids));
											obj.saveInBackground();//saveEventually();();
											
											reference_ids = obj;
										}
									} else {
									}
								} else {
									
								}
								
								//New custom notification code
								ParseObject notification = new ParseObject("Activity");
								notification.put("creator",ParseUser.getCurrentUser());
								notification.put("followers",Arrays.asList(friends_ids));
								notification.put("reference4",reference_ids);
								notification.put("notificationtype",6);
								notification.put("notificationDone",false);
								notification.put("readers",new JSONArray());
								notification.saveInBackground();//saveEventually();();	
								
								
							}
						});*/
			
					} catch (Exception e) {
					}
					notifyDataSetChanged();
				}
				}
			});
			
			if(!rowItem.getSupriseFriend()){
				holder.mSupriseFriend.setBackgroundResource(R.drawable.button);
				holder.mSupriseFriend.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus, 0, 0, 0);
			}else{
				holder.mSupriseFriend.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
				holder.mSupriseFriend.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
			}
				
			
		///////////////////////////ends
		return convertView;
	}
	
	
	public int getCount(){
		return list.size();
	}
	
	@SuppressLint("DefaultLocale")
	public ArrayList<Friend> search(String query){
		query = query.toLowerCase();
		Log.i("Search",query);
		list.clear();
		if(query.length() == 0){
			list.addAll(origin);
		}else{
			for(Friend mList:origin){
				Log.i("Search res",mList.getName().toLowerCase());
				if(mList.getName().toLowerCase().contains(query)){
					Log.i("Search state","Found");
					list.add(mList);
				}else{
					Log.i("Search state","Not Found");
				}
			}
			notifyDataSetChanged();
		}
		return list;
	}
	
	public static interface FriendListener {
		public void getPeopleAdded(JSONArray arr,JSONArray friendsId);
	}
	
	public void showSelected() {
		JSONArray myArray = new JSONArray();
		JSONArray myFriendsId = new JSONArray();
		ArrayList<Friend> myList = new ArrayList<Friend>();
		myList = list;
		for(Friend s: myList){
			if(s.getUserSelected()){
				try {
					JSONObject object = new JSONObject();
					object.put("userid",s.getUserId());
					object.put("name",s.getName());
					object.put("source",s.getSource());
					object.put("userImage",s.getUserImage());
					myArray.put(object);
					myFriendsId.put(s.getUserId());
					friendsId.add(s.getUserId());
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		Log.d("MyApp","Data: "+myList.toString());
		mListen.getPeopleAdded(myArray,myFriendsId);
		Log.d("MyApp friendsId",""+friendsId.toString());
	}
	public void showSelectedS() {
		JSONArray myArray = new JSONArray();
		JSONArray myFriendsId = new JSONArray();
		ArrayList<Friend> myList = new ArrayList<Friend>();
		myList = list;
		for(Friend s: myList){
			if(s.getSupriseFriend()){
				try {
					JSONObject object = new JSONObject();
					object.put("userid",s.getUserId());
					object.put("name",s.getName());
					object.put("source",s.getSource());
					object.put("userImage",s.getUserImage());
					myArray.put(object);
					myFriendsId.put(s.getUserId());
					friendsIds.add(s.getUserId());
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		Log.d("MyApp","Data: "+myList.toString());
		mListen.getPeopleAdded(myArray,myFriendsId);
		Log.d("MyApp friendsId",""+friendsIds.toString());
	}

	@Override
	public Friend getItem(int arg0) {
		// TODO Auto-generated method stub
		return list.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}
