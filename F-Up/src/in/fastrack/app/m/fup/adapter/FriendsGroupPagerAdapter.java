package in.fastrack.app.m.fup.adapter;

import in.fastrack.app.m.fup.FriendAddFragmentF;
import in.fastrack.app.m.fup.GroupsAddFragmentF;
import in.fastrack.app.m.fup.NewFacebookInviteFragmentF;
import in.fastrack.app.m.fup.GroupsF;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;


public class FriendsGroupPagerAdapter extends FragmentPagerAdapter {

	public FriendsGroupPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {
		switch (index) {
		case 0:
			// return new FacebookInviteFragment();
			return new NewFacebookInviteFragmentF();
		case 1:
			return new GroupsAddFragmentF();
		
		}
		return null;
	}

	@Override
	public int getCount() {
		return 2;
	}

}