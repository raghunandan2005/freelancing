package in.fastrack.app.m.fup.adapter;

import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.Group;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class GroupAdapter extends ArrayAdapter<Group> {
	
	private Context context;
	private int resource;
	private ArrayList<Group> list,origin;
	private JSONArray friends;

	public GroupAdapter(Context context, int resource, ArrayList<Group> list) {
		super(context, resource, list);
		this.context = context;
		this.resource = resource;
		this.list = new ArrayList<Group>();
		this.list.addAll(list);
	}
	
	private class ViewHolder{
		TextView mGroupMemberCount, mGroupName;
		ImageView mGroupImage;
		
	}
	
	@Override
	public View getView(int position,View convertView,ViewGroup parent){
		ViewHolder holder = null;
		Group rowItem = getItem(position);
		LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if(convertView == null){
			convertView = vi.inflate(resource,parent,false);
			holder = new ViewHolder();
			holder.mGroupName = (TextView) convertView.findViewById(R.id.mGroupName);
			holder.mGroupImage = (ImageView) convertView.findViewById(R.id.mGroupImage);
			holder.mGroupMemberCount = (TextView) convertView.findViewById(R.id.mGroupMemberCount);
		
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.mGroupName.setText(rowItem.getName());
		
		if(rowItem.getGroupImagePath() != null){
			Picasso.with(context).load(rowItem.getGroupImagePath()).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into(holder.mGroupImage);
		}else{
			holder.mGroupImage.setImageResource(R.drawable.friend_nopic);
		}
		
		holder.mGroupMemberCount.setText(String.valueOf(rowItem.getMemberCount()) + " Members");
		
		return convertView;
	}
	
	
	public int getCount(){
		return list.size();
	}
	
	
}
