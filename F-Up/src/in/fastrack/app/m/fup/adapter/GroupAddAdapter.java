package in.fastrack.app.m.fup.adapter;

import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.model.Group;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

public class GroupAddAdapter extends ArrayAdapter<Group> {
	
	private Context context;
	private int resource;
	private ArrayList<Group> list,origin;
	private GroupListener mListen;
	private JSONArray groups;
	private Button b;
	private Group p;
	private     ProgressBar pd;

	public GroupAddAdapter(Context context, int resource, ArrayList<Group> list, GroupListener mListen, JSONArray groups) {
		super(context, resource, list);
		this.context = context;
		this.resource = resource;
		this.list = list;
		this.origin = list;
//		this.list.addAll(list);
//		this.origin.addAll(list);
		this.mListen = mListen;
		this.groups = groups;
		System.out.println(groups.toString());
	}
	
	private class ViewHolder{
		TextView mGroupName,mGroupAddMemberCount;
		ImageView mGroupImage;
		Button mAddGroup;
		ProgressBar pb;
	}
	
	@Override
	public View getView(int position,View convertView,ViewGroup parent){
		ViewHolder holder = null;
		Group rowItem = getItem(position);
		LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if(convertView == null){
			convertView = vi.inflate(resource,null);
			holder = new ViewHolder();
			holder.mGroupName = (TextView) convertView.findViewById(R.id.mGroupName);
			holder.mGroupImage = (ImageView) convertView.findViewById(R.id.mGroupImage);
			holder.mAddGroup = (Button) convertView.findViewById(R.id.bgroupAdd);
			holder.pb = (ProgressBar) convertView.findViewById(R.id.progressBar1);
			holder.mGroupAddMemberCount = (TextView) convertView.findViewById(R.id.mGroupAddMemberCount);
			//holder.mSupriseFriend = (Button) convertView.findViewById(R.id.bSupriseFriend);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.mGroupName.setText(rowItem.getName());
		holder.mGroupAddMemberCount.setText(String.valueOf(rowItem.getMemberCount()) + " Members");
		holder.mAddGroup.setTag(R.string.objid1,rowItem);
		holder.mAddGroup.setTag(R.string.objid2,holder.pb);
		if(rowItem.getGroupImagePath() != null){
			Picasso.with(context).load(rowItem.getGroupImagePath()).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into(holder.mGroupImage);
		}else{
			holder.mGroupImage.setImageResource(R.drawable.friend_nopic);
		}
		if(rowItem.getGroupSelected()==true)
		{
			holder.mAddGroup.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
			holder.mAddGroup.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
			
		}
		else
		{
		
			holder.mAddGroup.setBackgroundResource(R.drawable.button);
			holder.mAddGroup.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus, 0, 0, 0);
		}
		
		holder.mAddGroup.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				b = (Button) v;
			    p = (Group) v.getTag(R.string.objid1);
			    pd = (ProgressBar) v.getTag(R.string.objid2);
			    pd.setVisibility(View.VISIBLE);
//				holder.mAddGroup.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
//				holder.mAddGroup.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
		    	 //showSelected();
		    	 //notifyDataSetChanged();
				Log.i("group id", ">>>>>>>>>>>>>>"+p.getGroupObject()+"\n"+p.getGroupObject().toString());
				ParseQuery<ParseObject> query = ParseQuery.getQuery("Groups");
				query.whereEqualTo("objectId", p.getGroupObject().toString());
				query.findInBackground(new FindCallback<ParseObject>() {
					public void done(List<ParseObject> objects,
							ParseException e) {
						if (objects.size() > 0) {

							// edit
							if (e == null) {
								for (ParseObject obj : objects) {
									if(obj.getBoolean("Gselected")==true)
									{
									obj.put("Gselected", false);
									p.setGroupSelected(false);
									
									}else if(obj.getBoolean("Gselected")==false)
									{
								    obj.put("Gselected", true);
							        p.setGroupSelected(true);

									}
									obj.saveInBackground(new SaveCallback() {
										   public void done(ParseException e) {
											     if (e == null) {
										
														 showSelected();
												    	 notifyDataSetChanged();
											    	 Toast.makeText(context, "Changed", Toast.LENGTH_LONG).show();
													pd.setVisibility(View.INVISIBLE);
											     } else {
											    	 e.printStackTrace();
											    	 Toast.makeText(context, "Not called", Toast.LENGTH_LONG).show();

											     }
											   }
											 });
									

								}
							} else {
							}
						} else {
							// save
							 ParseObject gr = new ParseObject("Groups");
							// gr.put("user",ParseUser.getCurrentUser());
							 gr.put("Gselected", true);
							 gr.saveInBackground(new SaveCallback() {
								   public void done(ParseException e) {
									     if (e == null) {
									    	 showSelected();
									    	 notifyDataSetChanged();
											
									     } else {
									      // myObjectSaveDidNotSucceed();
									    	 e.printStackTrace();
									    	 Toast.makeText(context, "Not called", Toast.LENGTH_LONG).show();
									    	 
									     }
									   }
									 });
						}
						
				    	/*b.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
					    b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
				    	 showSelected();
				    	 notifyDataSetChanged();*/
					}
				});
				
				
				
				
//				p.setGroupSelected(true);
//				p.setColorb(context.getResources().getColor(R.color.gray_button));
//				p.setTick(R.drawable.plus);
//				GroupAddAdapter.this.notifyDataSetChanged();
//				Log.d("MyApp","Selected: "+p.getGroupSelected().toString());
//				if(!p.getGroupSelected()){
//					//Not selected so select them
//					p.setGroupSelected(true);
//					b.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
//					b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
//				}else{
//					//Already selected, so unselect
//					p.setGroupSelected(false);
//					b.setBackgroundResource(R.drawable.button);
//					b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus, 0, 0, 0);
//				}
			
			}
		});
		
		if(!rowItem.getGroupSelected()){
			holder.mAddGroup.setBackgroundResource(R.drawable.button);
			holder.mAddGroup.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus, 0, 0, 0);
		}else{
			holder.mAddGroup.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
			holder.mAddGroup.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
		}
		
		return convertView;
	}
	
	
	public int getCount(){
		return list.size();
	}
	
	@SuppressLint("DefaultLocale")
	public void search(String query){
		query = query.toLowerCase();
		Log.i("Search",query);
		list.clear();
		if(query.length() == 0){
			list.addAll(origin);
		}else{
			for(Group mList:origin){
				Log.i("Search res",mList.getName().toLowerCase());
				if(mList.getName().toLowerCase().contains(query)){
					Log.i("Search state","Found");
					list.add(mList);
				}else{
					Log.i("Search state","Not Found");
				}
			}
			notifyDataSetChanged();
		}
	}
	
	public static interface GroupListener {
		public void getGroupAdded(JSONArray arr,JSONArray groupsId);
	}
	
	public void showSelected() {
		JSONArray myArray = new JSONArray();
		JSONArray mygroupsId = new JSONArray();
		ArrayList<Group> myList = new ArrayList<Group>();
		myList = list;
		for(Group s: myList){
			if(s.getGroupSelected()){
				try {
					JSONObject object = new JSONObject();
					object.put("groupid",s.getGroupObject());
					object.put("groupname",s.getName());
					object.put("friendslist",s.getFriendList());
					object.put("groupImage",s.getGroupImagePath());
					myArray.put(object);
					mygroupsId.put(s.getGroupObject());
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		Log.d("MyApp","Group: "+myList.toString());
		mListen.getGroupAdded(myArray,mygroupsId);
	}

}
