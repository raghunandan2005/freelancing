package in.fastrack.app.m.fup.adapter;

import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.Friend;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class GroupFriendAdapter extends ArrayAdapter<Friend> {
	
	private Context context;
	private int resource;
	private ArrayList<Friend> list,origin;
	private JSONArray friends;
	private LayoutInflater mInflater;

	public GroupFriendAdapter(Context context, int resource, ArrayList<Friend> list) {
		super(context, resource, list);
        mInflater = LayoutInflater.from(context);      
		this.resource = resource;
		this.list = new ArrayList<Friend>();
		this.list.addAll(list);
	}
	
	private class ViewHolder{
		TextView userProfileName, userAlarms;
		ImageView userprofilePic;
	}
	
	@Override
	public View getView(int position,View convertView,ViewGroup parent){
		ViewHolder holder = null;
		Friend rowItem = getItem(position);

		if(convertView == null){
			convertView = mInflater.inflate(resource,null);
			holder = new ViewHolder();
			holder.userProfileName = (TextView) convertView.findViewById(R.id.userProfileName);
			holder.userprofilePic = (ImageView) convertView.findViewById(R.id.userprofilePic);
			holder.userAlarms = (TextView) convertView.findViewById(R.id.userAlarms);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.userProfileName.setText(rowItem.getName());
		
		if(rowItem.getUserImage() != null){
			Picasso.with(context).load(rowItem.getUserImage()).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into(holder.userprofilePic);
		}else{
			holder.userprofilePic.setImageResource(R.drawable.friend_nopic);
		}
		
		//holder.userAlarms.setText(String.valueOf(rowItem.getMemberCount()) + " Members");
		
		return convertView;
	}
	
	
	public int getCount(){
		return list.size();
	}
	
	
}
