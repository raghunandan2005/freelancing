package in.fastrack.app.m.fup.adapter;

import in.fastrack.app.m.fup.HomeActivity;
import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.Group;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

public class GroupRemoveAdapter extends ArrayAdapter<Group> {

	private Context context;
	private int resource;
	private ArrayList<Group> list, origin;
	private JSONArray friends;
	String groupid;
	int pos;

	public GroupRemoveAdapter(Context context, int resource,
			ArrayList<Group> list) {
		super(context, resource, list);
		this.context = context;
		this.resource = resource;
		this.list = new ArrayList<Group>();
		this.list.addAll(list);
	}

	private class ViewHolder {
		TextView mGroupMemberCount, mGroupName;
		ImageView mGroupImage, mRemoveGroup;
		LinearLayout mllRemoveGroup;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Group getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		// Group rowItem = getItem(position);
		Group rowItem = list.get(position);
		LayoutInflater vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = vi.inflate(resource, null);
			holder = new ViewHolder();
			holder.mGroupName = (TextView) convertView
					.findViewById(R.id.mGroupName);
			holder.mGroupImage = (ImageView) convertView
					.findViewById(R.id.mGroupImage);
			holder.mGroupMemberCount = (TextView) convertView
					.findViewById(R.id.mGroupMemberCount);
			holder.mRemoveGroup = (ImageView) convertView
					.findViewById(R.id.mGroupCloseImage);
			holder.mllRemoveGroup = (LinearLayout) convertView
					.findViewById(R.id.mGroupCloseImagell);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.mRemoveGroup.setTag(rowItem);

		if (rowItem.getCretorstring() != null) {
			if (rowItem.getCretorstring().equalsIgnoreCase(
					ParseUser.getCurrentUser().getObjectId().toString())) {
				holder.mllRemoveGroup.setVisibility(View.VISIBLE);
			} else {
				holder.mllRemoveGroup.setVisibility(View.GONE);
			}
		} else {
			holder.mllRemoveGroup.setVisibility(View.GONE);
		}

		holder.mRemoveGroup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!Util.isNetworkAvailable(context)) {

					DialogFactory.getBeaconStreamDialog1(
							context,
							context.getResources().getString(
									R.string.nw_error_str),
							Util.onClickListner, Util.retryClickListner).show();
				} else

				{
					ImageView b = (ImageView) v;

					Group p = (Group) v.getTag();
					groupid = p.getGroupObject();
					pos = position;
					AlertDialog.Builder alertDialogBuilderDisconnect = new AlertDialog.Builder(
							context);
					alertDialogBuilderDisconnect
							.setMessage("Do you want to delete this group?");
					alertDialogBuilderDisconnect.setIcon(R.drawable.alarm);
					alertDialogBuilderDisconnect.setNegativeButton("NO",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});
					alertDialogBuilderDisconnect.setPositiveButton("YES",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									ParseQuery<ParseObject> deletegroup = ParseQuery
											.getQuery("Groups");
									if (groupid != null) {
										deletegroup.getInBackground(groupid,
												new GetCallback<ParseObject>() {
													@Override
													public void done(
															final ParseObject object,
															com.parse.ParseException e) {
														if (e == null) {
															object.put(
																	"deleteG",
																	(object.get("connectedGF")));// ParseUser.getCurrentUser().getObjectId().toString()
															object.add(
																	"deleteG",
																	ParseUser
																			.getCurrentUser()
																			.getObjectId());
															object.saveInBackground();
															list.remove(pos);
															notifyDataSetChanged();

														}
													}
												});
									}

								}
							});
					AlertDialog alertDialogDisconnect = alertDialogBuilderDisconnect
							.create();
					alertDialogDisconnect.show();

				}

			}
		});

		holder.mGroupName.setText(rowItem.getName());

		if (rowItem.getGroupImagePath() != null) {
			Picasso.with(context).load(rowItem.getGroupImagePath()).fit()
					.centerCrop().transform(new RoundedTransform())
					.placeholder(R.drawable.friend_nopic)
					.error(R.drawable.friend_nopic).into(holder.mGroupImage);
		} else {
			holder.mGroupImage.setImageResource(R.drawable.friend_nopic);
		}

		holder.mGroupMemberCount.setText(String.valueOf(rowItem
				.getMemberCount()) + " Members");

		return convertView;
	}

}
