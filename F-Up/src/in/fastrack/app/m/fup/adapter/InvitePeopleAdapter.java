package in.fastrack.app.m.fup.adapter;

import in.fastrack.app.m.fup.ContactsFragment;
import in.fastrack.app.m.fup.FacebookInviteFragment;
import in.fastrack.app.m.fup.NewFacebookInviteFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;


public class InvitePeopleAdapter extends FragmentPagerAdapter {


	public InvitePeopleAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int index) {
		switch(index){
		case 0:
			//return new FacebookInviteFragment();
			return new NewFacebookInviteFragment();
		case 1:
			return new ContactsFragment();
		}
		return null;
	}

	@Override
	public int getCount() {
		return 2;
	}

}
