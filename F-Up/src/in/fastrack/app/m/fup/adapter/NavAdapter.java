package in.fastrack.app.m.fup.adapter;

import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.model.NavList;

import java.util.ArrayList;

import com.parse.ParseFile;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavAdapter extends BaseAdapter {

	private ArrayList<NavList> navItems;
	private LayoutInflater mInflater;
	private Context mContext;
    private String t = null,imageUrl;
    ParseFile p;
	public NavAdapter(Context context, ArrayList<NavList> navItems) {

		this.navItems = navItems;
		mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		mContext = context;
	}

	@Override
	public int getCount() {
		return navItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.drawer_list_item, parent,
					false);
			holder.imgIcon = (ImageView) convertView.findViewById(R.id.icon);
			holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
			//holder.txtCount = (TextView) convertView.findViewById(R.id.counter);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
        if(position==3)
        {
        	
    		p = ParseUser.getCurrentUser().getParseFile("profilethumbnail");
    		if(p!=null){
    			String url = p.getUrl();
    			//Log.i("FB IMAGE",url);
    			imageUrl = url;
    		}else{
    			if(ParseUser.getCurrentUser().get("facebookid") != null && ParseUser.getCurrentUser().get("facebookid") != "") {
    				t = ParseUser.getCurrentUser().get("facebookid").toString();
    				if((t!=null)||(t!="")){
    					imageUrl = "https://graph.facebook.com/"
    							+ ParseUser.getCurrentUser().get("facebookid").toString()
    							+ "/picture/?type=square";
    				}
    				
    			}
    		}
//    		Picasso.with(mContext)
//    		  .load(imageUrl)
//    		  .transform(new RoundedTransform())
//    		  .placeholder(R.drawable.ic_launcher).error(R.drawable.ic_launcher)
//    		  .into(holder.imgIcon);
    		Picasso.with(mContext).load(imageUrl).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into(holder.imgIcon);
    		//End Code for getting profile image
        }else
        {
		holder.imgIcon.setImageResource(navItems.get(position).getIcon());
        }
		holder.txtTitle.setText(navItems.get(position).getTitle());

		// displaying count
		// check whether it set visible or not
//		if (navItems.get(position).getCounterVisibility()) {
//			holder.txtCount.setText(navItems.get(position).getCount());
//		} else {
//			// hide the counter view
//			holder.txtCount.setVisibility(View.GONE);
//		}

		return convertView;
	}

	static class ViewHolder {
		ImageView imgIcon;
		TextView txtTitle, txtCount;
	}

}
