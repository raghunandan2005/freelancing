package in.fastrack.app.m.fup.adapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SendCallback;
import com.squareup.picasso.Picasso;

import in.fastrack.app.m.fup.AlaramActivityNew;
import in.fastrack.app.m.fup.Alarams;
import in.fastrack.app.m.fup.FeedComments;
import in.fastrack.app.m.fup.HomeActivity;
import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.alarm.CreateNewAlarmActivity;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.database.DatabaseHandler;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Notification;
import in.fastrack.app.m.fup.model.NotificationModel;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class NotificationActivityAdapter extends ArrayAdapter<NotificationModel> {

	
	private Activity mContext;
	private int layout;
	private ArrayList<NotificationModel> list;
	AppConfig _config;
	String selectedAlarmType;
	String notification_type;
	String obj_id;
	private JSONArray friends = new JSONArray();
	private JSONArray friendss = new JSONArray();
	ArrayList<String> friendsL = new ArrayList<String>(); 
	ArrayList<String> friendssL = new ArrayList<String>(); 	
	private JSONArray friendsJ = new JSONArray();
	private JSONArray friendssJ = new JSONArray();
	
	
	JSONArray list1 = new JSONArray();     
	JSONArray list2 = new JSONArray();     
	JSONArray list3 = new JSONArray();     
	JSONArray list4 = new JSONArray();     
	JSONArray list5 = new JSONArray();     
	JSONArray list6 = new JSONArray();     
	JSONArray list7 = new JSONArray();     
	JSONArray list8 = new JSONArray(); 
	

	ParseObject reference_id,reference_ids ;
	//ParseObject user_obj;
	public NotificationActivityAdapter(Activity context, int resource,
			ArrayList<NotificationModel> list) {
		super(context, resource, list);
		this.mContext = context;
		
		this.list = new ArrayList<NotificationModel>();
		this.layout = resource;
		this.list.addAll(list);
		
		
		
	}
	
	private class ViewHolder{
		LinearLayout mActionButtonsHolder,mMain;
		ImageView mUserProfile;
		TextView mUserName,mNotificationTitle;
		Button mAccept,mDecline,mClose;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		ViewHolder holder = null;
		if(convertView == null){
			LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(layout,null);
			holder = new ViewHolder();
			holder.mActionButtonsHolder = (LinearLayout) convertView.findViewById(R.id.llNotificationButtonContainer);
			holder.mUserProfile = (ImageView) convertView.findViewById(R.id.ivNotificationImage);
			holder.mUserName = (TextView) convertView.findViewById(R.id.tvNotificationUser);
			holder.mNotificationTitle = (TextView) convertView.findViewById(R.id.tvNotificationTitle);
			holder.mAccept = (Button) convertView.findViewById(R.id.bNotificationAccept);
			holder.mDecline = (Button) convertView.findViewById(R.id.bNotificationDecline);
			holder.mUserProfile = (ImageView) convertView.findViewById(R.id.ivNotificationImage);
			holder.mMain = (LinearLayout) convertView.findViewById(R.id.llRow);
			_config = new AppConfig(mContext);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		
		
		
	    NotificationModel item = list.get(position);
		holder.mUserName.setText(item.getName());
		holder.mNotificationTitle.setText(item.getMessage());
		holder.mAccept.setTag(item);
		holder.mDecline.setTag(item);
		holder.mMain.setTag(item);
		
		
		Uri tempImage = Uri.parse(item.getImagepath());
		Picasso.with(mContext).load(tempImage).fit().centerInside().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into(holder.mUserProfile);
		
		//Show accept or decline view
		if(!item.getShowButtons()){
			holder.mActionButtonsHolder.setVisibility(View.GONE);
		}else{
			holder.mActionButtonsHolder.setVisibility(View.VISIBLE);
		}
		
		//Listviewclick
		holder.mMain.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				LinearLayout lv = (LinearLayout) v;
				final NotificationModel ls = (NotificationModel) v.getTag();
				//final String notificationid = ls.getAlarmid();
				Intent intent = ((Activity) mContext).getIntent();
				Bundle extras = new Bundle();
				switch(ls.getActivityType()){
				//Normal alarm notification
				case 1:
					
					extras.putString("alarmid",ls.getAlarmid());
					extras.putBoolean("onlyone",true);
					intent.putExtras(extras);
					((Activity) mContext).finish();
				break;
				//Someone commented
				case 2:
					intent = new Intent(mContext, FeedComments.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					extras.putString("alarmid",ls.getAlarmid());
					extras.putInt("position",0);
					extras.putBoolean("onlyone",true);
					intent.putExtras(extras);
					mContext.startActivity(intent);
				break;
				//Someone liked
				case 3:
					
					extras.putString("alarmid",ls.getAlarmid());
					extras.putBoolean("onlyone",true);
					intent.putExtras(extras);
					((Activity) mContext).finish();
				break;
				//Alarm updated
				case 4:
				
				break;
				//Friend notification
				case 5:
					
					extras.putString("alarmid",ls.getAlarmid());
					extras.putBoolean("onlyone",true);
					intent.putExtras(extras);
					((Activity) mContext).finish();
				break;
				//Surprise Friend notification
				case 6:
		
					extras.putString("alarmid",ls.getAlarmid());
					extras.putBoolean("onlyone",true);
					intent.putExtras(extras);
					((Activity) mContext).finish();
				break;
				//Accept alarm notification
				case 7:
	
					extras.putString("alarmid",ls.getAlarmid());
					extras.putBoolean("onlyone",true);
					intent.putExtras(extras);
					((Activity) mContext).finish();
				break;
				//Edit  alarm notification
				case 8:
				
					extras.putString("alarmid",ls.getAlarmid());
					extras.putBoolean("onlyone",true);
					intent.putExtras(extras);
					((Activity) mContext).finish();
				break;
				//Remove  alarm notification
				case 9:
			
					extras.putString("alarmid",ls.getAlarmid());
					extras.putBoolean("onlyone",true);
					intent.putExtras(extras);
					((Activity) mContext).finish();
				break;
				//Group notification
				case 10:
				
					extras.putString("alarmid",ls.getAlarmid());
					extras.putBoolean("onlyone",true);
					intent.putExtras(extras);
					((Activity) mContext).finish();
				break;
				case 11:
			
					extras.putString("alarmid",ls.getAlarmid());
					extras.putBoolean("onlyone",true);
					intent.putExtras(extras);
					((Activity) mContext).finish();
				break;
				case 12:
					
					extras.putString("alarmid",ls.getAlarmid());
					extras.putBoolean("onlyone",true);
					intent.putExtras(extras);
					((Activity) mContext).finish();
				break;
				case 13:
					break;
				case 14:
					break;
				}
			}
		});
		
		
		
		//Cancel alarm accept
		holder.mDecline.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				if (!Util.isNetworkAvailable(mContext)) {	
					
				     DialogFactory.getBeaconStreamDialog1(mContext, mContext.getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			       } else 
			
			       {
				
				
				
				
				
				Button mAcc = (Button) v;
				final NotificationModel ls = (NotificationModel) v.getTag();
				final String notificationid = ls.getAlarmid();
				
				obj_id = ls.getObj_id();
				notification_type = ls.getNotification_type();
				Log.i("obj_id","obj_id is"+obj_id);
				Log.i("notification_type","notification_type is"+notification_type);
				ls.setShowButtons(false);
				
				Log.i("notificationid","notificationid is"+notificationid);
				
				
				if(notification_type.equals("notifyalarm"))	
				{
				
				ParseQuery<ParseObject> not = ParseQuery.getQuery("alarms");
				not.getInBackground(notificationid, new GetCallback<ParseObject>() {
					@Override
					public void done(ParseObject object, com.parse.ParseException e) {						
						object.addAllUnique("declinedpeople",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
						object.addAllUnique("notificationactions",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
						object.addAllUnique("declinedpeopleedit",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
						object.addAllUnique("noficationsedit",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
						object.saveInBackground();//saveEventually();();
					}
				});
				}
				
				else if(notification_type.equals("notifyfriend"))
				{
				ParseQuery<ParseObject> no = ParseQuery.getQuery("Friends");
				no.getInBackground(notificationid, new GetCallback<ParseObject>() {
					@Override
					public void done(ParseObject object, com.parse.ParseException e) {
							ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
									query.whereEqualTo("user", ParseUser.getCurrentUser());
									query.findInBackground(new FindCallback<ParseObject>() {
										public void done(List<ParseObject> objects,ParseException e) {
											if (objects.size() > 0) {

												// edit
												if (e == null) {
													for (ParseObject obj : objects) {
														friends = obj.getJSONArray("pendingF");
													}
													if(friends!=null)
													for(int i= 0; i < friends.length(); i++) {
														try {
															if(friends.get(i).equals(obj_id)){																																	
																//friends.remove(i);																																																
															}
															else{
																list1.put(friends.get(i));
															}
															
														} catch (Exception e1) {
															// TODO Auto-generated catch block
															e1.printStackTrace();
														}
													}																										
												//	Log.d("list1","list1????????????????"+ list1);																									
													try {
														for (ParseObject obj : objects) {
															obj.addAllUnique("declinedF",Arrays.asList(obj_id));
															obj.put("pendingF" ,list1);
															obj.saveInBackground();//saveEventually();();
														}
													} catch (Exception e1) {
														// TODO Auto-generated catch block
														e1.printStackTrace();
													}
													
												} else {
												}
											}
										}
									});

									ParseQuery<ParseUser> query2 = ParseUser.getQuery();
									query2.whereEqualTo("user", obj_id);
									query2.getInBackground(obj_id,new GetCallback<ParseUser>() {
												@Override
												public void done(ParseUser object,ParseException e) {
													ParseQuery<ParseObject> query3 = ParseQuery.getQuery("Friends");
													query3.whereEqualTo("user", object);
													query3.findInBackground(new FindCallback<ParseObject>() {
														public void done(List<ParseObject> objects,ParseException e) {
															
															if (objects.size() > 0) {
																// edit
																if (e == null) {
																	for (ParseObject obj : objects) {
																		friendss = obj.getJSONArray("requestedF");
																	}
																	if(friendss!=null)
																	for(int i= 0; i < friendss.length(); i++) {
																		try {
																			
																			if(friendss.get(i).equals(ParseUser.getCurrentUser().getObjectId().toString())){															
																				//friendss.remove(i);																																																
																			}
																			else{
																				list2.put(friendss.get(i));
																			}
																		} catch (Exception e1) {
																			// TODO Auto-generated catch block
																			e1.printStackTrace();
																		}
																	}
																//	Log.d("list2","list2????????????????"+ list2);
																	try {
																		for (ParseObject obj : objects) {
																			obj.addAllUnique("declinedF",Arrays.asList(ParseUser.getCurrentUser().getObjectId().toString()));
																			obj.put("requestedF", list2);
																			obj.saveInBackground();//saveEventually();();
																		}
																	} catch (Exception e1) {
																		// TODO Auto-generated catch block
																		e1.printStackTrace();
																	}
																	
																	}
																} else {
																}
															
															
															notifyDataSetChanged();
															}
																										
																																						
																								
													});
												}
											});

								}
				});
				}
				
				else if(notification_type.equals("notifysurprisefriend")){

					ParseQuery<ParseObject> no = ParseQuery.getQuery("Friends");
					no.getInBackground(notificationid, new GetCallback<ParseObject>() {
						@Override
						public void done(ParseObject object, com.parse.ParseException e) {
								ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
										query.whereEqualTo("user", ParseUser.getCurrentUser());
										query.findInBackground(new FindCallback<ParseObject>() {
											public void done(List<ParseObject> objects,ParseException e) {
												if (objects.size() > 0) {

													// edit
													if (e == null) {
														for (ParseObject obj : objects) {
															friends = obj.getJSONArray("pendingSF");
														}
														if(friends!=null)
														for(int i= 0; i < friends.length(); i++) {
															try {
																if(friends.get(i).equals(obj_id)){															
																	//friends.remove(i);																																																
																}
																else{
																	list3.put(friends.get(i));
																}
															} catch (Exception e1) {
																// TODO Auto-generated catch block
																e1.printStackTrace();
															}
														}
														try {
															for (ParseObject obj : objects) {
																obj.addAllUnique("declinedSF",Arrays.asList(obj_id));
																obj.put("pendingSF" ,list3);
																obj.saveInBackground();//saveEventually();();
															}
														} catch (Exception e1) {
															// TODO Auto-generated catch block
															e1.printStackTrace();
														}
														
													} else {
													}
												}
											}
										});

										ParseQuery<ParseUser> query2 = ParseUser.getQuery();
										query2.whereEqualTo("user", obj_id);
										query2.getInBackground(obj_id,new GetCallback<ParseUser>() {
													@Override
													public void done(ParseUser object,ParseException e) {
														
														
														ParseQuery<ParseObject> query3 = ParseQuery.getQuery("Friends");
														query3.whereEqualTo("user", object);
														query3.findInBackground(new FindCallback<ParseObject>() {
															public void done(List<ParseObject> objects,ParseException e) {
																
																if (objects.size() > 0) {
																	// edit
																	if (e == null) {
																		for (ParseObject obj : objects) {
																			friendss = obj.getJSONArray("requestedSF");
																		}
																		if(friendss!=null)
																		for(int i= 0; i < friendss.length(); i++) {
																			try {
																				if(friendss.get(i).equals(ParseUser.getCurrentUser().getObjectId().toString())){															
																					//friendss.remove(i);																																																
																				}
																				else{
																					list4.put(friendss.get(i));
																				}
																			} catch (Exception e1) {
																				// TODO Auto-generated catch block
																				e1.printStackTrace();
																			}
																		}
																		
																		try {
																			for (ParseObject obj : objects) {
																				obj.addAllUnique("declinedSF",Arrays.asList(ParseUser.getCurrentUser().getObjectId().toString()));
																				obj.put("requestedSF", list4);
																				obj.saveInBackground();//saveEventually();();
																			}
																		} catch (Exception e1) {
																			// TODO Auto-generated catch block
																			e1.printStackTrace();
																		}
																		
																		}
																	} else {
																	}
																
																
																notifyDataSetChanged();
																}
																											
																																							
																									
														});
													}
												});

									}
					});
					
				}
				else{
					Log.i("notification_type","u r in else decline");
				}
			       }
			}
			
		});
		
		holder.mAccept.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
				
				if (!Util.isNetworkAvailable(mContext)) {	
					
				     DialogFactory.getBeaconStreamDialog1(mContext, mContext.getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			       } else 
			
			       {
				Button mAcc = (Button) v;
				final NotificationModel ls = (NotificationModel) v.getTag();
				final String notificationid = ls.getAlarmid();
				obj_id = ls.getObj_id();
				notification_type = ls.getNotification_type();
			//	Log.i("obj_id","obj_id is"+obj_id);
			//	Log.i("notification_type","notification_type is"+notification_type);
				ls.setShowButtons(false);
				notifyDataSetChanged();
				
				if(notification_type.equals("notifyalarm"))	
				{
				
				
				
				ParseQuery<ParseObject> not = ParseQuery.getQuery("alarms");
				not.getInBackground(notificationid, new GetCallback<ParseObject>() {
					@Override
					public void done(final ParseObject object, com.parse.ParseException e) {
						object.addAllUnique("acceptedpeople",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
						object.addAllUnique("notificationactions",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
						object.addAllUnique("acceptedpeopleedit",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
						object.addAllUnique("noficationsedit",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
						
						object.saveInBackground();//saveEventually();();
						//Set alarm
						Intent intent = new Intent(mContext, AlaramActivityNew.class);
						Bundle tempData = new Bundle();
						tempData.putString("alarm_category",object.getString("category"));
						tempData.putString("alarm_title", object.getString("title"));
						tempData.putBoolean("recorded",object.getBoolean("recorded"));
						tempData.putInt("alarmrequestcode",Integer.parseInt(object.getString("requestcode")));
						tempData.putString("alarmpath",object.getString("media"));
						tempData.putString("alarmfriends",object.getString("connectedlist"));
						if (object.getString("alarm_repeat_type") != null)
						tempData.putString("alarm_repeat_type",object.getString("alarm_repeat_type"));
						
						if (object.getString("alarm_repeat_type") != null)
						selectedAlarmType = object.getString("alarm_repeat_type");
						
						
						
						
						//Alarm for other user
						Alarams al = new Alarams();
						al.setCategory(object.getString("category"));
						al.setTitle(object.getString("title"));
						if (object.getBoolean("recorded") == true) {
							al.setRecorded("true");
						} else {
							al.setRecorded("false");
						}
						al.setRequestcode(String.valueOf(object.getString("requestcode")));
						al.setPath(object.getString("media"));
						al.setFriends(object.getString("connectedlist"));
						JSONArray jArray = object.getJSONArray("connected");
						try {
							al.setFriendsid(jArray.getString(0));
						} catch (JSONException e3) {
							// TODO Auto-generated catch block
							e3.printStackTrace();
						}
						al.setRepeattype(selectedAlarmType);
						
						al.setMilli(String.valueOf(object.getLong("alarmtimemilli")));
						
						String time = object.getString("alarmTime");
						String split[] = time.split("\\s");
						String date = split[0];
						Log.i("Date after split",date);
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault());
				
						//String cale = sdf(date);
						al.setAtime(date);
						Toast.makeText(mContext,"millllllli" + object.getLong("alarmtimemilli")+date, Toast.LENGTH_LONG).show();

						DatabaseHandler db = new DatabaseHandler(mContext);
						db.updateAlarm(al);
						
						
						intent.putExtras(tempData);
						PendingIntent pi = PendingIntent.getActivity(mContext,Integer.parseInt(object.getString("requestcode")),intent,PendingIntent.FLAG_CANCEL_CURRENT);
						AlarmManager am = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
						am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
						
						Calendar calendar = Calendar.getInstance();
						calendar.setTimeInMillis(calendar.getTimeInMillis());
						calendar.add(Calendar.SECOND, 30);
						if (selectedAlarmType.equalsIgnoreCase("Once")) {
							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
							
						} else if (selectedAlarmType.equalsIgnoreCase("Daily")) {

							calendar.add(Calendar.DAY_OF_WEEK, 1);
							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
							// am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,calendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pi);
						} else if (selectedAlarmType.equalsIgnoreCase("Weekly")) {

							calendar.add(Calendar.WEEK_OF_MONTH, 1);
							am.set(AlarmManager.RTC_WAKEUP,
									object.getLong("alarmtimemilli"), pi);
						} else if (selectedAlarmType.equalsIgnoreCase("Monthly")) {

							calendar.add(Calendar.MONTH, 1);
							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
						} /*else if (selectedAlarmType.equalsIgnoreCase("Yearly")) {

							calendar.add(Calendar.YEAR, 1);
							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
						}*/
						
						
						try {
							//Earlier here
							//Notification
							//Accept alarm notification code
						/*	ParseObject notification = new ParseObject("Activity");
							notification.put("creator",ParseUser.getCurrentUser());
							notification.put("followers",Arrays.asList(object.getString("creatorplain")));
							notification.put("reference",object);
							notification.put("notificationtype",7);
							notification.put("notificationDone",false);
							notification.put("readers",new JSONArray());
							notification.add("readers", ParseUser.getCurrentUser().getObjectId());
							notification.saveInBackground();//saveEventually();();
*/                        							
                            // parse push implementation
									
									ParseQuery pushQuery = ParseInstallation.getQuery();
									ParsePush push = new ParsePush();
									//pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId().toString()); // not me
									pushQuery.whereEqualTo("myuser",object.getString("creatorplain"));
									//
									push.setQuery(pushQuery);
									 JSONObject tempAlarmData = new JSONObject();	
									
									 
									  
										if (object != null)
										tempAlarmData.put("reference",object);
										
										tempAlarmData.put("notificationtype", 7);						
										tempAlarmData.put("alert","The alarm request is  accepted by "+ParseUser.getCurrentUser().getString("fullname"));
										tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivityNew");												
										push.setData(tempAlarmData);
										push.sendInBackground(new SendCallback() {
											   public void done(com.parse.ParseException e) {
												     if (e == null) {
//												    		//Shifted here
															//Notification
															//Accept alarm notification code
															ParseObject notification = new ParseObject("Activity");
															notification.put("creator",ParseUser.getCurrentUser());
															notification.put("followers",Arrays.asList(object.getString("creatorplain")));
															notification.put("reference",object);
															notification.put("notificationtype",7);
															notification.put("notificationDone",false);
															notification.put("readers",Arrays.asList(object.getString("creatorplain")));//Changes from `new JSONArray()` to Arrays.asList(object.getString("creatorplain"))
															notification.add("readers", ParseUser.getCurrentUser().getObjectId());
															notification.saveInBackground();//saveEventually();();
												       Log.d("push", "success!");
												     } else {
												       Log.d("push", "failure");
												     }
												   }
											
												 });
							} catch (Exception e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
							
						
						
						
						//Download audio if required
						final String alarmPath = object.getString("media");
						Log.i("File path",alarmPath);
						if(object.getBoolean("recorded")){
							ParseFile p = object.getParseFile("audioclip");
							if(p!=null){
								Log.i("File","A");
								p.getDataInBackground(new GetDataCallback() {
									
									@Override
									public void done(byte[] data,
											com.parse.ParseException e) {
										if(e==null){
											Log.i("File","B");
											try {
												Log.i("File","C");
												File newFile = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),alarmPath);
												OutputStream os = new FileOutputStream(newFile);
												os.write(data);
												os.close();
											} catch (FileNotFoundException e1) {
												Log.i("ALARM FILE LOADED","FALSE");
												e1.printStackTrace();
											} catch(IOException e1){
												e1.printStackTrace();
											}
										}else{
											Log.i("File",e.getMessage());
										}
									}
								});
							}
						}
						
					}
				});
				
				
				}
				
				else if(notification_type.equals("notifyfriend"))
				{
				ParseQuery<ParseObject> no = ParseQuery.getQuery("Friends");
				no.getInBackground(notificationid, new GetCallback<ParseObject>() {
					@Override
					public void done(ParseObject object, com.parse.ParseException e) {
						ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
						query.whereEqualTo("user", ParseUser.getCurrentUser());
						query.findInBackground(new FindCallback<ParseObject>() {
							public void done(List<ParseObject> objects,ParseException e) {
								if (objects.size() > 0) {

									// edit
									if (e == null) {
										for (ParseObject obj : objects) {
											friends = obj.getJSONArray("pendingF");
										}
										if(friends!=null)
										for(int i= 0; i < friends.length(); i++) {
											try {
												if(friends.get(i).equals(obj_id)){															
													//friends.remove(i);																																																
												}
												else{
													list5.put(friends.get(i));
												}
											} catch (Exception e1) {
												// TODO Auto-generated catch block
												e1.printStackTrace();
											}
										}
										try {
											for (ParseObject obj : objects) {
												obj.addAllUnique("connectedF",Arrays.asList(obj_id));
												obj.put("pendingF" ,list5);
												obj.saveInBackground();//saveEventually();();
											}
										} catch (Exception e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
										

									
									
									
									} else {
									}
								}
							}
						});

						ParseQuery<ParseUser> query2 = ParseUser.getQuery();
						query2.whereEqualTo("user", obj_id);
						query2.getInBackground(obj_id,new GetCallback<ParseUser>() {
									@Override
									public void done(ParseUser object,ParseException e) {
										
										
										ParseQuery<ParseObject> query3 = ParseQuery.getQuery("Friends");
										query3.whereEqualTo("user", object);
										query3.findInBackground(new FindCallback<ParseObject>() {
											public void done(List<ParseObject> objects,ParseException e) {
												
												if (objects.size() > 0) {
													// edit
													if (e == null) {
														for (ParseObject obj : objects) {
															friendss = obj.getJSONArray("requestedF");
															reference_id = obj;
														}
														if(friendss!=null)														
															for(int i= 0; i < friendss.length(); i++) {
															try {
																if(friendss.get(i).equals(ParseUser.getCurrentUser().getObjectId().toString())){															
																	//friendss.remove(i);																																																
																}
																else{
																	list6.put(friendss.get(i));
																}
															} catch (Exception e1) {
																// TODO Auto-generated catch block
																e1.printStackTrace();
															}
														}
														try {
															for (ParseObject obj : objects) {
																obj.addAllUnique("connectedF",Arrays.asList(ParseUser.getCurrentUser().getObjectId().toString()));
																obj.put("requestedF", list6);
																obj.saveInBackground();//saveEventually();();
															}
														} catch (Exception e1) {
															// TODO Auto-generated catch block
															e1.printStackTrace();
														}
														
														}
													} else {
													}
												
												try {
													
													//Earlier here
													//Notification
													//Accept Friend notification code
													/*ParseObject notification = new ParseObject("Activity");
													notification.put("creator",ParseUser.getCurrentUser());
													notification.put("followers",Arrays.asList(obj_id));
													//if(reference_id !=null)
													notification.put("reference3",reference_id);
													notification.put("notificationtype",11);
													notification.put("notificationDone",false);
													notification.put("readers",new JSONArray());
													notification.add("readers", ParseUser.getCurrentUser().getObjectId());
													notification.saveInBackground();//saveEventually();();
*/													
													
													
													ParseQuery pushQuery = ParseInstallation.getQuery();
													ParsePush push = new ParsePush();
													//pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId().toString()); // not me
													pushQuery.whereEqualTo("myuser",obj_id);
													//
													push.setQuery(pushQuery);
													 JSONObject tempAlarmData = new JSONObject();	
													
													 
													  
														if (reference_id != null)
														tempAlarmData.put("reference3",reference_id);
														
														tempAlarmData.put("notificationtype",11);						
														tempAlarmData.put("alert","The friend request is  accepted by "+ParseUser.getCurrentUser().getString("fullname"));
														tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivityNew");												
														push.setData(tempAlarmData);
														push.sendInBackground(new SendCallback() {
															   public void done(com.parse.ParseException e) {
																     if (e == null) {
//																    	    //Shifted to here
																			//Notification
																			//Accept Friend notification code
																			ParseObject notification = new ParseObject("Activity");
																			notification.put("creator",ParseUser.getCurrentUser());
																			notification.put("followers",Arrays.asList(obj_id));
																			//if(reference_id !=null)
																			notification.put("reference3",reference_id);
																			notification.put("notificationtype",11);
																			notification.put("notificationDone",false);
																			notification.put("readers",Arrays.asList(obj_id));//Changed from `new JSONArray()` to Arrays.asList(obj_id)
																			notification.add("readers", ParseUser.getCurrentUser().getObjectId());
																			notification.saveInBackground();//saveEventually();();
																       Log.d("push", "success!");
																     } else {
																       Log.d("push", "failure");
																     }
																   }
															
																 });
													
													
													
												} catch (Exception e1) {
													// TODO Auto-generated catch block
													e1.printStackTrace();
												}
												
										    	notifyDataSetChanged();
												}
																							
																																			
																					
										});
									}
								});
						
						
					

					}
				});
				}
				

				else if(notification_type.equals("notifysurprisefriend")){
					
					
					

					ParseQuery<ParseObject> no = ParseQuery.getQuery("Friends");
					no.getInBackground(notificationid, new GetCallback<ParseObject>() {
						@Override
						public void done(ParseObject object, com.parse.ParseException e) {
							ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
							query.whereEqualTo("user", ParseUser.getCurrentUser());
							query.findInBackground(new FindCallback<ParseObject>() {
								public void done(List<ParseObject> objects,ParseException e) {
									if (objects.size() > 0) {

										// edit
										if (e == null) {
											for (ParseObject obj : objects) {
												friends = obj.getJSONArray("pendingSF");
												//reference_ids = obj;//friends
											}
											if(friends!=null)
											for(int i= 0; i < friends.length(); i++) {
												try {
													if(friends.get(i).equals(obj_id)){															
														//friends.remove(i);																																																
													}
													else{
														list7.put(friends.get(i));
													}
												} catch (Exception e1) {
													// TODO Auto-generated catch block
													e1.printStackTrace();
												}
											}
											try {
												for (ParseObject obj : objects) {
													obj.addAllUnique("connectedSF",Arrays.asList(obj_id));
													obj.put("pendingSF" ,list7);
													obj.saveInBackground();//saveEventually();();
														}
											} catch (Exception e1) {
												// TODO Auto-generated catch block
												e1.printStackTrace();
											}

												  } else {
												}

											}
										}
									});

							ParseQuery<ParseUser> query2 = ParseUser.getQuery();
							query2.whereEqualTo("user", obj_id);
							query2.getInBackground(obj_id,new GetCallback<ParseUser>() {
										@Override
										public void done(ParseUser object,ParseException e) {
											
											
											ParseQuery<ParseObject> query3 = ParseQuery.getQuery("Friends");
											query3.whereEqualTo("user", object);
											query3.findInBackground(new FindCallback<ParseObject>() {
												public void done(List<ParseObject> objects,ParseException e) {
													
													if (objects.size() > 0) {
														// edit
														if (e == null) {
															for (ParseObject obj : objects) {
																friendss = obj.getJSONArray("requestedSF");
																reference_ids = obj;//creator
															}
															if(friendss!=null)														
																for(int i= 0; i < friendss.length(); i++) {
																try {
																	if(friendss.get(i).equals(ParseUser.getCurrentUser().getObjectId().toString())){	
																		
																		//friendss.remove(i);																																																
																	}
																	else{
																		list8.put(friendss.get(i));
																	}
																} catch (Exception e1) {
																	// TODO Auto-generated catch block
																	e1.printStackTrace();
																}
															}
															try {
																for (ParseObject obj : objects) {
																	obj.addAllUnique("connectedSF",Arrays.asList(ParseUser.getCurrentUser().getObjectId().toString()));
																	obj.put("requestedSF", list8);
																	obj.saveInBackground();//saveEventually();();
																}
															} catch (Exception e1) {
																// TODO Auto-generated catch block
																e1.printStackTrace();
															}
															
															}
														} else {
														}

													
													
													try {
														//Earlier here
														//Notification
														//Accept Surprise Friend notification code
														/*ParseObject notification = new ParseObject("Activity");
														notification.put("creator",ParseUser.getCurrentUser());
														notification.put("followers",Arrays.asList(obj_id));
														if(reference_ids !=null)
														notification.put("reference4",reference_ids);
														notification.put("notificationtype",12);
														notification.put("notificationDone",false);
														notification.put("readers",new JSONArray());
														notification.add("readers", ParseUser.getCurrentUser().getObjectId());
														notification.saveInBackground();//saveEventually();();
*/
														ParseQuery pushQuery = ParseInstallation.getQuery();
														ParsePush push = new ParsePush();
														//pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId().toString()); // not me
														pushQuery.whereEqualTo("myuser",obj_id);
														//
														push.setQuery(pushQuery);
														 JSONObject tempAlarmData = new JSONObject();	
														
														 
														  
															if (reference_ids != null)
															tempAlarmData.put("reference4",reference_ids);
															
															tempAlarmData.put("notificationtype",12);						
															tempAlarmData.put("alert","The surprise friend request is  accepted by "+ParseUser.getCurrentUser().getString("fullname"));
															tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivityNew");												
															push.setData(tempAlarmData);
															push.sendInBackground(new SendCallback() {
																   public void done(com.parse.ParseException e) {
																	     if (e == null) {
//																	    	//Notification
																				//Accept Surprise Friend notification code
																				ParseObject notification = new ParseObject("Activity");
																				notification.put("creator",ParseUser.getCurrentUser());
																				notification.put("followers",Arrays.asList(obj_id));
																				if(reference_ids !=null)
																				notification.put("reference4",reference_ids);
																				notification.put("notificationtype",12);
																				notification.put("notificationDone",false);
																				notification.put("readers",Arrays.asList(obj_id));//Changes from `new JSONArray()` to Arrays.asList(obj_id)
																				notification.add("readers", ParseUser.getCurrentUser().getObjectId());
																				notification.saveInBackground();//saveEventually();();
																	       Log.d("push", "success!");
																	     } else {
																	       Log.d("push", "failure");
																	     }
																	   }
																
																	 });
													} catch (Exception e1) {
														// TODO Auto-generated catch block
														e1.printStackTrace();
													}
													
													
													
													
													notifyDataSetChanged();
													}
																								
																																				
																						
											});
										}
									});

						}
					});
					
				}
				else{
					Log.i("notification_type","u r in else accept");
				}
			       }
			}
		});
		
		
		
		return convertView;
	}
	
}
