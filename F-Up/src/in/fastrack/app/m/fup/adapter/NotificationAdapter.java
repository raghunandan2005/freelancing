package in.fastrack.app.m.fup.adapter;

import in.fastrack.app.m.fup.AlaramActivityNew;
import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.model.Notification;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class NotificationAdapter extends ArrayAdapter<Notification> {
	
	private Context context;
	private int layout;
	private ArrayList<Notification> list;
	AppConfig _config;
	
	

	public NotificationAdapter(Context context, int resource,
			ArrayList<Notification> list) {
		super(context, resource, list);
		this.context = context;
		this.list = new ArrayList<Notification>();
		this.layout = resource;
		this.list.addAll(list);
	}
	
	private class ViewHolder{
		LinearLayout mActionButtonsHolder;
		ImageView mUserProfile;
		TextView mUserName,mNotificationTitle;
		Button mAccept,mDecline,mClose;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		ViewHolder holder = null;
		if(convertView == null){
			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(layout,null);
			holder = new ViewHolder();
			holder.mActionButtonsHolder = (LinearLayout) convertView.findViewById(R.id.llNotificationButtonContainer);
			holder.mUserProfile = (ImageView) convertView.findViewById(R.id.ivNotificationImage);
			holder.mUserName = (TextView) convertView.findViewById(R.id.tvNotificationUser);
			holder.mNotificationTitle = (TextView) convertView.findViewById(R.id.tvNotificationTitle);
			
			holder.mAccept = (Button) convertView.findViewById(R.id.bNotificationAccept);
			
			holder.mDecline = (Button) convertView.findViewById(R.id.bNotificationDecline);
			
			
			holder.mUserProfile = (ImageView) convertView.findViewById(R.id.ivNotificationImage);
			
			_config = new AppConfig(context);
			
			convertView.setTag(holder);
			
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		
		final Notification item = list.get(position);
		holder.mUserName.setText(item.getNotificationUser());
		String title = "";
		if((item.getNotififcationActionName().equals("alarm"))){
			Log.i("BUTTONS","TRUE");
			if(item.getNotificationSHowButtons().equals("pending")){
				holder.mActionButtonsHolder.setVisibility(View.VISIBLE);
			}else{
				holder.mActionButtonsHolder.setVisibility(View.GONE);
			}
			title = "Invited for ";
			title += item.getNotificationTitle()+"\n on "+item.getNotificationDate();
		}else if(item.getNotififcationActionName() == "like"){
			title = "Liked "+item.getNotificationTitle();
			holder.mActionButtonsHolder.setVisibility(View.GONE);
		}else if(item.getNotififcationActionName() == "comment"){
			title = "Commented on "+item.getNotificationTitle();
			holder.mActionButtonsHolder.setVisibility(View.GONE);
		}
		
		holder.mAccept.setTag(item);
		holder.mDecline.setTag(item);
		
		holder.mNotificationTitle.setText(title);
		
		Uri tempImage = Uri.parse(item.getNotificationUserImage());
		Picasso.with(context).load(tempImage).fit().centerInside().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into(holder.mUserProfile);
		
		holder.mDecline.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Button mAcc = (Button) v;
				final Notification ls = (Notification) v.getTag();
				final String notificationid = ls.getNotificationId();
				ls.setNotificationSHowButtons("declined");
				notifyDataSetChanged();
				ParseQuery<ParseObject> not = ParseQuery.getQuery("Notifications");
				not.getInBackground(notificationid, new GetCallback<ParseObject>() {
					
					@Override
					public void done(ParseObject object, com.parse.ParseException e) {
						object.put("alarmaccepted","declined");
						object.saveInBackground();
						
					}
				});
			}
		});
		
		holder.mAccept.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Button mAcc = (Button) v;
				final Notification ls = (Notification) v.getTag();
				String alarmresponseid = ls.getNotificationResourceId();
				final String notificationid = ls.getNotificationId();
				ls.setNotificationSHowButtons("accepted");
				notifyDataSetChanged();
				//v.mActionButtonsHolder.setVisibility(View.GONE);
				Log.i("ALRAM RESPONSE ID",alarmresponseid);
				ParseQuery<ParseObject> query = ParseQuery.getQuery("AlarmStore");
				query.whereEqualTo("alarmrequestid",Integer.parseInt(alarmresponseid));
				query.findInBackground(new FindCallback<ParseObject>() {
					
					@Override
					public void done(List<ParseObject> objects, com.parse.ParseException e) {
						if(e == null){
							ParseQuery<ParseObject> not = ParseQuery.getQuery("Notifications");
							//not.whereEqualTo("objectId",notificationid);
							not.getInBackground(notificationid, new GetCallback<ParseObject>() {
								
								@Override
								public void done(ParseObject object, com.parse.ParseException e) {
									object.put("alarmaccepted","accepted");
									object.saveInBackground();
									
								}
							});
							for(int i = 0; i< objects.size();i++){
								Bundle bundle = new Bundle();
								bundle.putString("alarm_title",objects.get(i).getString("alarmtitle"));
								bundle.putString("alarm_category",objects.get(i).getString("alarmcategory"));
								bundle.putBoolean("recorded",objects.get(i).getBoolean("recorded"));
								bundle.putString("alarmpath",objects.get(i).getString("alarmpath"));
								bundle.putString("alarmfriends",objects.get(i).getString("friends"));
								//bundle.putLong("alarmid",d.getId());
								final String alarmPath = objects.get(i).getString("alarmpath");
								if(objects.get(i).getBoolean("recorded")){
									//Find audio and download silently
									ParseFile p = objects.get(i).getParseFile("audioclip");
									if(p!=null){
										p.getDataInBackground(new GetDataCallback() {
											
											@Override
											public void done(byte[] data,
													com.parse.ParseException e) {
												if(e==null){
													try {
														File newFile = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),alarmPath);
														OutputStream os = new FileOutputStream(newFile);
														os.write(data);
														os.close();
													} catch (FileNotFoundException e1) {
														Log.i("ALARM FILE LOADED","FALSE");
														e1.printStackTrace();
													} catch(IOException e1){
														e1.printStackTrace();
													}
												}
											}
										});
									}
								}
								bundle.putInt("alarmrequestcode",objects.get(i).getInt("alarmrequestid"));
								Intent intent = new Intent(context, AlaramActivityNew.class);
								intent.putExtras(bundle);
								PendingIntent pi = PendingIntent.getActivity(context,objects.get(i).getInt("alarmrequestid"),intent,PendingIntent.FLAG_CANCEL_CURRENT);
								AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
								am.set(AlarmManager.RTC_WAKEUP,objects.get(i).getLong("longtime"), pi);
								Log.i("ALARM","ON");
							}
						}
					}
				});
			}
		});
		
		convertView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(item.getNotififcationActionName() != "alarm"){
					Toast.makeText(context,"Like or Comment Notification",Toast.LENGTH_LONG).show();
				}
			}
		});
		
		
		
		return convertView;
	}
	
	private String dateFormat(String time){
		String dt = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss",Locale.getDefault());
		Date mDate;
		try {
			mDate = sdf.parse(time);
			long timeinMilli = mDate.getTime();
			dt = (String) DateUtils.getRelativeTimeSpanString(timeinMilli,System.currentTimeMillis(),0);;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dt;
	}
	
	
}
