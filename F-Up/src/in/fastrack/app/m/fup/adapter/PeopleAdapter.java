package in.fastrack.app.m.fup.adapter;


import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.model.Friend;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class PeopleAdapter extends BaseAdapter implements Filterable {

	private Context context;
	private int resource;
	private ArrayList<Friend> list, origin;
	private PeopleListener mListen;
	private JSONArray friends;
	private boolean from_friends;

	public PeopleAdapter(Context context, int resource, ArrayList<Friend> list,
			PeopleListener mListen, JSONArray friends) {

		this.context = context;
		this.resource = resource;
		this.list = list;
		this.origin = list;
		// this.list.addAll(list);
		// this.origin.addAll(list);
		this.mListen = mListen;
		this.friends = friends;
		System.out.println(friends.toString());
	}

	private class ViewHolder {
		TextView mName;
		ImageView mUserImage;
		Button mAction, mSupriseFriend;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		Friend rowItem = getItem(position);
		LayoutInflater vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = vi.inflate(resource, null);
			holder = new ViewHolder();
			holder.mName = (TextView) convertView
					.findViewById(R.id.tvPhoneName);
			holder.mUserImage = (ImageView) convertView
					.findViewById(R.id.ivUserImage);
			holder.mAction = (Button) convertView
					.findViewById(R.id.bInviteUser);
			// holder.mSupriseFriend = (Button)
			// convertView.findViewById(R.id.bSupriseFriend);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.mName.setText(rowItem.getName());
		holder.mAction.setTag(rowItem);
		// holder.mSupriseFriend.setTag(rowItem);
		if (rowItem.getUserImage() != null) {
			Picasso.with(context).load(rowItem.getUserImage()).fit()
					.centerCrop().transform(new RoundedTransform())
					.placeholder(R.drawable.friend_nopic)
					.error(R.drawable.friend_nopic).into(holder.mUserImage);
		} else {
			holder.mUserImage.setImageResource(R.drawable.friend_nopic);
		}

		/*
		 * if(!rowItem.getIsInvite()){ //Add friend
		 * holder.mAction.setText("Add"); }else{ //Invite Friend
		 * holder.mAction.setText("Invite"); }
		 */

		holder.mAction.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Button b = (Button) v;
				Friend p = (Friend) v.getTag();
				Log.d("MyApp", "Selected: " + p.getUserSelected().toString());
				if (!p.getUserSelected()) {
					// Not selected so select them
					p.setUserSelected(true);
					b.setBackgroundColor(context.getResources().getColor(
							R.color.gray_button));
					b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick,
							0, 0, 0);
				} else {
					// Already selected, so unselect
					p.setUserSelected(false);
					b.setBackgroundResource(R.drawable.button);
					b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus,
							0, 0, 0);
				}
				showSelected();
			}
		});

		if (!rowItem.getUserSelected()) {
			holder.mAction.setBackgroundResource(R.drawable.button);
			holder.mAction.setCompoundDrawablesWithIntrinsicBounds(
					R.drawable.plus, 0, 0, 0);
		} else {
			holder.mAction.setBackgroundColor(context.getResources().getColor(
					R.color.gray_button));
			holder.mAction.setCompoundDrawablesWithIntrinsicBounds(
					R.drawable.tick, 0, 0, 0);
		}

		return convertView;
	}

	public int getCount() {
		return list.size();
	}

	public ArrayList<Friend> search(String query) {
		query = query.toLowerCase();
		Log.i("Search", query);
		list.clear();
		if (query.length() == 0) {
			list.addAll(origin);
		} else {
			for (Friend mList : origin) {
				Log.i("Search res", mList.getName().toLowerCase());
				if (mList.getName().toLowerCase().contains(query)) {
					Log.i("Search state", "Found");
					list.add(mList);
				} else {
					Log.i("Search state", "Not Found");
				}
			}
			notifyDataSetChanged();
		}
		return list;
	}

	public static interface PeopleListener {
		public void getPeopleAdded(JSONArray arr, JSONArray friendsId);
	}

	public void showSelected() {
		JSONArray myArray = new JSONArray();
		JSONArray myFriendsId = new JSONArray();
		ArrayList<Friend> myList = new ArrayList<Friend>();
		myList = list;
		for (Friend s : myList) {
			if (s.getUserSelected()) {
				try {
					JSONObject object = new JSONObject();
					object.put("userid", s.getUserId());
					object.put("name", s.getName());
					object.put("source", s.getSource());
					object.put("userImage", s.getUserImage());
					myArray.put(object);
					myFriendsId.put(s.getUserId());
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		Log.d("MyApp", "Data: " + myList.toString() + myFriendsId.toString());

		mListen.getPeopleAdded(myArray, myFriendsId);
	}

	@Override
	public Friend getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public Filter getFilter() {
		return new Filter() {
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				list = (ArrayList<Friend>) results.values;

				notifyDataSetInvalidated();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				if (TextUtils.isDigitsOnly(constraint)) {
					ArrayList<Friend> list = new ArrayList<Friend>(origin);
					result.values = list;
					result.count = list.size();
				} else {
					ArrayList<Friend> newValues = new ArrayList<Friend>();
					for (int i = 0; i < origin.size(); i++) {
						String item = origin.get(i).getName().toLowerCase();
						if (item.startsWith(constraint.toString())) {
							newValues.add(origin.get(i));
						}
					}
					if (newValues.size() > 0) {
						result.values = newValues;
						result.count = newValues.size();
					} else {
						result.values = origin;
						result.count = origin.size();
					}
				}

				return result;
			}
		};
	}

	public void setData() {
		// list.clear();
		list = origin;
	}
}
