package in.fastrack.app.m.fup.adapter;

import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.model.PhoneBook;
import in.fastrack.app.m.fup.model.PhoneBook;

import java.util.ArrayList;
import java.util.Locale;

import android.text.TextUtils;
import android.content.Context;
import android.net.Uri;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.Facebook;
import com.squareup.picasso.Picasso;

public class PhoneBookAdapter extends BaseAdapter implements Filterable {

	private Context context;
	private ArrayList<PhoneBook> list;
	private ArrayList<PhoneBook> origin;
	int layoutResourceId;

	@Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
			@Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
            	
            	list = (ArrayList<PhoneBook>) results.values;
                notifyDataSetInvalidated();
            }



           @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                if(TextUtils.isEmpty(constraint)) {
                    ArrayList<PhoneBook> list = new ArrayList<PhoneBook>(origin);
                    result.values = list;
                    result.count = list.size();
                } else {
                    ArrayList<PhoneBook> newValues = new ArrayList<PhoneBook>();
                    for(int i = 0; i < origin.size(); i++) {
                   
                        String team1 = origin.get(i).getName().toLowerCase(Locale.ENGLISH);
                        if(team1.startsWith(constraint.toString().toLowerCase(Locale.ENGLISH))) {
                            newValues.add(origin.get(i));

                        }
                      
                    }
                    if(newValues.size()>0)
                    {
                    result.values = newValues;
                    result.count = newValues.size();
                    }else
                    {
                    	result.values = origin;
                        result.count = origin.size();
                    }
                }  

                return result;
            }
        };
    }

    public void setData() { 
    	   list.clear();
           list = origin;
       }
	 
	public PhoneBookAdapter(Context context, int layoutResourceId,
			ArrayList<PhoneBook> list) {
	
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.list = list;
		this.origin = list;
	
	}

	private class ViewHolder {
		TextView mPhoneName, mPhoneNumber;
		Button mInvite;
		ImageView mUserImage;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(layoutResourceId, null);

			// setup ui fields
			holder = new ViewHolder();
			holder.mPhoneName = (TextView) convertView.findViewById(R.id.tvPhoneName);
			holder.mPhoneNumber = (TextView) convertView.findViewById(R.id.tvPhoneNumber);
			holder.mUserImage = (ImageView) convertView.findViewById(R.id.ivUserImage);
			holder.mInvite = (Button) convertView.findViewById(R.id.bInviteUser);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		// Handle all clicks
		holder.mInvite.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Button tButton = (Button) v;
				PhoneBook list = (PhoneBook) v.getTag();
				boolean alreadyInvited = list.getIsSelected();
				Log.d("alreadyInvited", String.valueOf(alreadyInvited));
				Log.d("list", list.getName());
				Log.d("list", list.getPhoneNumber());
				if (list.getPhoneNumber() != null
						&& list.getPhoneNumber() != "") {
					if (!alreadyInvited) {
						// User has not been invited. So give him permission to
						// invite
						tButton.setText(context.getString(R.string.invited));
						tButton.setBackgroundColor(context.getResources()
								.getColor(R.color.gray_button));
						tButton.setCompoundDrawablesWithIntrinsicBounds(
								R.drawable.tick, 0, 0, 0);
						list.setIsSelected(true);
						sendMessage(list.getPhoneNumber(), list.getName());
					}
				}
			}

			private void sendMessage(String phonenumber, String name) {
				SmsManager smsManager = SmsManager.getDefault();
				Log.d("Message Trigerred", phonenumber);
				if (phonenumber != "" && phonenumber != null) {
					String message = "Hi Check out the F-Up APP from the Play Store. http://fastrack.in/";
					smsManager.sendTextMessage(phonenumber, null, message,
							null, null);
					Toast.makeText(context, "Invite Sent", Toast.LENGTH_SHORT)
							.show();
				} else {
					Log.i("Message Not Triggered",
							"No Phone Number For the Contact");
					Toast.makeText(
							context,
							"There is no Phone Number to send Invite to this contact",
							Toast.LENGTH_SHORT).show();
				}

			}
		});

		PhoneBook items = getItem(position);
		holder.mPhoneName.setText(items.getName());
		holder.mPhoneNumber.setText(items.getPhoneNumber());
		holder.mInvite.setTag(items);
		if (items.getIsSelected()) {
			// Invited
			holder.mInvite.setText(context.getString(R.string.invited));
			holder.mInvite.setBackgroundColor(context.getResources().getColor(
					R.color.gray_button));
			holder.mInvite.setCompoundDrawablesWithIntrinsicBounds(
					R.drawable.tick, 0, 0, 0);
		} else {
			// Not Invited
			holder.mInvite.setText(context.getString(R.string.invite));
			holder.mInvite.setBackgroundResource(R.drawable.button);
			holder.mInvite.setCompoundDrawablesWithIntrinsicBounds(
					R.drawable.plus, 0, 0, 0);
		}

		if (items.getUserImage() != null) {
			if (items.getUserImage().startsWith("http://")) {
				String tempImage = items.getUserImage();
				Picasso.with(context).load(tempImage).fit().centerCrop()
						.transform(new RoundedTransform())
						.placeholder(R.drawable.friend_nopic)
						.error(R.drawable.friend_nopic).into(holder.mUserImage);
			} else {
				Uri tempImage = Uri.parse(items.getUserImage());
				Picasso.with(context).load(tempImage).fit().centerCrop()
						.transform(new RoundedTransform())
						.placeholder(R.drawable.friend_nopic)
						.error(R.drawable.friend_nopic).into(holder.mUserImage);
			}

		} else {
			holder.mUserImage.setImageResource(R.drawable.friend_nopic);
		}
		return convertView;
	}

	public int getCount() {
		return list.size();
	}

	@Override
	public PhoneBook getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	/*// Filter method
	public void afilter(String filterText) {
		Log.i("FILTER", filterText);
		filterText = filterText.toLowerCase();
		list.clear();
		if (filterText.length() == 0) {
			list.addAll(origin);
		} else {
			Log.i("FILTER", "YESYYY");
			for (PhoneBook mlist : origin) {
				Log.i("FILTER", mlist.getName());
				Log.i("FILTER IP", filterText);
				if (mlist.getName().toLowerCase().contains(filterText)) {
					list.add(mlist);
				}
			}
		}
		notifyDataSetChanged();
	}*/

}
