package in.fastrack.app.m.fup.adapter;

import java.util.ArrayList;

import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.lib.CustomAudio;
import in.fastrack.app.m.fup.model.Playlist;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PlaylistAdapter extends ArrayAdapter<Playlist> {
	
	private Context context;
	private ArrayList<Playlist> list;
	int resourceId;
	private PlaylistListener mListen;
	private PlaylistAudio mAudio;
	//private CustomAudio mAudio = new CustomAudio();
	
	public PlaylistAdapter(Context context,int resourceId,ArrayList<Playlist> items,PlaylistListener mListen,PlaylistAudio mAudio){
		super(context, resourceId, items);
		this.resourceId = resourceId;
		this.context = context;
		this.mListen = mListen;
		this.mAudio = mAudio;
		this.list = items;
	}
	
	private class ViewHolder{
		public TextView mTrackName;
		public ImageView mPreview;
	}
	
	@Override
	public View getView(int position,View convertView,ViewGroup parent){
		
		ViewHolder holder = null;
		Playlist rowItem = getItem(position);
		LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if(convertView == null){
			convertView = mInflater.inflate(resourceId,null);
			holder = new ViewHolder();
			holder.mTrackName = (TextView) convertView.findViewById(R.id.tvName);
			holder.mPreview = (ImageView) convertView.findViewById(R.id.bPlayButton);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder)convertView.getTag();
		}
		
		//All other stuff
		holder.mTrackName.setText(rowItem.getTrackName());
		holder.mPreview.setTag(rowItem);
		holder.mTrackName.setTag(rowItem);
		
		if(rowItem.isPlaying()){
			holder.mPreview.setBackgroundResource(R.drawable.play_active_icon);
		}else{
			holder.mPreview.setBackgroundResource(R.drawable.play_icon);
		}
		
		holder.mTrackName.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				TextView tv = (TextView) v;
				Playlist list = (Playlist) v.getTag();
				mListen.onTrackSelected(list.getTrackName(),list.getTrack());
			}
		});
		
		//Preview Track
		holder.mPreview.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				ImageView iv = (ImageView) v;
				Playlist lists = (Playlist) v.getTag();
				//int pos = lists.
				if(lists.isPlaying()){
					//Audio is on so turn off
					resetButton(false);
					lists.setPlaying(false);
					v.setBackgroundResource(R.drawable.play_icon);
					//mAudio.stopMusic();
					mAudio.onPlaying(null,0, true,false,false);
				}else{
					resetButton(false);
					lists.setPlaying(true);
					v.setBackgroundResource(R.drawable.play_active_icon);
					int trackId = context.getResources().getIdentifier(lists.getTrack(),"raw",context.getPackageName());
					//mAudio.playMedia(getContext(),null,trackId,true,false);
					mAudio.onPlaying(null,trackId, true,false,true);
					
				}
			}
		});
		
		return convertView;
	}
	
	public void resetButton(boolean state){
		ArrayList<Playlist> n = new ArrayList<Playlist>();
		n = list;
		for(Playlist nf:n){
			nf.setPlaying(state);
		}
		notifyDataSetChanged();
	}
	
	
	public static interface PlaylistListener{
		public void onTrackSelected(String trackName,String track);
	}
	
	public static interface PlaylistAudio{
		public void onPlaying(String trackName,int trackId,boolean builtIn,boolean loop,boolean play);
	}
	
}
