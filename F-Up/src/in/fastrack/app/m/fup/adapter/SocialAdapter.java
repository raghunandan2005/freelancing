package in.fastrack.app.m.fup.adapter;




import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.model.SocialList;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.squareup.picasso.Picasso;

public class SocialAdapter extends ArrayAdapter<SocialList> {
	private Context context;
	private ArrayList<SocialList> list;
	private ArrayList<SocialList> origin;
	int layoutResourceId;
	
	public SocialAdapter(Context context, int layoutResourceId, ArrayList<SocialList> list) {
		super(context, layoutResourceId, list);
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		this.list = new ArrayList<SocialList>();
		this.origin = new ArrayList<SocialList>();
		this.list.addAll(list);
		this.origin.addAll(list);
	}
	
	private class ViewHolder{
		TextView mName;
		ImageView mUserImage;
		Button mInvite;
	}
	
	public JSONArray showAllSelected(){
		JSONArray arr = new JSONArray();
		JSONObject object = new JSONObject();
		ArrayList<SocialList> myList = new ArrayList<SocialList>();
		myList = list;
		for(SocialList s: myList){
			if(s.getSelected()){
				try {
					object.put("userid",s.getUserId());
					object.put("name",s.getName());
					arr.put(object);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return arr;
	}
	
	public ArrayList<String> getArray(){
		ArrayList<String> data = new ArrayList<String>();
		ArrayList<SocialList> myList = new ArrayList<SocialList>();
		myList = list;
		for(SocialList s: myList){
			if(s.getSelected()){
				data.add(s.getUserId());
			}
		}
		return data;
	}
	
	public ArrayList<String> getObjects(){
		//ParseObject data = null;
		ArrayList<String> ds = new ArrayList<String>();
		//ArrayList<ParseObject> data = new ArrayList<ParseObject>;
		ArrayList<SocialList> myList = new ArrayList<SocialList>();
		myList = list;
		for(SocialList s: myList){
			ds.add(s.getUserobject());
			//data.add("connected",s.getUserobject());
		}
		Log.i("FRIENDS OBJ",ds.toString());
		return ds;
	}
	
	@Override
	public View getView(int position,View convertView,ViewGroup parent){
		ViewHolder holder = null;
		if(convertView == null){
			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(layoutResourceId,null);
			holder = new ViewHolder();
			holder.mName = (TextView) convertView.findViewById(R.id.tvPhoneName);
			holder.mUserImage = (ImageView) convertView.findViewById(R.id.ivUserImage);
			holder.mInvite = (Button) convertView.findViewById(R.id.bInviteUser);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		SocialList items = list.get(position);
		holder.mName.setText(items.getName());
		holder.mInvite.setTag(items);
		
		holder.mInvite.setOnClickListener(new View.OnClickListener() {
			
			private void sendRequestDialog(final String userId) {
		         Bundle params = new Bundle();       
		         params.putString("title", "App Invite");
		         params.putString("message", "Hi, Check the F-Up App in the Play Store. http://fastrack.in/ " +
		                 "Download now on your ANDROID device!");
		         // comma seperated list of facebook IDs to preset the recipients. If left out, it will show a Friend Picker.
		         params.putString("to",  userId);  // your friend id

		         WebDialog requestsDialog = ( new WebDialog.RequestsDialogBuilder(context,
		                 Session.getActiveSession(), params)).setOnCompleteListener(new OnCompleteListener() {
		            @Override
		            public void onComplete(Bundle values, FacebookException error) {
		                //   Auto-generated method stub                     
		                if (error != null) {
		                    if (error instanceof FacebookOperationCanceledException) {
		                        Toast.makeText(context, 
		                                "Request cancelled", Toast.LENGTH_SHORT).show();
		                    } else {
		                        Toast.makeText(context, 
		                                "Network Error",  Toast.LENGTH_SHORT).show();
		                    }
		                } else {
		                    final String requestId = values.getString("request");
		                    if (requestId != null) {
		                        Toast.makeText(context, 
		                                "Request sent",  Toast.LENGTH_SHORT).show();
		                        Log.i("TAG", " onComplete req dia ");                                   
		                    } else {
		                        Toast.makeText(context, 
		                                "Request cancelled", Toast.LENGTH_SHORT).show();
		                    }
		                }                   
		            }
		         }).build();
		         requestsDialog.show();
		     }
			
			private void sendRequestDialog() {
			    Bundle params = new Bundle();
			    params.putString("title", "App Invite");
			    params.putString("message", "Hi, Check the F-Up App in the Play Store. http://fastrack.in/ " +
		                 "Download now on your ANDROID device!");

			    WebDialog requestsDialog = (
			        new WebDialog.RequestsDialogBuilder(context,
			            Session.getActiveSession(),
			            params))
			            .setOnCompleteListener(new OnCompleteListener() {

			                @Override
			                public void onComplete(Bundle values,
			                    FacebookException error) {
			                    if (error != null) {
			                        if (error instanceof FacebookOperationCanceledException) {
			                            Toast.makeText(context, 
			                                "Request cancelled", 
			                                Toast.LENGTH_SHORT).show();
			                        } else {
			                            Toast.makeText(context, 
			                                "Network Error", 
			                                Toast.LENGTH_SHORT).show();
			                        }
			                    } else {
			                        final String requestId = values.getString("request");
			                        if (requestId != null) {
			                            Toast.makeText(context, 
			                                "Request sent",  
			                                Toast.LENGTH_SHORT).show();
			                        } else {
			                            Toast.makeText(context, 
			                                "Request cancelled", 
			                                Toast.LENGTH_SHORT).show();
			                        }
			                    }   
			                }

			            })
			            .build();
			    requestsDialog.show();
			}
			
			@Override
			public void onClick(View v) {
				Button tButton = (Button) v;
				SocialList list = (SocialList)v.getTag();
				boolean alreadyInvitedAdded = list.getSelected();
				if(!alreadyInvitedAdded){
					if(list.getInviteMode()){
						//Invite
						tButton.setText(context.getString(R.string.invited));
						Log.d("Facebook",list.getUserId());
						Log.d("FacebookInvite",list.getName());
						
						 //sendRequestDialog(list.getUserId()); 
						 sendRequestDialog(); 
						/*Bundle params = new Bundle();
						params.putString("message", "Take this bomb to blast your way to victory!");
						// Optionally provide a 'to' param to direct the request at a specific user
						params.putString("to", "100001753345198");
						// Give the structured request information
						params.putString("action_type", "send");
						params.putString("object_id", "YOUR_OBJECT_ID");  
						WebDialog requestsDialog = (
						        new WebDialog.RequestsDialogBuilder(context,
						            Session.getActiveSession(),
						            params))
						            .build();
						    requestsDialog.show();*/
						/*ParseObject pObj = new ParseObject("InviteUsers");
						pObj.put("invite, value)*/
					}else{
						//Add
						tButton.setText("Added");
					}
					tButton.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
					tButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
					list.setSelected(true);
				}
			}
		});
		
		if(items.getInviteMode()){
			
			//For Invite action
			
			if(items.getSelected()){
				//Invited
				holder.mInvite.setText(context.getString(R.string.invited));
				holder.mInvite.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
				holder.mInvite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
			}else{
				//Not Invited
				holder.mInvite.setText(context.getString(R.string.invite));
				holder.mInvite.setBackgroundResource(R.drawable.button);
				holder.mInvite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus, 0, 0, 0);
			}
		}else{
			
			//For Add action
			
			if(items.getSelected()){
				//Invited
				holder.mInvite.setText("Added");
				holder.mInvite.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
				holder.mInvite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
			}else{
				//Not Invited
				holder.mInvite.setText("Add");
				holder.mInvite.setBackgroundResource(R.drawable.button);
				holder.mInvite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus, 0, 0, 0);
			}
		}
		
		if(items.getImage() != null){
			if (items.getImage().startsWith("https://")){
				String tempImage = items.getImage();
				Picasso.with(context).load(tempImage).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into(holder.mUserImage);
			}else{
				Uri tempImage = Uri.parse(items.getImage());
				Picasso.with(context).load(tempImage).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into(holder.mUserImage);
			}
			
		}else{
			holder.mUserImage.setImageResource(R.drawable.friend_nopic);
		}
		
		return convertView;
	}
	
	public int getCount() {
		return list.size();
	}
	
	//Filter method
	public void afilter(String filterText){
		Log.i("FILTER",filterText);
		filterText = filterText.toLowerCase();
		list.clear();
		if(filterText.length() == 0){
			list.addAll(origin);
		}else{
			Log.i("FILTER","YESYYY");
			for(SocialList mlist : origin){
				Log.i("FILTER",mlist.getName());
				Log.i("FILTER IP",filterText);
				if(mlist.getName().toLowerCase().contains(filterText)){
					list.add(mlist);
				}
			}
		}
		notifyDataSetChanged();
	}
	
}
