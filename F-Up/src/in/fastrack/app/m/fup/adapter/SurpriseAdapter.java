package in.fastrack.app.m.fup.adapter;

import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.UserFriendRelation;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

public class SurpriseAdapter extends ArrayAdapter<Friend> {
	
	private Context context;
	private int resource;
	private ArrayList<Friend> list,origin;
	private SurpriseListener mListen;
	private JSONArray friends;
private boolean from_friends;
private UserFriendRelation ufr;
@Override
public Filter getFilter() {
    return new Filter() {
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null && results.count >= 0) {
                setData((ArrayList<Friend>) results.values);//if results of search is null set the searched results data
            } else {
                setData(origin);// set original values
            }

            notifyDataSetInvalidated();
        }



       @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            if (!TextUtils.isEmpty(constraint)) {
                //constraint = constraint.toString().toLowerCase();
                ArrayList<Friend> foundItems = new ArrayList<Friend>();
                if(list!=null)
                {
                for(int i=0;i<list.size();i++)
                {

                    if (list.get(i).getName().toString().toLowerCase().startsWith(constraint.toString())) {
                        
                        foundItems.add(list.get(i));

                    }
                    else
                    {

                    }
                }
                }
                result.count = foundItems.size();//search results found return count
                result.values = foundItems;// return values
            } 
            else
            {
                result.count=-1;// no search results found
            }


            return result;
        }
    };
}

public void setData(ArrayList<Friend> mPpst) {   
       list = mPpst;//contains class items data.
   }
	public SurpriseAdapter(Context context, int resource, ArrayList<Friend> list,SurpriseListener mListen, JSONArray friends, UserFriendRelation ufr) {
		super(context, resource, list);
		this.context = context;
		this.resource = resource;
		this.list = list ;
		this.origin = list;
		//this.list.addAll(list);
		//this.origin.addAll(list);
		this.mListen = mListen;
		this.friends = friends;
		this.ufr = ufr;
		System.out.println(friends.toString());
	}
	
	private class ViewHolder{
		TextView mName;
		ImageView mUserImage,mSurpriseImage;
		Button mAction,mSupriseFriend;
		//LinearLayout mllSurpriseImage;
	}
	
	@Override
	public View getView(int position,View convertView,ViewGroup parent){
		ViewHolder holder = null;
		Friend rowItem = getItem(position);
		LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if(convertView == null){
			convertView = vi.inflate(resource,null);
			holder = new ViewHolder();
			holder.mName = (TextView) convertView.findViewById(R.id.tvPhoneName);
			holder.mUserImage = (ImageView) convertView.findViewById(R.id.ivUserImage);
			holder.mAction = (Button) convertView.findViewById(R.id.bInviteUser);
			holder.mSurpriseImage = (ImageView) convertView.findViewById(R.id.ivSurpriseImage);
		//	holder.mllSurpriseImage = (LinearLayout) convertView.findViewById(R.id.llSurpriseImage);
			//holder.mSupriseFriend = (Button) convertView.findViewById(R.id.bSupriseFriend);
			
			if(rowItem.getStatus()!=null)
			{
			
			if(rowItem.getStatus().equals("Surprise")) {
				
				holder.mSurpriseImage.setVisibility(View.VISIBLE);
				Log.i("u r  inside"," u r adapter");
			}
			}
			
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.mName.setText(rowItem.getName());
		holder.mAction.setTag(rowItem);
		//holder.mSupriseFriend.setTag(rowItem);
		if(rowItem.getUserImage() != null){
			Picasso.with(context).load(rowItem.getUserImage()).fit().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into(holder.mUserImage);
		}else{
			holder.mUserImage.setImageResource(R.drawable.friend_nopic);
		}
		
		/*if(!rowItem.getIsInvite()){
			//Add friend
			holder.mAction.setText("Add");
		}else{
			//Invite Friend
			holder.mAction.setText("Invite");
		}*/
		
		
		holder.mAction.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Button b = (Button) v;
				Friend p = (Friend) v.getTag();
				Log.d("MyApp","Selected: "+p.getUserSelected().toString());
				if(!p.getUserSelected()){
					//Not selected so select them
					p.setUserSelected(true);
					b.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
					b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
				}else{
					//Already selected, so unselect
					p.setUserSelected(false);
					b.setBackgroundResource(R.drawable.button);
					b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus, 0, 0, 0);
				}
				showSelected();
			}
		});
		
		/*holder.mSupriseFriend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Button b = (Button) v;
				Friend p = (Friend) v.getTag();
				Log.d("MyApp","Selected: "+p.getUserSelected().toString());
				if(!p.getUserSelected()){
					//Not selected so select them
					p.setUserSelected(true);
					b.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
					b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
				}else{
					//Already selected, so unselect
					p.setUserSelected(false);
					b.setBackgroundResource(R.drawable.button);
					b.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus, 0, 0, 0);
				}
				showSelected();
			}
		});*/
		
		if(!rowItem.getUserSelected()){
			holder.mAction.setBackgroundResource(R.drawable.button);
			holder.mAction.setCompoundDrawablesWithIntrinsicBounds(R.drawable.plus, 0, 0, 0);
		}else{
			holder.mAction.setBackgroundColor(context.getResources().getColor(R.color.gray_button));
			holder.mAction.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
		}
		
		return convertView;
	}
	
	
	public int getCount(){
		return list.size();
	}
	
	@SuppressLint("DefaultLocale")
	/*public void search(String query){
		query = query.toLowerCase();
		Log.i("Search",query);
		list.clear();
		if(query.length() == 0){
			list.addAll(origin);
		}else{
			for(Friend mList:origin){
				Log.i("Search res",mList.getName().toLowerCase());
				if(mList.getName().toLowerCase().contains(query)){
					Log.i("Search state","Found");
					list.add(mList);
				}else{
					Log.i("Search state","Not Found");
				}
			}
			notifyDataSetChanged();
		}
	}*/
	public ArrayList<Friend> search(String query){
		query = query.toLowerCase();
		Log.i("Search",query);
		list.clear();
		if(query.length() == 0){
			list.addAll(origin);
		}else{
			for(Friend mList:origin){
				Log.i("Search res",mList.getName().toLowerCase());
				if(mList.getName().toLowerCase().contains(query)){
					Log.i("Search state","Found");
					list.add(mList);
				}else{
					Log.i("Search state","Not Found");
				}
			}
			notifyDataSetChanged();
		}
		return list;
	}
	
	public static interface SurpriseListener {
		public void getSurpriseAdded(JSONArray arr,JSONArray friendsId);
	}
	
	public void showSelected() {
		JSONArray myArray = new JSONArray();
		JSONArray myFriendsId = new JSONArray();
		ArrayList<Friend> myList = new ArrayList<Friend>();
		myList = list;
		for(Friend s: myList){
			if(s.getUserSelected()){
				try {
					JSONObject object = new JSONObject();
					object.put("userid",s.getUserId());
					object.put("name",s.getName());
					object.put("source",s.getSource());
					object.put("userImage",s.getUserImage());
					myArray.put(object);
					myFriendsId.put(s.getUserId());
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		Log.d("MyApp","Data: "+myList.toString()+myFriendsId.toString());
		
		mListen.getSurpriseAdded(myArray,myFriendsId);
	}

}
