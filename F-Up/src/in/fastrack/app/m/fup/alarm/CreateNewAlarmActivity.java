package in.fastrack.app.m.fup.alarm;

import in.fastrack.app.m.fup.AlaramActivityNew;
import in.fastrack.app.m.fup.Alarams;
import in.fastrack.app.m.fup.FriendGroupActivity;
import in.fastrack.app.m.fup.HomeActivity;
import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.SelectCategory;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.database.DatabaseHandler;
import in.fastrack.app.m.fup.dialogs.DateTimeFragment;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.NotifyingReceiver;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.FeedEvent;
import in.fastrack.app.m.fup.model.FeedObject;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.Group;
import in.fastrack.app.m.fup.model.NewsFeed;
import in.fastrack.app.m.fup.model.UserFriendRelation;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SendCallback;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.squareup.picasso.Picasso;

import de.greenrobot.event.EventBus;

public class CreateNewAlarmActivity extends Activity implements OnDateSetListener, TimePickerDialog.OnTimeSetListener, OnClickListener{
	
	private AppConfig _config;
	ParseUser currentUser;
	Button selectDateTime,selectRingtone,selectFriends,eventIconButton,addFriendToPlus,currentUserImage,isPrivateAlaram;
	LinearLayout setAlarmBoxTime,schedule_interval_ll;
	EditText mEventName;
	TextView dateText,timeMin,timeSec,friendCount, ampmshowtext,schedule_interval_tv;
	private Bundle bundle,extras = null;
	Calendar cal,newCal = null;
	private int[] userSelectedTime = {0,0,0,0,0};
	private boolean isValid = true;
	String passTime = "";
	private Spinner mCategory;
	private Preloader loader = new Preloader();
	private String track = null;
	private Boolean isRecorded = false;
	//private CheckBox includeMe;
	private JSONArray friendsID = null;
	private JSONArray friendsId = null;
	private JSONArray friends = null;
	private JSONArray arraynew = null;
	private JSONArray jarraydifferent =null;
	private JSONArray jarraydifferentunique =null;
	private JSONArray jarraysimilar =null;
	private JSONArray groups = null;
	private JSONArray surprisefriends ;
	private JSONArray surprisefriendsid ;
	private String selectedCategory;
	boolean isPressed = false;
	boolean isPrivateChecked =false;
	private ParseFile p;
	private String t = null;
	private String imageUrl = "image";
	private Boolean includeMe = true;
	private Boolean isPrivateAlaramCheck =false;
	
	private static final int PERIOD = 60000; // 900000 = 15 minutes
	private static final int INITIAL_DELAY = 60000; // 1 minutes
	
	public static final String DATEPICKER_TAG = "datepicker";
	public static final String TIMEPICKER_TAG = "timepicker"; 
	String monthNames[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	
	String[] alarmType = {"Once", "Daily","Weekly", "Monthly"};// , "Yearly"
	String selectedAlarmType = "Once";
	
	NewsFeed alarmDetails = null;
	/*NEWLY ADDED*/
	ArrayList<Friend> finalfriendList = new ArrayList<Friend>();
	ArrayList<Group> groupList = new ArrayList<Group>();
	
	String listfriendsID = null;
	/*NEWLY ADDED*/
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_create_alarm);
		
		
		
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
		query.whereEqualTo("user", ParseUser.getCurrentUser());
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> objects, com.parse.ParseException e) {
				if(e==null){
				if(objects.size() > 0) {
					for(ParseObject obj : objects) { 
						
						arraynew = obj.getJSONArray("connectedSF");
						
						break;
					}
				}
				}
			}

		
		});
		
		
		
		
		
		
		
		
		
		Calendar calendar = null;
		
		//*******************EDIT******************************
		if(getIntent().getStringExtra("alarm_action") != null && getIntent().getStringExtra("alarm_action").equals("edit")) {
			alarmDetails = (NewsFeed) getIntent().getSerializableExtra("alarm_details");
			Log.d("AlarmDetails", alarmDetails.toString());
			calendar = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");			
			//SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS z");
			try { 
				Date date = formatter.parse(alarmDetails.getAlarmTime());
				calendar.setTime(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else {
			//IF not Edit
			calendar = Calendar.getInstance();
		}
		
		//final Calendar calendar = Calendar.getInstance();
	    final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), true);
	   
	    final TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(this, calendar.get(Calendar.HOUR_OF_DAY) ,calendar.get(Calendar.MINUTE ), false, false);
	    timePickerDialog.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Dialog);
		_config = new AppConfig(getApplicationContext());
		configParse();
		//uncomment to show dialog
		/*if(!_config.getTNC()){
			//SHOW TNC of the app
			showTnc();
		}*/
		ActionBar bar = getActionBar();
		bar.setTitle(this.getString(R.string.createalarmtitle));
		bar.setDisplayShowHomeEnabled(true);
		bar.setHomeButtonEnabled(true);
		bar.setDisplayHomeAsUpEnabled(true);
		
		//TODO enhance is so that user can set a default track in the settings
		int themeId = getResources().getIdentifier("playlist","array",getPackageName());
		String[] themeList = getResources().getStringArray(themeId);
		track = Util.stringtolower(themeList[0]); //Load first track as default
		
		mEventName = (EditText) findViewById(R.id.etmEventName);
		//***************EDIT***************************
		if(alarmDetails != null && alarmDetails.getAlarmTitle() != null) {
			mEventName.setText(alarmDetails.getAlarmTitle());
		}
		//mEventName.setSingleLine();
		
		mEventName.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_NEXT) {
					DialogFragment df = new DateTimeFragment();
					df.show(getFragmentManager(),"dateTimeDialog");
	            }
				return false;
			}
		});
		
		eventIconButton = (Button)findViewById(R.id.eventIconButton);
		eventIconButton.setOnClickListener(this);
		//Default Alarm set to Alarm
		eventIconButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.alarm, 0, 0);
		selectedCategory = "Alarm";
		//***************EDIT***************************
		if(alarmDetails != null && alarmDetails.getAlarmCategory() != null) {
			selectedCategory = alarmDetails.getAlarmCategory();
			userSelectedCategory();
		}
		
		
		
		dateText = (TextView)findViewById(R.id.dateText);
		dateText.setText( calendar.get(Calendar.DAY_OF_MONTH) + " - " + monthNames[calendar.get(Calendar.MONTH)] + " - " + calendar.get(Calendar.YEAR));
		userSelectedTime[0] = calendar.get(Calendar.YEAR);
	    userSelectedTime[1] = calendar.get(Calendar.MONTH);
	    userSelectedTime[2] = calendar.get(Calendar.DAY_OF_MONTH);
		
		//dateText.setOnClickListener(this);
		dateText.setOnClickListener(new OnClickListener() {            
            public void onClick(View v) {
                datePickerDialog.setVibrate(true);
                datePickerDialog.setYearRange(2014, 2028);
                datePickerDialog.setCloseOnSingleTapDay(false);
                datePickerDialog.show(getFragmentManager(), DATEPICKER_TAG);
            }
        });
		
		timeMin = (TextView)findViewById(R.id.timeMin);
		timeSec = (TextView)findViewById(R.id.timeSec);
		DecimalFormat formatter = new DecimalFormat("00");
    	String hoursFormatted = formatter.format(calendar.get(Calendar.HOUR_OF_DAY));
    	timeMin.setText(hoursFormatted);
    	String minuteFormatted = formatter.format(calendar.get(Calendar.MINUTE));
    	timeSec.setText(minuteFormatted);
		userSelectedTime[3] = calendar.get(Calendar.HOUR_OF_DAY);
    	userSelectedTime[4] = calendar.get(Calendar.MINUTE);
		
    	ampmshowtext = (TextView) findViewById(R.id.ampmshowtext);
		if(calendar.get(Calendar.HOUR_OF_DAY) < 12) {
			ampmshowtext.setText("AM");
		} else {
			ampmshowtext.setText("PM");
		}
		
		selectRingtone = (Button)findViewById(R.id.ringtoneButton);
		selectRingtone.setOnClickListener(this);
		selectFriends = (Button)findViewById(R.id.addFriendButton);
		selectFriends.setOnClickListener(this);
		addFriendToPlus = (Button) findViewById(R.id.addFriendToPlus);
		addFriendToPlus.setOnClickListener(this);
		
		friendCount = (TextView)findViewById(R.id.addFriendToPlus);
		
		currentUserImage = (Button)findViewById(R.id.currentUserImage);
		currentUserImage.setOnClickListener(includeMeButtonListener);
		
		isPrivateAlaram = (Button)findViewById(R.id.isPrivateAlaram);
		isPrivateAlaram.setOnClickListener(isPrivateAlarmCheckButtonListener);
		
		setAlarmBoxTime = (LinearLayout)findViewById(R.id.setAlarmBoxTime);
		//setAlarmBoxTime.setOnClickListener(openDateTimeDialogListener);
		setAlarmBoxTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog.setVibrate(true);
                timePickerDialog.setCloseOnSingleTapMinute(false);
                timePickerDialog.show(getFragmentManager(), TIMEPICKER_TAG);
            }
        });

		p = ParseUser.getCurrentUser().getParseFile("profilethumbnail");
		if(p!=null){
			String url = p.getUrl();
			imageUrl = url;
		}else{
			if(ParseUser.getCurrentUser().get("facebookid") != null && ParseUser.getCurrentUser().get("facebookid") != "") {
				t = ParseUser.getCurrentUser().get("facebookid").toString();
				if((t!=null)||(t!="")){
					imageUrl = "https://graph.facebook.com/"
							+ ParseUser.getCurrentUser().get("facebookid").toString()
							+ "/picture/?type=square";
				}
			}
		}
		//Picasso.with(getApplicationContext()).load(R.drawable.add_friends_to_pic_tick).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.add_friends_to_pic_tick).error(R.drawable.add_friends_to_pic_tick).into((Button)findViewById(R.id.currentUserImage));
		currentUserImage.setCompoundDrawablesWithIntrinsicBounds(R.drawable.add_friends_to_pic_tick, 0, 0, 0);
		isPrivateAlaram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.add_friends_to_pic_minus, 0, 0, 0);
		
		if (savedInstanceState != null) {
            DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag(DATEPICKER_TAG);
            if (dpd != null) {
                dpd.setOnDateSetListener(this);
                onDateSet(dpd, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            }

            TimePickerDialog tpd = (TimePickerDialog) getFragmentManager().findFragmentByTag(TIMEPICKER_TAG);
            if (tpd != null) {
                tpd.setOnTimeSetListener(this);
            }
        }
		
		//***************EDIT***************************
		if(alarmDetails != null) {
			if(alarmDetails.isSelfAlarm()) {
				includeMe = true;
				isPressed = true;
				currentUserImage.setCompoundDrawablesWithIntrinsicBounds(R.drawable.add_friends_to_pic_tick, 0, 0, 0);
				//Picasso.with(getApplicationContext()).load(R.drawable.add_friends_to_pic_tick).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.add_friends_to_pic_tick).error(R.drawable.add_friends_to_pic_tick).into((ImageView)findViewById(R.id.currentUserImage));
			} else {
				currentUserImage.setCompoundDrawablesWithIntrinsicBounds(R.drawable.add_friends_to_pic_minus, 0, 0, 0);
				//Picasso.with(getApplicationContext()).load(R.drawable.add_friends_to_pic_minus).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.add_friends_to_pic_minus).error(R.drawable.add_friends_to_pic_minus).into((ImageView)findViewById(R.id.currentUserImage));
	        	includeMe = false;
	        	isPressed = false;
			}
			
			if(alarmDetails.isPrivateAlaramCheck()) {
				isPrivateAlaramCheck = true;
					isPrivateChecked=true;		
				isPrivateAlaram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.add_friends_to_pic_tick, 0, 0, 0);				
			} else {
				isPrivateAlaram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.add_friends_to_pic_minus, 0, 0, 0);				
				isPrivateAlaramCheck = false;
				isPrivateChecked=false;	
				
			}			
			
			
			if(alarmDetails.isRecorded()) {
				selectRingtone.setText("Ringtone: Custom");
			} else {
				selectRingtone.setText("Ringtone: "+ alarmDetails.getMedia());
			}
			
			try {
				friends = new JSONArray(alarmDetails.getAlarmFriends());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			try {
				friendsId = new JSONArray(alarmDetails.getAlarmFriendsid());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if(friends.length() == 0) {
				friendCount.setText("Add Friends");
			} else {
				friendCount.setText(String.valueOf(friends.length()) + " Others ");
			}
		}
		
		
		schedule_interval_ll = (LinearLayout)findViewById(R.id.schedule_interval_ll);
		schedule_interval_tv=(TextView) findViewById(R.id.schedule_interval_tv);
		
		
		//***************EDIT***************************
				if(alarmDetails != null) {
					if(alarmDetails.getAlarm_repeat_type() != null)
					schedule_interval_tv.setText(alarmDetails.getAlarm_repeat_type());	
					else
					schedule_interval_tv.setText("Once");	
				}
		
		schedule_interval_ll.setOnClickListener(new OnClickListener() {
	        @Override
	        public void onClick(View v) {
	        	  AlertDialog.Builder builder = new AlertDialog.Builder(CreateNewAlarmActivity.this);
	              builder.setTitle("Make your selection");
	              builder.setItems(alarmType, new DialogInterface.OnClickListener() {
	                  public void onClick(DialogInterface dialog, int item) {
	                      // Do something with the selection
	                	  selectedAlarmType = alarmType[item];
	                	  schedule_interval_tv.setText(selectedAlarmType);
	                	  
	                  }
	              });
	              AlertDialog alert = builder.create();
	              alert.show();

	        }
	    });
	}
	
	  OnClickListener includeMeButtonListener= new OnClickListener() {
		    @Override
		    public void onClick(View v) {
		        if(isPressed){
		        	//Picasso.with(getApplicationContext()).load(R.drawable.add_friends_to_pic_tick).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.add_friends_to_pic_tick).error(R.drawable.add_friends_to_pic_tick).into((ImageView)findViewById(R.id.currentUserImage));
		        	currentUserImage.setCompoundDrawablesWithIntrinsicBounds(R.drawable.add_friends_to_pic_tick, 0, 0, 0);
		        	includeMe = true;
		        	Toast.makeText(getApplicationContext(),"You have added yourself for this Alarm",Toast.LENGTH_LONG).show();
		        }else{
		        	//Picasso.with(getApplicationContext()).load(R.drawable.add_friends_to_pic_minus).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.add_friends_to_pic_minus).error(R.drawable.add_friends_to_pic_minus).into((ImageView)findViewById(R.id.currentUserImage));
		        	currentUserImage.setCompoundDrawablesWithIntrinsicBounds(R.drawable.add_friends_to_pic_minus, 0, 0, 0);
		        	includeMe = false;
		        	Toast.makeText(getApplicationContext(),"You have excluded yourself for this Alarm",Toast.LENGTH_LONG).show();
		        }
		        isPressed=!isPressed;
		   }
		};
		
		  OnClickListener isPrivateAlarmCheckButtonListener= new OnClickListener() {
			    @Override
			    public void onClick(View v) {
			        if(isPrivateChecked){	
			        	isPrivateAlaram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.add_friends_to_pic_minus, 0, 0, 0);
			        	isPrivateAlaramCheck = false;
			        	Toast.makeText(getApplicationContext(),"You made this Alaram as Public",Toast.LENGTH_LONG).show();			        
			        }else{		
			        	isPrivateAlaram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.add_friends_to_pic_tick, 0, 0, 0);
			        	isPrivateAlaramCheck = true;
			        	Toast.makeText(getApplicationContext(),"You made this Alaram as Private",Toast.LENGTH_LONG).show();			        
			        }
			        isPrivateChecked=!isPrivateChecked;
			   }
			};
		
	
		OnClickListener openDateTimeDialogListener= new OnClickListener() {
		    @Override
		    public void onClick(View v) {
		    	DialogFragment df = new DateTimeFragment();
				df.show(getFragmentManager(),"dateTimeDialog");
		   }
		};
    
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		 getMenuInflater().inflate(R.menu.action_menu_createalarm, menu);
		 return super.onCreateOptionsMenu(menu);
	}
	
	
	public void TextViewClicked(View v) {
		Log.d("asdf", "asdf");
	    /*ViewSwitcher switcher = (ViewSwitcher) findViewById(R.id.my_switcher);
	    switcher.showNext(); //or switcher.showPrevious();
	    TextView myTV = (TextView) switcher.findViewById(R.id.clickable_text_view);
	    EditText myET = (EditText) switcher.findViewById(R.id.hidden_edit_view);
	    myET.setText(myTV.getText());*/
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			/*case R.id.dateText:   
				DialogFragment df = new DateTimeFragment();
				df.show(getSupportFragmentManager(),"dateTimeDialog");
			break;*/
			case R.id.ringtoneButton:
				Intent chooseMusic = new Intent(this, SelectMusic.class);
				startActivityForResult(chooseMusic, 123); //Request code is 1
			break;
			case R.id.addFriendButton:
				Intent chooseFriends = new Intent(this,FriendGroupActivity.class);
				if(friends != null)
				{
					chooseFriends.putExtra("friends", friends.toString());//changed
				chooseFriends.putExtra("group", groups.toString());//changed
				startActivityForResult(chooseFriends,234);
				}
			break;
			case R.id.addFriendToPlus:
				Intent chooseFriendss = new Intent(this,FriendGroupActivity.class);
				if(friends != null)
					chooseFriendss.putExtra("friends", friends.toString());//changed
				startActivityForResult(chooseFriendss,234);
			break;
			case R.id.cbIncludeMe:
				
			break;
			case R.id.eventIconButton:
				Intent selectCategory = new Intent(this, SelectCategory.class);
				startActivityForResult(selectCategory,345); 
			break;
		}	
	}
	
	/*@Override
	public void onDateTimePicked(int year, int month, int day, int hour,
			int min, int[] fullDateTime, String AMPM, String humanTime) {
		userSelectedTime = fullDateTime;
		passTime = AMPM;
		Log.i("ALARM ORG TIME",userSelectedTime.toString());
		if(validateDate()){
			dateText.setText(String.valueOf(day) + "/" + String.valueOf(month+1) + "/" + String.valueOf(year));
			timeMin.setText(String.valueOf(userSelectedTime[3]));
			timeSec.setText(String.valueOf(userSelectedTime[4]));
			if(AMPM.equals("AM")) {
				amButton.setPressed(true);
				pmButton.setPressed(false);
			} else {
				amButton.setPressed(false);
				pmButton.setPressed(true);
			}
		}else{
			//Show Dialog
			AlertDialog.Builder ad = new AlertDialog.Builder(CreateNewAlarmActivity.this);
			ad.setTitle("Invalid alarm time");
			ad.setMessage("Please make sure that the alarm you have set is greater than the current one.");
			ad.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
					DialogFragment df = new DateTimeFragment();
					df.show(getSupportFragmentManager(),"dateTimeDialog");
				}
			});
			AlertDialog alertDialog = ad.create();
			alertDialog.show();
		}
	}*/
	
	public Boolean validateDate(){
		cal = Calendar.getInstance();
		newCal = Calendar.getInstance();
		newCal.set(Calendar.YEAR, userSelectedTime[0]);
		newCal.set(Calendar.MONTH, userSelectedTime[1]);
		newCal.set(Calendar.DAY_OF_MONTH, userSelectedTime[2]);
		newCal.set(Calendar.MINUTE, userSelectedTime[4]);
		newCal.set(Calendar.HOUR_OF_DAY,userSelectedTime[3]);
		newCal.set(Calendar.SECOND,0);
		newCal.set(Calendar.MILLISECOND,0);
		Log.i("CURRENT TIME",cal.getTime().toString());
		Log.i("NEW TIME",newCal.getTime().toString());
		boolean isLatest = cal.getTime().after(newCal.getTime());
		if(isLatest){
			return false;
		}
		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		exit();
	}
	
	private void exit(){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(CreateNewAlarmActivity.this);
		alertDialog.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
	    alertDialog.setNegativeButton("No", null);
	    alertDialog.setMessage("Do you want to exit without creating alarm?");
	    alertDialog.setTitle("Discard Changes");
	    alertDialog.show();
	}
	
	
	public void configParse(){
		_config = new AppConfig(this);
		currentUser = ParseUser.getCurrentUser();
	}
	
	private void showTnc() {
		String title = this.getString(R.string.tnctitle);
		String message = this.getString(R.string.tncmessagepara1)+"\n\n"+this.getString(R.string.tncmessagepara2);
		AlertDialog.Builder builder = new AlertDialog.Builder(this).setTitle(title).setMessage(message).setPositiveButton("Accept",new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface d, int arg1) {
				_config.setTNC(true);
				d.dismiss();
			}
		})
		.setNegativeButton("Decline",new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		builder.create().show();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			Intent i = new Intent(getApplicationContext(), HomeActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			finish();
			break;
		case R.id.action_next:
			isValid = true;
			
			if(!validateDate()){
				isValid = false;
				Toast.makeText(getApplicationContext(),"Set Date Time greater than Today's Date",Toast.LENGTH_LONG).show();
				break;
			}
			
			String cale = "";
			bundle = new Bundle();
			if(newCal!=null){
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss",Locale.getDefault());
				//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS z",Locale.getDefault());
				cale = sdf.format(newCal.getTime());
				bundle.putString("alarm_time",cale);
				Log.i("ALARM STEP",cale);
			}else{
				//Toast.makeText(getApplicationContext(),"A",Toast.LENGTH_LONG).show();
				isValid = false;
			}
			
			bundle.putString("alarm_category",selectedCategory);
			
			if(mEventName.getText().toString().length() > 0){
				bundle.putString("event_name",mEventName.getText().toString());
			}else{
				//Toast.makeText(getApplicationContext(),"B",Toast.LENGTH_LONG).show();
				isValid = false;
			}
			
			bundle.putBoolean("go_back",false);
			bundle.putIntArray("event_time",userSelectedTime);
			
			//Log.d("MyApp","Include Me: "+includeMe.isChecked());
			if(friendsId!=null){
				Log.d("MyApp","Friends with me: "+friendsId.toString());
			} 
			
			if((friends == null || friends.length() == 0) && includeMe == false) {
				Toast.makeText(getApplicationContext(),"Please set this alarm for yourself or for your friends",Toast.LENGTH_LONG).show();
				isValid = false;
				break;
			}
		
			
			if(isValid){
				
				
              if (!Util.isNetworkAvailable(CreateNewAlarmActivity.this)) {	
					
					DialogFactory.getBeaconStreamDialog1(CreateNewAlarmActivity.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
				} else 
				
				{

				
				
				
				
				if(alarmDetails != null) {
					//********************EDIT******************************
					final ParseObject localAlarm = new ParseObject("localalarm");		
					
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss",Locale.getDefault());
					//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS z",Locale.getDefault());
					long milli = 0;
					try {
						Date date = sdf.parse(cale);
						milli = date.getTime();
					} catch (java.text.ParseException e) {
						e.printStackTrace();
					}
				
					ParseQuery query = new ParseQuery("alarms");
					try {
						final ParseObject obj = query.get(alarmDetails.getAlarmid());
						obj.put("title",mEventName.getText().toString());
						obj.put("category",selectedCategory);
						obj.put("creator",currentUser);
						obj.put("media",track);
						obj.put("recorded",isRecorded);
						
				
						/*friendsID =	obj.getJSONArray("connected");
						
						if(friendsID == null){
							friendsID = new JSONArray();
							friendsID.put(currentUser.getObjectId().toString());
						}
						
						obj.put("connected",friendsID);
						
						*/
					/*	if(friendsId!=null){
							 Log.i("u r in ", "!null"+friendsId);
							// obj.put("connected",friendsId);
					     }
						else{
							friendsId = new JSONArray();
							if(friendsId!=null){
								
							}
							else{
								
								friendsId.put(currentUser.getObjectId().toString());
							}
							
							//
						   
						}
						   obj.put("connected",friendsId);
						    Log.i("u r in ", "null"+friendsId);*/
					/*	if(friendsId == null){
							friendsId = new JSONArray();
							
						}
						friendsId.put(currentUser.getObjectId().toString());
						obj.put("connected",friendsId);
						 Log.i("u r in ", "null"+friendsId);*/

						if (friendsId != null) {
							obj.put("connected", friendsId);							
							Log.i("u r in ", "!null" + friendsId);

						} else {
							friendsId = new JSONArray();
							friendsId.put(currentUser.getObjectId().toString());
							obj.put("connected", friendsId);
							
							Log.i("u r in ", "null" + friendsId);
						}

						if(friends!= null){
							
							//listfriendsID =	obj.getString("connectedlist");
							//Log.i("connectedlist are ", " listfriendsID"+listfriendsID);
							//obj.put("connectedlist",listfriendsID.toString());
							//Log.i("u r in ", "!=null"+listfriendsID.toString());
							obj.put("connectedlist",friends.toString());//changed
							Log.i("u r in ", "!null"+friends.toString());
							
							
						}
						else{
							friends = new JSONArray();
							obj.put("connectedlist",friends.toString());//changed
							Log.i("u r in ", "null"+friends.toString());
						}
						
						obj.put("acceptedpeopleedit",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
						obj.put("noficationsedit",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
						
						//obj.put("comments",0);
						//obj.put("likes",new JSONArray());
						obj.put("alarmTime", cale);
						obj.put("alarmtimemilli", milli);
						obj.put("requestcode", String.valueOf(alarmDetails.getAlarmrequestid()));
						obj.put("selfalarm",includeMe);
						obj.put("isPrivateAlaramCheck",isPrivateAlaramCheck);
						obj.put("status", true);
						obj.put("alarm_repeat_type", selectedAlarmType);
						//obj.saveEventually();
						Intent intent = new Intent(getApplicationContext(), AlaramActivityNew.class);//AlaramActivityNew
						
						if(includeMe){
							//Set self alarm
						//	Intent intent = new Intent(getApplicationContext(),AlarmActivity.class);
							Bundle tempData = new Bundle();
							tempData.putString("alarm_category", selectedCategory);
							tempData.putString("alarm_title", mEventName.getText().toString());
							tempData.putBoolean("recorded",isRecorded);
							tempData.putInt("alarmrequestcode", alarmDetails.getAlarmrequestid());
							tempData.putString("alarmpath",track);
							tempData.putString("alarmfriends",friends.toString());//changed
							tempData.putString("alarmfriendsid",friendsId.toString());//added
							tempData.putString("alarm_repeat_type",selectedAlarmType);
							
							//edit
							Alarams al = new Alarams();
							al.setCategory(selectedCategory);
							al.setTitle(mEventName.getText().toString());
							if (isRecorded == true) {
								al.setRecorded("true");
							} else {
								al.setRecorded("false");
							}
							al.setRequestcode(String.valueOf(alarmDetails.getAlarmrequestid()));
							al.setPath(track);
							al.setFriends(friends.toString());
							al.setFriendsid(friendsId.toString());
							al.setRepeattype(selectedAlarmType);
							al.setAtime(cale);
							al.setMilli(String.valueOf(milli));
							//Toast.makeText(getApplicationContext(),"millllllli" + milli, Toast.LENGTH_LONG).show();

							DatabaseHandler db = new DatabaseHandler(
									CreateNewAlarmActivity.this);
							db.updateAlarm(al);
							intent.putExtras(tempData);
							
							PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),alarmDetails.getAlarmrequestid(),intent,PendingIntent.FLAG_CANCEL_CURRENT);
							AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
							am.set(AlarmManager.RTC_WAKEUP,milli, pi);
							
							Calendar calendar = Calendar.getInstance();
							calendar.setTimeInMillis(calendar.getTimeInMillis());
							//calendar.add(Calendar.SECOND, 30);
							if (selectedAlarmType.equalsIgnoreCase("Once")) {
								am.set(AlarmManager.RTC_WAKEUP, milli, pi);
								
							} else if (selectedAlarmType.equalsIgnoreCase("Daily")) {

								calendar.add(Calendar.DAY_OF_WEEK, 1);
								am.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(), pi);
								// am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,calendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pi);
							} else if (selectedAlarmType.equalsIgnoreCase("Weekly")) {

								calendar.add(Calendar.WEEK_OF_MONTH, 1);
								am.set(AlarmManager.RTC_WAKEUP,
										calendar.getTimeInMillis(), pi);
							} else if (selectedAlarmType.equalsIgnoreCase("Monthly")) {

								calendar.add(Calendar.MONTH, 1);
								am.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(), pi);
							} /*else if (selectedAlarmType.equalsIgnoreCase("Yearly")) {

								calendar.add(Calendar.YEAR, 1);
								am.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(), pi);
							}*/
					
						}
						
						//alarm type added
						
						

						obj.saveInBackground(new SaveCallback() {
							
							@Override
							public void done(com.parse.ParseException e) {
								if(e==null){
									FeedObject object= new FeedObject(1,alarmDetails.getAlarmrequestid(),true,0,0);
									EventBus.getDefault().postSticky(new FeedEvent(object));
									if(isRecorded){
										File mFile = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),track);
										try {
											Log.i("AUDIO RECORD","TRUE");
											//ParseQuery q = new ParseQuery<ParseObject>(subclass)
											int size = (int) mFile.length();
											byte[] bytes = new byte[size];
											BufferedInputStream buf = new BufferedInputStream(new FileInputStream(mFile));
											buf.read(bytes,0,bytes.length);	
											buf.close();
											ParseFile audio = new ParseFile(track,bytes);
											audio.saveInBackground();
											obj.put("audioclip",audio);
											obj.saveInBackground();
											
										} catch (FileNotFoundException e1) {
											e1.printStackTrace();
										}catch (IOException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
									}
								}
							}
						});
						
						
						//start here
				           
	                     //For surprise friend
	                     updateArray() ;
	                     

						try {
							Log.i("u r ","before notification");
							
							//New custom notification code
						/*	ParseObject notification = new ParseObject("Activity");
							notification.put("creator",currentUser);
							notification.put("followers",friendsId);
							notification.put("reference",obj);
							notification.put("notificationtype",8);
							notification.put("notificationDone",false);
							notification.put("readers",new JSONArray());
							notification.add("readers", ParseUser.getCurrentUser().getObjectId());
							notification.saveInBackground();//saveEventually();();
							*/
							
							Log.d("friends", friends.toString());//changed
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
	                    
	                     if(jarraysimilar!=null){
	                    	                    	 
	                    		try {
	        						 
	                              
	            					
	            					
	            					// parse push implementation
	            						
	            						for (int j = 0; j < jarraysimilar.length(); j++) {
	            							 Log.d("friendsId.get(j).toString()", "--"+jarraysimilar.get(j).toString()+"ParseUser.getCurrentUser().getObjectId().toString())"+"--"+ParseUser.getCurrentUser().getObjectId().toString());
	            							if(!jarraysimilar.get(j).toString().equals(ParseUser.getCurrentUser().getObjectId().toString())){
	            								
	            								ParseQuery pushQuery = ParseInstallation.getQuery();
	            								ParsePush push = new ParsePush();
	            								//pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId().toString()); // not me
	            								pushQuery.whereEqualTo("myuser",jarraysimilar.get(j).toString());
	            								//
	            								push.setQuery(pushQuery);
	            								 JSONObject tempAlarmData = new JSONObject();	
	            								
	            								 
	            								  
	            									if (obj != null)
	            									tempAlarmData.put("reference",obj);	            									
	            									tempAlarmData.put("notificationtype", 13);
	            									tempAlarmData.put("title","");
	            									tempAlarmData.put("alert","");
	            									tempAlarmData.put("action","in.fastrack.app.m.fup.HomeActivity");												
	            									push.setData(tempAlarmData);
	            									push.sendInBackground(new SendCallback() {
	            										   public void done(com.parse.ParseException e) {
	            											     if (e == null) {
//	            											    	  
	            						            					//New custom notification code
	            						            					ParseObject notification = new ParseObject("Activity");
	            						            					notification.put("creator",currentUser);
	            						            					notification.put("followers",jarraysimilar);//friendsId//jarraydifferent//jarraysimilar
	            						            					notification.put("reference",obj);
	            						            					notification.put("notificationtype",13);
	            						            					notification.put("notificationDone",true);
	            						            					notification.put("readers",new JSONArray());
	            						            					//notification.put("readers",jarraysimilar);//changed from `new JSONArray()` to jarraysimilar
	            						            					//notification.add("readers", ParseUser.getCurrentUser().getObjectId());
	            						            					notification.saveInBackground();//saveEventually();();
	            											       Log.d("push", "success!");
	            											     } else {
	            											       Log.d("push", "failure");
	            											     }
	            											   }
	            										
	            											 });
	            							}
	            							}
	            							
	            					        
	            					
	            					
	            					
	            					}catch (Exception e1) {
	            							// TODO Auto-generated catch block
	            							e1.printStackTrace();
	            						} 
	            						
	            						
	            						
	                    	 
	                    	 
	                   	 
	                     }
	                     
	                     if(jarraydifferentunique!=null){                    	 
	                   		try {
	        						 
	            					
	            					
	            					// parse push implementation
	            						
	            						for (int j = 0; j < jarraydifferentunique.length(); j++) {
	            							 Log.d("jarraydifferentunique.get(j).toString()", "--"+jarraydifferentunique.get(j).toString()+"ParseUser.getCurrentUser().getObjectId().toString())"+"--"+ParseUser.getCurrentUser().getObjectId().toString());
	            							if(!jarraydifferentunique.get(j).toString().equals(ParseUser.getCurrentUser().getObjectId().toString())){
	            								
	            								ParseQuery pushQuery = ParseInstallation.getQuery();
	            								ParsePush push = new ParsePush();
	            								//pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId().toString()); // not me
	            								pushQuery.whereEqualTo("myuser",jarraydifferentunique.get(j).toString());
	            								//
	            								push.setQuery(pushQuery);
	            								 JSONObject tempAlarmData = new JSONObject();	
	            								
	            								 
	            								  
	            									if (obj != null)
	            									tempAlarmData.put("reference",obj);
	            									
	            									tempAlarmData.put("notificationtype", 8);						
	            									tempAlarmData.put("alert","The alarm request is edited by "+ParseUser.getCurrentUser().getString("fullname"));
	            									tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivityNew");												
	            									push.setData(tempAlarmData);
	            									push.sendInBackground(new SendCallback() {
	            										   public void done(com.parse.ParseException e) {
	            											     if (e == null) {
	            											    	 Toast.makeText(CreateNewAlarmActivity.this, "Alarm Edited sucessfully", Toast.LENGTH_LONG).show();
	            											    	//New custom notification code
	            						            					ParseObject notification = new ParseObject("Activity");
	            						            					notification.put("creator",currentUser);
	            						            					notification.put("followers",jarraydifferentunique);//friendsId//jarraydifferent//jarraysimilar
	            						            					notification.put("reference",obj);
	            						            					notification.put("notificationtype",8);
	            						            					notification.put("notificationDone",true);
	            						            					notification.put("readers",jarraydifferentunique);//changed from `new JSONArray()` to jarraydifferentunique
	            						            					notification.add("readers", ParseUser.getCurrentUser().getObjectId());
	            						            					notification.saveInBackground();//saveEventually();();
	            						            					
	            											       Log.d("push", "success!");
	            											     } else {
	            											       Log.d("push", "failure");
	            											     }
	            											   }
	            										
	            											 });
	            							}
	            							}
	            							
	            					        
	            					
	            					
	            					
	            					}catch (Exception e1) {
	            							// TODO Auto-generated catch block
	            							e1.printStackTrace();
	            						} 
	            						
	            						
	            						
	                    	 
	                    	 
	                     }
	                     
	                     if(jarraydifferent!=null && jarraydifferent.length() == 1  && jarraysimilar.length() == 0 ){                    	 
	                    		try {
	              					// parse push implementation
	             						
	             						for (int j = 0; j < jarraydifferent.length(); j++) {
	             							 Log.d("jarraydifferent.get(j).toString()", "--"+jarraydifferent.get(j).toString()+"ParseUser.getCurrentUser().getObjectId().toString())"+"--"+ParseUser.getCurrentUser().getObjectId().toString());
	             							if(jarraydifferent.get(j).toString().equals(ParseUser.getCurrentUser().getObjectId().toString())){
	             								
	             								
	             								//New custom notification code
					            					ParseObject notification = new ParseObject("Activity");
					            					notification.put("creator",currentUser);
					            					notification.put("followers",jarraydifferent);//friendsId//jarraydifferent//jarraysimilar
					            					notification.put("reference",obj);
					            					notification.put("notificationtype",8);
					            					notification.put("notificationDone",true);
					            					notification.put("readers",jarraydifferent);//changed from `new JSONArray()` to jarraydifferentunique
					            					//notification.add("readers", ParseUser.getCurrentUser().getObjectId());
					            					notification.saveInBackground();//saveEventually();();
	             								
	             							}
	             							}
	             							
	              					
	             					}catch (Exception e1) {
	             							// TODO Auto-generated catch block
	             							e1.printStackTrace();
	             						} 
	             						
	             						
	             						
	                     	 
	                     	 
	                      }
						
						//end here
	                     
	                     
	                     

					/*	try {
							Log.i("u r ","before notification");
							
							//New custom notification code
							ParseObject notification = new ParseObject("Activity");
							notification.put("creator",currentUser);
							notification.put("followers",friendsId);
							notification.put("reference",obj);
							notification.put("notificationtype",8);
							notification.put("notificationDone",false);
							notification.put("readers",new JSONArray());
							notification.add("readers", ParseUser.getCurrentUser().getObjectId());
							notification.saveInBackground();//saveEventually();();														
							Log.d("friends", friends.toString());//changed
							
							
							
							
							// parse push implementation
								
								for (int j = 0; j < friendsId.length(); j++) {
									 Log.d("friendsId.get(j).toString()", "--"+friendsId.get(j).toString()+"ParseUser.getCurrentUser().getObjectId().toString())"+"--"+ParseUser.getCurrentUser().getObjectId().toString());
									if(!friendsId.get(j).toString().equals(ParseUser.getCurrentUser().getObjectId().toString())){
										
										ParseQuery pushQuery = ParseInstallation.getQuery();
										ParsePush push = new ParsePush();
										//pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId().toString()); // not me
										pushQuery.whereEqualTo("myuser",friendsId.get(j).toString());
										//
										push.setQuery(pushQuery);
										 JSONObject tempAlarmData = new JSONObject();	
										
										 
										  
											if (obj != null)
											tempAlarmData.put("reference",obj);											
											tempAlarmData.put("notificationtype", 8);						
											tempAlarmData.put("alert","The alarm request is edited by "+ParseUser.getCurrentUser().getString("fullname"));
											tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivityNew");												
											push.setData(tempAlarmData);
											push.sendInBackground(new SendCallback() {
												   public void done(com.parse.ParseException e) {
													     if (e == null) {
													    	 Toast.makeText(CreateNewAlarmActivity.this, "Push Was Successfull", Toast.LENGTH_LONG).show();
													       Log.d("push", "success!");
													     } else {
													       Log.d("push", "failure");
													     }
													   }
												
													 });
									}
									}
									
							        
							
							
							
							
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}*/
	                     
	                     
	                     
	                     
	                     
						/**Notifications**/
						/*JSONObject friendsobject = new JSONObject();
						if(friends.length() > 0) {
							for(int k = 0; k<friends.length();k++){
								try {
									friendsobject = friends.getJSONObject(k);
									Log.i("FRIENDS",friendsobject.getString("userid"));
									ParseObject notifications = new ParseObject("Notifications");
									notifications.put("fromid",ParseUser.getCurrentUser()); 
									notifications.put("toid",friendsobject.getString("userid"));
									notifications.put("alarmaccepted","pending");
									notifications.put("notificationtype","alarm");
									notifications.put("actionref", obj);
									notifications.put("notificationDone",false);
									notifications.saveInBackground();
								} catch (JSONException en) {
									en.printStackTrace();
								}
							}
						}
						
						 Intent myAlarm = new Intent(getApplicationContext(), NotifyingReceiver.class);
				         PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm, PendingIntent.FLAG_UPDATE_CURRENT);
				         AlarmManager alarms = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
				         alarms.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+INITIAL_DELAY, PERIOD, recurringAlarm); //you can modify the interval of course
				        */
						/**Notifications**/
						
						Intent next = new Intent(getApplicationContext(),HomeActivity.class);
						startActivity(next);
						finish();
						
						
						
					} catch (com.parse.ParseException e) {
						e.printStackTrace();
					}
					
				} else {
					//*************************SAVE**********************************
					//Do local storage for the alarm
					final ParseObject localAlarm = new ParseObject("localalarm");
					Random rand = new Random();
					int random = rand.nextInt(9999);
					final String localid = Long.toString(random);				
					
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss",Locale.getDefault());
					//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS z",Locale.getDefault());
					long milli = 0;
					try {
						Date date = sdf.parse(cale);
						milli = date.getTime();
					} catch (java.text.ParseException e) {
						e.printStackTrace();
					}
					
					
					final ParseObject saveAlarm = new ParseObject("alarms");
					saveAlarm.put("title",mEventName.getText().toString());
					saveAlarm.put("category",selectedCategory);
					saveAlarm.put("creator",currentUser);
					saveAlarm.put("creatorplain",currentUser.getObjectId().toString());
					saveAlarm.put("media",track);
					saveAlarm.put("recorded",isRecorded);
					
					if(friendsId == null){
							friendsId = new JSONArray();
					}
					friendsId.put(currentUser.getObjectId().toString());
					
					if(friends == null){
						friends = new JSONArray();
					}
					
					saveAlarm.put("connected",friendsId);
					saveAlarm.put("connectedlist",friends.toString());//changed
					saveAlarm.put("comments",0);
					saveAlarm.put("likes",new JSONArray());
					saveAlarm.put("alarmTime", cale);
					saveAlarm.put("alarmtimemilli", milli);
					saveAlarm.put("requestcode",localid);
					saveAlarm.put("selfalarm",includeMe);
					saveAlarm.put("isPrivateAlaramCheck",isPrivateAlaramCheck);
					saveAlarm.put("status", true);
					//saveAlarm.put("alarm_repeat_type", "Once");
					saveAlarm.put("alarm_repeat_type", selectedAlarmType);
					
					
					
					//saveAlarm.saveEventually();
					Intent intent = new Intent(getApplicationContext(),AlaramActivityNew.class);//AlaramActivityNew
					
					
					
					/*PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),Integer.parseInt(localid),intent,PendingIntent.FLAG_CANCEL_CURRENT);
					AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
					am.set(AlarmManager.RTC_WAKEUP,milli, pi);
					
					Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis(calendar.getTimeInMillis());
					calendar.add(Calendar.SECOND, 30);
					if (selectedAlarmType.equalsIgnoreCase("Once")) {
						am.set(AlarmManager.RTC_WAKEUP, milli, pi);
						
					} else if (selectedAlarmType.equalsIgnoreCase("Daily")) {

						calendar.add(Calendar.DAY_OF_WEEK, 1);
						am.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(), pi);
						// am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,calendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pi);
					} else if (selectedAlarmType.equalsIgnoreCase("Weekly")) {

						calendar.add(Calendar.WEEK_OF_MONTH, 1);
						am.set(AlarmManager.RTC_WAKEUP,
								calendar.getTimeInMillis(), pi);
					} else if (selectedAlarmType.equalsIgnoreCase("Monthly")) {

						calendar.add(Calendar.MONTH, 1);
						am.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(), pi);
					} else if (selectedAlarmType.equalsIgnoreCase("Yearly")) {

						calendar.add(Calendar.YEAR, 1);
						am.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(), pi);
					}*/
					
					
					
					if(includeMe){
						//Set self alarm
						
						Bundle tempData = new Bundle();
						tempData.putString("alarm_category", selectedCategory);
						tempData.putString("alarm_title", mEventName.getText().toString());
						tempData.putBoolean("recorded",isRecorded);
						tempData.putInt("alarmrequestcode",Integer.parseInt(localid));
						tempData.putString("alarmpath",track);
						tempData.putString("alarmfriends",friends.toString());//changed
						tempData.putString("alarmfriendsid",friendsId.toString());//added
						//tempData.putString("alarm_repeat_type","Once");
						tempData.putString("alarm_repeat_type", selectedAlarmType);
						
						// Save Alarm Database
						Alarams al = new Alarams();
						al.setCategory(selectedCategory);
						al.setTitle(mEventName.getText().toString());
						if (isRecorded == true) {
							al.setRecorded("true");
						} else {
							al.setRecorded("false");
						}
						al.setRequestcode(localid);
						al.setPath(track);
						al.setFriends(friends.toString());
						al.setFriendsid(friendsId.toString());
						al.setRepeattype(selectedAlarmType);
						al.setAtime(cale);
						al.setMilli(String.valueOf(milli));
						//Toast.makeText(getApplicationContext(),		"millllllli" + milli, Toast.LENGTH_LONG).show();

						DatabaseHandler db = new DatabaseHandler(
								CreateNewAlarmActivity.this);
						db.addAlarm(al);
						intent.putExtras(tempData);
						/*PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),Integer.parseInt(localid),intent,PendingIntent.FLAG_CANCEL_CURRENT);
						AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
						am.set(AlarmManager.RTC_WAKEUP,milli, pi);
						*/
						
						
						PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),Integer.parseInt(localid),intent,PendingIntent.FLAG_CANCEL_CURRENT);
						AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
						am.set(AlarmManager.RTC_WAKEUP,milli, pi);
						
						Calendar calendar = Calendar.getInstance();
						calendar.setTimeInMillis(calendar.getTimeInMillis());
						//calendar.add(Calendar.SECOND, 30);
						if (selectedAlarmType.equalsIgnoreCase("Once")) {
							am.set(AlarmManager.RTC_WAKEUP, milli, pi);
							
						} else if (selectedAlarmType.equalsIgnoreCase("Daily")) {

							calendar.add(Calendar.DAY_OF_WEEK, 1);
							am.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(), pi);
							// am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,calendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pi);
						} else if (selectedAlarmType.equalsIgnoreCase("Weekly")) {

							calendar.add(Calendar.WEEK_OF_MONTH, 1);
							am.set(AlarmManager.RTC_WAKEUP,
									calendar.getTimeInMillis(), pi);
						} else if (selectedAlarmType.equalsIgnoreCase("Monthly")) {

							calendar.add(Calendar.MONTH, 1);
							am.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(), pi);
						}/* else if (selectedAlarmType.equalsIgnoreCase("Yearly")) {

							calendar.add(Calendar.YEAR, 1);
							am.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(), pi);
						}*/
					}
					//alaram repeat type
					
					
					
					
                     saveAlarm.saveInBackground(new SaveCallback() {
						
						@Override
						public void done(com.parse.ParseException e) {
							if(e==null){
								FeedObject object= new FeedObject(1,Integer.parseInt(localid),true,0,0);
								EventBus.getDefault().postSticky(new FeedEvent(object));
								if(isRecorded){
									File mFile = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),track);
									try {
										Log.i("AUDIO RECORD","TRUE");
										//ParseQuery q = new ParseQuery<ParseObject>(subclass)
										int size = (int) mFile.length();
										byte[] bytes = new byte[size];
										BufferedInputStream buf = new BufferedInputStream(new FileInputStream(mFile));
										buf.read(bytes,0,bytes.length);	
										buf.close();
										ParseFile audio = new ParseFile(track,bytes);
										audio.saveInBackground();
										saveAlarm.put("audioclip",audio);
										saveAlarm.saveInBackground();
										
									} catch (FileNotFoundException e1) {
										e1.printStackTrace();
									}catch (IOException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								}
							}
						}
					});
					
					
					/*for (int i = 0; i <= friendsId.length(); ++i) {
					    try {
							JSONObject friend = friends.getJSONObject(i);
							Log.d("asdf3232", friend.get("userid").toString());
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}*/
                 	
                     
                     //For surprise friend
                      updateArray() ;
                     
                   try {
					//New custom notification code
						/*ParseObject notification = new ParseObject("Activity");
						notification.put("creator",currentUser);
						notification.put("followers",friendsId);//friendsId//jarraydifferent//jarraysimilar
						notification.put("reference",saveAlarm);
						notification.put("notificationtype",1);
						notification.put("notificationDone",false);
						notification.put("readers",new JSONArray());
						notification.add("readers", ParseUser.getCurrentUser().getObjectId());
						notification.saveInBackground();//saveEventually();();
*/				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
 					
 					
                     if(jarraysimilar!=null){
                    	                    	 
                    		try {
        						 
                               // parse push implementation
            						
            						for (int j = 0; j < jarraysimilar.length(); j++) {
            							 Log.d("friendsId.get(j).toString()", "--"+jarraysimilar.get(j).toString()+"ParseUser.getCurrentUser().getObjectId().toString())"+"--"+ParseUser.getCurrentUser().getObjectId().toString());
            							if(!jarraysimilar.get(j).toString().equals(ParseUser.getCurrentUser().getObjectId().toString())){
            								
            								ParseQuery pushQuery = ParseInstallation.getQuery();
            								ParsePush push = new ParsePush();
            								//pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId().toString()); // not me
            								pushQuery.whereEqualTo("myuser",jarraysimilar.get(j).toString());
            								//
            								push.setQuery(pushQuery);
            								 JSONObject tempAlarmData = new JSONObject();	
            								
            								 
            								  
            									if (saveAlarm != null)
            									tempAlarmData.put("reference",saveAlarm);            									
            									tempAlarmData.put("notificationtype", 4);
            									push.setData(tempAlarmData);
            									push.sendInBackground(new SendCallback() {
            										   public void done(com.parse.ParseException e) {
            											     if (e == null) {
//            											    	  
            						            					//New custom notification code
            						            					ParseObject notification = new ParseObject("Activity");
            						            					notification.put("creator",currentUser);
            						            					notification.put("followers",jarraysimilar);//friendsId//jarraydifferent//jarraysimilar
            						            					notification.put("reference",saveAlarm);
            						            					notification.put("notificationtype",4);
            						            					notification.put("notificationDone",true);
            						            					notification.put("readers",new JSONArray());
            						            					//notification.put("readers",jarraysimilar);//changed from `new JSONArray()` to jarraysimilar
            						            					//notification.add("readers", ParseUser.getCurrentUser().getObjectId());
            						            					notification.saveInBackground();//saveEventually();();
            											       Log.d("push", "success!");
            											     } else {
            											       Log.d("push", "failure");
            											     }
            											   }
            										
            											 });
            							}
            							}
            							
            					        
            					
            					
            					
            					}catch (Exception e1) {
            							// TODO Auto-generated catch block
            							e1.printStackTrace();
            						} 
            						
            						
            						
                    	 
                    	 
                   	 
                     }
                     
                     if(jarraydifferentunique!=null){                    	 
                   		try {
        						 
            					
            					
            					// parse push implementation
            						
            						for (int j = 0; j < jarraydifferentunique.length(); j++) {
            							 Log.d("jarraydifferentunique.get(j).toString()", "--"+jarraydifferentunique.get(j).toString()+"ParseUser.getCurrentUser().getObjectId().toString())"+"--"+ParseUser.getCurrentUser().getObjectId().toString());
            							if(!jarraydifferentunique.get(j).toString().equals(ParseUser.getCurrentUser().getObjectId().toString())){
            								
            								ParseQuery pushQuery = ParseInstallation.getQuery();
            								ParsePush push = new ParsePush();
            								//pushQuery.whereNotEqualTo("myuser",ParseUser.getCurrentUser().getObjectId().toString()); // not me
            								pushQuery.whereEqualTo("myuser",jarraydifferentunique.get(j).toString());
            								//
            								push.setQuery(pushQuery);
            								 JSONObject tempAlarmData = new JSONObject();	
            								
            								 
            								  
            									if (saveAlarm != null)
            									tempAlarmData.put("reference",saveAlarm);
            									
            									tempAlarmData.put("notificationtype", 1);						
            									tempAlarmData.put("alert","The alarm request is by "+ParseUser.getCurrentUser().getString("fullname"));
            									tempAlarmData.put("action","in.fastrack.app.m.fup.NotificationActivityNew");												
            									push.setData(tempAlarmData);
            									push.sendInBackground(new SendCallback() {
            										   public void done(com.parse.ParseException e) {
            											     if (e == null) {
            											    	 Toast.makeText(CreateNewAlarmActivity.this, "Created Alarm Successfully", Toast.LENGTH_LONG).show();
            											    	//New custom notification code
            						            					ParseObject notification = new ParseObject("Activity");
            						            					notification.put("creator",currentUser);
            						            					notification.put("followers",jarraydifferentunique);//friendsId//jarraydifferent//jarraysimilar
            						            					notification.put("reference",saveAlarm);
            						            					notification.put("notificationtype",1);
            						            					notification.put("notificationDone",true);
            						            					notification.put("readers",jarraydifferentunique);//changed from `new JSONArray()` to jarraydifferentunique
            						            					notification.add("readers", ParseUser.getCurrentUser().getObjectId());
            						            					notification.saveInBackground();//saveEventually();();
            						            					
            											       Log.d("push", "success!");
            											     } else {
            											       Log.d("push", "failure");
            											     }
            											   }
            										
            											 });
            							}
            							}
            							
            					        
            					
            					
            					
            					}catch (Exception e1) {
            							// TODO Auto-generated catch block
            							e1.printStackTrace();
            						} 
            						
            						
            						
                    	 
                    	 
                     }
                     
                     
                     
                     if(jarraydifferent!=null && jarraydifferent.length() == 1 && jarraysimilar.length() == 0){                    	 
                    		try {
              					// parse push implementation
             						
             						for (int j = 0; j < jarraydifferent.length(); j++) {
             							 Log.d("jarraydifferent.get(j).toString()", "--"+jarraydifferent.get(j).toString()+"ParseUser.getCurrentUser().getObjectId().toString())"+"--"+ParseUser.getCurrentUser().getObjectId().toString());
             							if(jarraydifferent.get(j).toString().equals(ParseUser.getCurrentUser().getObjectId().toString())){
             								
             								
             								//New custom notification code
				            					ParseObject notification = new ParseObject("Activity");
				            					notification.put("creator",currentUser);
				            					notification.put("followers",jarraydifferent);//friendsId//jarraydifferent//jarraysimilar
				            					notification.put("reference",saveAlarm);
				            					notification.put("notificationtype",1);
				            					notification.put("notificationDone",true);
				            					notification.put("readers",jarraydifferent);//changed from `new JSONArray()` to jarraydifferentunique
				            					//notification.add("readers", ParseUser.getCurrentUser().getObjectId());
				            					notification.saveInBackground();//saveEventually();();
				            					
				            					//saveAlarm.put("", value)
				            					saveAlarm.addAllUnique("acceptedpeople",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
				            					saveAlarm.addAllUnique("notificationactions",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
				            					saveAlarm.saveInBackground();
             								
             							}
             							}
             							
              					
             					}catch (Exception e1) {
             							// TODO Auto-generated catch block
             							e1.printStackTrace();
             						} 
             						
             						
             						
                     	 
                     	 
                      }
                     
 
					/**Notifications**/
					
					Intent next = new Intent(getApplicationContext(),HomeActivity.class);
					//startActivity(next);
					setResult(RESULT_OK,next);
	        		finish();
				}
	
			}
			
			}else{
				Toast.makeText(getApplicationContext(),"Please Enter the title of the Alarm", Toast.LENGTH_LONG).show();
			}
		return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == 123) {
	        if(resultCode == RESULT_OK){
	        	Bundle ringtones = data.getExtras();
	        	if(ringtones != null){
	        		//Toast.makeText(getApplicationContext(),ringtones.getString("trackname"), Toast.LENGTH_LONG).show();
	        		track = ringtones.getString("trackname");
	        		if(ringtones.getBoolean("builtIn")){
	        			isRecorded = false;
	        			selectRingtone.setText("Ringtone: "+track);
	        		}else{
	        			isRecorded = true;
	        			selectRingtone.setText("Ringtone: Custom");
	        		}
	        	}
	        }
	        if (resultCode == RESULT_CANCELED) {
	            //Write your code if there's no result
	        }
	    }else if(requestCode == 234){
	    	if(resultCode == RESULT_OK){
	    		Bundle friendList = data.getExtras();
	    		if(friendList != null){
	    			//Toast.makeText(getApplicationContext(),friendList.getString("friendsid"), Toast.LENGTH_LONG).show();
	    			try {
	    				finalfriendList.clear();
	    				System.out.println(friendList.toString());
						friends = new JSONArray(friendList.getString("friends"));
						friendsId = new JSONArray(friendList.getString("friendsid"));
						groups = new JSONArray(friendList.getString("groups"));
					//	surprisefriends = new JSONArray(friendList.getString("surprisefriends"));
					//	surprisefriendsid = new JSONArray(friendList.getString("surprisefriendsid"));																											
						//friendCount.setText(String.valueOf(groups.length()) + " Others ");
						
						//friendCount.setText(String.valueOf(surprise.length()) + " Others ");	
				
						/*NEWLY ADDED*/
						for (int i = 0; i < friends.length(); i++) {
					        JSONObject explrObject = friends.getJSONObject(i);
					        Friend f = new Friend();
					        f.setName(explrObject.getString("name"));
					        //f.setSource(explrObject.getString("source"));
					        //f.setSupriseFriend(Boolean.parseBoolean(explrObject.getString("isSupriseFriend")));
					        f.setUserId(explrObject.getString("userid"));
					        f.setUserImage(explrObject.getString("userImage"));					        
					        finalfriendList.add(f);
					    }
						groups = new JSONArray(friendList.getString("groups"));
						for (int i = 0; i < groups.length(); i++) {
					        JSONObject explrObject = groups.getJSONObject(i);
					        Group g = new Group();
					        g.setName(explrObject.getString("groupname"));
					        if(!explrObject.isNull("groupImage"))
					        g.setGroupImagePath(explrObject.getString("groupImage"));
					        g.setFriendList(explrObject.getString("friendslist"));
					        
					        JSONArray friendsarr = new JSONArray(explrObject.getString("friendslist"));
					        Log.d("MyApp","Friends from Group: "+friendsarr.toString());
					        ArrayList<Friend> FList = new ArrayList<Friend>();
					        FList.clear();
							FList.addAll(finalfriendList);
							
					        for (int j = 0; j < friendsarr.length(); j++) {
						        JSONObject explrObj = friendsarr.getJSONObject(j);
						        Friend fg = new Friend();
						        fg.setName(explrObj.getString("name"));
						        //fg.setSource(explrObj.getString("source"));
						        //f.setSupriseFriend(Boolean.parseBoolean(explrObj.getString("isSupriseFriend")));
						        fg.setUserId(explrObj.getString("userid"));
						        fg.setUserImage(explrObj.getString("userImage"));
						        
						       
						        //if(!finalfriendList.contains(fg)) {
						        //	finalfriendList.add(fg);
						        //}
						        
						        if(FList.size() > 0) {
							        for(Friend iterateFriend : FList) {
							        	System.out.println(!iterateFriend.equals(fg));
							        	if(!iterateFriend.equals(fg)) {
							        		finalfriendList.add(fg);
							        		friendsId.put(fg.getUserId());
							        		
							        		
							        	} 
							        }
					        	} else {
					        		finalfriendList.add(fg);
					        		friendsId.put(fg.getUserId());
					        	
					        		
					        	}
						        
						        
						        
						        
						    }
							groupList.add(g);
					    }
						
						System.out.println("finalfriendList : " + finalfriendList.toString());
						/*NEWLY ADDED*/	
						JSONArray myArray = new JSONArray();						
						for(Friend s: finalfriendList){
							
								try {
									JSONObject object = new JSONObject();
									object.put("userid",s.getUserId());
									object.put("name",s.getName());
									//object.put("source",s.getSource());
									object.put("userImage",s.getUserImage());
									myArray.put(object);
									
								} catch (JSONException e) {
									e.printStackTrace();
								}
							
						}
						
						friends = myArray;						
						Log.d("MyApp>>>>>>>>>>>>>>>>>","Data: "+myArray.toString()+"\n"+friends.toString());
						
						friendCount.setText(String.valueOf(finalfriendList.size()) + " Others ");		
						Log.d("MyApp","Friends: "+friends.toString()+"friendsId"+friendsId.toString());
						
						
					} catch (JSONException e) {
						e.printStackTrace();
					}
	    		}
	    	}
	    	if (resultCode == RESULT_CANCELED) {
		      //Write your code if there's no result
		   }
	    } else if(requestCode == 345){
	    	if(resultCode == RESULT_OK){
	    		Bundle selectedCategoryBundle = data.getExtras();
	    		if(selectedCategoryBundle != null){
	    			Toast.makeText(getApplicationContext(),selectedCategoryBundle.getString("selectedCategory"), Toast.LENGTH_SHORT).show();
	    			selectedCategory = selectedCategoryBundle.getString("selectedCategory");
	    			userSelectedCategory();
	    		}
	    	}
	    	if (resultCode == RESULT_CANCELED) {
		      //Write your code if there's no result
		   }
	    }
	}


	
	
	 
	    @Override
	    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
	    	dateText.setText( day + " - " + monthNames[month] + " - " + year);
	        //Toast.makeText(CreateNewAlarmActivity.this, "new date:" + year + "-" + month + "-" + day, Toast.LENGTH_LONG).show();
	        userSelectedTime[0] = year;
	        userSelectedTime[1] = month;
	        userSelectedTime[2] = day;
	    }

	    @Override
	    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
	    	DecimalFormat formatter = new DecimalFormat("00");
	    	
	    	String hoursFormatted = formatter.format(hourOfDay);
	    	timeMin.setText(hoursFormatted);
	 
	    	String minuteFormatted = formatter.format(minute);
	    	timeSec.setText(minuteFormatted);
	    	
	    	if(hourOfDay < 12) {
				ampmshowtext.setText("AM");
			} else {
				ampmshowtext.setText("PM");
			}
	    	
	    	userSelectedTime[3] = hourOfDay;
	    	userSelectedTime[4] = minute;
	        //Toast.makeText(CreateNewAlarmActivity.this, "new time:" + hourOfDay + "-" + minute, Toast.LENGTH_LONG).show();
	    }
	    
	    
	    public void userSelectedCategory(){
	    	if(selectedCategory.equals( "Alarm" )) {
//				eventIconButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.birthday));
				eventIconButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.alarm, 0, 0);
				eventIconButton.setText("Alarm");
			} else if(selectedCategory.equals( "Birthday Party" )) {
//				eventIconButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.birthday));
				eventIconButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.birthday, 0, 0);
				eventIconButton.setText("Birthday Party");
			} else if (selectedCategory.equals( "Wedding Party")) {
				eventIconButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.wedding, 0, 0);
				eventIconButton.setText("Wedding Party");
			} else if (selectedCategory.equals( "Run")) {
				eventIconButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.run, 0, 0);
				eventIconButton.setText("Run");
			} else if (selectedCategory.equals( "Party Time")) {
				eventIconButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.party_time, 0, 0);
				eventIconButton.setText("Party Time");
			} else if (selectedCategory.equals( "Movie Night")) {
				eventIconButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.movie_night, 0, 0);
				eventIconButton.setText("Movie Night");
			} else if (selectedCategory.equals("Lunch")) {
				eventIconButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.lunch, 0, 0);
				eventIconButton.setText("Lunch");
			} else if (selectedCategory.equals("Ride")) {
				eventIconButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.let_ride, 0, 0);
				eventIconButton.setText("Ride");
			} else if (selectedCategory.equals("Dinner")) {
				eventIconButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.dinner, 0, 0);
				eventIconButton.setText("Dinner");
			} else if (selectedCategory.equals("Brunch")) {
				eventIconButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.brunch, 0, 0);
				eventIconButton.setText("Brunch");
			} else if (selectedCategory.equals("Breakfast")) {
				eventIconButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.breakfast, 0, 0);
				eventIconButton.setText("Breakfast");
			}
	    }

	public void updateArray() {

		
		jarraydifferent = new JSONArray();
		jarraysimilar = new JSONArray();
		jarraydifferentunique  = new JSONArray();

		if (friendsId != null) {
			Log.i("friendsId", "friendsId............."+friendsId.toString());
			for (int i = 0; i < friendsId.length(); i++) {
				try {

					if (arraynew != null && arraynew.length()!=0) {
						Log.i("arraynew", "arraynew.............."+arraynew.toString());
						for (int j = 0; j < arraynew.length(); j++) {
							try {
								if (arraynew.get(j).toString().equals(friendsId.get(i).toString())) {

									jarraysimilar.put(friendsId.get(i).toString());

								}
								else{
									
									jarraydifferent.put(friendsId.get(i).toString());
								}

							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
					else{
						System.out.println("in else......................"+jarraydifferent.toString());
						jarraydifferent.put(friendsId.get(i).toString());
						
					}
					
					System.out.println("111111......................"+jarraydifferent.toString()+"222222222222------------------------\n"+jarraysimilar.toString());
				}

				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		
		if (jarraydifferent != null) {
			
			try {
				for (int j = 0; j < jarraydifferent.length(); j++) {
					
						if (jarraydifferent.get(j).toString().equals(ParseUser.getCurrentUser().getObjectId().toString())) {

						}
						else{
							
							jarraydifferentunique.put(jarraydifferent.get(j).toString());
						}

						
				
}
				
				System.out.println("33333333333......................"+jarraydifferentunique.toString());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}
		
		

	}

}
