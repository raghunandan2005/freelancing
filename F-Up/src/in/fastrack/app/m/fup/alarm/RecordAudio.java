package in.fastrack.app.m.fup.alarm;


import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.config.AppConfig;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class RecordAudio extends Activity implements OnClickListener{
	
	private AppConfig _config;
	ProgressBar mProgressBar;
	int mProgress,recordTime;
	CountDownTimer mTimer;
	Bundle bundle;
	private boolean isRecoding,isExtra = false;
	MediaRecorder mRecorder = null;
	MediaPlayer mPlayer = null;
	TextView recordtimeframe;
	ImageView bRecord,bPreview;
	Button bUse,bRecordAgain;
	String tempAudioName,recordFilePath = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.record_ringtone);
		final ActionBar bar = getActionBar();
		bar.hide();
		_config = new AppConfig(getApplicationContext());
		recordTime = _config.getRecordDuration() * 1000;
		mProgressBar = (ProgressBar) findViewById(R.id.pbRecordTime);
		recordtimeframe = (TextView) findViewById(R.id.tvRecordTimeFrame);
		bRecord = (ImageView) findViewById(R.id.bRecord);
		bPreview = (ImageView) findViewById(R.id.bPreview);
		bRecordAgain = (Button) findViewById(R.id.bRecordAgain);
		bUse = (Button) findViewById(R.id.bUse);
		bRecord.setOnClickListener(this);
		bRecordAgain.setOnClickListener(this);
		bPreview.setOnClickListener(this);
		bUse.setOnClickListener(this);
		mProgressBar.setMax(_config.getRecordDuration());
		recordtimeframe.setText("00");
	}
	
	public void initRecorder(){
		if(mRecorder != null){
			mRecorder.release();
		}
		mRecorder = new MediaRecorder();
		mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
	}
	
	public void startTimer(){
		mTimer = new CountDownTimer(recordTime,1000) {
			
			@Override
			public void onTick(long millisUntilFinished) {
				mProgress = _config.getRecordDuration() - (int)(long) millisUntilFinished/1000 ;
				String tempNumber = String.format(Locale.getDefault(),"%02d",mProgress);
				mProgressBar.setProgress(mProgress);
				recordtimeframe.setText(tempNumber);
			}
			
			@Override
			public void onFinish() {
				mProgressBar.setProgress(mProgress+1);
				Toast.makeText(getApplicationContext(),"Audio recording completed. Preview!", Toast.LENGTH_LONG).show();
				stopRecording();
			}
		}.start();
	}
	
	private void startRecording(){
		initRecorder();
		isRecoding = true;
		bRecord.setVisibility(View.VISIBLE);
		bRecordAgain.setVisibility(View.GONE);
		bUse.setVisibility(View.GONE);
		recordFilePath = getFilePath().getAbsolutePath();
		mRecorder.setOutputFile(recordFilePath);
		try {
			Toast.makeText(getApplicationContext(),"Start Speaking...", Toast.LENGTH_LONG).show();
			mProgressBar.setMax(_config.getRecordDuration());
			//bRecord.setText("Stop Recording");
			mRecorder.prepare();
			mRecorder.start();
			startTimer();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void stopRecording(){
		if(mPlayer != null){
			mPlayer.release();
			mPlayer = null;
		}
		mTimer.cancel();
		mRecorder.stop();
		mRecorder.release();
		mRecorder = null;
		isRecoding = false;
		//bRecord.setText("Record New");
		bRecord.setVisibility(View.GONE);
		bPreview.setVisibility(View.VISIBLE);
		bRecordAgain.setVisibility(View.VISIBLE);
		bUse.setVisibility(View.VISIBLE);
		mProgress = 0;
		mProgressBar.setProgress(mProgress);
		Toast.makeText(getApplicationContext(),"Recording Completed. Preview!", Toast.LENGTH_LONG).show();
	}
	
	private File getFilePath(){
		tempAudioName = "audio_"+System.nanoTime()+".mp3";
		Boolean cnt = true;
		File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),_config.getRecordAudioPath());
		if(!f.exists()){
			if(!f.mkdirs()){
				cnt = false;
				Toast.makeText(getApplicationContext(),"Unable to save the audio", Toast.LENGTH_LONG).show();
			}
		}
		if(cnt){
			File a = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),tempAudioName);
			return a;
		}
		return f;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.record, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.bRecord:
			if(!isRecoding){
				startRecording();
			}else{
				stopRecording();
			}
		break;
		case R.id.bUse:
			if(mPlayer !=null){
				mPlayer.stop();
				mPlayer.reset();
				mPlayer = null;
			}
			Intent finalIntent = new Intent();
			finalIntent.putExtra("trackname",tempAudioName);
			setResult(Activity.RESULT_OK,finalIntent);
			finish();
		break;
		case R.id.bRecordAgain:
			if(!isRecoding){
				startRecording();
			}else{
				stopRecording();
			}
		break;
		case R.id.bPreview:
			mPlayer = new MediaPlayer();
			try {
				mPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
				recordtimeframe.setText("00");
				mPlayer.setDataSource(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath()+tempAudioName);
				mPlayer.prepare();
				mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
					
					@Override
					public void onPrepared(MediaPlayer mp) {
						int duration = mp.getDuration();
						mProgressBar.setMax(duration);
						mPlayer.start();
						mProgressBar.postDelayed(runProgressBar,30);
					}
				});
				//mPlayer.start();
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		break;
		}
	}
	
	private Runnable runProgressBar = new Runnable() {
		
		@Override
		public void run() {
			if(mPlayer != null){
				mProgressBar.setProgress(mPlayer.getCurrentPosition());
				if(mPlayer.isPlaying()){
					recordtimeframe.setText(String.format(Locale.getDefault(),"%02d",Math.round(mPlayer.getCurrentPosition()/1000)));
					mProgressBar.postDelayed(runProgressBar,30);
				}else{
					Log.v("END_MEDIA","DONE");
				}
			}
		}
	};
	
	@Override
	protected void onPause() {
		super.onPause();
		if(mRecorder != null){
			mRecorder.release();
		}
		if(mPlayer != null){
			//mProgressBar.removeCallbacks();
			mPlayer.stop();
			mPlayer.release();
			mPlayer = null;
		}
	}
	
	

}
