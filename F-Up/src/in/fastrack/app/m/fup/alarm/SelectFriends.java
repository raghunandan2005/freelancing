package in.fastrack.app.m.fup.alarm;

import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.adapter.PeopleAdapter;
import in.fastrack.app.m.fup.adapter.PeopleAdapter.PeopleListener;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.NewsFeed;
import in.fastrack.app.m.fup.model.People;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class SelectFriends extends Activity implements PeopleListener {
	
	private AppConfig _config;
	private Preloader preloader;
	ParseUser currentUser;
	int FB_REQUEST_ID = 900;
	static PeopleAdapter mAdapter = null;
	ProgressBar mPbar;
	EditText mSearch;
	ListView lv = null;
	private JSONArray friends = new JSONArray();
	private JSONArray friendsid = new JSONArray();
	
	final ArrayList<People> sList = new ArrayList<People>();
	String friendsListString = null;
	NewsFeed alarmDetails;
	ArrayList<String> selectedFriendsid = new ArrayList<String>(); 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//If selected Friends are there
		/*if(getIntent().getSerializableExtra("alarm_details") != null) {
			alarmDetails = (NewsFeed) getIntent().getSerializableExtra("alarm_details");
			Log.d("AlarmDetails", alarmDetails.toString());
			try {
				friends = new JSONArray(alarmDetails.getAlarmFriends());
				 for (int i = 0; i < friends.length(); i++) {
				        JSONObject explrObject = friends.getJSONObject(i);
				        selectedFriendsid.add(explrObject.getString("userid"));
				    }
				 System.out.println(selectedFriendsid.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}*/
		
		if(getIntent().getStringExtra("friends") != null) { 
			try {
				friends = new JSONArray(getIntent().getStringExtra("friends"));
				 for (int i = 0; i < friends.length(); i++) {
				        JSONObject explrObject = friends.getJSONObject(i);
				        selectedFriendsid.add(explrObject.getString("userid"));
				    }
				 System.out.println(selectedFriendsid.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		
		ActionBar bar = getActionBar();
		bar.setTitle(this.getString(R.string.selectfriends));
		
		setContentView(R.layout.friends_layout);
		
		preloader = new Preloader();
		
		configParse();
		
		mSearch = (EditText) findViewById(R.id.inputSearch);
		mPbar = (ProgressBar) findViewById(R.id.pbPhoneBook);
		lv = (ListView) findViewById(R.id.socialList);
		
		mSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
				String text = mSearch.getText().toString();
//				ArrayList<Friend> searchedList = mAdapter.search(text);
//				mAdapter.clear();
//				mAdapter.addAll(searchedList);
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				
			}
		});
		
		preloader.startPreloader(this,"Fetching Your Friends");
		
		

		//Activity
		if (!Util.isNetworkAvailable(SelectFriends.this)) {	
			try {
				preloader.stopLoading();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DialogFactory.getBeaconStreamDialog1(SelectFriends.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
		} else 
		
		{
		
		
		final Boolean[] parms = {false};
		new LoadSocialAsync().execute(parms);
		}
	}
	
	public void configParse(){
		_config = new AppConfig(this);
		currentUser = ParseUser.getCurrentUser();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.action_menu_select, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	public ParseUser getTheUser(String userid){
		ParseUser user = null;
		ParseQuery<ParseUser> query1 = ParseUser.getQuery();
		query1.whereEqualTo("objectId", userid);
		List<ParseUser> objects;
		try {
			objects = query1.find();
           // The query was successful.
       		for(ParseUser obj : objects) {
       			user = obj;
       			break;
       		}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}

	public class LoadSocialAsync extends AsyncTask<Boolean, Void, ArrayList<Friend>> {
		ArrayList<Friend> friendList = new ArrayList<Friend>();
		@Override
		protected ArrayList<Friend> doInBackground(Boolean... params) {
			
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
			query.whereEqualTo("user", ParseUser.getCurrentUser());
			try {
				List<ParseObject> objects = query.find();
				if(objects.size() > 0) {
					for(ParseObject obj : objects){
						JSONArray friendsList = obj.getJSONArray("connectedF");
						if(friendsList != null)  {
							for(int j = 0; j < friendsList.length(); j++) {
								try {
									String userid = friendsList.getString(j);
									if(userid != null) {
										ParseUser user = getTheUser(userid);
										
										Friend f = new Friend();
								        f.setName(user.getString("fullname"));
								        f.setUserId(userid);
								        String imageUrl = "image";
								        String t = null;
								        ParseFile p = user.getParseFile("profilethumbnail");
										if(p!=null){
											String url = p.getUrl();
											imageUrl = url;
										}else if(user.get("facebookid") != null){
											t = user.get("facebookid").toString();
											if((t!=null)||(t!="")){
												imageUrl = "https://graph.facebook.com/"
														+ user.get("facebookid").toString()
														+ "/picture/?type=square";
											}
										}
								        f.setUserImage(imageUrl);
								        if (selectedFriendsid.contains(userid)) {
								            f.setUserSelected(true);
								        } 
								        friendList.add(f);
									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			/*ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
			query.whereEqualTo("user", ParseUser.getCurrentUser());
			try {
				List<ParseObject> objects = query.find();
				if(objects.size() > 0) {
					for(ParseObject obj : objects){
						friendsListString = obj.getString("friendsList");
						try {	
							JSONArray jsonArray = new JSONArray(friendsListString);
						    for (int i = 0; i < jsonArray.length(); i++) {
						        JSONObject explrObject = jsonArray.getJSONObject(i);
						        Friend f = new Friend();
						        f.setName(explrObject.getString("name"));
						        f.setSource(explrObject.getString("source"));
						        f.setSupriseFriend(Boolean.parseBoolean(explrObject.getString("isSupriseFriend")));
						        f.setUserId(explrObject.getString("userId"));
						        f.setUserImage(explrObject.getString("userImage"));
						        if (selectedFriendsid.contains(explrObject.getString("userId"))) {
						            f.setUserSelected(true);
						        } 
						        friendList.add(f);
						    }
						} catch (JSONException e1) {
							e1.printStackTrace();
						}  
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}*/
			return friendList;
		}

		@Override
		protected void onPostExecute(ArrayList<Friend> result) {
			super.onPostExecute(result);
			updateList(result);
		}
		
	}
	
	public void updateList(ArrayList<Friend> res){
		mAdapter = new PeopleAdapter(getApplicationContext(),R.layout.people_item,res,this, friends);
		lv.setAdapter(mAdapter);
		lv.setVisibility(View.VISIBLE);
		mSearch.setVisibility(View.VISIBLE);
		preloader.stopLoading();
		Log.i("list", res.toString());
	}

	@Override
	public void getPeopleAdded(JSONArray list, JSONArray friendsId) {
		Log.d("MyApp","Friends: "+list.toString());
		friends = list;
		friendsid = friendsId;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.action_next:
			if(friends != null){
				//friends found
				Intent exit = new Intent();
        		Bundle data = new Bundle();
        		data.putString("friendsid",friendsid.toString());
        		data.putString("friends",friends.toString());
        		exit.putExtras(data);
        		setResult(RESULT_OK,exit);
        		finish();
			}
		return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
	
	
}
