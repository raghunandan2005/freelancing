package in.fastrack.app.m.fup.alarm;

import java.util.ArrayList;
import java.util.Locale;

import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.adapter.PlaylistAdapter;
import in.fastrack.app.m.fup.adapter.PlaylistAdapter.PlaylistAudio;
import in.fastrack.app.m.fup.adapter.PlaylistAdapter.PlaylistListener;
import in.fastrack.app.m.fup.lib.CustomAudio;
import in.fastrack.app.m.fup.lib.CustomAudio.MediaListener;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Playlist;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class SelectMusic extends Activity implements PlaylistListener,PlaylistAudio,MediaListener,OnClickListener {
	
	private Bundle extras = new Bundle();
	private PlaylistAdapter mAdapter = null;
	private ListView lv;
	private CustomAudio mAudio = new CustomAudio();
	Button mRecordAudio;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final ActionBar bar = getActionBar();
		bar.setTitle("Choose Ringtone");
		setContentView(R.layout.create_alarm_step2);
		lv = (ListView) findViewById(R.id.listRingtones);
		
		mRecordAudio = (Button) findViewById(R.id.bRecordAudio);
		mRecordAudio.setOnClickListener(this);
		
		loadTracks();
		
		
	}
	
	private void loadTracks(){
		ArrayList<Playlist> myList = new ArrayList<Playlist>();
		Playlist items = null;
		int themeId = getResources().getIdentifier("playlist","array",getPackageName());
		String[] themeList = getResources().getStringArray(themeId);
		for(int i=0;i<themeList.length;i++){
			items = new Playlist(themeList[i], Util.stringtolower(themeList[i]), false);
			myList.add(items);
		}
		
		mAdapter = new PlaylistAdapter(this,R.layout.playlist_item,myList,this,this);
		lv.setAdapter(mAdapter);
		
	}
	
	@Override
	public void onTrackSelected(String trackName, String track) {
		//User has selected a track return it back
		Intent exit = new Intent();
		Bundle data = new Bundle();
		data.putBoolean("builtIn",true);
		data.putString("trackname",trackName);
		exit.putExtras(data);
		setResult(RESULT_OK,exit);
		finish();
	}

	@Override
	protected void onStop() {
		super.onStop();
		mAudio.stopMusic();
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		mAudio.stopMusic();
	}

	@Override
	public void onPlaying(String trackName,int trackId, boolean builtIn, boolean loop,boolean play) {
		if(play){
			mAudio.playMedia(getApplicationContext(),trackName,trackId, builtIn, loop,this);
		}else{
			mAudio.stopMusic();
		}
	}

	@Override
	public void playerStopped(String audioPath, int audioId, boolean builtIn) {
		//mAudio.stopMusic();
		if(builtIn){
			Toast.makeText(getApplicationContext(),Integer.toString(audioId),Toast.LENGTH_LONG).show();
			mAdapter.resetButton(false);
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.bRecordAudio:
			Intent intent = new Intent(getApplicationContext(),RecordAudio.class);
			startActivityForResult(intent,90);
		break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 90) {
	        if(resultCode == RESULT_OK){
	        	Bundle audio = data.getExtras();
	        	if(audio != null){
	        		Intent exit2 = new Intent();
	        		Bundle data2 = new Bundle();
	        		data2.putBoolean("builtIn",false);
	        		data2.putString("trackname",audio.getString("trackname"));
	        		exit2.putExtras(data);
	        		setResult(RESULT_OK,exit2);
	        		finish();
	        	}
	        }
		}
	}
	
	
	
	
	
	

}
