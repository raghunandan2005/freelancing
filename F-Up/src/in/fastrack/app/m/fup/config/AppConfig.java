package in.fastrack.app.m.fup.config;

import in.fastrack.app.m.fup.SettingsActivity;
import in.fastrack.app.m.fup.SettingsFragment;
import android.app.Activity;
import android.app.AlarmManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class AppConfig {
	public static final String RECORDED_AUDIO_PATH = "audio_path";
	public static final String RECORD_DURATION = "record_duration";
	private static final String APP_SHARED_PREFS = AppConfig.class.getSimpleName();
	public static final String NOTIFICATION_REFRESH_DEFAULT = "1";
	private SharedPreferences _sharedPrefs;
    private Editor _prefsEditor;
    
    public AppConfig(Context context) {
    	this._sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
        this._prefsEditor = _sharedPrefs.edit();
    }
    
    public String[] getAppKeys(String keytype){
    	if(keytype.equals("parse")){
    		String data = _sharedPrefs.getString("parsekey","u4JPRU5uZb5alBa6xpGM16WqbiuX7b7eVzeiVh2x,IznqUWNXeemZY8fj3rUy8AExfpMHMyl9LjlgloJG");
    		return data.split(",");
    	}
    	if(keytype.equals("facebook")){
    		//String data = _sharedPrefs.getString("appid","569597773158412,0");
    		String data = _sharedPrefs.getString("appid","257191154452582,0");
    		return data.split(",");
    	}
    	return null;
    }
    
    public Boolean getFbLink(){
    	return _sharedPrefs.getBoolean("isfblink",false);
    }
    
    public Boolean getPhoneState(){
    	return _sharedPrefs.getBoolean("phoneState", false);
    }

	public void setPhoneState(Boolean phoneState){
		_prefsEditor.putBoolean("phoneState",phoneState);
		_prefsEditor.commit();
	}
    
    public Boolean getTNC(){
    	return _sharedPrefs.getBoolean("tnc", false);
    }
    
    public void setTNC(boolean tnc){
    	_prefsEditor.putBoolean("tnc",tnc);
    	_prefsEditor.commit();
    }
    
//    public long getNotificationRefresh(){
//    	String notificationTime = _sharedPrefs.getString(SettingsFragment.KEY_NOTIFICATION_REFRESH_PREFERENCE,NOTIFICATION_REFRESH_DEFAULT);
//    	if(notificationTime.equals("1")){
//    		return 60000L;
//    	}else if(notificationTime.equals("2")){
//    		return 120000L;
//    	}else if(notificationTime.equals("3")){
//    		return 180000L;
//    	}else if(notificationTime.equals("5")){
//    		return 300000L;
//    	}else if(notificationTime.equals("15")){
//    		return 900000L;
//    	}else if(notificationTime.equals("30")){
//    		return AlarmManager.INTERVAL_HALF_HOUR;
//    	}else if(notificationTime.equals("60")){
//    		return AlarmManager.INTERVAL_HOUR;
//    	}else{
//    		return 0L;
//    	}
//    }
    
    public void setNotificationRefresh(int time){
    	_prefsEditor.putInt("notificationrefresh",time);
    	_prefsEditor.commit();
    }
    
    public Integer getSnooze(){
    	return _sharedPrefs.getInt("snooze",1);
    }
    public void setSnooze(int time){
    	_prefsEditor.putInt("snooze",time);
    	_prefsEditor.commit();
    }
    
    public Boolean getDownloadOnWiFi(){
    	return _sharedPrefs.getBoolean("downloadonwifi",true);
    }
    
    public void setFbLink(Boolean isfblink){
    	_prefsEditor.putBoolean("isfblink",isfblink);
    	_prefsEditor.commit();
    }
    
    public Boolean getInviteMode(){
    	return _sharedPrefs.getBoolean("is_invite",true);
    }
    
    public void setInviteMode(Boolean state){
    	_prefsEditor.putBoolean("is_invite",state);
    	_prefsEditor.commit();
    }
    
    public String getRecordAudioPath(){
    	return _sharedPrefs.getString(RECORDED_AUDIO_PATH,"/Fup/Media/Audio/");
    }
    
    public int getRecordDuration(){
    	return _sharedPrefs.getInt(RECORD_DURATION,30);
    }
    
    public void setRecordAudioPath(String newaudiopath){
    	if(!newaudiopath.equals("")){
    		_prefsEditor.putString(RECORDED_AUDIO_PATH, newaudiopath);
        	_prefsEditor.commit();
    	}
    }
    
    public boolean getDownloadMode(){
    	String downloadvia = _sharedPrefs.getString(SettingsFragment.KEY_MEDIA_DOWNLOAD_PREFERENCE,"wifi");
    	if(downloadvia.equals("wifi")){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public void setRecordDuration(int recduration){
    	_prefsEditor.putInt(RECORD_DURATION, recduration);
    	_prefsEditor.commit();
    }
    
    public static void initModule(Context mContext) {
        mInstance = new AppConfig(mContext);
    }
    
    public static AppConfig get() {
        return mInstance;
    }

    private static AppConfig mInstance = null;
}
