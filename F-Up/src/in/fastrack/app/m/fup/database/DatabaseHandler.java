package in.fastrack.app.m.fup.database;

 
import in.fastrack.app.m.fup.Alarams;

import java.util.ArrayList;
import java.util.List;
 



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
 
public class DatabaseHandler extends SQLiteOpenHelper {
 
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "fupalaram";
 
    // Contacts table name
    private static final String TABLE_ALARM = "alarams";
 
    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String ALARM_CATEGORY = "alarm_category";
    private static final String ALARM_TITLE = "alarm_title";
    private static final String ALARM_RECORDED = "recorded";
    private static final String ALARM_REQUESTCODE = "alarmrequestcode";
    private static final String ALARM_PATH = "alarmpath";
    private static final String ALARM_friends = "alarmfriends";
    private static final String ALARM_friendsid = "alarmfriendsid";
    private static final String ALARM_REPEAT_TYPE = "alarm_repeat_type";
    private static final String ALARM_TIME_MILLI = "alarmtimemilli";
   // private static final String KEY_PH_NO = "phone_number";
 
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
    	
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_ALARM + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + ALARM_CATEGORY + " TEXT,"
                + ALARM_TITLE + " TEXT,"
                + ALARM_RECORDED + " TEXT,"
                + ALARM_REQUESTCODE + " TEXT,"
                + ALARM_PATH + " TEXT,"
                + ALARM_friends + " TEXT,"
                + ALARM_friendsid + " TEXT,"
                + ALARM_TIME_MILLI + " TEXT,"
                + ALARM_REPEAT_TYPE + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
        
    }
 
    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ALARM);
 
        // Create tables again
        onCreate(db);
    }
 
    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */
 
     // Adding new Alarm
     public void addAlarm(Alarams alarm) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        
        values.put(ALARM_TITLE, alarm.getTitle()); 
        Log.i("Alaram Title While Inserting to db",alarm.getTitle().toString());
        values.put(ALARM_RECORDED, alarm.getRecorded()); 
        values.put(ALARM_REQUESTCODE , alarm.getRequestcode());
        values.put(ALARM_PATH,alarm.getPath());
        values.put(ALARM_friends,alarm.getFriends());
        values.put(ALARM_friendsid, alarm.getFriendsid());
        values.put(ALARM_TIME_MILLI, alarm.getMilli());
        Log.i("..............Time.....",""+alarm.getMilli());
        values.put(ALARM_REPEAT_TYPE, alarm.getRepeattype());
 
        // Inserting Row
        db.insert(TABLE_ALARM, null, values);
        db.close(); // Closing database connection
    }
     
     public List<Alarams> getAllAlarams() {
    	    List<Alarams> alarmList = new ArrayList<Alarams>();
    	    // Select All Query
    	    String selectQuery = "SELECT  * FROM " + TABLE_ALARM;
    	 
    	    SQLiteDatabase db = this.getWritableDatabase();
    	    Cursor cursor = db.rawQuery(selectQuery, null);
    	 
    	    // looping through all rows and adding to list
    	    if (cursor.moveToFirst()) {
    	        do {
    	            Alarams alarm = new Alarams();
    	          //  cursor.getColumnIndex(columnName)
    	            alarm.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_ID))));
    	            alarm.setCategory(cursor.getString(cursor.getColumnIndex(ALARM_CATEGORY)));
    	            alarm.setTitle(cursor.getString(cursor.getColumnIndex(ALARM_TITLE)));
    	            Log.i("Alarm Title is ",cursor.getString(cursor.getColumnIndex(ALARM_TITLE)));
    	            alarm.setRecorded(cursor.getString(cursor.getColumnIndex(ALARM_RECORDED)));
    	            alarm.setRequestcode(cursor.getString(cursor.getColumnIndex(ALARM_REQUESTCODE)));
    	            alarm.setPath(cursor.getString(cursor.getColumnIndex(ALARM_PATH)));
    	            alarm.setFriends(cursor.getString(cursor.getColumnIndex(ALARM_friends)));
    	            alarm.setFriendsid(cursor.getString(cursor.getColumnIndex(ALARM_friendsid)));
    	            alarm.setMilli(cursor.getString(cursor.getColumnIndex(ALARM_TIME_MILLI)));
    	            alarm.setRepeattype(cursor.getString(cursor.getColumnIndex(ALARM_REPEAT_TYPE)));
    	           
    	            // Adding alarm to list
    	            alarmList.add(alarm);
    	        } while (cursor.moveToNext());
    	    }
    	 
    	    // return alarm list
    	    return alarmList;
    	}
 
     public int updateAlarm(Alarams alarm) {
         SQLiteDatabase db = this.getWritableDatabase();
  
         ContentValues values = new ContentValues();
         values.put(ALARM_TITLE, alarm.getTitle()); 
         Log.i("Alaram Title While Inserting to db",alarm.getTitle().toString());
         values.put(ALARM_RECORDED, alarm.getRecorded()); 
         values.put(ALARM_REQUESTCODE , alarm.getRequestcode());
         values.put(ALARM_PATH,alarm.getPath());
         values.put(ALARM_friends,alarm.getFriends());
         values.put(ALARM_friendsid, alarm.getFriendsid());
         values.put(ALARM_TIME_MILLI, alarm.getMilli());
         Log.i("............Updating Time.....",""+alarm.getMilli());
         values.put(ALARM_REPEAT_TYPE, alarm.getRepeattype());
  

         // updating row
         return db.update(TABLE_ALARM, values, KEY_ID + " = ?",
                 new String[] { String.valueOf(alarm.getId()) });
     }
 
     // Deleting single alarm
     public void deleteAlarm(int alarmrequestid) {
         SQLiteDatabase db = this.getWritableDatabase();
         db.delete(TABLE_ALARM, KEY_ID + " = ?",
                 new String[] { String.valueOf(alarmrequestid) });
         db.close();
     }
     
     //Deleting all alarms before current time
     
     public void deleteAlarmmilli(long alarmmilli) {
         SQLiteDatabase db = this.getWritableDatabase();
         db.execSQL("DELETE FROM " + TABLE_ALARM + " WHERE " + ALARM_TIME_MILLI  + "<" +  String.valueOf(alarmmilli));
         //db.delete(TABLE_ALARM, ALARM_TIME_MILLI + " = ?",
                // new String[] { String.valueOf(alarmmilli) });
         db.close();
     }
}