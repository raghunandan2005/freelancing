package in.fastrack.app.m.fup.dialogs;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment implements OnDateSetListener {
	private DatePickedListener mListen;
	private int mYear,mMonth,mDay;
	final Calendar cal = Calendar.getInstance();

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		cal.set(Calendar.YEAR,1986);
		mYear = cal.get(Calendar.YEAR);
		mMonth = cal.get(Calendar.MONTH);
		mDay = cal.get(Calendar.DAY_OF_MONTH);
		return new DatePickerDialog(getActivity(),this,mYear,mMonth,mDay);
	}

	@Override
	public void onDateSet(DatePicker view, int year, int month,int day) {
		mListen.onDatePicked(year, month, day);
	}
	
	public void onAttach(Activity activity){
		super.onAttach(activity);
		mListen = (DatePickedListener) activity;
	}
	
	public static interface DatePickedListener{
		public void onDatePicked(int year,int month,int day);
	}
}
