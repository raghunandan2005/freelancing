package in.fastrack.app.m.fup.dialogs;

import in.fastrack.app.m.fup.R;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TimePicker;

public class DateTimeFragment extends DialogFragment{

	private DateTimePickedListener mListen;
	private int year,month,day,hour,min;
	private int[] fullDateTime;
	private String humanDateTime;
	String monthNames[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	boolean isFirstTime = true;
	String ampm;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Calendar calendar = Calendar.getInstance();
		final Calendar c = new GregorianCalendar();
		year = calendar.get(Calendar.YEAR);
		month = calendar.get(Calendar.MONTH);
		day = calendar.get(Calendar.DAY_OF_MONTH);
		hour = calendar.get(Calendar.HOUR_OF_DAY);
		min = calendar.get(Calendar.MINUTE);
		//updateDateTime();
		View v = getActivity().getLayoutInflater().inflate(R.layout.dialog_datetime,null);
		
		DatePicker datePicker = (DatePicker)v.findViewById(R.id.datePicker);
		TimePicker timePicker = (TimePicker)v.findViewById(R.id.timePicker);
		//datePicker.
		
		datePicker.init(year, month, day, new OnDateChangedListener() {
			
			public void onDateChanged(DatePicker view, int year, int month,int day) {
				Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, day);
                if (c.after(newDate)) {
                    view.init(DateTimeFragment.this.year,DateTimeFragment.this.month,DateTimeFragment.this.day, this);
                }else{
                	DateTimeFragment.this.year = year;
                	DateTimeFragment.this.month = month;
                	DateTimeFragment.this.day = day;
                }
				
            	//updateDateTime();
			}
		});
		
		timePicker.setCurrentHour(hour);
        timePicker.setCurrentMinute(min);
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            public void onTimeChanged(TimePicker view, int hour, int min) {
            	DateTimeFragment.this.hour = hour;
            	DateTimeFragment.this.min = min;
            	//updateDateTime();
            }
        });
	
        return new AlertDialog.Builder(getActivity())
        .setView(v).setTitle("Select Date and Time").setPositiveButton("Set", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //sendResult(Activity.RESULT_OK);
            	updateDateTime();
            	//mListen.onDateTimePicked(year, month, day, hour, min,fullDateTime,humanDateTime);
            }
         }).create();
	}
	
	public void updateDateTime() {
		
		//if(hour <=)
		if(hour < 12){
			ampm = "AM";
		}else{
			ampm = "PM";
		}
		humanDateTime = String.format("%02d %s, %d at %02d:%02d %s",day,getMonth(month),year,hour,min,ampm);
		fullDateTime = new int[]{year, month, day, hour, min};
        mListen.onDateTimePicked(year, month, day, hour, min,fullDateTime,ampm,humanDateTime);
        Log.i("OP",toString());
    }
	
	public String getMonth(int m){
		return monthNames[m];
	}
	
	public void onAttach(Activity activity){
		super.onAttach(activity);
		mListen = (DateTimePickedListener) activity;
	}
	
	public static interface DateTimePickedListener{
		//Log.i("OP",toString());
		public void onDateTimePicked(int year,int month,int day, int hour,int min,int[] dateTime,String AMPM,String humanReadable);
	}

	@Override
	public String toString() {
		return "DateTimeFragment [mListen=" + mListen + ", year=" + year
				+ ", month=" + month + ", day=" + day + ", hour=" + hour
				+ ", min=" + min + ", fullDateTime="
				+ Arrays.toString(fullDateTime) + ", humanDateTime="
				+ humanDateTime + ", monthNames=" + Arrays.toString(monthNames)
				+ ", isFirstTime=" + isFirstTime + ", ampm=" + ampm + "]";
	}
	
	

	
	
	
	
}
