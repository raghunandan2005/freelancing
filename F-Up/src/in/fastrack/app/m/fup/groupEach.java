package in.fastrack.app.m.fup;

import in.fastrack.app.m.fup.adapter.GroupFriendAdapter;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.lib.DialogFactory;
import in.fastrack.app.m.fup.lib.Preloader;
import in.fastrack.app.m.fup.lib.RoundedTransform;
import in.fastrack.app.m.fup.lib.Util;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.Group;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

public class groupEach extends Activity{
	private Group group = null;
	private Preloader preloader;
	private AppConfig _config;
	ImageButton groupImage,addDeleteFriends = null;
	TextView groupName,groupMembersCount = null;
	JSONArray friendsListJsonArray = null;
	ListView lv = null;
	String friendsListString = null;
	ParseUser currentUser;
	GroupFriendAdapter mAdapter = null;
	
	private JSONArray friendsId = null;
	private JSONArray friends = null;
	private String GroupCreator;
	public void configParse(){
		_config = new AppConfig(this);
		currentUser = ParseUser.getCurrentUser();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.group_each);
		
		ActionBar bar = getActionBar();
		bar.setTitle("Group");
		
		preloader = new Preloader();
		configParse();
		
		if(getIntent().getSerializableExtra("group") != null) {
			group = (Group) getIntent().getSerializableExtra("group");
		}
		
		groupImage = (ImageButton) findViewById(R.id.epGroupProfileImage);
		groupName = (TextView) findViewById(R.id.epGroupName);
		groupMembersCount = (TextView) findViewById(R.id.epGroupMembersCount);
		lv = (ListView) findViewById(R.id.groupSelectedFriendsList);
		
		addDeleteFriends = (ImageButton) findViewById(R.id.addDeleteFriends);
		
		
		if(group != null) {
			groupName.setText(group.getName());
			GroupCreator=group.getCretorstring();
			Log.i("group.getCretor()", "is...."+group.getCretorstring());
			if(group.getGroupImagePath() != null){
				Picasso.with(getApplicationContext()).load(group.getGroupImagePath()).fit().centerCrop().transform(new RoundedTransform()).placeholder(R.drawable.friend_nopic).error(R.drawable.friend_nopic).into(groupImage);
			}else{
				groupImage.setImageResource(R.drawable.friend_nopic);
			}
			
			try {
				if(group.getFriendList() != null) {
					friendsListJsonArray = new JSONArray(group.getFriendList());
					groupMembersCount.setText(friendsListJsonArray.length() + " Members");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			preloader.startPreloader(this, "Loading Friends");
			//Activity
			if (!Util.isNetworkAvailable(groupEach.this)) {	
				try {
					preloader.stopLoading();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				DialogFactory.getBeaconStreamDialog1(groupEach.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
			} else 
			
			{
			
			final Boolean[] parms = {false};
			new LoadFriendsInGroupAsync().execute(parms);
			}
		}

		addDeleteFriends.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (GroupCreator != null) {
					Log.i("group.getCretor()", "is...."+group.getCretorstring()+"ugvjgvjv"+ParseUser.getCurrentUser().getObjectId().toString());
					if (GroupCreator.equalsIgnoreCase(ParseUser.getCurrentUser().getObjectId().toString()))
					{
						
						Intent chooseFriends = new Intent(getApplicationContext(), AddGroupsNext.class);
						if (friendsListJsonArray != null) {
							chooseFriends.putExtra("friends",friendsListJsonArray.toString());
							chooseFriends.putExtra("action", "edit");
						}
						startActivityForResult(chooseFriends, 234);
					}
					else{
						finish();
					}
				}
			}
		});
		
	}
	
	public class LoadFriendsInGroupAsync extends AsyncTask<Boolean, Void, ArrayList<Friend>> {
		ArrayList<Friend> friendList = new ArrayList<Friend>();
		@Override
		protected ArrayList<Friend> doInBackground(Boolean... params) {

			friendsListString = group.getFriendList();
			try {	
				System.out.println(friendsListString);
				JSONArray jsonArray = new JSONArray(friendsListString);
			    for (int i = 0; i < jsonArray.length(); i++) {
			        JSONObject explrObject = jsonArray.getJSONObject(i);
			        Friend f = new Friend();
			        f.setName(explrObject.getString("name"));
			        f.setUserId(explrObject.getString("userid"));
			        f.setUserImage(explrObject.getString("userImage"));
			        friendList.add(f);
			    }
			} catch (Exception e1) {
				e1.printStackTrace();
			}  

			return friendList;
		}

		@Override
		protected void onPostExecute(ArrayList<Friend> result) {
			super.onPostExecute(result);
			updateList(result);
		}
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		 getMenuInflater().inflate(R.menu.action_menu_createalarm, menu);
		 return super.onCreateOptionsMenu(menu);
	}
	
	public void updateList(ArrayList<Friend> res){
		try {
			mAdapter = new GroupFriendAdapter(getApplicationContext(),R.layout.group_selected_friends_item, res);
			lv.setAdapter(mAdapter);
			lv.setVisibility(View.VISIBLE);
			preloader.stopLoading();
			Log.i("list", res.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	   if(requestCode == 234){
	    	if(resultCode == RESULT_OK){
	    		Bundle friendList = data.getExtras();
	    		if(friendList != null){
	    			//Toast.makeText(getApplicationContext(),friendList.getString("friendsid"), Toast.LENGTH_LONG).show();
	    			try {
						friends = new JSONArray(friendList.getString("friends"));
						friendsListJsonArray = new JSONArray(friendList.getString("friends"));
						friendsId = new JSONArray(friendList.getString("friendsid"));
						//Log.d("MyApp","Friends: "+friends.toString());
						
						/*
						ArrayList<Friend> newfriendList = new ArrayList<Friend>();
						JSONArray jsonArray = new JSONArray(friendList.getString("friends"));
					    for (int i = 0; i < jsonArray.length(); i++) {
					        JSONObject explrObject = jsonArray.getJSONObject(i);
					        Friend f = new Friend();
					        f.setName(explrObject.getString("name"));
					        f.setUserId(explrObject.getString("userid"));
					        f.setUserImage(explrObject.getString("userImage"));
					        newfriendList.add(f);
					    }
					    
					    mAdapter.clear();
					    mAdapter.addAll(newfriendList);
					    mAdapter.notifyDataSetChanged();
						*/
					} catch (Exception e) {
						e.printStackTrace();
					}
	    		}
	    	}
	    	if (resultCode == RESULT_CANCELED) {
		      //Write your code if there's no result
		   }
	    } 
	
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
			case R.id.action_next:
				
				//Activity
				if (!Util.isNetworkAvailable(groupEach.this)) {	
					
					DialogFactory.getBeaconStreamDialog1(groupEach.this, getResources().getString(R.string.nw_error_str),Util.onClickListner, Util.retryClickListner).show();
				} else 
				
				{
				
				
				if (GroupCreator != null) {
					if (GroupCreator.equalsIgnoreCase(ParseUser.getCurrentUser().getObjectId().toString()))
					{
				
				if(group != null) {
				ParseQuery query = new ParseQuery("Groups");
				try {
					ParseObject obj = query.get(group.getGroupObject());
					if(friends!=null)
					obj.put("friendsList",friends.toString());
					obj.saveInBackground();//saveEventually();(); 
					Intent groupsActivity = new Intent(getApplicationContext(),Groups.class);
					groupsActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK&Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(groupsActivity);
					finish();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					
				}
				
					}
					else{
						finish();
					}
				}
				
				
				
				
		}
				break;
		}
		return true;
	}
    

}
