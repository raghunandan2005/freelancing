package in.fastrack.app.m.fup.lib;

import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.model.Friend;
import in.fastrack.app.m.fup.model.People;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.util.Log;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.model.GraphObject;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class ContactsSyncService extends Service {
	private AppConfig _config;
	ArrayList<Friend> sList = new ArrayList<Friend>();
	ArrayList<Friend> tList = new ArrayList<Friend>();
	ArrayList<Friend> finalList = new ArrayList<Friend>();
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onCreate() {
		Log.e("ContactsSyncService", "create");
		_config = new AppConfig(getApplicationContext());		
		super.onCreate();
		
		//If FB user get List of app users who are friends
		if(Util.isFbUser()){ 
			final Boolean[] parms = {true};
			new LoadSocialAsync().execute(parms);
		} else {
			//Get Contacts list of app users who are friends
			new LoadContactsAsync().execute();
		}
	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		 Log.e("ContactsSyncService", "Inside onStartCommand");
    	 return super.onStartCommand(intent, flags, startId);
	}
	
	
	public void onDestroy() {
	    super.onDestroy();
	    Log.d("ContactsSyncService", "destroy");
	}
	
	public class LoadSocialAsync extends AsyncTask<Boolean, Void, ArrayList<Friend>> {

		@Override
		protected ArrayList<Friend> doInBackground(Boolean... params) {
			//Calling from server
			String fqlQuery = "select uid, name,first_name, pic_square, is_app_user from user where is_app_user = 1 and uid in (select uid2 from friend where uid1 = me())";
			Bundle ops = new Bundle();
			ops.putString("q", fqlQuery);
			Request r = new Request(ParseFacebookUtils.getSession(),"/fql",ops,HttpMethod.GET,new Request.Callback() {
				
				@Override
				public void onCompleted(Response response) {
					if (response.getError() == null) {
						Log.i("FB OP","Got results: "+ response.toString());
						GraphObject gObject = response.getGraphObject();
						JSONObject jObject = gObject.getInnerJSONObject();
						try {
							ParseQuery<ParseUser> query = ParseUser.getQuery();
							JSONArray jArray = jObject.getJSONArray("data");
							ArrayList<String> fblist = new ArrayList<String>();
							for(int i = 0; i<jArray.length();i++){
								JSONObject jObj2 = jArray.getJSONObject(i);
								fblist.add(jObj2.getString("uid"));
							}
							query.whereContainedIn("facebookid",fblist);
							query.whereNotEqualTo("objectId",ParseUser.getCurrentUser().getObjectId());
							try {
								List<ParseUser> objects = query.find();
								Friend list = null;
								if(objects.size() > 0){
									for (int i = 0; i < objects.size(); i++) {
										String fbid = objects.get(i).get("facebookid").toString();
										
										String fbImage = "https://graph.facebook.com/"+ fbid+ "/picture/";
										String myObj = ParseUser.getCurrentUser().toString();
										String userobj = objects.get(i).getObjectId().toString();
										String userName = objects.get(i).get("fullname").toString();
										//list = new People(myObj,userobj,fbid,userName,"facebook",fbImage,false,false);
										list = new Friend(userobj, userName, "facebook", fbImage, false, false, "Add");
										sList.add(list);
									}
								}
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}catch (JSONException e) {
							e.printStackTrace();
						}
					}else{
						Log.i("MyApp",response.getError().toString());
					}
				}
			});
			Request.executeBatchAndWait(r);
			return sList;
		}
		
		protected void onPostExecute(ArrayList<Friend> result) {
			super.onPostExecute(result);
			finalList.addAll(result);
			new LoadContactsAsync().execute();
		}
	}
	
	private class LoadContactsAsync extends AsyncTask<Void, Void, ArrayList<Friend>> {
		@Override
		protected ArrayList<Friend> doInBackground(Void... params) {
		/*	ContentResolver cr = getContentResolver();
	        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
	 
	         if (cur != null && cur.getCount() > 0) {
	        	ParseQuery<ParseUser> query = ParseUser.getQuery();
	            while (cur.moveToNext()) {
	                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
	                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
	                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
	                    System.out.println("name : " + name + ", ID : " + id);
	 
	                    // get the phone number
	                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
	                                           ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
	                                           new String[]{id}, null);
	                    while (pCur.moveToNext()) {
	                          String phoneNum = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("\\s","");
	                          System.out.println("phone" + phoneNum);
	                          if (phoneNum.length() > 8) {
	  							try {
	  								query.whereEqualTo("phone", phoneNum);
	  								query.whereNotEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
	  								Friend items = null;
	  								List<ParseUser> qResult;
	  								qResult = query.find();
	  								for(int i=0;i<qResult.size();i++){
	  									Log.i("INFOOOOO", qResult.get(i).getObjectId().toString() + " --- " + qResult.get(i).get("fullname"));
	  									String imageUrl = "image";
	  									ParseFile p;
	  									p = qResult.get(i).getParseFile("profilethumbnail");
	  									if(p!=null){
	  										String url = p.getUrl();
	  										imageUrl = url;
	  									}
	  									items = new Friend(qResult.get(i).getObjectId().toString(), qResult.get(i).getString("fullname"), "contact", imageUrl, false, false);
	  									tList.add(items);
	  								}
	  							} catch (ParseException e) {
	  								// TODO Auto-generated catch block
	  								e.printStackTrace();
	  							}
	  						}
	                    }
	                    pCur.close();
	                }
	            }
	            cur.close();
	         }*/
	         
	 
				// Load data based on people who are using app
				
				/*Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
				String[] projection = new String[] { Phone._ID, Phone.DISPLAY_NAME, Phone.NUMBER, Photo.PHOTO };
				String selection = ContactsContract.Contacts.IN_VISIBLE_GROUP + "='" + ("1") + "'";
				String[] selectionArgs = null;
				String sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
		
				Cursor c = getBaseContext().getContentResolver().query(uri, projection, null, selectionArgs, sortOrder);
				if(c!=null && c.getCount() > 0){
					ParseQuery<ParseUser> query = ParseUser.getQuery();
					while (c.moveToNext()) {
						String name = c.getString(c.getColumnIndex(Phone.DISPLAY_NAME));
						String phoneNum = c.getString(c.getColumnIndex(Phone.NUMBER)).replaceAll("\\s","");
						if (phoneNum.length() > 8) {
							try {
								query.whereEqualTo("phone", phoneNum);
								Log.i("MY CONTACTS>>>", name + "  --  " + phoneNum);
								query.whereNotEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
								Friend items = null;
								List<ParseUser> qResult;
								qResult = query.find();
								for(int i=0;i<qResult.size();i++){
									Log.i("INFOOOOO", qResult.get(i).getObjectId().toString() + " --- " + qResult.get(i).get("fullname"));
									items = new Friend(qResult.get(i).getObjectId().toString(), qResult.get(i).getString("fullname"), "contact");
									tList.add(items);
								}
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
				JSONArray jsonFriendsArray;
				try {
					jsonFriendsArray = new JSONArray(tList.toString());
					Log.d("FriendsList", jsonFriendsArray.toString());
				} catch (JSONException e) {
					e.printStackTrace();
				}
				c.close();*/
			return (tList);
		}
		
		 protected void onPostExecute(ArrayList<Friend> result) {
 			super.onPostExecute(result);
 			finalList.addAll(result);
 			
 			if(finalList.size() > 0) {
 				final JSONArray jsonFriendsArray;
 				try {
 					jsonFriendsArray = new JSONArray(finalList.toString());
 					
 					ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
 					query.whereEqualTo("user", ParseUser.getCurrentUser());
 					query.findInBackground(new FindCallback<ParseObject>() { 
 						public void done(List<ParseObject> objects, ParseException e) {
 							if(objects.size() > 0) {
 								//edit
	 							if(e == null){
	 								for(ParseObject obj : objects){
	 									obj.put("friendsList", jsonFriendsArray.toString());
	 									obj.saveInBackground();//saveEventually();();
	 								}
	 							} else {
	 									
	 							}
 							} else {
 								//save
 								ParseObject friends = new ParseObject("Friends");
 			 					friends.put("user",ParseUser.getCurrentUser()); 
 			 					friends.put("friendsList", jsonFriendsArray.toString());
 			 					friends.saveInBackground();//saveEventually();(); 
 							}
 						}
 					});
 					Log.d("FinalList", jsonFriendsArray.toString());
 				} catch (JSONException e) {
 					e.printStackTrace();
 				}
 			}
 		}
		
		
		
		}
	
}
