package in.fastrack.app.m.fup.lib;

import java.io.IOException;

import in.fastrack.app.m.fup.config.AppConfig;
import android.annotation.SuppressLint;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.widget.Toast;

public class CustomAudio {
	
	MediaPlayer mPlayer = new MediaPlayer();
	AppConfig customConfig;
	public MediaListener mAudioStopped;
	
	@SuppressLint("ServiceCast")
	public void playMedia(final Context c,final String audioPath,final int audioId,final boolean builtIn,boolean loop,final MediaListener mAudioStopped){
		this.mAudioStopped = mAudioStopped;
		customConfig = new AppConfig(c);
		mPlayer.reset();
		if(builtIn){
			//Load from RAW folder
			String fileName = "android.resource://" + c.getPackageName() + "/" +audioId;
			try {
				mPlayer.setDataSource(c.getApplicationContext(),Uri.parse(fileName));
				mPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
				float count=100*.01f;
				mPlayer.setVolume(count,count);
				mPlayer.prepare();
				mPlayer.setLooping(loop);
				mPlayer.start();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			//Load from FUP folder
		}
		mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer arg0) {
				mAudioStopped.playerStopped(audioPath, audioId, builtIn);
			}
		});
	}
	
	public void stopMusic(){
		/*if(mPlayer != null){
			mPlayer.stop();
			mPlayer.reset();
			mPlayer.release();
			//mPlayer = null;
		}*/
		
		mPlayer.stop();
		mPlayer.reset();
	}
	
	public void killMusic(){
		if(mPlayer != null){
			if(mPlayer.isPlaying()){
				mPlayer.stop();
				mPlayer.reset();
				mPlayer.release();
			}
			//mPlayer = null;
		}
	}
	
	public static interface MediaListener{
		public void playerStopped(String audioPath,int audioId, boolean builtIn);
	}

}
