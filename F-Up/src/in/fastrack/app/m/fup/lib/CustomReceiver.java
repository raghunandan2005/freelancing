package in.fastrack.app.m.fup.lib;

import in.fastrack.app.m.fup.AlaramActivityNew;
import in.fastrack.app.m.fup.Alarams;
import in.fastrack.app.m.fup.FeedComments;
import in.fastrack.app.m.fup.HomeActivity;
import in.fastrack.app.m.fup.NotificationActivityNew;
import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.alarm.CreateNewAlarmActivity;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.database.DatabaseHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseBroadcastReceiver;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class CustomReceiver extends ParseBroadcastReceiver {

	private Context mContext;
	private Bundle extras;
	private JSONObject json;
	private Intent resultIntent;
	private int mId=2;
	private ParseObject parseOO;
	private NotificationCompat.Builder mBuilder;
	private TaskStackBuilder stackBuilder; 
	private PendingIntent resultPendingIntent;
	private NotificationManager mNotificationManager; 
	private JSONArray connectedSurpriseF = new JSONArray();


	
	@Override
	public void onReceive(Context context, Intent intent) {

		super.onReceive(context, intent);

			mContext = context;
			extras = intent.getExtras();
			if(intent.hasExtra("com.parse.Data")) 
			{
				try
				{
				 json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
				 int notificationtype = json.getInt("notificationtype");
				 switch(notificationtype)
				 {
				  case 1:
					  
						mBuilder = new NotificationCompat.Builder(context)
				        .setSmallIcon(R.drawable.ic_launcher)
				        .setContentTitle("F-Up")
				        .setContentText(json.getString("alert"));
						mBuilder.setAutoCancel(true);
				 		// Creates an explicit intent for an Activity in your app
				 		resultIntent = new Intent(context,  NotificationActivityNew.class);
						extras.putString("alarmid",json.getString("reference"));
					    extras.putBoolean("onlyone",true);														
						resultIntent.putExtras(extras);
				 		// The stack builder object will contain an artificial back stack for the
				 		// started Activity.
				 		// This ensures that navigating backward from the Activity leads out of
				 		// your application to the Home screen.
				 		stackBuilder = TaskStackBuilder.create(context);
				 		// Adds the back stack for the Intent (but not the Intent itself)
				 		stackBuilder.addParentStack(NotificationActivityNew.class);
				 		// Adds the Intent that starts the Activity to the top of the stack
				 		stackBuilder.addNextIntent(resultIntent);
				 		resultPendingIntent = stackBuilder.getPendingIntent( 0, PendingIntent.FLAG_UPDATE_CURRENT);
				 		mBuilder.setContentIntent(resultPendingIntent);
				 		mNotificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				 		// mId allows you to update the notification later on.
				 		mNotificationManager.notify(mId, mBuilder.build());
				    	  
				    	  
				    break;
				 	case 2:
				 		
				 		mBuilder = new NotificationCompat.Builder(context)
				        .setSmallIcon(R.drawable.ic_launcher)
				        .setContentTitle("F-Up")
				        .setContentText(json.getString("alert"));
				 		mBuilder.setAutoCancel(true);
				 		// Creates an explicit intent for an Activity in your app
				 		resultIntent = new Intent(context, FeedComments.class);
				 		extras.putString("alarmid", json.getString("reference"));
				 		extras.putInt("position", 0);
				 		resultIntent.putExtras(extras);
				 		stackBuilder = TaskStackBuilder.create(context);
				 		stackBuilder.addParentStack(FeedComments.class);
				 		stackBuilder.addNextIntent(resultIntent);
				 		resultPendingIntent = stackBuilder.getPendingIntent( 0, PendingIntent.FLAG_UPDATE_CURRENT);
				 		mBuilder.setContentIntent(resultPendingIntent);
				 		mNotificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				 		mNotificationManager.notify(mId, mBuilder.build());

				    break;
				 	case 3:
				 		
						mBuilder = new NotificationCompat.Builder(context)
				        .setSmallIcon(R.drawable.ic_launcher)
				        .setContentTitle("F-Up")
				        .setContentText(json.getString("alert"));
						mBuilder.setAutoCancel(true);
				 		resultIntent = new Intent(context,  NotificationActivityNew.class);
						extras.putString("alarmid",json.getString("reference"));
					    extras.putBoolean("onlyone",true);														
						resultIntent.putExtras(extras);
				 
				 		stackBuilder = TaskStackBuilder.create(context);
				 		stackBuilder.addParentStack(NotificationActivityNew.class);

				 		stackBuilder.addNextIntent(resultIntent);
				 		resultPendingIntent = stackBuilder.getPendingIntent( 0, PendingIntent.FLAG_UPDATE_CURRENT);
				 		mBuilder.setContentIntent(resultPendingIntent);
				 		mNotificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				 		mNotificationManager.notify(mId, mBuilder.build());
				 		
				   break;		
				 	case 4:
				 		Intent updatedatabase= new Intent(mContext,UpDateService.class);
				 		mContext.startService(updatedatabase);
				   	
				    break;	
				 	case 5:
				 		
				 		mBuilder = new NotificationCompat.Builder(context)
				        .setSmallIcon(R.drawable.ic_launcher)
				        .setContentTitle("F-Up")
				        .setContentText(json.getString("alert"));
						mBuilder.setAutoCancel(true);
				 		resultIntent = new Intent(context, NotificationActivityNew.class);
						extras.putString("alarmid",json.getString("reference3"));
					    extras.putBoolean("onlyone",true);														
						resultIntent.putExtras(extras);
				 
				 		stackBuilder = TaskStackBuilder.create(context);
				 		stackBuilder.addParentStack(NotificationActivityNew.class);

				 		stackBuilder.addNextIntent(resultIntent);
				 		resultPendingIntent = stackBuilder.getPendingIntent( 0, PendingIntent.FLAG_UPDATE_CURRENT);
				 		mBuilder.setContentIntent(resultPendingIntent);
				 		mNotificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				 		mNotificationManager.notify(mId, mBuilder.build());
				 		
				 	break;
				 	case 6:
//				 		link = new Intent(HomeActivity.this,NotificationActivityNew.class);
//						extras.putString("alarmid", json.getString("reference4"));
//						extras.putBoolean("onlyone", true);
//						link.putExtras(extras);
//						startActivity(link);
				 		Log.i("........................................",".........................................");
				 		Toast.makeText(mContext, "Notification Type 6", Toast.LENGTH_SHORT).show();
				 		mBuilder = new NotificationCompat.Builder(context)
				        .setSmallIcon(R.drawable.ic_launcher)
				        .setContentTitle("F-Up")
				        .setContentText(json.getString("alert"));
						mBuilder.setAutoCancel(true);
				 		resultIntent = new Intent(context, NotificationActivityNew.class);
						extras.putString("alarmid",json.getString("reference4"));
					    extras.putBoolean("onlyone",true);														
						resultIntent.putExtras(extras);
				 
				 		stackBuilder = TaskStackBuilder.create(context);
				 		stackBuilder.addParentStack(NotificationActivityNew.class);

				 		stackBuilder.addNextIntent(resultIntent);
				 		resultPendingIntent = stackBuilder.getPendingIntent( 0, PendingIntent.FLAG_UPDATE_CURRENT);
				 		mBuilder.setContentIntent(resultPendingIntent);
				 		mNotificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				 		mNotificationManager.notify(mId, mBuilder.build());
				 		
				 	break;
				 	case 7:
				 		
				 		mBuilder = new NotificationCompat.Builder(context)
				        .setSmallIcon(R.drawable.ic_launcher)
				        .setContentTitle("F-Up")
				        .setContentText(json.getString("alert"));
						mBuilder.setAutoCancel(true);
				 		resultIntent = new Intent(context, NotificationActivityNew.class);
						extras.putString("alarmid",json.getString("reference"));
					    extras.putBoolean("onlyone",true);														
						resultIntent.putExtras(extras);
				 
				 		stackBuilder = TaskStackBuilder.create(context);
				 		stackBuilder.addParentStack(NotificationActivityNew.class);

				 		stackBuilder.addNextIntent(resultIntent);
				 		resultPendingIntent = stackBuilder.getPendingIntent( 0, PendingIntent.FLAG_UPDATE_CURRENT);
				 		mBuilder.setContentIntent(resultPendingIntent);
				 		mNotificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				 		mNotificationManager.notify(mId, mBuilder.build());
				 		
				 	break;
				 	case 8:
				 		
				 		mBuilder = new NotificationCompat.Builder(context)
				        .setSmallIcon(R.drawable.ic_launcher)
				        .setContentTitle("F-Up")
				        .setContentText(json.getString("alert"));
						mBuilder.setAutoCancel(true);
				 		resultIntent = new Intent(context, NotificationActivityNew.class);
						extras.putString("alarmid",json.getString("reference"));
					    extras.putBoolean("onlyone",true);														
						resultIntent.putExtras(extras);
				 
				 		stackBuilder = TaskStackBuilder.create(context);
				 		stackBuilder.addParentStack(NotificationActivityNew.class);

				 		stackBuilder.addNextIntent(resultIntent);
				 		resultPendingIntent = stackBuilder.getPendingIntent( 0, PendingIntent.FLAG_UPDATE_CURRENT);
				 		mBuilder.setContentIntent(resultPendingIntent);
				 		mNotificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				 		mNotificationManager.notify(mId, mBuilder.build());
				 		
				  break;
				  case 9:
						mBuilder = new NotificationCompat.Builder(context)
				        .setSmallIcon(R.drawable.ic_launcher)
				        .setContentTitle("F-Up")
				        .setContentText(json.getString("alert"));
						mBuilder.setAutoCancel(true);
				 		resultIntent = new Intent(context, NotificationActivityNew.class);
						extras.putString("alarmid",json.getString("reference"));
					    extras.putBoolean("onlyone",true);														
						resultIntent.putExtras(extras);
				 
				 		stackBuilder = TaskStackBuilder.create(context);
				 		stackBuilder.addParentStack(NotificationActivityNew.class);

				 		stackBuilder.addNextIntent(resultIntent);
				 		resultPendingIntent = stackBuilder.getPendingIntent( 0, PendingIntent.FLAG_UPDATE_CURRENT);
				 		mBuilder.setContentIntent(resultPendingIntent);
				 		mNotificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				 		mNotificationManager.notify(mId, mBuilder.build());
				 		
				 		Intent deleteService = new Intent(mContext,DeleteService.class);
				 		mContext.startService(deleteService);
				 							   

				  break;	
				  case 10:
				 		
				 		mBuilder = new NotificationCompat.Builder(context)
				        .setSmallIcon(R.drawable.ic_launcher)
				        .setContentTitle("F-Up")
				        .setContentText(json.getString("alert"));
						mBuilder.setAutoCancel(true);
				 		resultIntent = new Intent(context, NotificationActivityNew.class);
						extras.putString("alarmid",json.getString("reference5"));
					    extras.putBoolean("onlyone",true);														
						resultIntent.putExtras(extras);
				 
				 		stackBuilder = TaskStackBuilder.create(context);
				 		stackBuilder.addParentStack(NotificationActivityNew.class);

				 		stackBuilder.addNextIntent(resultIntent);
				 		resultPendingIntent = stackBuilder.getPendingIntent( 0, PendingIntent.FLAG_UPDATE_CURRENT);
				 		mBuilder.setContentIntent(resultPendingIntent);
				 		mNotificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				 		mNotificationManager.notify(mId, mBuilder.build());
				 		
				  break;
				  case 11:
				 		
				 		mBuilder = new NotificationCompat.Builder(context)
				        .setSmallIcon(R.drawable.ic_launcher)
				        .setContentTitle("F-Up")
				        .setContentText(json.getString("alert"));
						mBuilder.setAutoCancel(true);
				 		resultIntent = new Intent(context, NotificationActivityNew.class);
						extras.putString("alarmid",json.getString("reference3"));
					    extras.putBoolean("onlyone",true);														
						resultIntent.putExtras(extras);
				 
				 		stackBuilder = TaskStackBuilder.create(context);
				 		stackBuilder.addParentStack(NotificationActivityNew.class);

				 		stackBuilder.addNextIntent(resultIntent);
				 		resultPendingIntent = stackBuilder.getPendingIntent( 0, PendingIntent.FLAG_UPDATE_CURRENT);
				 		mBuilder.setContentIntent(resultPendingIntent);
				 		mNotificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				 		mNotificationManager.notify(mId, mBuilder.build());
				 		
				  break;
				  
				  case 12:
				 		
				 		mBuilder = new NotificationCompat.Builder(context)
				        .setSmallIcon(R.drawable.ic_launcher)
				        .setContentTitle("F-Up")
				        .setContentText(json.getString("alert"));
						mBuilder.setAutoCancel(true);
				 		resultIntent = new Intent(context, NotificationActivityNew.class);
						extras.putString("alarmid",json.getString("reference4"));
					    extras.putBoolean("onlyone",true);														
						resultIntent.putExtras(extras);
				 
				 		stackBuilder = TaskStackBuilder.create(context);
				 		stackBuilder.addParentStack(NotificationActivityNew.class);

				 		stackBuilder.addNextIntent(resultIntent);
				 		resultPendingIntent = stackBuilder.getPendingIntent( 0, PendingIntent.FLAG_UPDATE_CURRENT);
				 		mBuilder.setContentIntent(resultPendingIntent);
				 		mNotificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				 		mNotificationManager.notify(mId, mBuilder.build());
				 		
				  break;
				  case 13:
				 		
				 		Intent editalaram= new Intent(mContext,EditAlarmService.class);
				 		mContext.startService(editalaram);
				   	
				  break;
				  case 14:
					  
				   		if(ParseUser.getCurrentUser() != null){
			   			ParseQuery<ParseObject> query = ParseQuery.getQuery("Activity");
			   			query.whereEqualTo("followers",ParseUser.getCurrentUser().getObjectId());
			   			//query.whereNotEqualTo("readers", ParseUser.getCurrentUser().getObjectId());
			   			query.include("creator");
			   			query.include("reference");			   			
			   			query.orderByDescending("createdAt");
			   			query.setLimit(10);
			   			query.findInBackground(new FindCallback<ParseObject>() {
			   				
			   				@Override
			   				public void done(List<ParseObject> objects, ParseException e) {
			   					if(e==null){
			   						try {
			   							//Data found
			   							Log.i("Parse","Data present");
			   							int k = 1;
			   							for(ParseObject o:objects){
			   							parseOO = o;
			   								//notificationCount++;
			   								int notificationType = o.getInt("notificationtype");
			   								Intent intent = null;
			   								extras = new Bundle();
			   					
			   								Intent removesfservice= new Intent(mContext,RemovesfService.class);
			   								removesfservice.putExtra("name", Integer.parseInt(parseOO.getParseObject("reference").getString("requestcode")));
			   						 		mContext.startService(removesfservice);
			   			
			   					    	  
			   								switch(notificationType){
			   								case 14:
			   									try {
				   									List<String> arr = parseOO.getParseObject("reference").getList("Hideit");
				   									
				   									if(arr != null){
				   										Log.i("Actions","A"+arr.toString()+"request code is"+Integer.parseInt(parseOO.getParseObject("reference").getString("requestcode"))+"current user"+ParseUser.getCurrentUser().getObjectId().toString());
				   										
				   										if(arr.contains(ParseUser.getCurrentUser().getObjectId().toString())){
				   											Intent intent1 = new Intent(mContext, AlaramActivityNew.class);
				   											PendingIntent pendingIntent = PendingIntent.getActivity(mContext,Integer.parseInt(parseOO.getParseObject("reference").getString("requestcode")), intent1, PendingIntent.FLAG_CANCEL_CURRENT);
				   											AlarmManager alarmManager = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
				   											alarmManager.cancel(pendingIntent);
				   											
				   											Log.i("Actions","B");
				   										}
				   									}
				   								} catch (Exception e13) {
				   									// TODO Auto-generated catch block
				   									e13.printStackTrace();
				   								}
			   									ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");			   									
			   									query.whereEqualTo("user", ParseUser.getCurrentUser());
			   									query.findInBackground(new FindCallback<ParseObject>() {
			   										@Override
			   										public void done(List<ParseObject> objects, ParseException e) {

			   											if (e == null) {
			   												try {
			   													if (objects.size() > 0) {
			   														for (ParseObject obj : objects) {														 
			   															connectedSurpriseF = obj.getJSONArray("connectedSF");
			   															//Log.i("Parse", "connectedSurpriseF"+connectedSurpriseF.toString());
			   															break;
			   														}
			   														
			   														if(connectedSurpriseF!=null)
			   														{
			   															//Log.i("File", "connected"+ connectedSurpriseF.toString());
			   															if (connectedSurpriseF.length() > 0) {
			   															for (int i = 0; i < connectedSurpriseF.length(); i++) {
			   																
			   																	
			   																		if (connectedSurpriseF.get(i).equals(parseOO.getParseObject("reference").getString("creatorplain"))) {//ParseUser.getCurrentUser().getObjectId().toString()

			   																			try {
			   																		 
			   													
			   																				
			   																				//parseOO.addAllUnique("readers",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
			   																				
			   																				parseOO.put("readers", Arrays.asList(ParseUser.getCurrentUser().getObjectId()));//ParseUser.getCurrentUser().getObjectId().toString()
			   																				parseOO.add("readers", parseOO.getParseObject("reference").getString("creatorplain"));																				
			   																				parseOO.saveInBackground();
			   																			
			   																			} catch (Exception e10) {
			   																				// TODO Auto-generated catch block
			   																				e10.printStackTrace();
			   																			}
			   															
			   																			
			   																			
			   																		} 
			   																		else{
			   																		 	Intent link =new Intent(mContext, NotificationActivityNew.class);											
			   																			extras.putString("alarmid",json.getString("reference"));
			   																			extras.putBoolean("onlyone",true);														
			   																			link.putExtras(extras);
			   																			mContext.startActivity(link);
			   																		}
			   																
			   															}
			   															
			   															}
			   															else{
			   																Intent link =new Intent(mContext, NotificationActivityNew.class);											
   																			extras.putString("alarmid",json.getString("reference"));
   																			extras.putBoolean("onlyone",true);														
   																			link.putExtras(extras);
   																			mContext.startActivity(link);
			   															}
			   															
			   														}
			   														else{
			   															Intent link =new Intent(mContext, NotificationActivityNew.class);											
																			extras.putString("alarmid",json.getString("reference"));
																			extras.putBoolean("onlyone",true);														
																			link.putExtras(extras);
																			mContext.startActivity(link);
			   														}
			   														
			   													}
			   												} catch (Exception e1) {
			   													// TODO Auto-generated catch block
			   													e1.printStackTrace();
			   												}

			   											} else {
			   												Log.i("Parse", e.getMessage());
			   											}

			   										}
			   									});
			   								
			   								
			   								break;
			   								
			   								
			   								}
			   								
			   							
			   								
			   							}
			   							
			   						} catch (Exception e14) {
			   							// TODO Auto-generated catch block
			   							e14.printStackTrace();
			   						}
			   					}else{
			   						Log.i("Parse",e.getMessage());
			   						//System.out.println(e.printStackTrace());
			   					}
			   				}
			   			});
			   		}

						
				 		
				  break;	 
				 
				  	  
				    	 
				 }
				}catch(Exception e)
				{
					e.printStackTrace();
				}
		
			}


			      


	}
	

}
