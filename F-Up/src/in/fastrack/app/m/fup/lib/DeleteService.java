package in.fastrack.app.m.fup.lib;

import in.fastrack.app.m.fup.AlaramActivityNew;
import in.fastrack.app.m.fup.database.DatabaseHandler;

import java.util.List;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class DeleteService extends IntentService {

	private Context mContext;
	private ParseObject parseOO;
	
	public DeleteService() {
		super("Delete Service");

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
		mContext = this;
   		if(ParseUser.getCurrentUser() != null){
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Activity");
			query.whereEqualTo("followers",ParseUser.getCurrentUser().getObjectId());
			query.include("creator");
			query.include("reference");			   			
			query.orderByDescending("createdAt");
			query.setLimit(10);
			query.findInBackground(new FindCallback<ParseObject>() {
				
				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					if(e==null){
						try {

							for(ParseObject o:objects){
							parseOO = o;
							DatabaseHandler db = new DatabaseHandler(mContext);
							db.deleteAlarm(Integer.parseInt(parseOO.getParseObject("reference").getString("requestcode")));
							int notificationType = o.getInt("notificationtype");
							switch(notificationType){
							case 9:
								
   									List<String> arr = parseOO.getParseObject("reference").getList("Hideit");
   									if(arr != null){
   											if(arr.contains(ParseUser.getCurrentUser().getObjectId().toString())){
   											Intent intent1 = new Intent(getApplicationContext(), AlaramActivityNew.class);
   											PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),Integer.parseInt(parseOO.getParseObject("reference").getString("requestcode")), intent1, PendingIntent.FLAG_CANCEL_CURRENT);
   											AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
   											alarmManager.cancel(pendingIntent);
   						
   										   }
   									}
   								}
							break;
							}
	
						} catch (Exception e14) {
	
							e14.printStackTrace();
						}
					}
				}
			});
		}	
	}

}
