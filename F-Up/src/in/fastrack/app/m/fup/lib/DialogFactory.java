package in.fastrack.app.m.fup.lib;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


import in.fastrack.app.m.fup.R;

public class DialogFactory {
	static String supporting_language;
	static String ok,retry,cancel,yes;
	static String appname,errorsmall,no;
	private static final String TAG = "DialogFactory";
	static DialogFactory df = new DialogFactory();

	private DialogFactory() {

	}

	public static DialogFactory getInstance() {

		if (df == null) {
			df = new DialogFactory();
		}
		return df;
	}
	

	public static Dialog getBeaconStreamDialog1(Context ctx, String message,OnClickListener onClickListener,OnClickListener onClickListener1) {

			Log.d(TAG, "Inside AlertDialog Builder");
			
			Dialog dialog = null;
			
			try
			{
		    cancel=ctx.getResources().getString(R.string.cancel_str);
		    errorsmall=ctx.getResources().getString(R.string.error_str);
			retry=ctx.getResources().getString(R.string.retry_str);			
			dialog = new Dialog(ctx,android.R.style.Theme_Dialog);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog.setContentView(R.layout.beacon_alert2);
			TextView title_tv = (TextView)dialog.findViewById(R.id.title);
			title_tv.setText(errorsmall);
			dialog.setCancelable(false);
			TextView message_tv = (TextView)dialog.findViewById(R.id.message);
			message_tv.setText(message);
			Button cancel_btn = (Button)dialog.findViewById(R.id.button1);
			cancel_btn.setText(cancel);
			cancel_btn.setTag(dialog);
			cancel_btn.setOnClickListener(onClickListener);
			Button retry_btn = (Button)dialog.findViewById(R.id.button2);
			retry_btn.setText(retry);
			retry_btn.setTag(dialog);
			retry_btn.setOnClickListener(onClickListener1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			return dialog;
		}
	
	public static Dialog getBeaconStreamDialog2(Context ctx, String message,OnClickListener onClickListener) {

		Log.d(TAG, "Inside AlertDialog Builder");
		
		Dialog dialog = null;
		
		try
		{
	    ok=ctx.getResources().getString(R.string.ok_str);
	    errorsmall=ctx.getResources().getString(R.string.success);
		retry=ctx.getResources().getString(R.string.retry_str);			
		dialog = new Dialog(ctx,android.R.style.Theme_Dialog);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog.setContentView(R.layout.beacon_alert2);
		TextView title_tv = (TextView)dialog.findViewById(R.id.title);
		title_tv.setText(errorsmall);
		dialog.setCancelable(false);
		TextView message_tv = (TextView)dialog.findViewById(R.id.message);
		message_tv.setText(message);
		Button cancel_btn = (Button)dialog.findViewById(R.id.button1);
		cancel_btn.setText(ok);
		cancel_btn.setTag(dialog);
		cancel_btn.setOnClickListener(onClickListener);
		
		Button retry_btn = (Button)dialog.findViewById(R.id.button2);
		retry_btn.setVisibility(View.GONE);
		View view = (View)dialog.findViewById(R.id.view1);
		view.setVisibility(View.GONE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return dialog;
	}
	
}
