package in.fastrack.app.m.fup.lib;

import in.fastrack.app.m.fup.AlaramActivityNew;
import in.fastrack.app.m.fup.Alarams;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.database.DatabaseHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class EditAlarmService extends IntentService {

	private JSONArray connectedSurpriseF = new JSONArray();
	private String selectedAlarmType;
	private Context mContext;
	private AppConfig _config;
	
	public EditAlarmService() {
		super("EditAlarmService");

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		mContext =this;
		_config = new AppConfig(this);
		// TODO Auto-generated method stub
		if(ParseUser.getCurrentUser() != null){
   			ParseQuery<ParseObject> query = ParseQuery.getQuery("Activity");
   			query.whereEqualTo("followers",ParseUser.getCurrentUser().getObjectId());
   			//query.whereNotEqualTo("readers", ParseUser.getCurrentUser().getObjectId());
   			query.include("creator");
   			query.include("reference");			   			
   			query.orderByDescending("createdAt");
   			query.setLimit(10);
   			query.findInBackground(new FindCallback<ParseObject>() {
   				
   				@Override
   				public void done(List<ParseObject> objects, ParseException e) {
   					if(e==null){
   						try {
   							for(ParseObject o:objects){
   								final ParseObject parseO = o;
   								int notificationType = o.getInt("notificationtype");
   								switch(notificationType){
   								case 13:
   									
   									ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");			   									
   									query.whereEqualTo("user", ParseUser.getCurrentUser());
   									query.findInBackground(new FindCallback<ParseObject>() {
   										@Override
   										public void done(List<ParseObject> objects, ParseException e) {

   											if (e == null) {
   												try {
   													if (objects.size() > 0) {
   														for (ParseObject obj : objects) {														 
   															connectedSurpriseF = obj.getJSONArray("connectedSF");

   															break;
   														}
   														
   														if(connectedSurpriseF!=null)
   														{
   															if (connectedSurpriseF.length() > 0) {
   															for (int i = 0; i < connectedSurpriseF.length(); i++) {
   																
   																	//if (connectedSurpriseF != null) {
   																		Log.i("File", "in if \n" +connectedSurpriseF.get(i)+"\n" + parseO.getParseObject("reference").getString("creatorplain"));
   																		if (connectedSurpriseF.get(i).equals(parseO.getParseObject("reference").getString("creatorplain"))) 
   																		{
   																		
   					
   																				
   																				parseO.put("readers", Arrays.asList(ParseUser.getCurrentUser().getObjectId()));//ParseUser.getCurrentUser().getObjectId().toString()
   																				parseO.add("readers", parseO.getParseObject("reference").getString("creatorplain"));																				
   																				parseO.saveInBackground();
   																				
   																				ParseQuery<ParseObject> not = ParseQuery.getQuery("alarms");
   																				not.getInBackground(parseO.getParseObject("reference").getObjectId(), new GetCallback<ParseObject>() {
   																					@Override
   																					public void done(ParseObject object, com.parse.ParseException e) {
   																						object.addAllUnique("acceptedpeople",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
   																						object.addAllUnique("notificationactions",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
   																						object.saveInBackground();//saveEventually();();
   																						//Set alarm
   																						Intent intent = new Intent(getApplicationContext(),AlaramActivityNew.class);
   																						Bundle tempData = new Bundle();
   																						tempData.putString("alarm_category",object.getString("category"));
   																						tempData.putString("alarm_title", object.getString("title"));
   																						tempData.putBoolean("recorded",object.getBoolean("recorded"));
   																						tempData.putInt("alarmrequestcode",Integer.parseInt(object.getString("requestcode")));
   																						tempData.putString("alarmpath",object.getString("media"));
   																						tempData.putString("alarmfriends",object.getString("connectedlist"));
   																						if (object.getString("alarm_repeat_type") != null)
   																						tempData.putString("alarm_repeat_type",object.getString("alarm_repeat_type"));
   																						
   																						if (object.getString("alarm_repeat_type") != null)
   																						selectedAlarmType = object.getString("alarm_repeat_type");
   																					//Alarm for other surprise edit user
   																						Alarams al = new Alarams();
   																						al.setCategory(object.getString("category"));
   																						al.setTitle(object.getString("title"));
   																						if (object.getBoolean("recorded") == true) {
   																							al.setRecorded("true");
   																						} else {
   																							al.setRecorded("false");
   																						}
   																						al.setRequestcode(String.valueOf(object.getString("requestcode")));
   																						al.setPath(object.getString("media"));
   																						al.setFriends(object.getString("connectedlist"));
   																						JSONArray jArray = object.getJSONArray("connected");
   																						try {
   																							al.setFriendsid(jArray.getString(0));
   																						} catch (JSONException e3) {
   																							// TODO Auto-generated catch block
   																							e3.printStackTrace();
   																						}
   																						
   																						al.setRepeattype(selectedAlarmType);
   																						
   																						al.setMilli(String.valueOf(object.getLong("alarmtimemilli")));
   																						
   																						String time = object.getString("alarmTime");
   																						String split[] = time.split("\\s");
   																						String date = split[0];

   																						al.setAtime(date);
   																					

   																						DatabaseHandler db = new DatabaseHandler(mContext);
   																						db.updateAlarm(al);
   																						//ends
   																						intent.putExtras(tempData);
   																						PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),Integer.parseInt(object.getString("requestcode")),intent,PendingIntent.FLAG_CANCEL_CURRENT);
   																						AlarmManager am = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
   																						am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
   																						
   																						Calendar calendar = Calendar.getInstance();
   																						calendar.setTimeInMillis(calendar.getTimeInMillis());
   																						calendar.add(Calendar.SECOND, 30);
   																						if (selectedAlarmType.equalsIgnoreCase("Once")) {
   																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
   																							
   																						} else if (selectedAlarmType.equalsIgnoreCase("Daily")) {

   																							calendar.add(Calendar.DAY_OF_WEEK, 1);
   																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
   																							
   																						} else if (selectedAlarmType.equalsIgnoreCase("Weekly")) {

   																							calendar.add(Calendar.WEEK_OF_MONTH, 1);
   																							am.set(AlarmManager.RTC_WAKEUP,
   																									object.getLong("alarmtimemilli"), pi);
   																						} else if (selectedAlarmType.equalsIgnoreCase("Monthly")) {

   																							calendar.add(Calendar.MONTH, 1);
   																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
   																						}
   																						final String alarmPath = object.getString("media");
   																					
   																						if(object.getBoolean("recorded")){
   																							ParseFile p = object.getParseFile("audioclip");
   																							if(p!=null){
   																							
   																								p.getDataInBackground(new GetDataCallback() {
   																									
   																									@Override
   																									public void done(byte[] data,
   																											com.parse.ParseException e) {
   																										if(e==null){
   																								
   																											try {
   																								
   																												File newFile = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),alarmPath);
   																												OutputStream os = new FileOutputStream(newFile);
   																												os.write(data);
   																												os.close();
   																											} catch (FileNotFoundException e1) {
   																									
   																												e1.printStackTrace();
   																											} catch(IOException e1){
   																												e1.printStackTrace();
   																											}
																										} 
																									}
																								});
																							}
																						}

																					}
																				});
   																			}

																	}

   															}

														}

													}
												} catch (Exception e1) {

													e1.printStackTrace();
												}

											}

										}
									});

									break;

								}
							}

						} catch (Exception e14) {
							// TODO Auto-generated catch block
							e14.printStackTrace();
						}
					}
				}
			});
		}
	}

}
