//package in.fastrack.app.m.fup.lib;
//
//import in.fastrack.app.m.fup.AlaramActivityNew;
//
//import in.fastrack.app.m.fup.FeedComments;
//import in.fastrack.app.m.fup.HomeActivity;
//import in.fastrack.app.m.fup.NotificationActivityNew;
//import in.fastrack.app.m.fup.R;
//import in.fastrack.app.m.fup.model.FeedEvent;
//import in.fastrack.app.m.fup.model.FeedObject;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.OutputStream;
//import java.util.Arrays;
//import java.util.Calendar;
//import java.util.Iterator;
//import java.util.List;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import com.parse.FindCallback;
//import com.parse.GetCallback;
//import com.parse.GetDataCallback;
//import com.parse.ParseException;
//import com.parse.ParseFile;
//import com.parse.ParseObject;
//import com.parse.ParseQuery;
//import com.parse.ParseUser;
//
//import de.greenrobot.event.EventBus;
//import android.app.AlarmManager;
//import android.app.Notification;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.os.Bundle;
//import android.os.Environment;
//import android.support.v4.app.NotificationCompat;
//import android.util.Log;
//
//public class NotificationReceiver extends BroadcastReceiver {
//	
//	private static final String TAG = "NotificationReceiver";
//	private static final int NOTIFICATION_ID = 1;
//	Context mContext;
//	String selectedAlarmType;
//	@Override
//	public void onReceive(Context context, Intent intent) {
//		this.mContext = context;
//		try {
//			if (intent == null)
//			{
//				Log.d(TAG, "Receiver intent null");
//			}
//			else
//			{
//				String action = intent.getAction();
//				Log.d(TAG, "got action " + action );
//				if (action.equals("in.fasttrack.app.m.fup.pushnotification.UPDATE_STATUS"))
//				{
//					String channel = intent.getExtras().getString("com.parse.Channel");
//					JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
//
//					Log.d(TAG, "got action " + action + " on channel " + channel + " with:");
//					Iterator itr = json.keys();
//					while (itr.hasNext()) {
//						String key = (String) itr.next();
//						if (key.equals("notificationTitle"))
//						{
//							//Intent pupInt = new Intent(context, ShowPopUp.class);
//							//pupInt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
//							//context.context.startActivity(pupInt);
//							 String notificationTitle = "My Custom Notification Title";
//				        	 String notificationDesc = "My Custom Notification Desc";
//							 createNotification(Calendar.getInstance().getTimeInMillis(), json.getString("notificationTitle"), json.getString("notificationDesc") );
//						}
//						Log.d(TAG, "..." + key + " => " + json.getString(key));
//					}
//				}
//			}
//
//		//query for fetchting all alarms related to the user
//
//			if(ParseUser.getCurrentUser() != null){
//				ParseQuery<ParseObject> query = ParseQuery.getQuery("Activity");				
//				query.whereEqualTo("readers", ParseUser.getCurrentUser().getObjectId());
//				query.include("creator");
//				query.include("reference");				
//				query.orderByDescending("createdAt");
//				query.findInBackground(new FindCallback<ParseObject>() {					
//					@Override
//					public void done(List<ParseObject> objects, ParseException e) {
//						if(e==null){
//							
//								//Data found
//								Log.i("Parse","Data present");
//								int k = 1;
//								for(ParseObject o:objects){
//								
//									int notificationType = o.getInt("notificationtype");
//									   
//								    if(notificationType==1 ||notificationType==8){
//								    	 //created or edited
//										Intent intent = new Intent(mContext, AlaramActivityNew.class);
//										Bundle tempData = new Bundle();
//										tempData.putString("alarm_category",o.getParseObject("reference").getString("category"));
//										tempData.putString("alarm_title",o.getParseObject("reference").getString("title"));
//										tempData.putBoolean("recorded",o.getParseObject("reference").getBoolean("recorded"));
//										tempData.putInt("alarmrequestcode",Integer.parseInt(o.getParseObject("reference").getString("requestcode")));
//										tempData.putString("alarmpath",o.getParseObject("reference").getString("media"));
//										tempData.putString("alarmfriends",o.getParseObject("reference").getString("connectedlist"));
//										if (o.getParseObject("reference").getString("alarm_repeat_type") != null)
//										tempData.putString("alarm_repeat_type",o.getParseObject("reference").getString("alarm_repeat_type"));
//										
//										if (o.getParseObject("reference").getString("alarm_repeat_type") != null)
//										selectedAlarmType = o.getParseObject("reference").getString("alarm_repeat_type");
//										
//										intent.putExtras(tempData);
//										PendingIntent pi = PendingIntent.getActivity(mContext,Integer.parseInt(o.getParseObject("reference").getString("requestcode")),intent,PendingIntent.FLAG_CANCEL_CURRENT);
//										AlarmManager am = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
//										am.set(AlarmManager.RTC_WAKEUP,o.getParseObject("reference").getLong("alarmtimemilli"), pi);
//										
//										Calendar calendar = Calendar.getInstance();
//										calendar.setTimeInMillis(calendar.getTimeInMillis());
//										calendar.add(Calendar.SECOND, 30);
//										if (selectedAlarmType.equalsIgnoreCase("Once")) {
//											am.set(AlarmManager.RTC_WAKEUP,o.getParseObject("reference").getLong("alarmtimemilli"), pi);
//											
//										} else if (selectedAlarmType.equalsIgnoreCase("Daily")) {
//
//											calendar.add(Calendar.DAY_OF_WEEK, 1);
//											am.set(AlarmManager.RTC_WAKEUP,o.getParseObject("reference").getLong("alarmtimemilli"), pi);
//											// am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,calendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pi);
//										} else if (selectedAlarmType.equalsIgnoreCase("Weekly")) {
//
//											calendar.add(Calendar.WEEK_OF_MONTH, 1);
//											am.set(AlarmManager.RTC_WAKEUP,o.getParseObject("reference").getLong("alarmtimemilli"), pi);
//										} else if (selectedAlarmType.equalsIgnoreCase("Monthly")) {
//
//											calendar.add(Calendar.MONTH, 1);
//											am.set(AlarmManager.RTC_WAKEUP,o.getParseObject("reference").getLong("alarmtimemilli"), pi);
//										}
//								    }
//								    
//								
//								
//								}
//								
//						}
//		
//								}
//								});
//		
//		
//		
//		
//		
//			}
//		
//		//ends here
//		
//		
//		} catch (Exception e) {
//			Log.d(TAG, "JSONException: " + e.getMessage());
//		}
//	}
//	
//	private void createNotification(long when, String title, String desc){
//		String notificationTitle = title;
//		String notificationContent = desc;
//		//large icon for notification,normally use App icon
//		Bitmap largeIcon = BitmapFactory.decodeResource(mContext.getResources(),R.drawable.ic_launcher);
//		int smalIcon =R.drawable.ic_launcher;
//		//String notificationData="This is data : notification data";
//
//		/*create intent for show notification details when user clicks notification*/
//		Intent intent =new Intent(mContext, HomeActivity.class);
//		intent.putExtra("notificationId", NOTIFICATION_ID);
//
//		/*create unique this intent from  other intent using setData */
//		//intent.setData(Uri.parse("content://"+when));
//		/*create new task for each notification with pending intent so we set Intent.FLAG_ACTIVITY_NEW_TASK */
//		PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
//		
//		/*get the system service that manage notification NotificationManager*/
//		NotificationManager notificationManager =(NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE); 
//
//		/*build the notification*/
//		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
//				mContext)
//				.setWhen(when)//time
//				.setContentTitle(notificationTitle)
//				.setContentText(notificationContent)				
//				.setSmallIcon(smalIcon)
//				.setAutoCancel(true)
//				.setTicker(notificationTitle)
//				.setLargeIcon(largeIcon)
//				.setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_VIBRATE| Notification.DEFAULT_SOUND)
//				.setContentIntent(pendingIntent);
//		
//		
//		/*Create notification with builder*/
//		Notification notification=notificationBuilder.build();
//
//		/*sending notification to system.Here we use unique id (when)for making different each notification
//		 * if we use same id,then first notification replace by the last notification*/
//		notificationManager.notify(NOTIFICATION_ID, notification);
//	}
//
//}
