package in.fastrack.app.m.fup.lib;


import in.fastrack.app.m.fup.AlaramActivityNew;
import in.fastrack.app.m.fup.FeedComments;
import in.fastrack.app.m.fup.HomeActivity;
import in.fastrack.app.m.fup.NotificationActivityNew;
import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.config.AppConfig;
import in.fastrack.app.m.fup.model.FeedEvent;
import in.fastrack.app.m.fup.model.FeedObject;
import in.fastrack.app.m.fup.model.UserFriendRelation;
import android.os.StrictMode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import de.greenrobot.event.EventBus;

public class NotificationService extends Service {
	
	private static int NOTIFICATION_ID = 1;
	private AppConfig _config;
	private int notificationCount = 0;
	UserFriendRelation ufr = null;
	String selectedAlarmType,str;
	Bitmap largeIcon ;
	int smallIcon ;
	private ParseFile p;
	private String t = null;
	ParseUser user;
	//JSONArray connectedFriendsS = null;
	JSONArray connectedSurpriseF = new JSONArray();
	String prefix,message = null,suffix,postfix,fromName,toName,fromId,toId,title = "";
	Bundle extras;
	
	String fullnameA = null,titleA = null, categoryA = null,objectidA = null;
	boolean createNotification;
	Intent intent11 = null;
	Intent intent22 = null;
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onCreate() {
		_config = new AppConfig(getApplicationContext());
		notificationCount = 0;
		super.onCreate();
	   if (android.os.Build.VERSION.SDK_INT > 9) {
	        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);
	    }
	 

	 
	   
	   /*ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
		query.whereEqualTo("user", ParseUser.getCurrentUser());
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> objects, ParseException e) {

				if (e == null) {
					try {
						if (objects.size() > 0) {
							for (ParseObject obj : objects) {
								  ufr = new UserFriendRelation();
								  ufr.setUserId(ParseUser.getCurrentUser().getObjectId().toString());
								  ufr.setConnectedFriendsS(obj.getJSONArray("connectedSF"));
								break;
							}
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				} else {
					Log.i("Parse", e.getMessage());
				}

			}
		});*/
	}
	
	
	
	

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if(ParseUser.getCurrentUser() != null){
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Activity");
			query.whereEqualTo("followers",ParseUser.getCurrentUser().getObjectId());
			//query.whereEqualTo("notificationDone",false);
			query.whereNotEqualTo("readers", ParseUser.getCurrentUser().getObjectId());
			query.include("creator");
			query.include("reference");
			query.include("reference2");
			query.include("reference3");
			query.include("reference4");
			query.include("reference5");
			query.orderByDescending("createdAt");
			query.setLimit(10);
			query.findInBackground(new FindCallback<ParseObject>() {
				
				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					if(e==null){
						try {
							//Data found
							Log.i("Parse","Data present");
							int k = 1;
							for(ParseObject o:objects){
								final ParseObject parseO = o;
								notificationCount++;
								int notificationType = o.getInt("notificationtype");
								
								createNotification = true;
								Intent intent = null;
								
								
								extras = new Bundle();
								
								/////////////////////////////////
                             /*   if(o.getParseObject("reference")!=null) {
								ParseQuery<ParseUser> query2 = ParseUser.getQuery();
								Log.i("Parse","Data present11111"+o.getParseObject("reference").getString("creatorplain"));
								query2.whereEqualTo("user", o.getParseObject("reference").getString("creatorplain"));
								//query2.whereNotEqualTo("readers", ParseUser.getCurrentUser().getObjectId());
								query2.getInBackground(o.getParseObject("reference").getString("creatorplain"),new GetCallback<ParseUser>() {
											@Override
											public void done(ParseUser object,ParseException e) {
												ParseQuery<ParseObject> query3 = ParseQuery.getQuery("Friends");
												query3.whereEqualTo("user", object);
												query3.findInBackground(new FindCallback<ParseObject>() {
													public void done(List<ParseObject> objects,ParseException e) {
														
														if (objects.size() > 0) {
															// edit
															if (e == null) {
																	
																try {
																	if (objects.size() > 0) {
																		for (ParseObject obj : objects) {
																			if(obj.getJSONArray("connectedSF")!=null){
																			Log.i("Parse","Data present11111"+obj.getJSONArray("connectedSF").toString());
																			ufr.setConnectedFriendsS(obj.getJSONArray("connectedSF"));
																			//connectedFriendsS = obj.getJSONArray("connectedSF");
																			
																			}
																			break;
																		}
																	}
																} catch (Exception e1) {
																	// TODO Auto-generated catch block
																	e1.printStackTrace();
																}

															
															
															}
															else{
																Log.i("Parse", e.getMessage());
															}
															} else {
																
																Log.i("Parse", e.getMessage());
															}
														
														
														
														}
																									
																																					
																							
												});
											}
										});
     }*/
								//////////////////////////////
									
								
								
								switch(notificationType){
								case 1:
									
									ParseQuery<ParseObject> query = ParseQuery.getQuery("Friends");
									
									query.whereEqualTo("user", ParseUser.getCurrentUser());
									query.findInBackground(new FindCallback<ParseObject>() {
										@Override
										public void done(List<ParseObject> objects, ParseException e) {

											if (e == null) {
												try {
													if (objects.size() > 0) {
														for (ParseObject obj : objects) {														 
															connectedSurpriseF = obj.getJSONArray("connectedSF");
															//Log.i("Parse", "connectedSurpriseF"+connectedSurpriseF.toString());
															break;
														}
														
														if(connectedSurpriseF!=null)
														{
															//Log.i("File", "connected"+ connectedSurpriseF.toString());
															if (connectedSurpriseF.length() > 0) {
															for (int i = 0; i < connectedSurpriseF.length(); i++) {
																
																	//if (connectedSurpriseF != null) {
																		Log.i("File", "in if \n" +connectedSurpriseF.get(i)+"\n" + parseO.getParseObject("reference").getString("creatorplain"));
																		if (connectedSurpriseF.get(i).equals(parseO.getParseObject("reference").getString("creatorplain"))) {//ParseUser.getCurrentUser().getObjectId().toString()

																			
																			
																			
																			//started
																			try {
																		 
																				createNotification = false;
																				
																				//parseO.addAllUnique("readers",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
																				
																				parseO.put("readers", Arrays.asList(ParseUser.getCurrentUser().getObjectId()));//ParseUser.getCurrentUser().getObjectId().toString()
																				parseO.add("readers", parseO.getParseObject("reference").getString("creatorplain"));																				
																				parseO.saveInBackground();
																				
																				ParseQuery<ParseObject> not = ParseQuery.getQuery("alarms");
																				not.getInBackground(parseO.getParseObject("reference").getObjectId(), new GetCallback<ParseObject>() {
																					@Override
																					public void done(ParseObject object, com.parse.ParseException e) {
																						object.addAllUnique("acceptedpeople",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
																						object.addAllUnique("notificationactions",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
																						object.saveInBackground();//saveEventually();();
																						//Set alarm
																						Intent intent = new Intent(getApplicationContext(),AlaramActivityNew.class);
																						Bundle tempData = new Bundle();
																						tempData.putString("alarm_category",object.getString("category"));
																						tempData.putString("alarm_title", object.getString("title"));
																						tempData.putBoolean("recorded",object.getBoolean("recorded"));
																						tempData.putInt("alarmrequestcode",Integer.parseInt(object.getString("requestcode")));
																						tempData.putString("alarmpath",object.getString("media"));
																						tempData.putString("alarmfriends",object.getString("connectedlist"));
																						if (object.getString("alarm_repeat_type") != null)
																						tempData.putString("alarm_repeat_type",object.getString("alarm_repeat_type"));
																						
																						if (object.getString("alarm_repeat_type") != null)
																						selectedAlarmType = object.getString("alarm_repeat_type");
																						
																						intent.putExtras(tempData);
																						PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),Integer.parseInt(object.getString("requestcode")),intent,PendingIntent.FLAG_CANCEL_CURRENT);
																						AlarmManager am = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
																						am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																						
																						Calendar calendar = Calendar.getInstance();
																						calendar.setTimeInMillis(calendar.getTimeInMillis());
																						calendar.add(Calendar.SECOND, 30);
																						if (selectedAlarmType.equalsIgnoreCase("Once")) {
																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																							
																						} else if (selectedAlarmType.equalsIgnoreCase("Daily")) {

																							calendar.add(Calendar.DAY_OF_WEEK, 1);
																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																							// am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,calendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pi);
																						} else if (selectedAlarmType.equalsIgnoreCase("Weekly")) {

																							calendar.add(Calendar.WEEK_OF_MONTH, 1);
																							am.set(AlarmManager.RTC_WAKEUP,
																									object.getLong("alarmtimemilli"), pi);
																						} else if (selectedAlarmType.equalsIgnoreCase("Monthly")) {

																							calendar.add(Calendar.MONTH, 1);
																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																						}
																						
																					
																						
																						//Download audio if required
																						final String alarmPath = object.getString("media");
																						Log.i("File path",alarmPath);
																						if(object.getBoolean("recorded")){
																							ParseFile p = object.getParseFile("audioclip");
																							if(p!=null){
																								Log.i("File","A");
																								p.getDataInBackground(new GetDataCallback() {
																									
																									@Override
																									public void done(byte[] data,
																											com.parse.ParseException e) {
																										if(e==null){
																											Log.i("File","B");
																											try {
																												Log.i("File","C");
																												File newFile = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),alarmPath);
																												OutputStream os = new FileOutputStream(newFile);
																												os.write(data);
																												os.close();
																											} catch (FileNotFoundException e1) {
																												Log.i("ALARM FILE LOADED","FALSE");
																												e1.printStackTrace();
																											} catch(IOException e1){
																												e1.printStackTrace();
																											}
																										}else{
																											Log.i("File",e.getMessage());
																										}
																									}
																								});
																							}
																						}
																						
																					}
																				});
																			} catch (Exception e10) {
																				// TODO Auto-generated catch block
																				e10.printStackTrace();
																			}
																			//ended
																			
																			
																		} else {

																			Log.i("File", "in else !!!!!!!!!!!=============== ");
																			//Normal alarm notification
																		   	intent11 =new Intent(getApplicationContext(), NotificationActivityNew.class);
																			Log.i("Parse","ALarm");
																			prefix = parseO.getParseObject("creator").getString("fullname")+" ";
																			title = "An alarm request from "+prefix;
																			suffix = "added you to '"+parseO.getParseObject("reference").getString("title")+"' ";
																			postfix = "for category '"+parseO.getParseObject("reference").getString("category")+"' ";
																			message = prefix+suffix+postfix;
																			extras.putString("alarmid",parseO.getParseObject("reference").getObjectId());
																			extras.putBoolean("onlyone",true);														
																			intent11.putExtras(extras);
																		}
																	//}
																
															}
															
															}else{
																Log.i("File", "in else length== 0 ");
																Log.i("File", "in else ");
																//Normal alarm notification
															   	intent11 =new Intent(getApplicationContext(), NotificationActivityNew.class);
																Log.i("Parse","ALarm");
																prefix = parseO.getParseObject("creator").getString("fullname")+" ";
																title = "An alarm request from "+prefix;
																suffix = "added you to '"+parseO.getParseObject("reference").getString("title")+"' ";
																postfix = "for category '"+parseO.getParseObject("reference").getString("category")+"' ";
																message = prefix+suffix+postfix;
																extras.putString("alarmid",parseO.getParseObject("reference").getObjectId());
																extras.putBoolean("onlyone",true);														
																intent11.putExtras(extras);
															}
															
														}
														else
														{
	
															//Log.i("File", "in else ");
															Log.i("File", "connectedSurpriseF=null");
															//Normal alarm notification
														    intent11 =new Intent(getApplicationContext(), NotificationActivityNew.class);
															Log.i("Parse","ALarm");
															prefix = parseO.getParseObject("creator").getString("fullname")+" ";
															title = "An alarm request from "+prefix;
															suffix = "added you to '"+parseO.getParseObject("reference").getString("title")+"' ";
															postfix = "for category '"+parseO.getParseObject("reference").getString("category")+"' ";
															message = prefix+suffix+postfix;
															extras.putString("alarmid",parseO.getParseObject("reference").getObjectId());
															extras.putBoolean("onlyone",true);														
															intent11.putExtras(extras);
															
														}
														
														
													}
												} catch (Exception e1) {
													// TODO Auto-generated catch block
													e1.printStackTrace();
												}

											} else {
												Log.i("Parse", e.getMessage());
											}

										}
									});
									
									
									/*try {
										if(parseO != null){
											parseO=null;
										}
									} catch (Exception e2) {
										// TODO Auto-generated catch block
										e2.printStackTrace();
									}*/
									
									
									
									
									/*
									try {
								if(ufr != null && ufr.getUserId().equals(ParseUser.getCurrentUser().getObjectId().toString())) {
								connectedFriendsS = ufr.getConnectedFriendsS();
								  
									if( (connectedFriendsS != null) ){//
										
										Log.i("File", "connected"+ connectedFriendsS.toString());
										if (connectedFriendsS.length() > 0) {
										for (int i = 0; i < connectedFriendsS.length(); i++) {
											
												if (connectedFriendsS != null) {
													if (connectedFriendsS.get(i).equals(o.getParseObject("reference").getString("creatorplain"))) {//ParseUser.getCurrentUser().getObjectId().toString()

														Log.i("File", "in if \n" +connectedFriendsS.get(i)+"\n" + o.getParseObject("reference").getString("creatorplain"));
														
														
														//started
														try {
													 
															createNotification = false;
															
															o.addAllUnique("readers",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
															o.saveInBackground();
															
															ParseQuery<ParseObject> not = ParseQuery.getQuery("alarms");
															not.getInBackground(o.getParseObject("reference").getObjectId(), new GetCallback<ParseObject>() {
																@Override
																public void done(ParseObject object, com.parse.ParseException e) {
																	object.addAllUnique("acceptedpeople",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
																	object.addAllUnique("notificationactions",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
																	object.saveEventually();
																	//Set alarm
																	Intent intent = new Intent(getApplicationContext(),AlaramActivityNew.class);
																	Bundle tempData = new Bundle();
																	tempData.putString("alarm_category",object.getString("category"));
																	tempData.putString("alarm_title", object.getString("title"));
																	tempData.putBoolean("recorded",object.getBoolean("recorded"));
																	tempData.putInt("alarmrequestcode",Integer.parseInt(object.getString("requestcode")));
																	tempData.putString("alarmpath",object.getString("media"));
																	tempData.putString("alarmfriends",object.getString("connectedlist"));
																	if (object.getString("alarm_repeat_type") != null)
																	tempData.putString("alarm_repeat_type",object.getString("alarm_repeat_type"));
																	
																	if (object.getString("alarm_repeat_type") != null)
																	selectedAlarmType = object.getString("alarm_repeat_type");
																	
																	intent.putExtras(tempData);
																	PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),Integer.parseInt(object.getString("requestcode")),intent,PendingIntent.FLAG_CANCEL_CURRENT);
																	AlarmManager am = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
																	am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																	
																	Calendar calendar = Calendar.getInstance();
																	calendar.setTimeInMillis(calendar.getTimeInMillis());
																	calendar.add(Calendar.SECOND, 30);
																	if (selectedAlarmType.equalsIgnoreCase("Once")) {
																		am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																		
																	} else if (selectedAlarmType.equalsIgnoreCase("Daily")) {

																		calendar.add(Calendar.DAY_OF_WEEK, 1);
																		am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																		// am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,calendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pi);
																	} else if (selectedAlarmType.equalsIgnoreCase("Weekly")) {

																		calendar.add(Calendar.WEEK_OF_MONTH, 1);
																		am.set(AlarmManager.RTC_WAKEUP,
																				object.getLong("alarmtimemilli"), pi);
																	} else if (selectedAlarmType.equalsIgnoreCase("Monthly")) {

																		calendar.add(Calendar.MONTH, 1);
																		am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																	}
																	
																
																	
																	//Download audio if required
																	final String alarmPath = object.getString("media");
																	Log.i("File path",alarmPath);
																	if(object.getBoolean("recorded")){
																		ParseFile p = object.getParseFile("audioclip");
																		if(p!=null){
																			Log.i("File","A");
																			p.getDataInBackground(new GetDataCallback() {
																				
																				@Override
																				public void done(byte[] data,
																						com.parse.ParseException e) {
																					if(e==null){
																						Log.i("File","B");
																						try {
																							Log.i("File","C");
																							File newFile = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),alarmPath);
																							OutputStream os = new FileOutputStream(newFile);
																							os.write(data);
																							os.close();
																						} catch (FileNotFoundException e1) {
																							Log.i("ALARM FILE LOADED","FALSE");
																							e1.printStackTrace();
																						} catch(IOException e1){
																							e1.printStackTrace();
																						}
																					}else{
																						Log.i("File",e.getMessage());
																					}
																				}
																			});
																		}
																	}
																	
																}
															});
														} catch (Exception e10) {
															// TODO Auto-generated catch block
															e10.printStackTrace();
														}
														//ended
														
														
													} else {

														Log.i("File", "in else ");
														//Normal alarm notification
														intent =new Intent(getApplicationContext(), NotificationActivityNew.class);
														Log.i("Parse","ALarm");
														prefix = o.getParseObject("creator").getString("fullname")+" ";
														title = "An alarm request from "+prefix;
														suffix = "added you to '"+o.getParseObject("reference").getString("title")+"' ";
														postfix = "for category '"+o.getParseObject("reference").getString("category")+"' ";
														message = prefix+suffix+postfix;
														extras.putString("alarmid",o.getParseObject("reference").getObjectId());
														extras.putBoolean("onlyone",true);														
														intent.putExtras(extras);
													}
												}
											
										}
										
										}else{
											Log.i("File", "in else length== 0 ");
										}
									}
									else{
										Log.i("File", "in elseeeeeeeeeeeeeeeeeee ");
										//Normal alarm notification
										intent =new Intent(getApplicationContext(), NotificationActivityNew.class);
										Log.i("Parse","ALarm");
										prefix = o.getParseObject("creator").getString("fullname")+" ";
										title = "An alarm request from "+prefix;
										suffix = "added you to '"+o.getParseObject("reference").getString("title")+"' ";
										postfix = "for category '"+o.getParseObject("reference").getString("category")+"' ";
										message = prefix+suffix+postfix;
										extras.putString("alarmid",o.getParseObject("reference").getObjectId());
										extras.putBoolean("onlyone",true);										
										intent.putExtras(extras);
									}
										}
								} catch (Exception e91) {
									// TODO Auto-generated catch
									// block
									e91.printStackTrace();
									
								
								}
									
									*/
									
									
									
								/*										
									//Normal alarm notification
									intent =new Intent(getApplicationContext(), HomeActivity.class);
									Log.i("Parse","ALarm");
									prefix = o.getParseObject("creator").getString("fullname")+" ";
									title = "An alarm request from "+prefix;
									suffix = "created an Alarm '"+o.getParseObject("reference").getString("title")+"' ";
									postfix = "";
									message = prefix+suffix+postfix;
									extras.putString("alarmid",o.getParseObject("reference").getObjectId());
									extras.putBoolean("onlyone",true);									
									intent.putExtras(extras);*/
								
								break;
								case 2:
									//Someone commented
									intent =new Intent(getApplicationContext(), FeedComments.class);
									Log.i("Parse","Comment");
									prefix = o.getParseObject("creator").getString("fullname")+" ";
									//if(o.getParseObject("creator").getObjectId() == o.getParseObject("reference").get("creator")
									title = prefix+" commented on a alarm";
									suffix = o.getParseObject("reference2").getString("comment");
									postfix = "";
									message = suffix+postfix;
									extras.putString("alarmid",o.getParseObject("reference").getObjectId());
									extras.putInt("position",0);
									intent.putExtras(extras);
								break;
								case 3:
									//Someone liked
									intent =new Intent(getApplicationContext(), HomeActivity.class);
									Log.i("Parse","Liked");
									prefix = o.getParseObject("creator").getString("fullname")+" ";
									//if(o.getParseObject("creator").getObjectId() == o.getParseObject("reference").get("creator")
									title = "F-Up";
									suffix = prefix+"liked an Alarm";
									postfix = "";
									message = suffix+postfix;
									extras.putString("alarmid",o.getParseObject("reference").getObjectId());
									extras.putBoolean("onlyone",true);
									intent.putExtras(extras);
								break;
								case 4:
									//Alarm updated
								break;
								case 5:
									//Friend notification
									intent =new Intent(getApplicationContext(), NotificationActivityNew.class);
									Log.i("Parse","FriendRequest");
									prefix = o.getParseObject("creator").getString("fullname")+" ";
									title = "An friend request from "+prefix;
									//suffix = "send an friend request '"+o.getParseObject("reference3").getString("title")+"' ";
									suffix = "send an friend request";
									postfix = "";
									message = prefix+suffix+postfix;
									extras.putString("alarmid",o.getParseObject("reference3").getObjectId());
									extras.putBoolean("onlyone",true);
									intent.putExtras(extras);
									//List<String> arr = o.getList("followers");
									
								break;
								case 6: 
									//Surprise Friend notification
									intent =new Intent(getApplicationContext(), NotificationActivityNew.class);
									Log.i("Parse","SurpriseFriendRequest");
									prefix = o.getParseObject("creator").getString("fullname")+" ";
									title = "An surprise request from  "+prefix;
									//suffix = "want to be your surprise friend and can set alarm for you at any time '"+o.getParseObject("reference4").getString("title")+"' ";
									suffix = "requested to be your surprise friend";
									postfix = "";
									message = prefix+suffix+postfix;
									extras.putString("alarmid",o.getParseObject("reference4").getObjectId());
									extras.putBoolean("onlyone",true);
									intent.putExtras(extras);
									//List<String> arr = o.getList("followers");
									
								break;
								
								case 7:
									//Accept alarm notification
									intent =new Intent(getApplicationContext(), NotificationActivityNew.class);
									Log.i("Parse","Accept ALarm");
									prefix = o.getParseObject("creator").getString("fullname")+" ";
									title = "Alarm request was accepted by"+prefix;
									suffix = "accepted your request for '"+o.getParseObject("reference").getString("title")+"' ";
									postfix = "of category '"+o.getParseObject("reference").getString("category")+"' ";
									message = prefix+suffix+postfix;
									extras.putString("alarmid",o.getParseObject("reference").getObjectId());
									extras.putBoolean("onlyone",true);
									intent.putExtras(extras);
									//List<String> arr = o.getList("followers");
									break;
								case 8:
									
									//Edit alarm notification
                                     ParseQuery<ParseObject> query_edit = ParseQuery.getQuery("Friends");
									
                                     query_edit.whereEqualTo("user", ParseUser.getCurrentUser());
                                     query_edit.findInBackground(new FindCallback<ParseObject>() {
										@Override
										public void done(List<ParseObject> objects, ParseException e) {

											if (e == null) {
												try {
													if (objects.size() > 0) {
														for (ParseObject obj : objects) {														 
															connectedSurpriseF = obj.getJSONArray("connectedSF");
															//Log.i("Parse", "connectedSurpriseF"+connectedSurpriseF.toString());
															break;
														}
														
														if(connectedSurpriseF!=null)
														{
															//Log.i("File", "connected"+ connectedSurpriseF.toString());
															if (connectedSurpriseF.length() > 0) {
															for (int i = 0; i < connectedSurpriseF.length(); i++) {
																
																	//if (connectedSurpriseF != null) {
																		Log.i("File", "in if \n" +connectedSurpriseF.get(i)+"\n" + parseO.getParseObject("reference").getString("creatorplain"));
																		if (connectedSurpriseF.get(i).equals(parseO.getParseObject("reference").getString("creatorplain"))) {//ParseUser.getCurrentUser().getObjectId().toString()

																			
																			
																			
																			//started
																			try {
																		 
																				createNotification = false;
																				
																				//parseO.addAllUnique("readers",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
																				
																				parseO.put("readers", Arrays.asList(ParseUser.getCurrentUser().getObjectId()));//ParseUser.getCurrentUser().getObjectId().toString()
																				parseO.add("readers", parseO.getParseObject("reference").getString("creatorplain"));																				
																				parseO.saveInBackground();
																				
																				ParseQuery<ParseObject> not = ParseQuery.getQuery("alarms");
																				not.getInBackground(parseO.getParseObject("reference").getObjectId(), new GetCallback<ParseObject>() {
																					@Override
																					public void done(ParseObject object, com.parse.ParseException e) {
																						object.addAllUnique("acceptedpeople",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
																						object.addAllUnique("notificationactions",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
																						object.saveInBackground();//saveEventually();();
																						
																						//Set alarm
																						Intent intent = new Intent(getApplicationContext(),AlaramActivityNew.class);
																						Bundle tempData = new Bundle();
																						tempData.putString("alarm_category",object.getString("category"));
																						tempData.putString("alarm_title", object.getString("title"));
																						tempData.putBoolean("recorded",object.getBoolean("recorded"));
																						tempData.putInt("alarmrequestcode",Integer.parseInt(object.getString("requestcode")));
																						tempData.putString("alarmpath",object.getString("media"));
																						tempData.putString("alarmfriends",object.getString("connectedlist"));
																						if (object.getString("alarm_repeat_type") != null)
																						tempData.putString("alarm_repeat_type",object.getString("alarm_repeat_type"));
																						
																						if (object.getString("alarm_repeat_type") != null)
																						selectedAlarmType = object.getString("alarm_repeat_type");
																						
																						intent.putExtras(tempData);
																						PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),Integer.parseInt(object.getString("requestcode")),intent,PendingIntent.FLAG_CANCEL_CURRENT);
																						AlarmManager am = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
																						am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																						
																						Calendar calendar = Calendar.getInstance();
																						calendar.setTimeInMillis(calendar.getTimeInMillis());
																						calendar.add(Calendar.SECOND, 30);
																						if (selectedAlarmType.equalsIgnoreCase("Once")) {
																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																							
																						} else if (selectedAlarmType.equalsIgnoreCase("Daily")) {

																							calendar.add(Calendar.DAY_OF_WEEK, 1);
																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																							// am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,calendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pi);
																						} else if (selectedAlarmType.equalsIgnoreCase("Weekly")) {

																							calendar.add(Calendar.WEEK_OF_MONTH, 1);
																							am.set(AlarmManager.RTC_WAKEUP,
																									object.getLong("alarmtimemilli"), pi);
																						} else if (selectedAlarmType.equalsIgnoreCase("Monthly")) {

																							calendar.add(Calendar.MONTH, 1);
																							am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																						}
																						
																					
																						
																						//Download audio if required
																						final String alarmPath = object.getString("media");
																						Log.i("File path",alarmPath);
																						if(object.getBoolean("recorded")){
																							ParseFile p = object.getParseFile("audioclip");
																							if(p!=null){
																								Log.i("File","A");
																								p.getDataInBackground(new GetDataCallback() {
																									
																									@Override
																									public void done(byte[] data,
																											com.parse.ParseException e) {
																										if(e==null){
																											Log.i("File","B");
																											try {
																												Log.i("File","C");
																												File newFile = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),alarmPath);
																												OutputStream os = new FileOutputStream(newFile);
																												os.write(data);
																												os.close();
																											} catch (FileNotFoundException e1) {
																												Log.i("ALARM FILE LOADED","FALSE");
																												e1.printStackTrace();
																											} catch(IOException e1){
																												e1.printStackTrace();
																											}
																										}else{
																											Log.i("File",e.getMessage());
																										}
																									}
																								});
																							}
																						}
																						
																					}
																				});
																			} catch (Exception e10) {
																				// TODO Auto-generated catch block
																				e10.printStackTrace();
																			}
																			//ended
																			
																			
																		} else {

																			//Edit alarm notification
																			Log.i("File", "in else !!!!!!!!!!!=============== edit");
																			intent11 =new Intent(getApplicationContext(), NotificationActivityNew.class);
																			Log.i("Parse","Edit ALarm");
																			prefix = parseO.getParseObject("creator").getString("fullname")+" ";
																			title = "Alarm was edited by"+prefix;
																			suffix = "edited the alarm for '"+parseO.getParseObject("reference").getString("title")+"'";
																			postfix = "of category '"+parseO.getParseObject("reference").getString("category")+"' ";
																			message = prefix+suffix+postfix;
																			extras.putString("alarmid",parseO.getParseObject("reference").getObjectId());
																			extras.putBoolean("onlyone",true);
																			intent11.putExtras(extras);
																			//List<String> arr = o.getList("followers");
																		}
																	//}
																
															}
															
															}else{
																Log.i("File", "in else length== 0 in edit ");
																intent11 =new Intent(getApplicationContext(), NotificationActivityNew.class);
																Log.i("Parse","Edit ALarm");
																prefix = parseO.getParseObject("creator").getString("fullname")+" ";
																title = "Alarm was edited by"+prefix;
																suffix = "edited the alarm for '"+parseO.getParseObject("reference").getString("title")+"'";
																postfix = "of category '"+parseO.getParseObject("reference").getString("category")+"' ";
																message = prefix+suffix+postfix;
																extras.putString("alarmid",parseO.getParseObject("reference").getObjectId());
																extras.putBoolean("onlyone",true);
																intent11.putExtras(extras);
																
															}
															
														}
														else
														{
	
															//Log.i("File", "in else ");
															Log.i("File", "connectedSurpriseF=null edit");
															intent11 =new Intent(getApplicationContext(), NotificationActivityNew.class);
															Log.i("Parse","Edit ALarm");
															prefix = parseO.getParseObject("creator").getString("fullname")+" ";
															title = "Alarm was edited by"+prefix;
															suffix = "edited the alarm for '"+parseO.getParseObject("reference").getString("title")+"'";
															postfix = "of category '"+parseO.getParseObject("reference").getString("category")+"' ";
															message = prefix+suffix+postfix;
															extras.putString("alarmid",parseO.getParseObject("reference").getObjectId());
															extras.putBoolean("onlyone",true);
															intent11.putExtras(extras);
															
														}
														
														
													}
												} catch (Exception e1) {
													// TODO Auto-generated catch block
													e1.printStackTrace();
												}

											} else {
												Log.i("Parse", e.getMessage());
											}

										}
									});
									
									
								/*	try {
										
										JSONArray connectedFriendsS = ufr.getConnectedFriendsS();
										if (connectedFriendsS != null) {

											Log.i("File", "connected"+ connectedFriendsS.toString());

											for (int i = 0; i < connectedFriendsS.length(); i++) {
												
													if (connectedFriendsS != null) {
														if (connectedFriendsS.get(i).equals(o.getParseObject("reference").getString("creatorplain"))) {//if (connectedFriendsS.get(i).equals(ParseUser.getCurrentUser().getObjectId().toString())) {//

															Log.i("File", "in if \n" +connectedFriendsS.get(i)+"\n" + o.getParseObject("reference").getString("creatorplain"));
															
															
															//started
															try {
														 
																createNotification = false;
																
																o.addAllUnique("readers",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
																o.saveInBackground();
																
																ParseQuery<ParseObject> not = ParseQuery.getQuery("alarms");
																not.getInBackground(o.getParseObject("reference").getObjectId(), new GetCallback<ParseObject>() {
																	@Override
																	public void done(ParseObject object, com.parse.ParseException e) {
																		object.addAllUnique("acceptedpeople",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
																		object.addAllUnique("notificationactions",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
																		object.saveEventually();
																		//Set alarm
																		Intent intent = new Intent(getApplicationContext(),AlaramActivityNew.class);
																		Bundle tempData = new Bundle();
																		tempData.putString("alarm_category",object.getString("category"));
																		tempData.putString("alarm_title", object.getString("title"));
																		tempData.putBoolean("recorded",object.getBoolean("recorded"));
																		tempData.putInt("alarmrequestcode",Integer.parseInt(object.getString("requestcode")));
																		tempData.putString("alarmpath",object.getString("media"));
																		tempData.putString("alarmfriends",object.getString("connectedlist"));
																		if (object.getString("alarm_repeat_type") != null)
																		tempData.putString("alarm_repeat_type",object.getString("alarm_repeat_type"));
																		
																		if (object.getString("alarm_repeat_type") != null)
																		selectedAlarmType = object.getString("alarm_repeat_type");
																		
																		intent.putExtras(tempData);
																		PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),Integer.parseInt(object.getString("requestcode")),intent,PendingIntent.FLAG_CANCEL_CURRENT);
																		AlarmManager am = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
																		am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																		
																		Calendar calendar = Calendar.getInstance();
																		calendar.setTimeInMillis(calendar.getTimeInMillis());
																		calendar.add(Calendar.SECOND, 30);
																		if (selectedAlarmType.equalsIgnoreCase("Once")) {
																			am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																			
																		} else if (selectedAlarmType.equalsIgnoreCase("Daily")) {

																			calendar.add(Calendar.DAY_OF_WEEK, 1);
																			am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																			// am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,calendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pi);
																		} else if (selectedAlarmType.equalsIgnoreCase("Weekly")) {

																			calendar.add(Calendar.WEEK_OF_MONTH, 1);
																			am.set(AlarmManager.RTC_WAKEUP,
																					object.getLong("alarmtimemilli"), pi);
																		} else if (selectedAlarmType.equalsIgnoreCase("Monthly")) {

																			calendar.add(Calendar.MONTH, 1);
																			am.set(AlarmManager.RTC_WAKEUP,object.getLong("alarmtimemilli"), pi);
																		}
																	
																		
																		//Download audio if required
																		final String alarmPath = object.getString("media");
																		Log.i("File path",alarmPath);
																		if(object.getBoolean("recorded")){
																			ParseFile p = object.getParseFile("audioclip");
																			if(p!=null){
																				Log.i("File","A");
																				p.getDataInBackground(new GetDataCallback() {
																					
																					@Override
																					public void done(byte[] data,
																							com.parse.ParseException e) {
																						if(e==null){
																							Log.i("File","B");
																							try {
																								Log.i("File","C");
																								File newFile = new File(Environment.getExternalStorageDirectory()+_config.getRecordAudioPath(),alarmPath);
																								OutputStream os = new FileOutputStream(newFile);
																								os.write(data);
																								os.close();
																							} catch (FileNotFoundException e1) {
																								Log.i("ALARM FILE LOADED","FALSE");
																								e1.printStackTrace();
																							} catch(IOException e1){
																								e1.printStackTrace();
																							}
																						}else{
																							Log.i("File",e.getMessage());
																						}
																					}
																				});
																			}
																		}
																		
																	}
																});
															} catch (Exception e101) {
																// TODO Auto-generated catch block
																e101.printStackTrace();
															}
															//ended
															
															
														} else {


															//Edit alarm notification
															intent =new Intent(getApplicationContext(), NotificationActivityNew.class);
															Log.i("Parse","Edit ALarm");
															prefix = o.getParseObject("creator").getString("fullname")+" ";
															title = "Alarm was edited by"+prefix;
															suffix = "edited the alarm for '"+o.getParseObject("reference").getString("title")+"'";
															postfix = "of category '"+o.getParseObject("reference").getString("category")+"' ";
															message = prefix+suffix+postfix;
															extras.putString("alarmid",o.getParseObject("reference").getObjectId());
															extras.putBoolean("onlyone",true);
															intent.putExtras(extras);
															//List<String> arr = o.getList("followers");
														}
													}
												
											}
										}
										else{

											//Edit alarm notification
											intent =new Intent(getApplicationContext(), NotificationActivityNew.class);
											Log.i("Parse","Edit ALarm");
											prefix = o.getParseObject("creator").getString("fullname")+" ";
											title = "Alarm was edited by"+prefix;
											suffix = "edited the alarm for '"+o.getParseObject("reference").getString("title")+"'";
											postfix = "of category '"+o.getParseObject("reference").getString("category")+"' ";
											message = prefix+suffix+postfix;
											extras.putString("alarmid",o.getParseObject("reference").getObjectId());
											extras.putBoolean("onlyone",true);
											intent.putExtras(extras);
											//List<String> arr = o.getList("followers");
										}
										
									} catch (Exception e92) {
										// TODO Auto-generated catch
										// block
										e92.printStackTrace();
									}*/
									
										
							    /*
									//Edit alarm notification
									intent =new Intent(getApplicationContext(), NotificationActivityNew.class);
									Log.i("Parse","Edit ALarm");
									prefix = o.getParseObject("creator").getString("fullname")+" ";
									title = "Alarm was edited by"+prefix;
									suffix = "edited the alarm for '"+o.getParseObject("reference").getString("title")+"'";
									postfix = "";
									message = prefix+suffix+postfix;
									extras.putString("alarmid",o.getParseObject("reference").getObjectId());
									extras.putBoolean("onlyone",true);
									intent.putExtras(extras);
									//List<String> arr = o.getList("followers");
                                */	
									
									
									
								break;
								case 9:
									
									//Remove alarm notification
									
									//Edit alarm notification
                                    ParseQuery<ParseObject> query_remove = ParseQuery.getQuery("Friends");
									
                                    query_remove.whereEqualTo("user", ParseUser.getCurrentUser());
                                    query_remove.findInBackground(new FindCallback<ParseObject>() {
										@Override
										public void done(List<ParseObject> objects, ParseException e) {

											if (e == null) {
												try {
													if (objects.size() > 0) {
														for (ParseObject obj : objects) {														 
															connectedSurpriseF = obj.getJSONArray("connectedSF");
															//Log.i("Parse", "connectedSurpriseF"+connectedSurpriseF.toString());
															break;
														}
														
														if(connectedSurpriseF!=null)
														{
															//Log.i("File", "connected"+ connectedSurpriseF.toString());
															if (connectedSurpriseF.length() > 0) {
															for (int i = 0; i < connectedSurpriseF.length(); i++) {
																
																	//if (connectedSurpriseF != null) {
																		Log.i("File", "in if \n" +connectedSurpriseF.get(i)+"\n" + parseO.getParseObject("reference").getString("creatorplain"));
																		if (connectedSurpriseF.get(i).equals(parseO.getParseObject("reference").getString("creatorplain"))) {//ParseUser.getCurrentUser().getObjectId().toString()

																			
																			
																			
																			//started
																			try {
																		 
																				createNotification = false;
																				
																				parseO.put("readers", Arrays.asList(ParseUser.getCurrentUser().getObjectId()));//ParseUser.getCurrentUser().getObjectId().toString()
																				parseO.add("readers", parseO.getParseObject("reference").getString("creatorplain"));																				
																				parseO.saveInBackground();
																																																																				
																			} catch (Exception e102) {
																				// TODO Auto-generated catch block
																				e102.printStackTrace();
																			}
																			//ended
																			
																			
																		} else {

																			//Edit alarm notification
																			Log.i("File", "in else !!!!!!!!!!!=============== remove");
																			//Remove alarm notification
																			intent11 =new Intent(getApplicationContext(), NotificationActivityNew.class);
																			Log.i("Parse","Remove ALarm");
																			prefix = parseO.getParseObject("creator").getString("fullname")+" ";
																			title = "Alarm was removed by"+prefix;
																			suffix = "removed the alarm for '"+parseO.getParseObject("reference").getString("title")+"'";
																			postfix = "of category '"+parseO.getParseObject("reference").getString("category")+"' ";
																			message = prefix+suffix+postfix;
																			extras.putString("alarmid",parseO.getParseObject("reference").getObjectId());
																			extras.putBoolean("onlyone",true);
																			intent11.putExtras(extras);
																		}
																	//}
																
															}
															
															}else{
																Log.i("File", "in else length== 0 in remove ");
																//Remove alarm notification
																intent11 =new Intent(getApplicationContext(), NotificationActivityNew.class);
																Log.i("Parse","Remove ALarm");
																prefix = parseO.getParseObject("creator").getString("fullname")+" ";
																title = "Alarm was removed by"+prefix;
																suffix = "removed the alarm for '"+parseO.getParseObject("reference").getString("title")+"'";
																postfix = "of category '"+parseO.getParseObject("reference").getString("category")+"' ";
																message = prefix+suffix+postfix;
																extras.putString("alarmid",parseO.getParseObject("reference").getObjectId());
																extras.putBoolean("onlyone",true);
																intent11.putExtras(extras);
																
															}
															
														}
														else
														{
	
															//Log.i("File", "in else ");
															Log.i("File", "connectedSurpriseF=null remove");
															//Remove alarm notification
															intent11 =new Intent(getApplicationContext(), NotificationActivityNew.class);
															Log.i("Parse","Remove ALarm");
															prefix = parseO.getParseObject("creator").getString("fullname")+" ";
															title = "Alarm was removed by"+prefix;
															suffix = "removed the alarm for '"+parseO.getParseObject("reference").getString("title")+"'";
															postfix = "of category '"+parseO.getParseObject("reference").getString("category")+"' ";
															message = prefix+suffix+postfix;
															extras.putString("alarmid",parseO.getParseObject("reference").getObjectId());
															extras.putBoolean("onlyone",true);
															intent11.putExtras(extras);
															
															
														}
														
														
													}
												} catch (Exception e1) {
													// TODO Auto-generated catch block
													e1.printStackTrace();
												}

											} else {
												Log.i("Parse", e.getMessage());
											}

										}
									});
									
								/*	try {
										
										JSONArray connectedFriendsS = ufr.getConnectedFriendsS();
										if (connectedFriendsS != null) {

											Log.i("File", "connected"+ connectedFriendsS.toString());

											for (int i = 0; i < connectedFriendsS.length(); i++) {
												
													if (connectedFriendsS != null) {
														if (connectedFriendsS.get(i).equals(o.getParseObject("reference").getString("creatorplain"))) {//if (connectedFriendsS.get(i).equals(ParseUser.getCurrentUser().getObjectId().toString())) {//

															Log.i("File", "in if \n" +connectedFriendsS.get(i)+"\n" + o.getParseObject("reference").getString("creatorplain"));
															
															
															//started
															try {
														 
																createNotification = false;
																
																o.addAllUnique("readers",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
																o.saveInBackground();
																																																																
															} catch (Exception e102) {
																// TODO Auto-generated catch block
																e102.printStackTrace();
															}
															//ended
															
															
														} else {

															//Remove alarm notification
															intent =new Intent(getApplicationContext(), NotificationActivityNew.class);
															Log.i("Parse","Remove ALarm");
															prefix = o.getParseObject("creator").getString("fullname")+" ";
															title = "Alarm was removed by"+prefix;
															suffix = "removed the alarm for '"+o.getParseObject("reference").getString("title")+"'";
															postfix = "of category '"+o.getParseObject("reference").getString("category")+"' ";
															message = prefix+suffix+postfix;
															extras.putString("alarmid",o.getParseObject("reference").getObjectId());
															extras.putBoolean("onlyone",true);
															intent.putExtras(extras);
															//List<String> arr = o.getList("followers");
														}
													}
												
											}
										}
										else{

											//Remove alarm notification
											intent =new Intent(getApplicationContext(), NotificationActivityNew.class);
											Log.i("Parse","Remove ALarm");
											prefix = o.getParseObject("creator").getString("fullname")+" ";
											title = "Alarm was removed by"+prefix;
											suffix = "removed the alarm for '"+o.getParseObject("reference").getString("title")+"'";
											postfix = "of category '"+o.getParseObject("reference").getString("category")+"' ";
											message = prefix+suffix+postfix;
											extras.putString("alarmid",o.getParseObject("reference").getObjectId());
											extras.putBoolean("onlyone",true);
											intent.putExtras(extras);
											//List<String> arr = o.getList("followers");
										}
										
									} catch (Exception e93) {
										// TODO Auto-generated catch
										// block
										e93.printStackTrace();
									}*/
									
									
									
									
									
									
									
								/*	//Remove alarm notification
									intent =new Intent(getApplicationContext(), NotificationActivityNew.class);
									Log.i("Parse","Remove ALarm");
									prefix = o.getParseObject("creator").getString("fullname")+" ";
									title = "Alarm was removed by"+prefix;
									suffix = "removed the alarm for '"+o.getParseObject("reference").getString("title")+"'";
									postfix = "";
									message = prefix+suffix+postfix;
									extras.putString("alarmid",o.getParseObject("reference").getObjectId());
									extras.putBoolean("onlyone",true);
									intent.putExtras(extras);
									//List<String> arr = o.getList("followers");						
*/									try {
										List<String> arr = o.getParseObject("reference").getList("Hideit");
										
										if(arr != null){
											Log.i("Actions","A");
											
											if(arr.contains(ParseUser.getCurrentUser().getObjectId())){
												Intent intent1 = new Intent(getApplicationContext(), AlaramActivityNew.class);
												PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),Integer.parseInt(o.getParseObject("reference").getString("requestcode")), intent1, PendingIntent.FLAG_CANCEL_CURRENT);
												AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
												alarmManager.cancel(pendingIntent);
												
												Log.i("Actions","B");
											}
										}
									} catch (Exception e13) {
										// TODO Auto-generated catch block
										e13.printStackTrace();
									}
									
																
								break;
								
								case 10:
									//Group notification
									intent =new Intent(getApplicationContext(), NotificationActivityNew.class);
									Log.i("Parse","Add group");
									prefix = o.getParseObject("creator").getString("fullname")+" ";
									title = "Added you to Group by"+prefix;
									suffix = "added you to group'"+o.getParseObject("reference5").getString("groupName")+"'";
									postfix = "";
									message = prefix+suffix+postfix;
									extras.putString("alarmid",o.getParseObject("reference5").getObjectId());
									extras.putBoolean("onlyone",true);
									intent.putExtras(extras);
									//List<String> arr = o.getList("followers");
									
								break;
								case 11:
									//Accept Friend notification
									intent =new Intent(getApplicationContext(), NotificationActivityNew.class);
									Log.i("Parse","FriendRequest");
									prefix = o.getParseObject("creator").getString("fullname")+" ";
									title = "Friend request was accepted by"+prefix;
									//suffix = "created an friend request '"+o.getParseObject("reference3").getString("title")+"' ";
									suffix = "accepted your friend request";
									postfix = "";
									message = prefix+suffix+postfix;
									extras.putString("alarmid",o.getParseObject("reference3").getObjectId());
									extras.putBoolean("onlyone",true);
									intent.putExtras(extras);
									//List<String> arr = o.getList("followers");
									
								break;
								case 12:
									//Accept Surprise Friend notification
									intent =new Intent(getApplicationContext(), NotificationActivityNew.class);
									Log.i("Parse","SurpriseFriendRequest");
									prefix = o.getParseObject("creator").getString("fullname")+" ";
									title = "Surprise Friend request was accepted by"+prefix;
									//suffix = " '"+o.getParseObject("reference4").getString("title")+"' ";
									suffix = "accepted your surprise friend request";
									postfix = "";
									message = prefix+suffix+postfix;
									extras.putString("alarmid",o.getParseObject("reference4").getObjectId());
									extras.putBoolean("onlyone",true);
									intent.putExtras(extras);
									//List<String> arr = o.getList("followers");
									
								break;
								
								}
								
							
								
								if(o.getParseObject("creator").getObjectId() == ParseUser.getCurrentUser().getObjectId()){
									Log.i("Parse","same user");
									createNotification = false;
								}
								if(createNotification){
									if(notificationType == 1 || notificationType == 2 || notificationType == 3 || notificationType == 7 || notificationType == 8 ||notificationType == 9  )
									{
									if(o.getParseObject("reference").getString("category")!=null){
										
										if(o.getParseObject("reference").getString("category").equals( "Alarm" )) {
											 smallIcon = R.drawable.alarm;
										} else if(o.getParseObject("reference").getString("category").equals( "Birthday Party" )) {
											 smallIcon = R.drawable.birthday;											
										} else if (o.getParseObject("reference").getString("category").equals( "Wedding Party")) {
											 smallIcon = R.drawable.wedding;											
										} else if (o.getParseObject("reference").getString("category").equals( "Run")) {
											 smallIcon = R.drawable.run;									
										} else if (o.getParseObject("reference").getString("category").equals( "Party Time")) {
											 smallIcon = R.drawable.party_time;											
										} else if (o.getParseObject("reference").getString("category").equals( "Movie Night")) {
											 smallIcon = R.drawable.movie_night;									
										} else if (o.getParseObject("reference").getString("category").equals("Lunch")) {
											 smallIcon = R.drawable.lunch;										
										} else if (o.getParseObject("reference").getString("category").equals("Ride")) {
											 smallIcon = R.drawable.let_ride;
										} else if (o.getParseObject("reference").getString("category").equals("Dinner")) {
											 smallIcon = R.drawable.dinner;											
										} else if (o.getParseObject("reference").getString("category").equals("Brunch")) {
											 smallIcon = R.drawable.brunch;											
										} else if (o.getParseObject("reference").getString("category").equals("Breakfast")) {
											 smallIcon = R.drawable.breakfast ;											
										}
										
									}
									else{								    	
										smallIcon =R.drawable.ic_launcher;
									}
									}
									else{
										smallIcon =R.drawable.ic_launcher;
									}
									// largeIcon = BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher);
									/*String imageUrl = "image";
									p = ParseUser.getCurrentUser().getParseFile("profilethumbnail");
									if(p!=null){
										String url = p.getUrl();
										imageUrl = url;
									}else{
										if(ParseUser.getCurrentUser().get("facebookid") != null && ParseUser.getCurrentUser().get("facebookid") != "") {
											t = ParseUser.getCurrentUser().get("facebookid").toString();
											if((t!=null)||(t!="")){
												imageUrl = "https://graph.facebook.com/"
														+ ParseUser.getCurrentUser().get("facebookid").toString()
														+ "/picture/?type=square";
											}
										}
									}
									*/
									
									//Creator of alarm
									//o.getParseObject("reference").getString("creatorplain")
									//Creator of notification
									//o.getParseObject("creator").getObjectId()
									
									if(o.getParseObject("creator").getObjectId().equals(ParseUser.getCurrentUser().getObjectId())) {
										 user = ParseUser.getCurrentUser();
									} else {
										
									str =	o.getParseObject("creator").getObjectId();
									if(str!=null)	{
								    user = getTheUser(); 
										 
									}
									else{
										Log.i("Parse","in else......");
									}
									}
									
									
									
									
									
								  String  imageUrl = "image";
									p = user.getParseFile("profilethumbnail");
									if(p!=null){
										String url = p.getUrl();
										//Log.i("FB IMAGE",url);
										imageUrl = url;
									}else{
										if(user.get("facebookid") != null && user.get("facebookid") != "") {
											t = user.get("facebookid").toString();
											if((t!=null)||(t!="")){
												imageUrl = "https://graph.facebook.com/"
														+ user.get("facebookid").toString()
														+ "/picture/?type=square";
											}
										}
									}
									
									
									if(!imageUrl.equals("image"))
									{																			
										   largeIcon = getBitmapFromURL(imageUrl);
										 
									}
									else{
										largeIcon = BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher);										
										Log.i("Parse","in else.....+++++.");
									}
									
									
									if(intent!=null)
									{
									Log.i("Parse","Notification intent");
     								PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
									NotificationManager notificationManager =(NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE); 
									NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
											getApplicationContext())
											.setWhen(Calendar.getInstance().getTimeInMillis())//time
											.setContentTitle(title)
											.setContentText(message)				
											.setSmallIcon(smallIcon)
											.setAutoCancel(true)											                                     									      
											.setTicker(title)
											.setLargeIcon(largeIcon)
											.setPriority(NotificationCompat.PRIORITY_MAX)
											.setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_VIBRATE| Notification.DEFAULT_SOUND)
											.setContentIntent(pendingIntent);
									Notification notification=notificationBuilder.build();
									notificationManager.notify(k, notification);
									//o.put("notificationDone",true);
									o.addAllUnique("readers",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
									o.saveInBackground();
									k++;
									
									}
									else if(intent11 !=null){
										Log.i("Parse","Notification intent11");
	     								PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent11, Intent.FLAG_ACTIVITY_NEW_TASK);
										NotificationManager notificationManager =(NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE); 
										NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
												getApplicationContext())
												.setWhen(Calendar.getInstance().getTimeInMillis())//time
												.setContentTitle(title)
												.setContentText(message)				
												.setSmallIcon(smallIcon)
												.setAutoCancel(true)											                                     									      
												.setTicker(title)
												.setLargeIcon(largeIcon)
												.setPriority(NotificationCompat.PRIORITY_MAX)
												.setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_VIBRATE| Notification.DEFAULT_SOUND)
												.setContentIntent(pendingIntent);
										Notification notification=notificationBuilder.build();
										notificationManager.notify(k, notification);
										//o.put("notificationDone",true);
										o.addAllUnique("readers",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
										o.saveInBackground();
										k++;
									}
									/*else if (intent22 !=null){
										Log.i("Parse","Notification intent22");
	     								PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent22, Intent.FLAG_ACTIVITY_NEW_TASK);
										NotificationManager notificationManager =(NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE); 
										NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
												getApplicationContext())
												.setWhen(Calendar.getInstance().getTimeInMillis())//time
												.setContentTitle(title)
												.setContentText(message)				
												.setSmallIcon(smallIcon)
												.setAutoCancel(true)											                                     									      
												.setTicker(title)
												.setLargeIcon(largeIcon)
												.setPriority(NotificationCompat.PRIORITY_MAX)
												.setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_VIBRATE| Notification.DEFAULT_SOUND)
												.setContentIntent(pendingIntent);
										Notification notification=notificationBuilder.build();
										notificationManager.notify(k, notification);
										//o.put("notificationDone",true);
										o.addAllUnique("readers",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
										o.saveInBackground();
										k++;
									}*/
									
									
									
					/*				if(notificationType == 1 || notificationType == 5 || notificationType == 6  || notificationType == 8  )
									{	
										Log.i("Parse","Notification");
	     								PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
										NotificationManager notificationManager =(NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE); 
										NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
												getApplicationContext())
												.setWhen(Calendar.getInstance().getTimeInMillis())//time
												.setContentTitle(title)
												.setContentText(message)				
												.setSmallIcon(smallIcon)
												.setAutoCancel(true)											
												.addAction(R.drawable.add_friends_to_pic_tick, "Accept", pendingIntent)
	                                            .addAction(R.drawable.add_friends_to_pic_minus, "Decline", pendingIntent)                                         									      
												.setTicker(title)
												.setLargeIcon(largeIcon)
												.setPriority(NotificationCompat.PRIORITY_MAX)
												.setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_VIBRATE| Notification.DEFAULT_SOUND)
												.setContentIntent(pendingIntent);
										Notification notification=notificationBuilder.build();
										notificationManager.notify(k, notification);
										//o.put("notificationDone",true);
										o.addAllUnique("readers",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
										o.saveInBackground();
										k++;
										
									}
									
									else{
										
										Log.i("Parse","Notification");
	     								PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
										NotificationManager notificationManager =(NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE); 
										NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
												getApplicationContext())
												.setWhen(Calendar.getInstance().getTimeInMillis())//time
												.setContentTitle(title)
												.setContentText(message)				
												.setSmallIcon(smallIcon)
												.setAutoCancel(true)											                                     									      
												.setTicker(title)
												.setLargeIcon(largeIcon)
												.setPriority(NotificationCompat.PRIORITY_MAX)
												.setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_VIBRATE| Notification.DEFAULT_SOUND)
												.setContentIntent(pendingIntent);
										Notification notification=notificationBuilder.build();
										notificationManager.notify(k, notification);
										//o.put("notificationDone",true);
										o.addAllUnique("readers",Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
										o.saveInBackground();
										k++;
										
									}*/
									
								
									
									
									
									
									
									
									
								}
							}
							//Set notification count if the app is open
							FeedObject object2= new FeedObject(3,objects.size(),false,0,0);
							EventBus.getDefault().postSticky(new FeedEvent(object2));
						} catch (Exception e14) {
							// TODO Auto-generated catch block
							e14.printStackTrace();
						}
					}else{
						Log.i("Parse",e.getMessage());
						//System.out.println(e.printStackTrace());
					}
				}
			});
		}
		return super.onStartCommand(intent, flags, startId);
	}
	
	


	public Bitmap getBitmapFromURL(String strURL) {
	    try {
	        URL url = new URL(strURL);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setDoInput(true);
	        connection.connect();
	        InputStream input = connection.getInputStream();
	        Bitmap myBitmap = BitmapFactory.decodeStream(input);
	        return myBitmap;
	    } catch (Exception e15) {
	        e15.printStackTrace();
	      //  largeIcon = BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher);
	        return null;
	    }
	}
	
	 
	public ParseUser getTheUser(){
		ParseUser user = null;
		ParseQuery<ParseUser> query1 = ParseUser.getQuery();
		query1.whereEqualTo("objectId",str);
		List<ParseUser> objects;
		try {
			objects = query1.find();
           // The query was successful.
       		for(ParseUser obj : objects) {
       			user = obj;
       			break;
       		}
		} catch (Exception e16) {
			// TODO Auto-generated catch block
			e16.printStackTrace();
		}
		return user;
	}

}
