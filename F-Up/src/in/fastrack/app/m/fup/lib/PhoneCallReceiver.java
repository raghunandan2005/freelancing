package in.fastrack.app.m.fup.lib;

import in.fastrack.app.m.fup.config.AppConfig;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

public class PhoneCallReceiver extends BroadcastReceiver {
	
	AppConfig _config;

	@Override
	public void onReceive(Context paramContext, Intent paramIntent) {
		Log.i("PAC","Phone Call Receiver");
		_config = new AppConfig(paramContext);
		int i = ((TelephonyManager) paramContext.getSystemService("phone")).getCallState();
		if((i==1) || (i == 2)){
			_config.setPhoneState(true);
		}else{
			_config.setPhoneState(false);
		}
	}

}
