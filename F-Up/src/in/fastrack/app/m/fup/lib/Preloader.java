package in.fastrack.app.m.fup.lib;

import android.app.ProgressDialog;
import android.content.Context;

public class Preloader {
	
	ProgressDialog mPreLoader;
	
	public void startPreloader(Context c,String message){
		mPreLoader = new ProgressDialog(c);
		mPreLoader.setMessage(message);
		mPreLoader.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mPreLoader.setCancelable(false);
		mPreLoader.show();
	}
	
	public void stopLoading() {
		mPreLoader.dismiss();
		mPreLoader = null;
	}

}
