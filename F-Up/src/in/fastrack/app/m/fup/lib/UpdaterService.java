package in.fastrack.app.m.fup.lib;

import in.fastrack.app.m.fup.NotificationActivity;
import in.fastrack.app.m.fup.R;
import in.fastrack.app.m.fup.config.AppConfig;

import java.util.Calendar;
import java.util.List;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class UpdaterService extends Service {
	private static final int NOTIFICATION_ID = 1;
	private AppConfig _config;
	private int notificationCount = 0;
	
	private String notificationTitle = "";
	private String notificationDesc = "";
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onCreate() {
		Log.e("Service", "create");
		_config = new AppConfig(getApplicationContext());
		notificationCount = 0;
		super.onCreate();
	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		 Log.e("UpdaterService", "Inside onStartCommand");
    	 ParseQuery<ParseObject> query = ParseQuery.getQuery("Notifications");
    	 query.whereEqualTo("toid",ParseUser.getCurrentUser().getObjectId());
    	 query.whereEqualTo("notificationDone",false);
    	 query.include("actionref");
 		 query.include("fromid");
 		 query.orderByDescending("createdAt");
 		 query.setLimit(10);
 		 query.findInBackground(new FindCallback<ParseObject>() {
 			public void done(List<ParseObject> objects, ParseException e) {
 				if(e == null){
 					for(ParseObject obj: objects){
 						notificationCount++;
 						notificationTitle = "An new alarm created \"" + obj.getParseObject("actionref").getString("title") + "\"";
 						notificationDesc = "Alarm created by " + obj.getParseObject("fromid").getString("fullname") + "." ;
 						Log.e("UpdaterService", "created Notification");
 						createNotification(Calendar.getInstance().getTimeInMillis(), notificationTitle, notificationDesc);
 						obj.put("notificationDone", true);
 						obj.saveInBackground();
 					}
 				} else {
 					
 				}
 			}
 		 });
    	 //createNotification(Calendar.getInstance().getTimeInMillis(), notificationTitle, notificationDesc);
    	 return super.onStartCommand(intent, flags, startId);
	}
	
	
	public void onDestroy() {
	    super.onDestroy();
	    Log.d("Service", "destroy");
	    Intent myAlarm = new Intent(getApplicationContext(), NotifyingReceiver.class);
	    //myAlarm.putExtra("project_id",project_id); //put the SAME extras
	    PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm, PendingIntent.FLAG_CANCEL_CURRENT);
	    AlarmManager alarms = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
	    alarms.cancel(recurringAlarm);
	}
	
	private void createNotification(long when, String title, String desc){
		String notificationTitle = title;
		String notificationContent = desc;
		//large icon for notification,normally use App icon
		Bitmap largeIcon = BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher);
		int smalIcon =R.drawable.ic_launcher;
		//String notificationData="This is data : notification data";

		/*create intent for show notification details when user clicks notification*/
		Intent intent =new Intent(getApplicationContext(), NotificationActivity.class);
		intent.putExtra("notificationId", NOTIFICATION_ID);

		/*create unique this intent from  other intent using setData */
		//intent.setData(Uri.parse("content://"+when));
		/*create new task for each notification with pending intent so we set Intent.FLAG_ACTIVITY_NEW_TASK */
		PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
		
		/*get the system service that manage notification NotificationManager*/
		NotificationManager notificationManager =(NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE); 

		/*build the notification*/
		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
				getApplicationContext())
				.setWhen(when)//time
				.setContentTitle(notificationTitle)
				.setContentText(notificationContent)				
				.setSmallIcon(smalIcon)
				.setAutoCancel(true)
				.setTicker(notificationTitle)
				.setLargeIcon(largeIcon)
				.setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_VIBRATE| Notification.DEFAULT_SOUND)
				.setContentIntent(pendingIntent);
		
		
		/*Create notification with builder*/
		Notification notification=notificationBuilder.build();

		/*sending notification to system.Here we use unique id (when)for making different each notification
		 * if we use same id,then first notification replace by the last notification*/
		notificationManager.notify(NOTIFICATION_ID, notification);
	}

	
}
