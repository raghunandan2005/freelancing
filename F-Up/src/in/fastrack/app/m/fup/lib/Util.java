package in.fastrack.app.m.fup.lib;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.view.View;
import android.view.View.OnClickListener;

import com.facebook.Session;
import com.parse.ParseFacebookUtils;

public class Util {
	
	public static String current_profile_id; 
	
	public static boolean isNetworkAvailable(Context ctx) {

		ConnectivityManager connectivityManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		try {
			if (connectivityManager != null) {
				if (connectivityManager.getNetworkInfo(
						ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting() == true|| connectivityManager.getNetworkInfo(
								ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting() == true) {
					return true;
				}
			}
		} catch (Exception e) {

		}
		return false;
	}
	public static OnClickListener onClickListner = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Dialog dialog = (Dialog) v.getTag();
			dialog.dismiss();
		}
	
	};
	public static OnClickListener retryClickListner = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Dialog dialog = (Dialog) v.getTag();
			dialog.dismiss();
					
		}	
	};
	public static String getCurrent_profile_id() {
		return current_profile_id;
	}
	public static void setCurrent_profile_id(String current_profile_id) {
		Util.current_profile_id = current_profile_id;
	}
	
	////// Code for Date
	public static String getFullDate(Date date) {
        SimpleDateFormat formatted = new SimpleDateFormat("hh:mm aa - dd MMM yy");
        return formatted.format(date);
    }

    private static String getShortDateYear(Date date) {
        SimpleDateFormat formatted = new SimpleDateFormat("dd MMM yy, hh:mm aa");
        return formatted.format(date);
    }

    private static String getShortDate(Date date) {
        SimpleDateFormat formatted = new SimpleDateFormat("dd MMM, hh:mm aa");
        return formatted.format(date);
    }

    private static String getTimeOnly(Date date) {
        SimpleDateFormat formatted = new SimpleDateFormat("hh:mm aa");
        return formatted.format(date);
    }

    /*
     * Given 1234567890, return "1,234,567,890"
     */
    public static String getPrettyCount(int count) {
        String regex = "(\\d)(?=(\\d{3})+$)";
        return Integer.toString(count).replaceAll(regex, "$1,");
    }


    public static String getPrettyFullDate(Date date) {
        Date currentDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        int diffInDays = sdf.format(currentDate).compareTo(sdf.format(date));

        if (diffInDays > 0) {
            if (diffInDays > 300) {
                return Util.getShortDateYear(date);
            } else {
                return Util.getShortDate(date);
            }
        } else {
            return Util.getTimeOnly(date);
        }
    }

    /*
     *
	 */
    public static String getPrettyDate(Date createdAt) {

        return getPrettyDate(createdAt, new Date());
    }

    /*
	 * 
	 */
    private static String getPrettyDate(Date olderDate, Date newerDate) {

        String result;

        int diffInDays = (int) ((newerDate.getTime() - olderDate.getTime()) / (1000 * 60 * 60 * 24));
        if (diffInDays > 365) {
            SimpleDateFormat formatted = new SimpleDateFormat("dd MMM yy");
            result = formatted.format(olderDate);
        } else if (diffInDays > 0) {
            if (diffInDays == 1) {
                result = "1d";
            } else if (diffInDays < 8) {
                result = diffInDays + "d";
            } else {
                SimpleDateFormat formatted = new SimpleDateFormat("dd MMM");
                result = formatted.format(olderDate);
            }
        } else {
            int diffInHours = (int) ((newerDate.getTime() - olderDate.getTime()) / (1000 * 60 * 60));
            if (diffInHours > 0) {
                if (diffInHours == 1) {
                    result = "1h";
                } else {
                    result = diffInHours + "h";
                }
            } else {
                int diffInMinutes = (int) ((newerDate.getTime() - olderDate
                        .getTime()) / (1000 * 60));
                if (diffInMinutes > 0) {
                    if (diffInMinutes == 1) {
                        result = "1m";
                    } else {
                        result = diffInMinutes + "m";
                    }
                } else {
                    int diffInSeconds = (int) ((newerDate.getTime() - olderDate
                            .getTime()) / (1000));
                    if (diffInSeconds < 5) {
                        result = "now";
                    } else {
                        result = diffInSeconds + "s";
                    }
                }
            }
        }

        return result;
    }
    
    ///End of date code
    
    
    // String lower case with underscore replacement
    public static String stringtolower(String myString){
		return myString.replaceAll(" ","_").toLowerCase(Locale.getDefault());
	}
    
    public static boolean isFbUser(){
    	Session fbUser = ParseFacebookUtils.getSession();
    	if(fbUser != null){
    		return true;
    	}
    	return false;
    }

}
