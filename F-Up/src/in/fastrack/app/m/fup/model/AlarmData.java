package in.fastrack.app.m.fup.model;

import java.util.List;

import android.content.Context;
import android.text.style.UpdateAppearance;
import android.util.Log;

import com.orm.SugarRecord;
import com.orm.query.Select;

public class AlarmData extends SugarRecord<AlarmData> {
	
	String userid,username,alarmcategory,alarmtitle,alarmtime,alarm_repeat_type,alarmstatus,friends,alarmpath;

	int alarmid,alarmrequestcode;
	boolean isRecorded = false;

	public AlarmData(Context ctx) {
		super(ctx);
	}
	
	public AlarmData(Context ctx,String userid,String username,String alarmcategory,String alarmtitle,String alarmtime,String alarmpath,String alarmstatus,int alarmrequestcode,String friends,boolean isRecorded,String alarm_repeat_type){
		super(ctx);
		this.userid = userid;
		this.username = username;
		this.alarmcategory = alarmcategory;
		this.alarmtitle = alarmtitle;
		this.alarmtime = alarmtime;
		
		this.alarmpath = alarmpath;
		this.isRecorded = isRecorded;
		this.alarmrequestcode = alarmrequestcode;
		this.alarmstatus = alarmstatus;
		this.alarm_repeat_type = alarm_repeat_type;
	}
	
	
	
	

	public String getFriends() {
		return friends;
	}

	public void setFriends(String friends) {
		this.friends = friends;
	}

	public String getAlarmstatus() {
		return alarmstatus;
	}

	public void setAlarmstatus(String alarmstatus) {
		this.alarmstatus = alarmstatus;
	}

	public int getAlarmrequestcode() {
		return alarmrequestcode;
	}

	public void setAlarmrequestcode(int alarmrequestcode) {
		this.alarmrequestcode = alarmrequestcode;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAlarmcategory() {
		return alarmcategory;
	}

	public void setAlarmcategory(String alarmcategory) {
		this.alarmcategory = alarmcategory;
	}

	public String getAlarmtitle() {
		return alarmtitle;
	}

	public void setAlarmtitle(String alarmtitle) {
		this.alarmtitle = alarmtitle;
	}

	public String getAlarmtime() {
		return alarmtime;
	}

	public void setAlarmtime(String alarmtime) {
		this.alarmtime = alarmtime;
	}
	public String getAlarm_repeat_type() {
		return alarm_repeat_type;
	}

	public void setAlarm_repeat_type(String alarm_repeat_type) {
		this.alarm_repeat_type = alarm_repeat_type;
	}

	public String getAlarmpath() {
		return alarmpath;
	}

	public void setAlarmpath(String alarmpath) {
		this.alarmpath = alarmpath;
	}

	public boolean isRecorded() {
		return isRecorded;
	}

	public void setRecorded(boolean isRecorded) {
		this.isRecorded = isRecorded;
	}
	
	public static List<AlarmData> findData(Context ctx,String aid){
		List<AlarmData> data = AlarmData.findWithQuery(AlarmData.class,"Select * from ALARM_DATA where alarmid = ?",aid);
		Log.i("DATA OP",data.toString());
		if(!data.isEmpty()){
			return data;
		}
		return null;
	}

	@Override
	public String toString() {
		return "AlarmData [userid=" + userid + ", username=" + username
				+ ", alarmcategory=" + alarmcategory + ", alarmtitle="
				+ alarmtitle + ", alarmtime=" + alarmtime + ", alarmstatus="
				+ alarmstatus + ", friends=" + friends + ", alarmpath="
				+ alarmpath + ", alarmid=" + alarmid + ", alarmrequestcode="
				+ alarmrequestcode + ", isRecorded=" + isRecorded + "]";
	}
	
	


	
	
}
