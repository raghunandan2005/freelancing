package in.fastrack.app.m.fup.model;




public class AlarmFriend  {
	
	String name;
	String userId;;
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserImage() {
		return userImage;
	}

	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}

	String userImage;

	public AlarmFriend() {
	}
	public AlarmFriend(String name, String userImage) {
		super();
		this.name = name;
		this.userImage = userImage;

	}
	@Override
	public String toString() {
		return "{\"name\":\"" + this.name + "\", \"userImage\":\""
				+ this.userImage + "\"}";
	}


}
