package in.fastrack.app.m.fup.model;

import java.io.Serializable;

public class Comment implements Serializable{

	private static final long serialVersionUID = 1L;
	String alarmCreator,alarmId,commentId,comment,commentUserProfile,commentUserName,commentUserId,commentCreatedDate = null;
	
	boolean commentsOpen = true;
	
	public Comment(String alarmCreator,String alarmId, String commentId, String comment,
			String commentUserProfile, String commentUserName,
			String commentUserId, String commentCreatedDate,
			boolean commentsOpen) {
		super();
		this.alarmCreator = alarmCreator;
		this.alarmId = alarmId;
		this.commentId = commentId;
		this.comment = comment;
		this.commentUserProfile = commentUserProfile;
		this.commentUserName = commentUserName;
		this.commentUserId = commentUserId;
		this.commentCreatedDate = commentCreatedDate;
		this.commentsOpen = commentsOpen;
	}
	public String getAlarmCreator() {
		return alarmCreator;
	}

	public void setAlarmCreator(String alarmCreator) {
		this.alarmCreator = alarmCreator;
	}

	public String getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(String alarmId) {
		this.alarmId = alarmId;
	}

	public String getCommentId() {
		return commentId;
	}

	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCommentUserProfile() {
		return commentUserProfile;
	}

	public void setCommentUserProfile(String commentUserProfile) {
		this.commentUserProfile = commentUserProfile;
	}

	public String getCommentUserName() {
		return commentUserName;
	}

	public void setCommentUserName(String commentUserName) {
		this.commentUserName = commentUserName;
	}

	public String getCommentUserId() {
		return commentUserId;
	}

	public void setCommentUserId(String commentUserId) {
		this.commentUserId = commentUserId;
	}

	public String getCommentCreatedDate() {
		return commentCreatedDate;
	}

	public void setCommentCreatedDate(String commentCreatedDate) {
		this.commentCreatedDate = commentCreatedDate;
	}

	public boolean isCommentsOpen() {
		return commentsOpen;
	}

	public void setCommentsOpen(boolean commentsOpen) {
		this.commentsOpen = commentsOpen;
	}
	
	
	
	
}
