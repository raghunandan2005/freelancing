package in.fastrack.app.m.fup.model;

public class FeedEvent {

	private FeedObject mFeedObject;
	
	public FeedEvent(FeedObject mObject){
		mFeedObject = mObject;
	}
	
	public FeedObject getFeedObject(){
		return mFeedObject;
	}
	
}
