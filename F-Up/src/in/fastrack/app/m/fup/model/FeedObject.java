package in.fastrack.app.m.fup.model;

public class FeedObject {
	
	private int eventType;
	private int alarmid;
	private boolean alarmStatus;
	private int rowPostion;
	private int commentCount;
	
	
	public FeedObject(int eventType, int alarmid, boolean alarmStatus,
			int rowPostion, int commentCount) {
		this.eventType = eventType;
		this.alarmid = alarmid;
		this.alarmStatus = alarmStatus;
		this.rowPostion = rowPostion;
		this.commentCount = commentCount;
	}


	public int getEventType() {
		return eventType;
	}


	public void setEventType(int eventType) {
		this.eventType = eventType;
	}


	public int getAlarmid() {
		return alarmid;
	}


	public void setAlarmid(int alarmid) {
		this.alarmid = alarmid;
	}


	public boolean isAlarmStatus() {
		return alarmStatus;
	}


	public void setAlarmStatus(boolean alarmStatus) {
		this.alarmStatus = alarmStatus;
	}


	public int getRowPostion() {
		return rowPostion;
	}


	public void setRowPostion(int rowPostion) {
		this.rowPostion = rowPostion;
	}


	public int getCommentCount() {
		return commentCount;
	}


	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}


	@Override
	public String toString() {
		return "FeedEvent [eventType=" + eventType + ", alarmid=" + alarmid
				+ ", alarmStatus=" + alarmStatus + ", rowPostion=" + rowPostion
				+ ", commentCount=" + commentCount + "]";
	}
	
	
	
	

}
