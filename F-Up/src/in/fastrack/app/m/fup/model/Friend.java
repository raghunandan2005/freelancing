package in.fastrack.app.m.fup.model;

public class Friend {
	String userId;
	String name;
	String source;
	String userImage;
	boolean isSupriseFriend, userSelected;
	String status;
	int visibility,mActionVisibility,mRemoveVisibility;
	
	public int getmRemoveVisibility() {
		return mRemoveVisibility;
	}

	public void setmRemoveVisibility(int mRemoveVisibility) {
		this.mRemoveVisibility = mRemoveVisibility;
	}

	public int getmActionVisibility() {
		return mActionVisibility;
	}

	public void setmActionVisibility(int mActionVisibility) {
		this.mActionVisibility = mActionVisibility;
	}

	public int getVisibility() {
		return visibility;
	}

	public void setVisibility(int visibility) {
		this.visibility = visibility;
	}

	public Friend() {
	}
	
	public Friend(String userId, String name, String source, String userImage, boolean isSupriseFriend, boolean userSelected, String status) {
		super();
		this.name = name;
		this.userId = userId;
		this.source = source;
		this.isSupriseFriend = isSupriseFriend;
		this.userImage = userImage;
		this.userSelected = userSelected;
		this.status = status;
	}

	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getUserSelected() {
		return userSelected;
	}
	public void setUserSelected(Boolean userSelected) {
		this.userSelected = userSelected;
	}
	public String getUserImage() {
		return userImage;
	}
	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}
	public Boolean getSupriseFriend() {
		return isSupriseFriend;
	}
	public void setSupriseFriend(Boolean isSupriseFriend) {
		this.isSupriseFriend = isSupriseFriend;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	@Override
	public String toString() {
		return "{\"userId\":\"" + this.userId +"\", \"name\":\"" + this.name +"\", \"userImage\":\"" + this.userImage +"\", \"isSupriseFriend\":\"" + this.isSupriseFriend +"\", \"source\":\"" + this.source +"\"}";
	}
	/*NEWLY ADDED*/
	public boolean equals(Object o) {
	    if(o == null) return false;
	    if(!(o instanceof Friend)) return false;

	    Friend other = (Friend) o;
	    if(! this.userId.equals(other.userId)) return false;
	    if(! this.name.equals(other.name)) return false;
	    return true;
	}
	/*NEWLY ADDED*/
}
