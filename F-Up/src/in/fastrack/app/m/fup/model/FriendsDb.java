package in.fastrack.app.m.fup.model;

import java.util.List;

import android.content.Context;
import android.util.Log;

import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

public class FriendsDb extends SugarRecord<FriendsDb> {
	
	String userid,friendid,fbid,name,source,lastupdate;

	public FriendsDb(Context ctx) {
		super(ctx);
	}
	
	public FriendsDb(Context ctx,String userid,String friendid,String name,String source,String lastupdate,String fbid){
		super(ctx);
		this.userid = userid;
		this.friendid = friendid;
		this.name = name;
		this.fbid = fbid;
		this.source = source;
		this.lastupdate = lastupdate;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getFriendid() {
		return friendid;
	}

	public void setFriendid(String friendid) {
		this.friendid = friendid;
	}

	public String getFbid() {
		return fbid;
	}

	public void setFbid(String fbid) {
		this.fbid = fbid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getLastupdate() {
		return lastupdate;
	}

	public void setLastupdate(String lastupdate) {
		this.lastupdate = lastupdate;
	}
	
	public List<FriendsDb> getFriends(String userid,String source){
		@SuppressWarnings("unchecked")
		List<FriendsDb> data = Select.from(FriendsDb.class).where(Condition.prop("userid").eq(userid)).where(Condition.prop("source").eq(source)).list();
		if(!data.isEmpty()){
			return data;
		}
		return null;
	}
	
	/*public Boolean infoExists(String userid,String source){
		return false;
	}*/
	
	
	public Boolean infoExists(String userid,String fbid,String source){
		@SuppressWarnings("unchecked")
		List<FriendsDb> data = Select.from(FriendsDb.class).where(Condition.prop("userid").eq(userid)).where(Condition.prop("source").eq(source)).where(Condition.prop("fbid").eq(fbid)).list();
		//List<FriendsDb> data = FriendsDb.find(FriendsDb.class,"userid = ? AND fbid = ? AND source = ?",new String[]{userid,fbid,source});
		Log.d("INFO EXIST",data.toString());
		if(data.size() > 0){
			return true;
		}
		return false;
	}

}
