package in.fastrack.app.m.fup.model;

import java.io.Serializable;

public class Group implements Serializable{

	private static final long serialVersionUID = 1L;
	private String name,groupObject,cretor,cretorstring,groupImagePath,friendList = null;
	String status;


	private int memberCount = 0;
	private Boolean groupSelected = false; 
    private int colorb;
    private int tick;
    
	
	public int getTick() {
		return tick;
	}
	public void setTick(int tick) {
		this.tick = tick;
	}
	public Group(String name, String groupObject, String cretor, String cretorstring, String groupImagePath, String friendList, int memberCount, boolean groupSelected, String status) {
		super();
		this.name = name;
		this.groupObject = groupObject;
		this.cretor = cretor;
		this.cretorstring = cretorstring;
		this.groupImagePath = groupImagePath;
		this.friendList = friendList;
		this.memberCount = memberCount;
		this.groupSelected = groupSelected;
		this.status = status;
	}
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	public boolean getGroupSelected() {
		return groupSelected;
	}

	public int getColorb() {
		return colorb;
	}
	public void setColorb(int colorb) {
		this.colorb = colorb;
	}
	public void setGroupSelected(Boolean groupSelected) {
		this.groupSelected = groupSelected;
	}


	public int getMemberCount() {
		return memberCount;
	}

	public void setMemberCount(int memberCount) {
		this.memberCount = memberCount;
	}

	public Group() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroupObject() {
		return groupObject;
	}

	public void setGroupObject(String groupObject) {
		this.groupObject = groupObject;
	}

	public String getCretor() {
		return cretor;
	}

	public void setCretor(String cretor) {
		this.cretor = cretor;
	}
	public String getCretorstring() {
		return cretorstring;
	}

	public void setCretorstring(String cretorstring) {
		this.cretorstring = cretorstring;
	}
	public String getGroupImagePath() {
		return groupImagePath;
	}

	public void setGroupImagePath(String groupImagePath) {
		this.groupImagePath = groupImagePath;
	}

	public String getFriendList() {
		return friendList;
	}

	public void setFriendList(String friendList) {
		this.friendList = friendList;
	}
	

	@Override
	public String toString() {
		return "Group [name=" + name + ", groupObject=" + groupObject
				+ ", cretor=" + cretor + ", cretorstring=" + cretorstring
				+ ", groupImagePath=" + groupImagePath + ", friendList="
				+ friendList + ", memberCount=" + memberCount
				+ ", groupSelected=" + groupSelected + "]";
	}
}
