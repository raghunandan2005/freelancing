package in.fastrack.app.m.fup.model;

import java.io.Serializable;

public class NewsFeed implements Serializable{
	private static final long serialVersionUID = 1L;

	String alarmTitle,alarmCategory,userName,userProfileImage,userCreatedTime,alarmTime,alarmid,alarmFriends,alarmFriendsid, alarm_repeat_type,alarmpath,creator,media = null;
	

	int alarmrequestid,likes,comments,page = 0;
	long longtime;
	boolean playing,liked,selfAlarm,isPrivateAlaramCheck;
	public boolean recorded = false;

	public NewsFeed(String alarmTitle, String alarmCategory, String userName,
			String userProfileImage, String userCreatedTime, String alarmTime,
			String alarmFriends,String alarmFriendsid, int alarmrequestid, int likes, int comments,
			int page, long longtime,boolean liked,String alarmid,String alarmpath,boolean recorded,boolean playing, String creator, boolean selfAlarm, String media,String alarm_repeat_type,boolean privateAlaram) {
		super();
		this.alarmTitle = alarmTitle;
		this.alarmCategory = alarmCategory;
		this.userName = userName;
		this.userProfileImage = userProfileImage;
		this.userCreatedTime = userCreatedTime;
		this.alarmTime = alarmTime;
		this.alarmFriends = alarmFriends;
		this.alarmFriendsid = alarmFriendsid;
		this.alarmrequestid = alarmrequestid;
		this.likes = likes;
		this.comments = comments;
		this.page = page;
		this.longtime = longtime;
		this.liked = liked;
		this.alarmid = alarmid;
		this.playing = playing;
		this.alarmpath = alarmpath;
		this.recorded = recorded;
		this.creator = creator;
		this.selfAlarm = selfAlarm;
		this.media = media;
		this.alarm_repeat_type = alarm_repeat_type;
		this.isPrivateAlaramCheck = privateAlaram;
	}	
	
	
	
	public String getAlarm_repeat_type() {
		return alarm_repeat_type;
	}









	public void setAlarm_repeat_type(String alarm_repeat_type) {
		this.alarm_repeat_type = alarm_repeat_type;
	}









	public NewsFeed() {
	}









	public String getMedia() {
		return media;
	}









	public void setMedia(String media) {
		this.media = media;
	}









	public boolean isSelfAlarm() {
		return selfAlarm;
	}









	public void setSelfAlarm(boolean selfAlarm) {
		this.selfAlarm = selfAlarm;
	}

	/* added by binu */
	public void setIsPrivateAlaramCheck(boolean privateAlaram){
		this.isPrivateAlaramCheck = privateAlaram;
	}
	
	public boolean isPrivateAlaramCheck() {
		return isPrivateAlaramCheck;
	}







	public String getCreator() {
		return creator;
	}









	public void setCreator(String creator) {
		this.creator = creator;
	}









	public String getAlarmpath() {
		return alarmpath;
	}



	public void setAlarmpath(String alarmpath) {
		this.alarmpath = alarmpath;
	}





	public boolean isRecorded() {
		return recorded;
	}



	public void setRecorded(boolean recorded) {
		this.recorded = recorded;
	}









	public boolean isPlaying() {
		return playing;
	}






	public void setPlaying(boolean playing) {
		this.playing = playing;
	}






	public String getAlarmid() {
		return alarmid;
	}




	public void setAlarmid(String alarmid) {
		this.alarmid = alarmid;
	}




	public boolean isLiked() {
		return liked;
	}




	public void setLiked(boolean liked) {
		this.liked = liked;
	}




	public String getAlarmTitle() {
		return alarmTitle;
	}
	public void setAlarmTitle(String alarmTitle) {
		this.alarmTitle = alarmTitle;
	}
	public String getAlarmCategory() {
		return alarmCategory;
	}
	public void setAlarmCategory(String alarmCategory) {
		this.alarmCategory = alarmCategory;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserProfileImage() {
		return userProfileImage;
	}
	public void setUserProfileImage(String userProfileImage) {
		this.userProfileImage = userProfileImage;
	}
	public String getUserCreatedTime() {
		return userCreatedTime;
	}
	public void setUserCreatedTime(String userCreatedTime) {
		this.userCreatedTime = userCreatedTime;
	}
	public String getAlarmTime() {
		return alarmTime;
	}
	public void setAlarmTime(String alarmTime) {
		this.alarmTime = alarmTime;
	}
	public String getAlarmFriends() {
		return alarmFriends;
	}
	public void setAlarmFriends(String alarmFriends) {
		this.alarmFriends = alarmFriends;
	}
	public String getAlarmFriendsid() {
		return alarmFriendsid;
	}

	public void setAlarmFriendsid(String alarmFriendsid) {
		this.alarmFriendsid = alarmFriendsid;
	}

	public int getAlarmrequestid() {
		return alarmrequestid;
	}
	public void setAlarmrequestid(int alarmrequestid) {
		this.alarmrequestid = alarmrequestid;
	}
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	public int getComments() {
		return comments;
	}
	public void setComments(int comments) {
		this.comments = comments;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public long getLongtime() {
		return longtime;
	}
	public void setLongtime(long longtime) {
		this.longtime = longtime;
	}









	@Override
	public String toString() {
		return "NewsFeed [alarmTitle=" + alarmTitle + ", alarmCategory="
				+ alarmCategory + ", userName=" + userName
				+ ", userProfileImage=" + userProfileImage
				+ ", userCreatedTime=" + userCreatedTime + ", alarmTime="
				+ alarmTime + ", alarmid=" + alarmid + ", alarmFriends="
				+ alarmFriends+ ", alarmFriendsid="+ alarmFriendsid +",alarmpath=" + alarmpath + ", creator="
				+ creator + ", alarmrequestid=" + alarmrequestid + ", likes="
				+ likes + ", comments=" + comments + ", page=" + page
				+ ", longtime=" + longtime + ", playing=" + playing
				+ ", liked=" + liked + ", recorded=" + recorded + "]";
	}
	
	
	
	
}
