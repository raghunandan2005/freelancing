package in.fastrack.app.m.fup.model;

public class Notification {

	String notificationId,notificationResourceId,notificationTitle,notificationDescription,notificationDate,notificationUser,notificationSHowButtons,notificationUserId,notificationUserImage,notififcationActionName = null;
	boolean notificationAccepted,notificationCanceled,notificaionRowVisible;
	public Notification(String notificationId,String notificationResourceId,
			String notificationTitle, String notificationDescription,
			String notificationDate, String notificationUser,String notificationUserId,String notificationUserImage,
			String notififcationActionName, String notificationSHowButtons) {
		super();
		this.notificationId = notificationId;
		this.notificationResourceId = notificationResourceId;
		this.notificationTitle = notificationTitle;
		this.notificationDescription = notificationDescription;
		this.notificationDate = notificationDate;
		this.notificationUser = notificationUser;
		this.notificationUserId = notificationUserId;
		this.notificationUserImage = notificationUserImage;
		this.notififcationActionName = notififcationActionName;
		this.notificationSHowButtons = notificationSHowButtons;
	}
	
	
	
	
	
	
	public String getNotificationId() {
		return notificationId;
	}






	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}






	public String getNotificationUserId() {
		return notificationUserId;
	}




	public void setNotificationUserId(String notificationUserId) {
		this.notificationUserId = notificationUserId;
	}




	public String getNotificationUserImage() {
		return notificationUserImage;
	}




	public void setNotificationUserImage(String notificationUserImage) {
		this.notificationUserImage = notificationUserImage;
	}




	public String getNotificationSHowButtons() {
		return notificationSHowButtons;
	}


	public void setNotificationSHowButtons(String notificationSHowButtons) {
		this.notificationSHowButtons = notificationSHowButtons;
	}


	public String getNotificationResourceId() {
		return notificationResourceId;
	}
	public void setNotificationResourceId(String notificationResourceId) {
		this.notificationResourceId = notificationResourceId;
	}
	public String getNotificationTitle() {
		return notificationTitle;
	}
	public void setNotificationTitle(String notificationTitle) {
		this.notificationTitle = notificationTitle;
	}
	public String getNotificationDescription() {
		return notificationDescription;
	}
	public void setNotificationDescription(String notificationDescription) {
		this.notificationDescription = notificationDescription;
	}
	public String getNotificationDate() {
		return notificationDate;
	}
	public void setNotificationDate(String notificationDate) {
		this.notificationDate = notificationDate;
	}
	public String getNotificationUser() {
		return notificationUser;
	}
	public void setNotificationUser(String notificationUser) {
		this.notificationUser = notificationUser;
	}
	public String getNotififcationActionName() {
		return notififcationActionName;
	}
	public void setNotififcationActionName(String notififcationActionName) {
		this.notififcationActionName = notififcationActionName;
	}
	public boolean isNotificationAccepted() {
		return notificationAccepted;
	}
	public void setNotificationAccepted(boolean notificationAccepted) {
		this.notificationAccepted = notificationAccepted;
	}
	public boolean isNotificationCanceled() {
		return notificationCanceled;
	}
	public void setNotificationCanceled(boolean notificationCanceled) {
		this.notificationCanceled = notificationCanceled;
	}
	public boolean isNotificaionRowVisible() {
		return notificaionRowVisible;
	}
	public void setNotificaionRowVisible(boolean notificaionRowVisible) {
		this.notificaionRowVisible = notificaionRowVisible;
	}

	
	
	
	
}
