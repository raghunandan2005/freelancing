package in.fastrack.app.m.fup.model;

import com.parse.ParseObject;
import com.parse.ParseUser;

public class NotificationModel {
	
	String name,message,imagepath,activitydate,alarmid = null;
	Boolean isAccepted,showButtons;
	int activityType = 1;
	String notification_type,obj_id;
	
	 public String getObj_id() {
		return obj_id;
	}


	public void setObj_id(String obj_id) {
		this.obj_id = obj_id;
	}


	ParseObject userobj;
	


	public ParseObject getUserobj() {
		return userobj;
	}


	public void setUserobj(ParseObject userobj) {
		this.userobj = userobj;
	}


	public String getNotification_type() {
		return notification_type;
	}


	public void setNotification_type(String notification_type) {
		this.notification_type = notification_type;
	}


	public NotificationModel(String name, String message, String imagepath,
			String alarmid, String activitydate, int activityType,
			Boolean isAccepted, Boolean showButtons,String notification_type,String obj_id) {
		super();
		this.name = name;
		this.message = message;
		this.imagepath = imagepath;
		this.alarmid = alarmid;
		this.activitydate = activitydate;
		this.activityType = activityType;
		this.isAccepted = isAccepted;
		this.showButtons = showButtons;
		this.notification_type = notification_type;
		this.obj_id=obj_id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getImagepath() {
		return imagepath;
	}


	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}


	public String getActivitydate() {
		return activitydate;
	}


	public void setActivitydate(String activitydate) {
		this.activitydate = activitydate;
	}


	public String getAlarmid() {
		return alarmid;
	}


	public void setAlarmid(String alarmid) {
		this.alarmid = alarmid;
	}


	public Boolean getIsAccepted() {
		return isAccepted;
	}


	public void setIsAccepted(Boolean isAccepted) {
		this.isAccepted = isAccepted;
	}


	public Boolean getShowButtons() {
		return showButtons;
	}


	public void setShowButtons(Boolean showButtons) {
		this.showButtons = showButtons;
	}


	public int getActivityType() {
		return activityType;
	}


	public void setActivityType(int activityType) {
		this.activityType = activityType;
	}
	
	
	
	
	
	

}
