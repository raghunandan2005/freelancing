package in.fastrack.app.m.fup.model;

public class People {
	private int id;
	private String fullName,userObject,userSource,userId,userImagePath,friendId = null;
	private Boolean userSelected,isInvite = false;
	
	
	public People(String userObject,String friendId,String userId, String fullName,
			String userSource, String userImagePath, Boolean userSelected,
			Boolean isInvite) {
		super();
		this.userObject = userObject;
		this.userId = userId;
		this.fullName = fullName;
		this.userSource = userSource;
		this.userImagePath = userImagePath;
		this.userSelected = userSelected;
		this.isInvite = isInvite;
		this.friendId = friendId;
	}
	
	





	public String getFriendId() {
		return friendId;
	}







	public void setFriendId(String friendId) {
		this.friendId = friendId;
	}







	



	@Override
	public String toString() {
		return "People [id=" + id + ", fullName=" + fullName + ", userObject="
				+ userObject + ", userSource=" + userSource + ", userId="
				+ userId + ", userImagePath=" + userImagePath + ", friendId="
				+ friendId + ", userSelected=" + userSelected + ", isInvite="
				+ isInvite + "]";
	}







	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getUserObject() {
		return userObject;
	}
	public void setUserObject(String userObject) {
		this.userObject = userObject;
	}
	public String getUserSource() {
		return userSource;
	}
	public void setUserSource(String userSource) {
		this.userSource = userSource;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserImagePath() {
		return userImagePath;
	}
	public void setUserImagePath(String userImagePath) {
		this.userImagePath = userImagePath;
	}
	public Boolean getUserSelected() {
		return userSelected;
	}
	public void setUserSelected(Boolean userSelected) {
		this.userSelected = userSelected;
	}
	public Boolean getIsInvite() {
		return isInvite;
	}
	public void setIsInvite(Boolean isInvite) {
		this.isInvite = isInvite;
	}
	
	
}
