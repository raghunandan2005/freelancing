package in.fastrack.app.m.fup.model;

public class PhoneBook {

	String name,phonenumber = null;
	String userImage;
	boolean isSelected;
	
	public PhoneBook(String name,String phonenumber,String userImage,boolean isSelected){
		super();
		this.name = name;
		this.phonenumber = phonenumber;
		this.isSelected = isSelected;
		this.userImage = userImage;
	}
	
	public String getName(){
		return name;
	}
	
	public String getPhoneNumber(){
		return phonenumber;
	}
	
	public String getUserImage(){
		return userImage;
	}
	
	public boolean getIsSelected(){
		return isSelected;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setPhoneNumber(String phonenumber){
		this.phonenumber = phonenumber;
	}
	
	public void setUserImage(String userImage){
		this.userImage = userImage;
	}
	
	public void setIsSelected(boolean isSelected){
		this.isSelected = isSelected;
	}
	
	
}
