package in.fastrack.app.m.fup.model;

public class Playlist {

	private int id;
	private String trackName,track = null;
	private boolean trackSelected,isPlaying = false;
	
	
	

	public Playlist(String trackName, String track, boolean trackSelected){
		super();
		this.trackName = trackName;
		this.track =track;
		this.trackSelected = trackSelected;
	}
	
	public String getTrack() {
		return track;
	}

	public void setTrack(String track) {
		this.track = track;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTrackName() {
		return trackName;
	}
	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}
	public boolean isTrackSelected() {
		return trackSelected;
	}
	public void setTrackSelected(boolean trackSelected) {
		this.trackSelected = trackSelected;
	}
	public boolean isPlaying() {
		return isPlaying;
	}
	public void setPlaying(boolean isPlaying) {
		this.isPlaying = isPlaying;
	}
	
	
	
}
