package in.fastrack.app.m.fup.model;

import com.parse.ParseObject;


public class SocialList {
	
	String name,userid,source,image = null;
	boolean inviteMode,selected = false;
	String userobject;
	
	public SocialList(String userid,String userobject,String name,String image, String source, boolean selected, boolean inviteMode){
		super();
		this.name = name;
		this.userid = userid;
		this.image = image;
		this.source = source;
		this.selected = selected;
		this.inviteMode = inviteMode;
		this.userobject = userobject;
	}
	
	public String getUserobject() {
		return userobject;
	}

	public void setUserobject(String userobject) {
		this.userobject = userobject;
	}

	//Getter and setter
	public String getName(){
		return name;
	}
	
	public String getUserId(){
		return userid;
	}
	
	public String getImage(){
		return image;
	}
	
	public String getSource(){
		return source;
	}
	
	public boolean getSelected(){
		return selected;
	}
	
	public boolean getInviteMode(){
		return inviteMode;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setUserId(String userid){
		this.userid = userid;
	}
	
	public void setImage(String image){
		this.image = image;
	}
	
	public void setSource(String source){
		this.source = source;
	}
	
	public void setSelected(boolean selected){
		this.selected = selected;
	}
	
	public void setInviteMode(boolean inviteMode){
		this.inviteMode = inviteMode;
	}

}
