package in.fastrack.app.m.fup.model;

import org.json.JSONArray;

public class UserFriendRelation {
	private String userId;
	private JSONArray connectedGroups;
	private JSONArray pendingFriends;
	private JSONArray requestedFriends;
	private JSONArray connectedFriends;
	public JSONArray getPendingFriendsS() {
		return pendingFriendsS;
	}
	public void setPendingFriendsS(JSONArray pendingFriendsS) {
		this.pendingFriendsS = pendingFriendsS;
	}
	public JSONArray getRequestedFriendsS() {
		return requestedFriendsS;
	}
	public void setRequestedFriendsS(JSONArray requestedFriendsS) {
		this.requestedFriendsS = requestedFriendsS;
	}
	public JSONArray getConnectedFriendsS() {
		return connectedFriendsS;
	}
	public void setConnectedFriendsS(JSONArray connectedFriendsS) {
		this.connectedFriendsS = connectedFriendsS;
	}
	private JSONArray pendingFriendsS;
	private JSONArray requestedFriendsS;
	private JSONArray connectedFriendsS;
	
	public JSONArray getConnectedGroups() {
		return connectedGroups;
	}
	public void setConnectedGroups(JSONArray connectedGroups) {
		this.connectedGroups = connectedGroups;
	}
	public JSONArray getPendingFriends() {
		return pendingFriends;
	}
	public void setPendingFriends(JSONArray pendingFriends) {
		this.pendingFriends = pendingFriends;
	}
	public JSONArray getRequestedFriends() {
		return requestedFriends;
	}
	public void setRequestedFriends(JSONArray requestedFriends) {
		this.requestedFriends = requestedFriends;
	}
	public JSONArray getConnectedFriends() {
		return connectedFriends;
	}
	public void setConnectedFriends(JSONArray connectedFriends) {
		this.connectedFriends = connectedFriends;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
